package com.petrosoft.smena.view.recycler.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.petrosoft.smena.view.recycler.element.BaseElement;

/**
 * Created by a.baskakov on 22/08/16.
 */
public abstract class BaseViewHolder<T extends BaseElement> extends RecyclerView.ViewHolder {
    T mElement;

    public BaseViewHolder(View itemView) {
        super(itemView);
    }

    public void setElement(T element) {
        mElement = element;
    }
}
