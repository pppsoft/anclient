package com.petrosoft.smena.view.recycler.element;

/**
 * Created by a.baskakov on 22/08/16.
 */
public abstract class BaseElement<T> {
    T mData;

    public BaseElement(T data) {
        this.mData = data;
    }

    public T getData() {
        return mData;
    }
}
