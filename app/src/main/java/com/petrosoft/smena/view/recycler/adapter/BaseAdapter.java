package com.petrosoft.smena.view.recycler.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;

import com.petrosoft.smena.view.recycler.element.BaseElement;
import com.petrosoft.smena.view.recycler.holder.BaseViewHolder;

import java.util.List;
import java.util.Map;

/**
 * Created by a.baskakov on 22/08/16.
 */
public abstract class BaseAdapter extends RecyclerView.Adapter<BaseViewHolder> {
    private final Map<Class<? extends BaseElement>, Integer> mTypeMap;
    private final List<BaseElement> mList;
    private final Context mContext;

    public BaseAdapter(List<BaseElement> mList, Map<Class<? extends BaseElement>, Integer> typeMap, Context mContext) {
        this.mList = mList;
        this.mTypeMap = typeMap;
        this.mContext = mContext;
    }

    @Override
    public int getItemViewType(int position) {
        BaseElement element = getElement(position);
        if (mTypeMap.containsKey(element.getClass())) {
            return mTypeMap.get(element.getClass());
        }
        return 0;
    }

    public BaseElement getElement(int position) {
        return mList.get(position);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }
}
