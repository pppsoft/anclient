package com.petrosoft.smena.activity.svalka;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.petrosoft.smena.GreenApp;
import com.petrosoft.smena.R;
import com.petrosoft.smena.adapters.ImageAdapter;
import com.petrosoft.smena.adapters.ImageRemover;
import com.petrosoft.smena.dao.DaoSession;
import com.petrosoft.smena.dao.PendingImage;
import com.petrosoft.smena.dao.PendingImageDao;
import com.petrosoft.smena.db.DbHelper;
import com.petrosoft.smena.server.response.data.svalka.CommonSvalkaResult;
import com.petrosoft.smena.server.response.data.svalka.SvalkaIssuer;
import com.petrosoft.smena.service.SentPendingDataService;
import com.petrosoft.smena.utils.PrefsHelper;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.os.Environment.DIRECTORY_PICTURES;

public class AddSvalkaIssuerActivity extends AppCompatActivity implements ImageRemover {
    private static final int TYPE_PHOTO = 44;
    private static final int REQUEST_IMAGE_CAPTURE = 6466;
    @BindView(R.id.fab_save)
    FloatingActionButton mSaveButton;

    @BindView(R.id.name_fiz)
    EditText mNameFiz;
    @BindView(R.id.location_fiz)
    EditText mLocationFiz;
    @BindView(R.id.name_yur)
    EditText mNameYur;
    @BindView(R.id.comment_text)
    EditText mCommentText;
    @BindView(R.id.photos_list)
    RecyclerView mPhotosRecycler;
    @BindView(R.id.add_photo_button)
    TextView mAddPhotoButton;
    private Uri mSavedPhotoFile;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    private Long mSvalkaId;
    private List<Uri> mPhotosList;
    private ImageAdapter mImageAdapter;
    private File directory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_svalka_issue);
        ButterKnife.bind(this);
        createDirectory();
        mPhotosList = new ArrayList<>();
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("Добавить правонарушителя");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        mPhotosRecycler.setLayoutManager(linearLayoutManager);
        mImageAdapter = new ImageAdapter(this);
        mPhotosRecycler.setAdapter(mImageAdapter);

        mSvalkaId = getIntent().getLongExtra("svalka_id", -1);
        mSaveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (GreenApp.checkNetwork()) {
                    addIssue();
                } else {
                    // GreenApp.showNoNetworkSnackbar(mMainLayout,null);
                }

            }
        });
        clearFields();
        mAddPhotoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dispatchTakePictureIntent();
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            if (mSavedPhotoFile != null) {
                mImageAdapter.addImage(mSavedPhotoFile);
                mImageAdapter.notifyDataSetChanged();
                mPhotosList.add(mSavedPhotoFile);

            }
        }
        super.onActivityResult(requestCode, resultCode, data);
        //   QuickImagePick.handleActivityResult(getApplicationContext(), requestCode, resultCode, data, mCallback);

    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        mSavedPhotoFile = generateFileUri(TYPE_PHOTO);
        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, mSavedPhotoFile);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    private void clearFields() {
        mNameFiz.setText("");
        mNameYur.setText("");
        mLocationFiz.setText("");
        mCommentText.setText("");
        // SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());
        //mRegisteredAt.setText(sdf.format(Calendar.getInstance().getTime()));
    }

    private void addIssue() {

        SvalkaIssuer svalkaIssuer = new SvalkaIssuer();
        svalkaIssuer.setSvalka_id(mSvalkaId);
        svalkaIssuer.setFull_name_fiz(mNameFiz.getText().toString());
        svalkaIssuer.setLocation_fiz(mLocationFiz.getText().toString());
        svalkaIssuer.setName_yur(mNameYur.getText().toString());
        svalkaIssuer.setRegistered_at(new Date());
        svalkaIssuer.setComment(mCommentText.getText().toString());
        GreenApp.getInstance().getSvalkaApi().addIssuer(svalkaIssuer).enqueue(new Callback<CommonSvalkaResult>() {
            @Override
            public void onResponse(@NonNull Call<CommonSvalkaResult> call, @NonNull Response<CommonSvalkaResult> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(AddSvalkaIssuerActivity.this, "Данные успешно отправлены на сервер", Toast.LENGTH_SHORT).show();
                    CommonSvalkaResult commonSvalkaResult = response.body();

                    DaoSession daoSession = DbHelper.getDaoMaster(AddSvalkaIssuerActivity.this).newSession();
                    PendingImageDao pendingImageDao = daoSession.getPendingImageDao();
                    for (Uri uri : mPhotosList) {

                        if (commonSvalkaResult.getImageCode() != null) {
                            PendingImage pendingImage = new PendingImage();
                            pendingImage.setEntity_id(commonSvalkaResult.getId());
                            pendingImage.setImage_code(commonSvalkaResult.getImageCode());
                            pendingImage.setImage_path(uri.getPath());
                            pendingImage.setToken(PrefsHelper.loadLastLoginData(AddSvalkaIssuerActivity.this).mToken);
                            pendingImage.setIs_completed(false);
                            pendingImage.setPoint_id(commonSvalkaResult.getPoint_id());
                            pendingImageDao.insert(pendingImage);
                        }
                    }
                    Intent intent = new Intent(AddSvalkaIssuerActivity.this, SentPendingDataService.class);
                    startService(intent);
                    finish();
                } else {
                    Toast.makeText(AddSvalkaIssuerActivity.this, "Ошибка добавления данных", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<CommonSvalkaResult> call, @NonNull Throwable t) {
                Toast.makeText(AddSvalkaIssuerActivity.this, "Фатальная ошибка добавления данных " + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void notifyFileRemoved(final int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Удаление фотографии");
        builder.setMessage("Подтвердите удаление фото");
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                try {
                    mPhotosList.remove(position);
                    mImageAdapter.deleteImage(position);
                    mImageAdapter.notifyDataSetChanged();
                } catch (Exception ignored) {

                }

            }
        });
        builder.setNegativeButton(R.string.cancel, null);
        builder.create().show();

    }

    private Uri generateFileUri(int type) {
        File file = null;
        switch (type) {
            case TYPE_PHOTO:
                file = new File(directory.getAbsolutePath() + "/" + "photo_"
                        + System.currentTimeMillis() + ".jpg");
                break;

        }
        Log.d("File generate", "fileName = " + file);
        return Uri.fromFile(file);
    }

    private void createDirectory() {
        directory = GreenApp.getInstance().getExternalFilesDir(DIRECTORY_PICTURES);
        if (!directory.exists()) {
            boolean x = directory.mkdirs();
            Log.v("Folder create result", String.valueOf(x));
        }


    }
}
