package com.petrosoft.smena.activity;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.icu.text.StringSearch;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.CursorAdapter;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.crashlytics.android.answers.AddToCartEvent;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.ContentViewEvent;
import com.petrosoft.smena.GreenApp;
import com.petrosoft.smena.R;
import com.petrosoft.smena.dao.CObject;
import com.petrosoft.smena.dao.CObjectDao;
import com.petrosoft.smena.dao.DaoSession;
import com.petrosoft.smena.dao.GreenElement;
import com.petrosoft.smena.dao.GreenElementDao;
import com.petrosoft.smena.dao.ObjectType;
import com.petrosoft.smena.dao.ObjectTypeDao;
import com.petrosoft.smena.db.DbHelper;
import com.petrosoft.smena.server.RequestHelper;
import com.petrosoft.smena.server.response.data.add_issue.Element;
import com.petrosoft.smena.server.response.data.plan.CObjectData;
import com.petrosoft.smena.service.ObjectSyncService;
import com.petrosoft.smena.utils.DialogHelper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SelectObjectActivity extends AppCompatActivity {


    private SimpleCursorAdapter mAdapter;
    private ArrayList<String> finalOut;
    private CObjectData[] mAllObjects;
    private long mObjectTypeServerId;
    @BindView(R.id.object_list)
    ListView mListView;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.object_menu, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        SearchView searchView = (SearchView) MenuItemCompat
                .getActionView(menu.findItem(R.id.action_search));
        searchView.setSuggestionsAdapter(mAdapter);
        searchView.setIconifiedByDefault(false);
        // Getting selected (clicked) item suggestion
        searchView.setOnSuggestionListener(new SearchView.OnSuggestionListener() {
            //@TargetApi(Build.VERSION_CODES.M)
            @Override
            public boolean onSuggestionClick(int position) {
                Cursor searchCursor = mAdapter.getCursor();
                if (searchCursor.moveToPosition(position)) {
                    int selectedId = searchCursor.getInt(0);
                    String strLabel = searchCursor.getString(1);

                    Log.d("search", strLabel);
                    Answers.getInstance().logSearch(new com.crashlytics.android.answers.SearchEvent().putQuery(strLabel));


                    try {
                        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                        if (imm != null) {
                            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                        }

                    } catch (Exception e) {
                        Log.d("keyboard", "no virtual keyboard");
                    }

                }
                return true; // replace default search manager behaviour
            }


            @Override
            public boolean onSuggestionSelect(int position) {
                // Your code here
                return true;
            }
        });
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
////                ArrayList<String> l = new ArrayList<>();
////                for(String str: finalOut){
////                    if (str.contains(s)){
////                        l.add(str);
////                    }
////                }
////             ArrayAdapter<String> adapter = new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_list_item_1, l);
//                ((ArrayAdapter) mListView.getAdapter()).getFilter().filter(s);
//                //   mListView.setAdapter(adapter);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                // populateAdapter(s);
                ArrayList<String> l = new ArrayList<>();

                for (String str : finalOut) {
                    if (str.toLowerCase().contains(s.toLowerCase())) {
                        l.add(str);
                    }
                }
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_list_item_1, l);
                mListView.setAdapter(adapter);
                Log.v("search", s);
                return false;
            }

        });

        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_object);
        ButterKnife.bind(this);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(getString(R.string.select_object));
        getSupportActionBar().setSubtitle(getString(R.string.object));


        finalOut = new ArrayList<>();
        Intent intent = getIntent();
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                // mListView.getSelectedItemId();
                Log.v("Item clicked", String.valueOf(i) + ": " + String.valueOf(adapterView.getItemAtPosition(i)));
                int selItem = finalOut.indexOf(adapterView.getItemAtPosition(i));
                Log.v("Item original index ", String.valueOf(selItem));

                CObjectData cObjectData = mAllObjects[selItem];
                final DaoSession daoSession = DbHelper.getDaoMaster(getBaseContext()).newSession();
                CObjectDao cObjectDao = daoSession.getCObjectDao();
                ObjectTypeDao otDao = daoSession.getObjectTypeDao();
                ObjectType ot = otDao.queryBuilder().where(ObjectTypeDao.Properties.Id.eq(mObjectTypeServerId)).unique();
                CObject cObject = cObjectDao.queryBuilder().where(CObjectDao.Properties.ServerId.eq(cObjectData.mId)).unique();
                if (cObject == null) {
                    cObject = new CObject();

                    cObject.setServerId(cObjectData.mId);
                    cObject.setName(cObjectData.mName);
                    cObject.setOrganisations(cObjectData.mOrganisaitons);
                    cObject.setObjectType(ot);
                    cObject.setObjectTypeId(mObjectTypeServerId);
                    cObject.setId((long) cObjectData.mId);
                    cObject.setCurrent(true);
                    cObject.setCheck(false);
                    cObject.setOid(cObjectData.mOid);
                    cObjectDao.insert(cObject);

                    Intent syncintent = new Intent(SelectObjectActivity.this, ObjectSyncService.class);
                    syncintent.setAction(ObjectSyncService.ACTION_SYNC_OBJECT_ELEMENTS);
                    syncintent.putExtra(ObjectSyncService.OBJECT_ID, cObject.getId());
                    startService(syncintent);



                    Intent intent = new Intent();
                    intent.putExtra("added", true);
                    Answers.getInstance().logAddToCart(new AddToCartEvent().putItemId(String.valueOf(cObject.getServerId())).putItemName(cObject.getName()).putItemType(ot.getName()));
                    setResult(RESULT_OK, intent);
                    finish();

                }
            }
        });
        mObjectTypeServerId = intent.getLongExtra("object_type_id", -1);
        loadObjectByType(mObjectTypeServerId);
        final String[] from = new String[]{""};
        final int[] to = new int[]{android.R.id.text1};
        mAdapter = new SimpleCursorAdapter(this,
                android.R.layout.simple_list_item_1,
                null,
                from,
                to,
                CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);

    }

    void loadObjectByType(Long id) {

        final AlertDialog dlg = DialogHelper.showProgressDialog(this, "Загрузка объектов");
        dlg.show();
        RequestHelper.objectsByTypeTypes(getApplicationContext(), id, new Callback<CObjectData[]>() {
            @Override
            public void onResponse(Call<CObjectData[]> call, Response<CObjectData[]> response) {
                dlg.hide();
                Answers.getInstance().logContentView(new ContentViewEvent().putContentId(String.valueOf(mObjectTypeServerId)));
                if (response.isSuccessful()) {
                    if (response.body().length == 0) {
                        GreenApp.ownSnackbar(findViewById(R.id.activity_select_object), "Получено 0 объектов").setAction("Назад", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                onBackPressed();
                            }
                        }).show();

                    } else {
                        mAllObjects = response.body();

                        for (CObjectData cObjectData : mAllObjects) {
                            finalOut.add(cObjectData.mName);
                        }


                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_list_item_1, finalOut);

                        mListView.setAdapter(adapter);
                    }
                }

            }

            @Override
            public void onFailure(Call<CObjectData[]> call, Throwable t) {
                dlg.hide();
                Log.e("objects", "Failet to get objects");
                GreenApp.ownSnackbar(findViewById(R.id.activity_select_object), "Ошибка получения списка объектов: ").setAction("Назад", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        onBackPressed();
                    }
                }).show();
            }
        });

    }

}
