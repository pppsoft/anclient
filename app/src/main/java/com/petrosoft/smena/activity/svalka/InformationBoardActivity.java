package com.petrosoft.smena.activity.svalka;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import com.petrosoft.smena.GreenApp;
import com.petrosoft.smena.R;
import com.petrosoft.smena.adapters.ImageAdapter;
import com.petrosoft.smena.adapters.ImageRemover;
import com.petrosoft.smena.dao.DaoSession;
import com.petrosoft.smena.dao.PendingImage;
import com.petrosoft.smena.dao.PendingImageDao;
import com.petrosoft.smena.db.DbHelper;
import com.petrosoft.smena.server.response.data.svalka.CommonSvalkaResult;
import com.petrosoft.smena.server.response.data.svalka.SimpleElementRequest;
import com.petrosoft.smena.server.response.data.svalka.SimpleElementResponse;
import com.petrosoft.smena.service.SentPendingDataService;
import com.petrosoft.smena.utils.PrefsHelper;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.os.Environment.DIRECTORY_PICTURES;

public class InformationBoardActivity extends AppCompatActivity implements ImageRemover {

    private static final int TYPE_PHOTO = 44;
    private static final int REQUEST_IMAGE_CAPTURE = 6466;

    @BindView(R.id.checkbox_completed)
    CheckBox mSwitchCompleted;
    @BindView(R.id.fab_save)
    FloatingActionButton mSaveButton;

    @BindView(R.id.future_photos)
    RecyclerView mFuturePhotosList;
    private ImageAdapter mImageAdapter;
    private List<Uri> mPhotosList;
    private Uri mSavedPhotoFile;

    @BindView(R.id.add_photo_button)
    TextView mAddPhotoButton;

    private File directory;
    private long mSvalkaId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_information_board);
        ButterKnife.bind(this);
        createDirectory();
        mSvalkaId = getIntent().getLongExtra("svalka_id", -1L);
        mImageAdapter = new ImageAdapter(this);
        loadData();

        LinearLayoutManager llm = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        mFuturePhotosList.setLayoutManager(llm);
        mFuturePhotosList.setAdapter(mImageAdapter);
        mPhotosList = new ArrayList<>();

        //mFuturePhotosList.setVisibility(View.INVISIBLE);
       // mAddPhotoButton.setVisibility(View.INVISIBLE);

        mAddPhotoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dispatchTakePictureIntent();
            }
        });
        mSaveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveItem();
            }
        });


    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        mSavedPhotoFile = generateFileUri(TYPE_PHOTO);
        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, mSavedPhotoFile);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    private void loadData() {
        GreenApp.getInstance().getSvalkaApi().loadElementInfo(mSvalkaId, "3.4").enqueue(new Callback<SimpleElementResponse>() {
            @Override
            public void onResponse(Call<SimpleElementResponse> call, Response<SimpleElementResponse> response) {
                if (response.isSuccessful()) {
                    SimpleElementResponse simpleElementResponse = response.body();
                    mSwitchCompleted.setChecked(simpleElementResponse.getFact_date() != null);

                }
            }


            @Override
            public void onFailure(Call<SimpleElementResponse> call, Throwable t) {
                Log.v("v", t.getMessage());
            }
        });
    }

    private void saveItem() {
        SimpleElementRequest simpleElementRequest = new SimpleElementRequest();
        simpleElementRequest.setCheck_id(GreenApp.getInstance().getSvalkaCheckId());
        simpleElementRequest.setSvalka_id(mSvalkaId);
        if (mSwitchCompleted.isChecked()) {
            simpleElementRequest.setFact_date(new Date());
        } else {
            simpleElementRequest.setFact_date(null);
        }
        GreenApp.getInstance().getSvalkaApi().saveInfoBoard(simpleElementRequest).enqueue(new Callback<CommonSvalkaResult>() {
            @Override
            public void onResponse(Call<CommonSvalkaResult> call, Response<CommonSvalkaResult> response) {
                if (response.isSuccessful()) {
                    CommonSvalkaResult commonSvalkaResult = response.body();
                    DaoSession daoSession = DbHelper.getDaoMaster(InformationBoardActivity.this).newSession();
                    PendingImageDao pendingImageDao = daoSession.getPendingImageDao();
                    for (Uri uri : mPhotosList) {
                        PendingImage pendingImage = new PendingImage();
                        pendingImage.setEntity_id(commonSvalkaResult.getId());
                        pendingImage.setImage_code(commonSvalkaResult.getImageCode());
                        pendingImage.setImage_path(uri.getPath());
                        pendingImage.setToken(PrefsHelper.loadLastLoginData(InformationBoardActivity.this).mToken);
                        pendingImage.setIs_completed(false);
                        pendingImage.setPoint_id(commonSvalkaResult.getPoint_id());
                        pendingImageDao.insert(pendingImage);

                    }
                    Intent intent = new Intent(InformationBoardActivity.this, SentPendingDataService.class);
                    startService(intent);
                    Toast.makeText(InformationBoardActivity.this, "Данные обновлены", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }

            @Override
            public void onFailure(Call<CommonSvalkaResult> call, Throwable t) {

            }
        });

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            if (mSavedPhotoFile != null) {
                mImageAdapter.addImage(mSavedPhotoFile);
                mImageAdapter.notifyDataSetChanged();
                mPhotosList.add(mSavedPhotoFile);

            }
        }
        super.onActivityResult(requestCode, resultCode, data);
        //   QuickImagePick.handleActivityResult(getApplicationContext(), requestCode, resultCode, data, mCallback);

    }
    @Override
    public void notifyFileRemoved(final int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Удаление фотографии");
        builder.setMessage("Подтвердите удаление фото");
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                try {
                    mPhotosList.remove(position);
                    mImageAdapter.deleteImage(position);
                    mImageAdapter.notifyDataSetChanged();
                } catch (Exception ignored) {

                }

            }
        });
        builder.setNegativeButton(R.string.cancel, null);
        builder.create().show();
    }

    private Uri generateFileUri(int type) {
        File file = null;
        switch (type) {
            case TYPE_PHOTO:
                file = new File(directory.getAbsolutePath() + "/" + "photo_"
                        + System.currentTimeMillis() + ".jpg");
                break;

        }
        Log.d("File generate", "fileName = " + file);
        return Uri.fromFile(file);
    }

    private void createDirectory() {
        directory = GreenApp.getInstance().getExternalFilesDir(DIRECTORY_PICTURES);
        if (!directory.exists()) {
            boolean x = directory.mkdirs();
            Log.v("Folder create result", String.valueOf(x));
        }


    }
}
