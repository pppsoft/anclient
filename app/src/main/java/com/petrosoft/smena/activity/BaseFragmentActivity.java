package com.petrosoft.smena.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.crashlytics.android.answers.SignUpEvent;
import com.petrosoft.smena.GreenApp;
import com.petrosoft.smena.R;
import com.petrosoft.smena.fragment.BaseFragment;
import com.petrosoft.smena.server.IRequestListener;
import com.petrosoft.smena.server.RequestHelper;
import com.petrosoft.smena.service.LocationService;
import com.petrosoft.smena.utils.DialogHelper;
import com.petrosoft.smena.utils.PrefsHelper;

/**
 * Created by a.baskakov on 25/08/16.
 */
public class BaseFragmentActivity extends FragmentActivity {
    BaseFragment mCurFragment = null;

    public void addFragmentRoot(BaseFragment fragment) {
        FragmentManager fm = getSupportFragmentManager();
        for (int i = 0; i < fm.getBackStackEntryCount(); ++i) {
            fm.popBackStack();
        }
        addFragment(fragment, true);
    }

    public void addFragment(BaseFragment fragment) {
        addFragment(fragment, true);
    }

    public void addFragment(BaseFragment fragment, boolean addToBackStack) {
        hideKeyBoard();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        fragmentTransaction.replace(R.id.fragment_container, fragment);

        if (addToBackStack) {
            fragmentTransaction.addToBackStack(null);
        }

        fragmentTransaction.commitAllowingStateLoss();
        fragmentManager.executePendingTransactions();
        mCurFragment = fragment;
    }

    public void hideKeyBoard() {
        try {
            // Check if no view has focus:
            View view = getCurrentFocus();
            if (view != null) {
                InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            }
        } catch (Throwable ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        onBackPressed(false);
    }

    public void onBackPressed(boolean fromMenu) {
        hideKeyBoard();
        if (mCurFragment != null) {
            if ((mCurFragment).backPressed()) {
                return;
            }
        }
        FragmentManager fm = getSupportFragmentManager();
        if (fm.getBackStackEntryCount() > 1) {
            super.onBackPressed();
        } else {
            finish();
        }
    }

    public void finishWork() {

        final AlertDialog alertDialog = DialogHelper.showProgressDialog(this, this.getString(R.string.loading));
        alertDialog.show();
        RequestHelper.tryToFinishWorkAsync(this, new IRequestListener<Boolean>() {
            @Override
            public void success(Boolean element) {
//TODO Clear database
                alertDialog.hide();
                RequestHelper.clearDataBase(getApplicationContext());
                Answers.getInstance().logSignUp(new SignUpEvent().putMethod("Выход").putSuccess(true));
                LocationService.stop(BaseFragmentActivity.this);
                PrefsHelper.saveLastLoginData(null, BaseFragmentActivity.this);
                Intent intent = new Intent(BaseFragmentActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }

            @Override
            public void error() {
                alertDialog.hide();
                Answers.getInstance().logSignUp(new SignUpEvent().putMethod("Выход").putSuccess(false));
                GreenApp.ownSnackbar(mCurFragment.getView(), "Ошибка завершения смены").setAction("Очистка базы", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Answers.getInstance().logCustom(new CustomEvent("Нештатная ситуация").putCustomAttribute("Очистка базы", "Принудительно"));
                        RequestHelper.clearDataBase(getApplicationContext());
                        LocationService.stop(BaseFragmentActivity.this);
                        PrefsHelper.saveLastLoginData(null, BaseFragmentActivity.this);
                        Intent intent = new Intent(BaseFragmentActivity.this, LoginActivity.class);
                        startActivity(intent);
                        finish();
                    }
                }).show();
            }
        });
    }
}
