package com.petrosoft.smena.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ToggleButton;

import com.esri.android.map.GraphicsLayer;
import com.esri.android.map.MapView;
import com.esri.android.map.ags.ArcGISFeatureLayer;
import com.esri.android.map.ags.ArcGISTiledMapServiceLayer;
import com.esri.android.map.event.OnSingleTapListener;
import com.esri.android.map.event.OnStatusChangedListener;
import com.esri.android.runtime.ArcGISRuntime;
import com.esri.core.geometry.GeometryEngine;
import com.esri.core.geometry.Point;
import com.esri.core.geometry.SpatialReference;
import com.esri.core.map.CallbackListener;
import com.esri.core.map.FeatureSet;
import com.esri.core.map.Graphic;
import com.esri.core.symbol.PictureMarkerSymbol;
import com.esri.core.symbol.SimpleMarkerSymbol;
import com.esri.core.tasks.ags.query.Query;
import com.petrosoft.smena.GreenApp;
import com.petrosoft.smena.R;
import com.petrosoft.smena.activity.svalka.SvalkaActivity;
import com.petrosoft.smena.dao.CObjectDao;
import com.petrosoft.smena.dao.Claim;
import com.petrosoft.smena.dao.ClaimDao;
import com.petrosoft.smena.dao.DaoSession;
import com.petrosoft.smena.dao.GreenElement;
import com.petrosoft.smena.dao.GreenElementDao;
import com.petrosoft.smena.dao.GreenElementType;
import com.petrosoft.smena.dao.GreenElementTypeDao;
import com.petrosoft.smena.dao.Route;
import com.petrosoft.smena.dao.RouteDao;
import com.petrosoft.smena.db.DbHelper;
import com.petrosoft.smena.server.RequestHelper;
import com.petrosoft.smena.server.response.data.geocoding.GeoData;
import com.petrosoft.smena.service.MapDownloadService;
import com.petrosoft.smena.utils.LocationHelper;
import com.petrosoft.smena.utils.map.MBTilesLayer;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ArcGisMapActivity extends AppCompatActivity {
    public static final String ACTION_SELECT_OBJECTS = "select_objects";
    public static final String ACTION_FORISSUE = "show_object";
    public static final String ACTION_SHOW_CLAIMS = "show_claims";
    public static final String SHOW_SVALKA = "show_svalka";
    public static final String SHOW_ROUTES = "show_routes";
    public static final String LATITUDE = "latitude";
    public static final String LONGITUDE = "longitude";
    public static final String ACTION_SHOW_MULTIPOINT = "multipoint";


    @BindView(R.id.mapView)
    MapView mMapView;

    ArcGISFeatureLayer mFeatureLayer;
    @BindView(R.id.next_button)
    Button mNextButton;
    @BindView(R.id.toggleClaim)
    ToggleButton mToggleClaimButton;
    @BindView(R.id.toggleElementButton)
    ToggleButton mToggleElemntsButton;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    private List<Graphic> mFeatures;
    private Graphic mCurrentFeature;
    private ArrayList<ArcGISFeatureLayer> mElementsLayer;

    private MBTilesLayer mOfflineLayer;
    private ArcGISTiledMapServiceLayer mArcGISTiledMapServiceLayer;

    private GraphicsLayer mGraphicLayer, mClaimGraphicLayer;
    private double mLatitude;
    private double mLongtitude;
    private List<Claim> mClaims;


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.map_menu, menu);
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_arc_gis_map);
        ButterKnife.bind(this);
        ArcGISRuntime.setClientId(getString(R.string.arcgis_license));
        setSupportActionBar(mToolbar);
        getSupportActionBar().setSubtitle("");
        mFeatures = new ArrayList<>();

        String mapPath = GreenApp.getInstance().getExternalFilesDir("maps").getAbsolutePath() + "/iac.mbtiles";

        File f = new File(mapPath);
        if (!f.exists()) {
            mArcGISTiledMapServiceLayer = new ArcGISTiledMapServiceLayer("http://gis.toris.kis.gov.spb.ru/arccod1031/rest/services/BM_WGS84_PNG8/MapServer");
            mArcGISTiledMapServiceLayer.setVisible(true);
            int x = mMapView.addLayer(mArcGISTiledMapServiceLayer);
            mArcGISTiledMapServiceLayer.setOnStatusChangedListener(new OnStatusChangedListener() {
                @Override
                public void onStatusChanged(Object o, STATUS status) {
                    Log.v("map online", status.name());
                    if (status == STATUS.INITIALIZED && (o instanceof ArcGISTiledMapServiceLayer || o instanceof MBTilesLayer)) {
                        initMap();
                    }
                }
            });

        } else {
            try {
                mOfflineLayer = new MBTilesLayer(mapPath);
                mOfflineLayer.setVisible(true);
            } catch (Exception e) {
                e.printStackTrace();
                f.delete();
                mArcGISTiledMapServiceLayer = new ArcGISTiledMapServiceLayer("http://gis.toris.kis.gov.spb.ru/arccod1031/rest/services/BM_WGS84_PNG8/MapServer");
                mArcGISTiledMapServiceLayer.setVisible(true);
                mMapView.addLayer(mArcGISTiledMapServiceLayer);
            }

            mMapView.addLayer(mOfflineLayer);

            mOfflineLayer.setOnStatusChangedListener(new OnStatusChangedListener() {
                @Override
                public void onStatusChanged(Object o, STATUS status) {
                    Log.v("map offline", status.name());
                    if (status == STATUS.INITIALIZED && (o instanceof ArcGISTiledMapServiceLayer || o instanceof MBTilesLayer)) {
                        initMap();
                    }
                }
            });

        }

        mMapView.enableWrapAround(false);
        mMapView.setVisibility(View.VISIBLE);
        mNextButton.setVisibility(View.VISIBLE);
        mGraphicLayer = new GraphicsLayer();
        mClaimGraphicLayer = null;
        mMapView.addLayer(mGraphicLayer);


        mMapView.setOnStatusChangedListener(new OnStatusChangedListener() {
            @Override
            public void onStatusChanged(Object o, STATUS status) {
                Log.v("map", status.name());

//                if (status == STATUS.LAYER_LOADED && (o instanceof ArcGISTiledMapServiceLayer || o instanceof MBTilesLayer)) {
//                    initMap();
//                }

            }
        });

        //initMap();


    }

    private void initMap() {
        final DaoSession daoSession = DbHelper.getDaoMaster(getApplicationContext()).newSession();
        if (getIntent().getAction().equalsIgnoreCase(ACTION_SELECT_OBJECTS)) {
            List<Integer> al = getIntent().getIntegerArrayListExtra("oids");
            final List<Integer> ol = getIntent().getIntegerArrayListExtra("ids");
            String layer = getIntent().getStringExtra("layer") + "/0";
            searchObjectsByOID(layer, al);
            //claims show
            mToggleElemntsButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        //show elements
                        //get all elements in plan
                        //for each element group - query for show

                        GreenElementDao greenElementDao = daoSession.getGreenElementDao();

                        GreenElementTypeDao greenElementTypeDao = daoSession.getGreenElementTypeDao();

                        List<GreenElementType> list = new ArrayList<GreenElementType>(new HashSet<GreenElementType>(greenElementTypeDao.queryRawCreate(" inner join GREEN_ELEMENT on T._id=GREEN_ELEMENT.ELEMENT_TYPE_ID where OID is not null").list()));

                        mElementsLayer = new ArrayList<ArcGISFeatureLayer>();
                        for (GreenElementType greenElementType : list) {
                            //add feature server and query elements
                            final ArcGISFeatureLayer fl = new ArcGISFeatureLayer(greenElementType.getGis_layer_url() + "/0", ArcGISFeatureLayer.MODE.SELECTION);
                            fl.setSelectionColor(Color.RED);
                            //fl.setSelectionColorWidth(20);

                            mElementsLayer.add(fl);
                            mMapView.addLayer(fl);


                            List<Integer> al = new ArrayList<Integer>();
                            for (GreenElement g : greenElementType.getGreenElements()) {
                                if (g.getOid() != null) {
                                    al.add(g.getOid());
                                }
                            }
                            final Query query1 = new Query();
                            query1.setWhere("OID in (" + TextUtils.join(",", al) + ")");
                            fl.selectFeatures(query1, ArcGISFeatureLayer.SELECTION_METHOD.NEW, new CallbackListener<FeatureSet>() {
                                @Override
                                public void onCallback(FeatureSet featureSet) {

                                    Log.v("Fs", featureSet.toString());
                                    mMapView.invalidate();

                                    //  fl.addAll(Arrays.asList(featureSet.getGraphics()));

                                }

                                @Override
                                public void onError(Throwable throwable) {
                                    Log.e("Error get elements : ", throwable.getMessage());
                                }
                            });


                        }
                    } else {

                        if (mElementsLayer != null) {
                            for (ArcGISFeatureLayer afl : mElementsLayer) {
                                mMapView.removeLayer(afl);
                            }
                            mElementsLayer = null;
                        }
                    }
                }
            });
            mToggleClaimButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        showClaims(ol);

                    } else {
                        if (mClaimGraphicLayer != null) {
                            try {
                                mMapView.removeLayer(mClaimGraphicLayer);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }

                    }
                }
            });

            //showClaims(ol);
            //
            mNextButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    int index = mFeatures.indexOf(mCurrentFeature) + 1;
                    if (mFeatures.size() == 0 || mCurrentFeature == null || index == 0) {

                        AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
                        builder.setTitle(R.string.app_name);
                        //TODO
                        builder.setMessage("Список объектов найденных на карте пуст. Проверьте OID на Feature слое сервера");
                        builder.setPositiveButton("OK", null);
                        builder.create().show();

                        return;
                    }

                    if (index >= mFeatures.size() || index == -1) {
                        index = 0;
                    }
                    mCurrentFeature = mFeatures.get(index);
                    mMapView.setExtent(mCurrentFeature.getGeometry(), 20);
                    mMapView.invalidate();
                    getSupportActionBar().setSubtitle(mCurrentFeature.getAttributeValue("NAME").toString());
                }
            });
        }

        if (getIntent().getAction().equalsIgnoreCase(SHOW_SVALKA)) {
            mToggleElemntsButton.setVisibility(View.GONE);
            mToggleClaimButton.setVisibility(View.GONE);
            mNextButton.setVisibility(View.VISIBLE);
            mLatitude = getIntent().getFloatExtra(LATITUDE, 59.0f);
            mLongtitude = getIntent().getFloatExtra(LONGITUDE, 30.3f);
            if (mGraphicLayer != null) {
                mGraphicLayer.removeAll();

                Point p = (Point) GeometryEngine.project(new Point(mLongtitude, mLatitude), SpatialReference.create(SpatialReference.WKID_WGS84), SpatialReference.create(3857));
                if (p != null) {
                    Graphic g = new Graphic(p, new PictureMarkerSymbol(getResources().getDrawable(R.drawable.ic_marker)));
                    mGraphicLayer.addGraphic(g);
                    mMapView.zoomToScale(p, mMapView.getMaxScale());
                    localGeocode(new Point(mLongtitude, mLatitude));
                }
            } else {
                GreenApp.ownSnackbar(mMapView, String.format("Error showing geoposition %1$s, %2$s", String.valueOf(mLatitude), String.valueOf(mLongtitude))).show();
            }
            mNextButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent();
                    intent.putExtra("latitude", mLatitude);
                    intent.putExtra("longitude", mLongtitude);
                    intent.putExtra("address", mNextButton.getText().toString());

                    setResult(RESULT_OK, intent);
                    finish();
                }
            });
            mMapView.setOnSingleTapListener(new OnSingleTapListener() {
                @Override
                public void onSingleTap(float v, float v1) {
                    Point mapPoint = mMapView.toMapPoint(v, v1);
                    Graphic g = new Graphic(mapPoint, new PictureMarkerSymbol(getResources().getDrawable(R.drawable.ic_marker)).setOffsetX(0).setOffsetY(20));
                    if (mClaimGraphicLayer != null) {
                        mGraphicLayer.removeAll();
                    }
                    mMapView.removeLayer(mGraphicLayer);
                    Point np = (Point) GeometryEngine.project(mapPoint, mMapView.getSpatialReference(), SpatialReference.create(SpatialReference.WKID_WGS84));
                    localGeocode(np);
                    mLatitude = np.getY();
                    mLongtitude = np.getX();
                    mGraphicLayer = new GraphicsLayer();
                    mGraphicLayer.addGraphic(g);
                    mMapView.addLayer(mGraphicLayer);
                }
            });


        }

        if (getIntent().getAction().equalsIgnoreCase(ACTION_FORISSUE)) {
            mToggleClaimButton.setVisibility(View.INVISIBLE);
            // mToggleElemntsButton.setVisibility(View.INVISIBLE);
            final int oid = getIntent().getIntExtra("oid", -1);
            mToggleElemntsButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {


                        GreenElementDao greenElementDao = daoSession.getGreenElementDao();

                        GreenElementTypeDao greenElementTypeDao = daoSession.getGreenElementTypeDao();

                        List<GreenElementType> list = new ArrayList<GreenElementType>(new HashSet<GreenElementType>(greenElementTypeDao.queryRawCreate(" inner join GREEN_ELEMENT on T._id=GREEN_ELEMENT.ELEMENT_TYPE_ID where OID is not null").list()));

                        mElementsLayer = new ArrayList<ArcGISFeatureLayer>();
                        for (GreenElementType greenElementType : list) {
                            //add feature server and query elements
                            final ArcGISFeatureLayer fl = new ArcGISFeatureLayer(greenElementType.getGis_layer_url() + "/0", ArcGISFeatureLayer.MODE.SELECTION);
                            fl.setSelectionColor(Color.RED);
                            //fl.setSelectionColorWidth(20);

                            mElementsLayer.add(fl);
                            mMapView.addLayer(fl);


                            List<Integer> al = new ArrayList<Integer>();
                            for (GreenElement g : greenElementType.getGreenElements()) {
                                if (g.getOid() != null) {
                                    al.add(g.getOid());
                                }
                            }
                            final Query query1 = new Query();
                            query1.setWhere("OID in (" + TextUtils.join(",", al) + ")");
                            fl.selectFeatures(query1, ArcGISFeatureLayer.SELECTION_METHOD.NEW, new CallbackListener<FeatureSet>() {
                                @Override
                                public void onCallback(FeatureSet featureSet) {

                                    Log.v("Fs", featureSet.toString());
                                    mMapView.invalidate();

                                    //  fl.addAll(Arrays.asList(featureSet.getGraphics()));

                                }

                                @Override
                                public void onError(Throwable throwable) {
                                    Log.e("Error get elements : ", throwable.getMessage());
                                }
                            });


                        }
                    } else {

                        if (mElementsLayer != null) {
                            for (ArcGISFeatureLayer afl : mElementsLayer) {
                                mMapView.removeLayer(afl);
                            }
                            mElementsLayer = null;
                        }
                    }


                }
            });
            String layer = getIntent().getStringExtra("layer") + "/0";
            mNextButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent();
                    intent.putExtra("address", mNextButton.getText().toString());
                    intent.putExtra("latitude", mLatitude);
                    intent.putExtra("longitude", mLongtitude);
                    setResult(RESULT_OK, intent);
                    finish();
                }
            });

            if (oid != -1) {
                ArrayList<Integer> al = new ArrayList<>();
                al.add(oid);
                searchObjectsByOID(layer, al);
            } else {
                GreenApp.ownSnackbar(mMapView, "OID объекта не задан. Использую текущую позицию").show();
                Handler h = new Handler();
                h.postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        Location location = LocationHelper.getLastKnownLocation(getBaseContext());
                        if (location != null) {
                            mLongtitude = location.getLongitude();
                            mLatitude = location.getLatitude();

                            Point p = (Point) GeometryEngine.project(new Point(mLongtitude, mLatitude), SpatialReference.create(SpatialReference.WKID_WGS84), SpatialReference.create(3857));
                            mMapView.zoomToScale(p, mMapView.getMaxScale());
                            Graphic g = new Graphic(p, new PictureMarkerSymbol(getResources().getDrawable(R.drawable.ic_marker)));
                            mGraphicLayer.addGraphic(g);

                            localGeocode(new Point(mLongtitude, mLatitude));
                        } else {
                            GreenApp.ownSnackbar(mMapView, "OID не задан и не удалось определить текущую геопозицию").show();
                        }

                    }
                }, 1000);


            }
            mMapView.setOnSingleTapListener(new OnSingleTapListener() {
                @Override
                public void onSingleTap(float v, float v1) {
                    Point mapPoint = mMapView.toMapPoint(v, v1);
                    Graphic g = new Graphic(mapPoint, new PictureMarkerSymbol(getResources().getDrawable(R.drawable.ic_marker)).setOffsetX(0).setOffsetY(20));
                    mMapView.removeLayer(mGraphicLayer);
                    Point np = (Point) GeometryEngine.project(mapPoint, mMapView.getSpatialReference(), SpatialReference.create(SpatialReference.WKID_WGS84));
                    localGeocode(np);
                    mLatitude = np.getY();
                    mLongtitude = np.getX();
                    mGraphicLayer = new GraphicsLayer();
                    mGraphicLayer.addGraphic(g);
                    mMapView.addLayer(mGraphicLayer);

                }
            });
        }


        if (getIntent().getAction().equalsIgnoreCase(ACTION_SHOW_MULTIPOINT)) {
            final Map<Integer, Route> routeGMap = new HashMap<>();
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(R.string.route_points);
            mNextButton.setVisibility(View.GONE);
            mToggleClaimButton.setVisibility(View.INVISIBLE);
            mToggleElemntsButton.setVisibility(View.INVISIBLE);

            mMapView.setOnStatusChangedListener(new OnStatusChangedListener() {
                @Override
                public void onStatusChanged(Object o, STATUS status) {
                    if (status == STATUS.LAYER_LOADED) {
                        RouteDao routeDao = daoSession.getRouteDao();
                        List<Route> routeList = routeDao.loadAll();
                        if (mGraphicLayer != null) {
                            mGraphicLayer.removeAll();
                        } else {
                            mGraphicLayer = new GraphicsLayer();

                        }
                        mMapView.addLayer(mGraphicLayer);
                        for (Route r : routeList) {
                            Point p = (Point) GeometryEngine.project(new Point(r.getLongtitude(), r.getLatitude()), SpatialReference.create(SpatialReference.WKID_WGS84), SpatialReference.create(3857));
                            PictureMarkerSymbol pictureMarkerSymbol = null;
                            if (r.getIs_visible()) {
                                pictureMarkerSymbol = new PictureMarkerSymbol(getResources().getDrawable(R.drawable.ic_route_marker));
                            } else {
                                pictureMarkerSymbol = new PictureMarkerSymbol(getResources().getDrawable(R.drawable.ic_svalka_marker));
                            }

                            Graphic g = new Graphic(p, pictureMarkerSymbol);
                            int gid = mGraphicLayer.addGraphic(g);
                            routeGMap.put(gid, r);
                        }


//                        CObjectDao cObjectDao = daoSession.getCObjectDao();
//                        cObjectDao.queryBuilder().where(CObjectDao.Properties.ObjectTypeId.eq(9999)).list();

                        mMapView.addLayer(mGraphicLayer);
                        mMapView.setOnSingleTapListener(new OnSingleTapListener() {
                            @Override
                            public void onSingleTap(float v, float v1) {
                                int[] graphicIDs = mGraphicLayer.getGraphicIDs(v, v1, 100);
                                if (graphicIDs.length == 1) {
                                    final Route route = routeGMap.get(graphicIDs[0]);
                                    Log.v("Route", route.getName());
                                    AlertDialog.Builder builder = new AlertDialog.Builder(ArcGisMapActivity.this);
                                    if (!route.getIs_visible()) {
                                        builder.setTitle(R.string.title_activity_svalka);
                                    } else {
                                        builder.setTitle(R.string.route);
                                    }
                                    builder.setMessage(route.getName());
                                    builder.setPositiveButton("Перейти", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            Intent intent = new Intent();
                                            intent.putExtra(SvalkaActivity.IS_SVALKA, !route.getIs_visible());
                                            if (!route.getIs_visible()) {
                                                intent.putExtra(SvalkaActivity.ENTITY_ID, route.getCobject_id());
                                            } else {
                                                intent.putExtra(SvalkaActivity.ENTITY_ID, route.getId());
                                            }
                                            intent.putExtra(SvalkaActivity.ENTITY_NAME, route.getName());
                                            setResult(RESULT_OK, intent);
                                            finish();
                                        }
                                    });
                                    builder.setNegativeButton("OK", null);
                                    builder.create().show();
                                }
                            }
                        });
                    }
                }
            });


        }
    }

    private void showClaims(List<Integer> ol) {
        DaoSession daoSession = DbHelper.getDaoMaster(getApplicationContext()).newSession();
        ClaimDao claimDao = daoSession.getClaimDao();

        mClaims = claimDao.queryBuilder().where(ClaimDao.Properties.CObjectId.in(ol)).list();
        if (mClaims.size() > 0) {
            mClaimGraphicLayer = new GraphicsLayer();
            for (Claim c : mClaims) {
                Point p = (Point) GeometryEngine.project(new Point(c.getLongtitude(), c.getLatitude()), SpatialReference.create(SpatialReference.WKID_WGS84), SpatialReference.create(3857));
                //  mMapView.zoomToScale(p, mMapView.getMaxScale());
                Graphic g = new Graphic(p, new PictureMarkerSymbol(getResources().getDrawable(R.drawable.ic_marker_claim)));
                mClaimGraphicLayer.addGraphic(g);
            }
            mMapView.addLayer(mClaimGraphicLayer);
        } else {
            mClaimGraphicLayer = null;
        }
    }

    @Override
    protected void onResume() {
        mMapView.unpause();
        super.onResume();
    }

    @Override
    protected void onPause() {
        mMapView.pause();
        super.onPause();
    }

    private void localGeocode(Point np) {
        if (np != null) {
            RequestHelper.reverseGeoCoding(np.getY(), np.getX(), new Callback<GeoData>() {
                @Override
                public void onResponse(Call<GeoData> call, Response<GeoData> response) {
                    if (response.isSuccessful()) {
                        final String addr = response.body().address;
                        if (addr != null && !addr.isEmpty()) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    mNextButton.setText(addr);
                                }
                            });
                        }
                    }


                }

                @Override
                public void onFailure(Call<GeoData> call, Throwable t) {

                }
            });
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.my_location:


                showMyGeoPosition();
                break;


            case R.id.load_offline:
                loadOfflineMap();


                break;
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }

    private void showMyGeoPosition() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Location location = LocationHelper.getLastKnownLocation(getApplicationContext());


        if (location == null) {
            GreenApp.ownSnackbar(mMapView, "Не удается определить текущее местоположение").show();
            return;
        }
        mLatitude = location.getLatitude();
        mLongtitude = location.getLongitude();
        Point p = (Point) GeometryEngine.project(new Point(mLongtitude, mLatitude), SpatialReference.create(SpatialReference.WKID_WGS84), SpatialReference.create(3857));
        if (p != null) {
            if (mGraphicLayer != null) {
                mGraphicLayer.removeAll();
                Graphic g = new Graphic(p, new PictureMarkerSymbol(getResources().getDrawable(R.drawable.ic_marker)));
                mGraphicLayer.addGraphic(g);
                mMapView.zoomToScale(p, mMapView.getMaxScale());
                localGeocode(new Point(mLongtitude, mLatitude));
            }
        } else {
            GreenApp.ownSnackbar(mMapView, String.format("Error showing geoposition %1$s, %2$s", String.valueOf(mLatitude), String.valueOf(mLongtitude))).show();
        }
    }

    private void loadOfflineMap() {
        Intent intent = new Intent(getBaseContext(), MapDownloadService.class);
        startService(intent);
    }


    private void searchObjectsByOID(final String layer, List<Integer> al) {


        mFeatureLayer = new ArcGISFeatureLayer(layer, ArcGISFeatureLayer.MODE.SELECTION);
        mFeatureLayer.setSelectionColor(Color.BLUE);
        mFeatureLayer.setSelectionColorWidth(1);
        mMapView.addLayer(mFeatureLayer);

        Query query = new Query();
        final String oids_string = TextUtils.join(",", al);
        query.setWhere("OID in (" + oids_string + ")");
        final ProgressDialog dlg = new ProgressDialog(ArcGisMapActivity.this);
        dlg.setTitle(R.string.app_name);
        dlg.setMessage("загрузка данных");
        dlg.show();
        mFeatureLayer.selectFeatures(query, ArcGISFeatureLayer.SELECTION_METHOD.ADD, new CallbackListener<FeatureSet>() {
                    @Override
                    public void onCallback(FeatureSet featureSet) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                dlg.hide();
                            }
                        });

                        Graphic[] graphics = featureSet.getGraphics();
                        if (graphics.length == 0) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    AlertDialog.Builder builder = new AlertDialog.Builder(ArcGisMapActivity.this);
                                    builder.setTitle(R.string.app_name);
                                    builder.setMessage(String.format("OID объекта был получен (%1$s) , но не удалось найти его геометрию на слое (%2$s) ", oids_string, layer));
                                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            showMyGeoPosition();
                                            //onBackPressed();
                                        }
                                    });
                                    builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
                                        @Override
                                        public void onCancel(DialogInterface dialog) {
                                            onBackPressed();
                                        }
                                    });
                                    builder.create().show();
                                }
                            });

                            // return;
                        } else {

                            mFeatures.addAll(Arrays.asList(graphics));

                            if (mFeatures.size() > 0) {
                                mCurrentFeature = mFeatures.get(0);
                                final String title = mFeatures.get(0).getAttributeValue("NAME").toString();
                                String addr = title;
                                if (mFeatures.get(0).getAttributeValue("ADDRESS") != null) {
                                    addr = mFeatures.get(0).getAttributeValue("ADDRESS").toString();

                                }
                                final String address = addr;

                                if (mFeatures.size() > 1) {
                                    mNextButton.setVisibility(View.VISIBLE);
                                }
                                mMapView.setExtent(mCurrentFeature.getGeometry(), 20);
                                try {
                                    Thread.sleep(1000);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }

                                //mMapView.setScale(mMapView.getMaxScale());
                                if (getIntent().getAction().equalsIgnoreCase(ACTION_FORISSUE)) {
                                    mMapView.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            mGraphicLayer.removeAll();
                                            SimpleMarkerSymbol markerSymbol = new SimpleMarkerSymbol(Color.RED, 30, SimpleMarkerSymbol.STYLE.CIRCLE);
                                            mGraphicLayer.addGraphic(new Graphic(mMapView.getCenter(), markerSymbol));
                                            Point np = (Point) GeometryEngine.project(mMapView.getCenter(), mMapView.getSpatialReference(), SpatialReference.create(SpatialReference.WKID_WGS84));
                                            localGeocode(np);
                                            try {
                                                mLatitude = np.getY();
                                                mLongtitude = np.getX();

                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }

                                        }
                                    }, 5000);


                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            mNextButton.setText(address);
                                            getSupportActionBar().setSubtitle(title);
                                        }
                                    });
                                }
                            } else {

                                //  mNextButton.callOnClick();
                            }
                        }
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        Log.e("OID", throwable.getMessage());
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                dlg.hide();
                            }
                        });

                    }
                }

        );


    }


}
