package com.petrosoft.smena.activity.svalka;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.petrosoft.smena.GreenApp;
import com.petrosoft.smena.R;
import com.petrosoft.smena.adapters.ImageAdapter;
import com.petrosoft.smena.adapters.ImageRemover;
import com.petrosoft.smena.dao.DaoSession;
import com.petrosoft.smena.dao.PendingImage;
import com.petrosoft.smena.dao.PendingImageDao;
import com.petrosoft.smena.db.DbHelper;
import com.petrosoft.smena.server.response.data.svalka.CommonSvalkaResult;
import com.petrosoft.smena.server.response.data.svalka.IssueCompletion;
import com.petrosoft.smena.server.response.data.svalka.Svalka;
import com.petrosoft.smena.service.SentPendingDataService;
import com.petrosoft.smena.utils.PrefsHelper;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.os.Environment.DIRECTORY_PICTURES;


public class LiquidationResultActivity extends AppCompatActivity implements ImageRemover {

    private static final int TYPE_PHOTO = 44;
    private static final int REQUEST_IMAGE_CAPTURE = 6466;

    @BindView(R.id.lay_add_issue)
    View layout_add_issue;

    @BindView(R.id.no_issues_box)
    CheckBox mNoIssuesCheck;
    private long mSvalkaId;
    private IssueCompletion mIssueCompletion;
    @BindView(R.id.comment_text)
    EditText mComment;
    @BindView(R.id.add_issue_button)
    Button mAddIssueButton;

    @BindView(R.id.photos_list)
    RecyclerView mPhotosRecycler;
    private ImageAdapter mImageAdapter;
    private List<Uri> mPhotosList;
    private File directory;

    @BindView(R.id.add_photo_button)
    TextView mAddPhotoButton;
    private Uri mSavedPhotoFile;
    @BindView(R.id.toolbar)
     Toolbar mToolbar;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            if (mSavedPhotoFile != null) {
                mImageAdapter.addImage(mSavedPhotoFile);
                mImageAdapter.notifyDataSetChanged();
                mPhotosList.add(mSavedPhotoFile);

            }
        }
        super.onActivityResult(requestCode, resultCode, data);
        //   QuickImagePick.handleActivityResult(getApplicationContext(), requestCode, resultCode, data, mCallback);

    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        mSavedPhotoFile = generateFileUri(TYPE_PHOTO);
        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, mSavedPhotoFile);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_liquidation_result);
        ButterKnife.bind(this);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("Результаты ликвидации");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        createDirectory();
        mSvalkaId = getIntent().getLongExtra("svalka_id", -1L);
        mIssueCompletion = new IssueCompletion();
        mIssueCompletion.setDumpId(mSvalkaId);
        mIssueCompletion.setCheckId(GreenApp.getInstance().getSvalkaCheckId());
        mIssueCompletion.setEventDate(new Date());
        mImageAdapter = new ImageAdapter(this);
        mPhotosList = new ArrayList<>();
        loadSvalka();
        mNoIssuesCheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    layout_add_issue.setVisibility(View.GONE);
                } else {
                    layout_add_issue.setVisibility(View.VISIBLE);
                }
            }
        });
        mAddIssueButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                mIssueCompletion.setComment(mComment.getText().toString());
                GreenApp.getInstance().getSvalkaApi().addIssueCompletion(mIssueCompletion).enqueue(new Callback<CommonSvalkaResult>() {
                    @Override
                    public void onResponse(Call<CommonSvalkaResult> call, Response<CommonSvalkaResult> response) {
                        if (response.isSuccessful()) {
                            Toast.makeText(LiquidationResultActivity.this, "Данные обновлены", Toast.LENGTH_SHORT).show();
                            DaoSession daoSession = DbHelper.getDaoMaster(LiquidationResultActivity.this).newSession();
                            PendingImageDao pendingImageDao = daoSession.getPendingImageDao();
                            for (Uri uri : mPhotosList) {

                                CommonSvalkaResult commonSvalkaResult = response.body();
                                if (commonSvalkaResult.getImageCode() != null) {
                                    PendingImage pendingImage = new PendingImage();
                                    pendingImage.setEntity_id(commonSvalkaResult.getId());
                                    pendingImage.setImage_code(commonSvalkaResult.getImageCode());
                                    pendingImage.setImage_path(uri.getPath());
                                    pendingImage.setToken(PrefsHelper.loadLastLoginData(LiquidationResultActivity.this).mToken);
                                    pendingImage.setIs_completed(false);
                                    pendingImage.setPoint_id(commonSvalkaResult.getPoint_id());
                                    pendingImageDao.insert(pendingImage);
                                }
                            }
                            Intent intent = new Intent(LiquidationResultActivity.this, SentPendingDataService.class);
                            startService(intent);
                            finish();
                        }

                    }

                    @Override
                    public void onFailure(Call<CommonSvalkaResult> call, Throwable t) {


                    }
                });
            }
        });
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        mPhotosRecycler.setLayoutManager(linearLayoutManager);
        mPhotosRecycler.setAdapter(mImageAdapter);
        mAddPhotoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dispatchTakePictureIntent();
            }
        });


    }

    private void loadSvalka() {
        GreenApp.getInstance().getSvalkaApi().loadSvalka(mSvalkaId).enqueue(new Callback<Svalka>() {
            @Override
            public void onResponse(Call<Svalka> call, Response<Svalka> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(LiquidationResultActivity.this, "Данные по свалке загружены", Toast.LENGTH_SHORT).show();
                    Svalka svalka = response.body();
                    mIssueCompletion.setLatitude(svalka.latitude);
                    mIssueCompletion.setLongitude(svalka.longitude);

                }
            }

            @Override
            public void onFailure(Call<Svalka> call, Throwable t) {
                Toast.makeText(LiquidationResultActivity.this, "Данные по свалке не загружены", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void notifyFileRemoved(final int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Удаление фотографии");
        builder.setMessage("Подтвердите удаление фото");
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                try {
                    mPhotosList.remove(position);
                    mImageAdapter.deleteImage(position);
                    mImageAdapter.notifyDataSetChanged();
                } catch (Exception ignored) {

                }

            }
        });
        builder.setNegativeButton(R.string.cancel, null);
        builder.create().show();

    }

    private Uri generateFileUri(int type) {
        File file = null;
        switch (type) {
            case TYPE_PHOTO:
                file = new File(directory.getAbsolutePath() + "/" + "photo_"
                        + System.currentTimeMillis() + ".jpg");
                break;

        }
        Log.d("File generate", "fileName = " + file);
        return Uri.fromFile(file);
    }

    private void createDirectory() {
        directory = GreenApp.getInstance().getExternalFilesDir(DIRECTORY_PICTURES);
        if (!directory.exists()) {
            boolean x = directory.mkdirs();
            Log.v("Folder create result", String.valueOf(x));
        }


    }
}
