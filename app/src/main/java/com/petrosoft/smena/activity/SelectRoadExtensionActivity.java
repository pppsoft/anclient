package com.petrosoft.smena.activity;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.petrosoft.smena.GreenApp;
import com.petrosoft.smena.R;
import com.petrosoft.smena.adapters.RoadExtensionAdapter;
import com.petrosoft.smena.server.response.data.add_issue.RoadExtension;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SelectRoadExtensionActivity extends AppCompatActivity {

    public static final String EXTENSION = "extension";

    @BindView(R.id.road_extensions_list)
    RecyclerView mRoadExtensionRecycler;
    private RoadExtensionAdapter mRoadExtensionAdapter;
    private long mId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_road_extension);
        mId = getIntent().getLongExtra("id", -1L);
        ButterKnife.bind(this);
        mRoadExtensionRecycler.setLayoutManager(new LinearLayoutManager(this));
        mRoadExtensionAdapter = new RoadExtensionAdapter(this);
        mRoadExtensionRecycler.setAdapter(mRoadExtensionAdapter);

        loadData();
    }

    private void loadData() {
        GreenApp.getInstance().getApi().getRoadExtensionById(mId).enqueue(new Callback<List<RoadExtension>>() {
            @Override
            public void onResponse(Call<List<RoadExtension>> call, Response<List<RoadExtension>> response) {
                if (response.isSuccessful()) {
                    List<RoadExtension> body = response.body();
                    if (body.size() > 0) {
                        mRoadExtensionAdapter.setItems(body);
                        mRoadExtensionAdapter.notifyDataSetChanged();
                    } else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(SelectRoadExtensionActivity.this);
                        builder.setTitle(R.string.road_extension);
                        builder.setMessage(R.string.not_found);
                        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                setResult(RESULT_CANCELED);
                                finish();
                            }
                        });
                        builder.create().show();

                    }
                }
            }

            @Override
            public void onFailure(Call<List<RoadExtension>> call, Throwable t) {

            }
        });
    }
}
