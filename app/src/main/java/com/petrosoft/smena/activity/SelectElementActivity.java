package com.petrosoft.smena.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Spinner;

import com.petrosoft.smena.R;
import com.petrosoft.smena.adapters.ElementAdapter;
import com.petrosoft.smena.adapters.ElementGroupAdapter;
import com.petrosoft.smena.dao.CObject;
import com.petrosoft.smena.dao.CObjectDao;
import com.petrosoft.smena.dao.CObjectToElements;
import com.petrosoft.smena.dao.CObjectToElementsDao;
import com.petrosoft.smena.dao.DaoSession;
import com.petrosoft.smena.dao.GreenElement;
import com.petrosoft.smena.db.DbHelper;
import com.petrosoft.smena.server.RequestHelper;
import com.petrosoft.smena.server.response.data.add_issue.Element;
import com.petrosoft.smena.server.response.data.add_issue.ElementGroup;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SelectElementActivity extends AppCompatActivity {
    @BindView(R.id.recycler_view)
    RecyclerView mRecyclerView;
    SelectElementActivity mActivity;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    private SearchView mSearchView;

    @BindView(R.id.srl)
    SwipeRefreshLayout mSwipeRefreshLayout;
    @BindView(R.id.group_element)
    Spinner mElementsSpinner;
    private ArrayList<Element> mElements;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_element);
        ButterKnife.bind(this);
        mActivity = this;
        setSupportActionBar(mToolbar);

        getSupportActionBar().setTitle("");
        LinearLayoutManager llm = new LinearLayoutManager(getBaseContext(), LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(llm);
        final long object_id = getIntent().getLongExtra("object_id", -1);
        DaoSession daoSession = DbHelper.getDaoMaster(getApplicationContext()).newSession();
        CObjectDao cObjectDao = daoSession.getCObjectDao();
        //load only elements in plan
        CObjectToElementsDao cObjectToElementsDao = daoSession.getCObjectToElementsDao();
        CObject cObject = cObjectDao.queryBuilder().where(CObjectDao.Properties.ServerId.eq(object_id)).unique();
        if (cObject.getElementsToElemenstCobjects().size() == 0) {
            AlertDialog.Builder builder = new AlertDialog.Builder(SelectElementActivity.this);
            builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    finish();
                }
            });
            builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    finish();
                }
            });
            builder.setMessage("Для данного объекта отсутствуют элементы, добавленные в план");
            builder.setTitle("Внимание");
            builder.create().show();


            //  finish();
//            loadObjectDetail(object_id);
//            mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
//                @Override
//                public void onRefresh() {
//                    loadObjectDetail(object_id);
//                }
//            });
        } else {

            List<Element> list = new ArrayList(cObject.getElementsToElemenstCobjects().size());
            for (CObjectToElements greenElement : cObject.getElementsToElemenstCobjects()) {
                GreenElement element = greenElement.getGreenElement();
                Element e = new Element(element.getId());
                e.element_type_id = element.getElement_type_id();
                e.inventory_number = element.getInventory_number();
                e.name = element.getName();
                list.add(e);
            }
            loadAndShowElements((ArrayList<Element>) list);
        }


        mElementsSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (mElementsSpinner.getAdapter() == null) {

                    return;
                }
                ElementGroupAdapter leg = (ElementGroupAdapter) mElementsSpinner.getAdapter();
                try {

                    int gn = leg.getItem(position).id;
                    if (mRecyclerView.getAdapter() != null) {
                        ElementAdapter ea = (ElementAdapter) mRecyclerView.getAdapter();
                        ea.search(gn);
                        ea.notifyDataSetChanged();
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void loadElementGroup() {
        RequestHelper.getElementTypes(new Callback<List<ElementGroup>>() {
            @Override
            public void onResponse(Call<List<ElementGroup>> call, Response<List<ElementGroup>> response) {
                if (response.isSuccessful()) {
                    ElementGroup[] elemArray = response.body().toArray(new ElementGroup[response.body().size()]);
                    ArrayList<ElementGroup> leg = new ArrayList<ElementGroup>();
                    for (ElementGroup g : elemArray) {
                        for (Element me : mElements) {
                            if (me.element_type_id == g.id) {
                                leg.add(g);
                                break;
                            }
                        }
                    }
                    ElementGroupAdapter elementAdapter = new ElementGroupAdapter(getBaseContext(), android.R.layout.simple_spinner_item, leg.toArray(new ElementGroup[leg.size()]));
                    mElementsSpinner.setAdapter(elementAdapter);
                    mElementsSpinner.callOnClick();
                }
            }

            @Override
            public void onFailure(Call<List<ElementGroup>> call, Throwable t) {

            }
        });


    }

    private void loadObjectDetail(long object_id) {
        mSwipeRefreshLayout.setRefreshing(true);
        RequestHelper.getElements(object_id, new Callback<ArrayList<Element>>() {
            @Override
            public void onResponse(Call<ArrayList<Element>> call, Response<ArrayList<Element>> response) {

                if (response.isSuccessful()) {

                    loadAndShowElements(response.body());
                }
                mSwipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onFailure(Call<ArrayList<Element>> call, Throwable t) {
                mSwipeRefreshLayout.setRefreshing(false);
                Log.e("elements", t.getMessage());
            }
        });
    }

    private void loadAndShowElements(ArrayList<Element> response) {
        ElementAdapter adapter = new ElementAdapter(response, mActivity);
        mRecyclerView.setAdapter(adapter);
        mElements = response;
        loadElementGroup();
    }

    private void setupSearch(Menu menu) {
        mSearchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        mSearchView.setSubmitButtonEnabled(false);
        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (mRecyclerView.getAdapter() != null) {
                    ElementAdapter ea = (ElementAdapter) mRecyclerView.getAdapter();
                    ea.search(query);
                    ea.notifyDataSetChanged();
                }
                try {
                    // Check if no view has focus:
                    View view = getCurrentFocus();
                    if (view != null) {
                        InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                    }
                } catch (Throwable ex) {
                    ex.printStackTrace();
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                return false;
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.object_menu, menu);
        setupSearch(menu);
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
}
