package com.petrosoft.smena.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.petrosoft.smena.R;
import com.petrosoft.smena.dao.CObject;
import com.petrosoft.smena.dao.CheckTable;
import com.petrosoft.smena.dao.CheckToObject;
import com.petrosoft.smena.dao.DaoSession;
import com.petrosoft.smena.dao.ObjectType;
import com.petrosoft.smena.dao.ObjectTypeDao;
import com.petrosoft.smena.db.DbHelper;
import com.petrosoft.smena.fragment.BaseFragment;
import com.petrosoft.smena.fragment.main.MainFragment;
import com.petrosoft.smena.fragment.main.recycler.element.ObjectTypeElement;
import com.petrosoft.smena.utils.PrefsHelper;

import java.util.List;

public class MainActivity extends BaseFragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (mCurFragment == null) {

            CheckTable currentCheck = DbHelper.getCurrentCheck(this);
            CObject currentObject = DbHelper.getCheckCObject(this);

            if (currentCheck != null) {
                if (currentObject == null) {
                    List<CheckToObject> checkToObjectList = currentCheck.getCheckToCObject();
                    if (checkToObjectList.size() > 0) {
                        currentObject = checkToObjectList.get(0).getCObject();
                    }
                }


                DaoSession daoSession = DbHelper.getDaoMaster(this).newSession();
                ObjectTypeDao objectTypeDao = daoSession.getObjectTypeDao();

                ObjectType objectType = objectTypeDao.queryBuilder().where(ObjectTypeDao.Properties.Id.eq(currentObject.getObjectTypeId())).unique();
                openObjectTypeActivity(objectType);
            }
            mCurFragment = new MainFragment();
            addFragment(mCurFragment);
        }
    }

    public void openObjectTypeActivity(ObjectType objectType) {
        Intent intent = new Intent(this, ObjectTypeActivity.class);
        intent.putExtra(ObjectTypeActivity.EXTRA_OBJECT_TYPE_SERVER_ID, objectType.getId());
        startActivity(intent);
    }
}
