package com.petrosoft.smena.activity;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.LoginEvent;
import com.crashlytics.android.core.BuildConfig;
import com.crashlytics.android.core.CrashlyticsCore;
import com.petrosoft.smena.R;
import com.petrosoft.smena.server.RequestHelper;
import com.petrosoft.smena.server.response.data.login.LoginData;
import com.petrosoft.smena.service.LocationService;
import com.petrosoft.smena.service.LoginSyncService;
import com.petrosoft.smena.utils.DialogHelper;
import com.petrosoft.smena.utils.PrefsHelper;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnLongClick;
import io.fabric.sdk.android.Fabric;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.petrosoft.smena.GreenApp.checkNetwork;
import static com.petrosoft.smena.GreenApp.showNoNetworkSnackbar;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class LoginActivity extends Activity {
    public final static String PARAM_PINTENT = "pendingIntent";
    public final static int STATUS_FINISH = 200;
    @BindView(R.id.login)
    TextView mLogin;

    @BindView(R.id.password)
    TextView mPassword;
    private AlertDialog mProgressAlertDialog;

    @OnClick(R.id.button_login)
    public void doLogin() {
        if (!checkNetwork()) {
            showNoNetworkSnackbar(findViewById(R.id.main_layout), new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    doLogin();
                }
            });
            return;
        }
        hideKeyBoard();
        final String login = String.valueOf(mLogin.getText());
        String password = String.valueOf(mPassword.getText());
        if (!TextUtils.isEmpty(login) && !TextUtils.isEmpty(password)) {

            mProgressAlertDialog = DialogHelper.showProgressDialog(LoginActivity.this, getString(R.string.loading));
            mProgressAlertDialog.show();
            RequestHelper.login(
                    login,
                    password,
                    new Callback<LoginData>() {
                        @Override
                        public void onResponse(Call<LoginData> call, Response<LoginData> response) {


                            if (response.isSuccessful()) {
                                Answers.getInstance().logLogin(new LoginEvent().putMethod("new").putSuccess(true).putCustomAttribute("login", login));
                                Date date = new Date();
                                LoginData newLoginData = response.body();
                                newLoginData.mDate = date;
                                PrefsHelper.saveLastLoginData(response.body(), LoginActivity.this);
                                Intent i = new Intent();
                                i.putExtra("action", "sync");
                                PendingIntent pi = createPendingResult(0, i, 0);
                                Intent intent = new Intent(LoginActivity.this, LoginSyncService.class);
                                intent.putExtra("token", newLoginData.mToken);
                                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
                                intent.putExtra("date", simpleDateFormat.format(newLoginData.mDate));
                                intent.putExtra(PARAM_PINTENT, pi);
                                startService(intent);
                            } else {
                                mProgressAlertDialog.hide();
                                showErrorLoginDialog();
                            }
                        }

                        @Override
                        public void onFailure(Call<LoginData> call, Throwable t) {
                            mProgressAlertDialog.hide();
                            Answers.getInstance().logLogin(new LoginEvent().putMethod("new").putSuccess(false).putCustomAttribute("login", login));
                            showErrorLoginDialog();
                        }
                    });
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == STATUS_FINISH) {
            boolean result = data.getBooleanExtra("result", false);
            if (result) {
                if (mProgressAlertDialog != null && mProgressAlertDialog.isShowing()) {
                    mProgressAlertDialog.hide();
                }
                startMainActivity();
            } else {
                showErrorLoginDialog();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @OnLongClick(R.id.main_layout)
    public boolean showSettings() {
        Intent i = new Intent(this, SettingsActivity.class);
        startActivity(i);
        return true;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Создаём Crashlytics,выключенный для debug сборок
        Crashlytics crashlyticsKit = new Crashlytics.Builder()
                .core(new CrashlyticsCore.Builder().disabled(BuildConfig.DEBUG).build())
                .build();
        Fabric.with(this, crashlyticsKit, new Crashlytics());
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);


        ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION}, 225);

        LoginData loginData = PrefsHelper.loadLastLoginData(this);
        if (loginData.mToken != null) {
//            Answers.getInstance().logLogin(new LoginEvent().putMethod("saved").putSuccess(true).putCustomAttribute("login",loginData.mLogin));
            startMainActivity();
            return;
        }
        if (!checkNetwork()) {
            showNoNetworkSnackbar(findViewById(R.id.main_layout), new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    doLogin();
                }
            });
        }
    }


    private void showErrorLoginDialog() {
        DialogHelper.showDialog(LoginActivity.this,
                LoginActivity.this.getString(R.string.error),
                LoginActivity.this.getString(R.string.login_error),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
    }

    public void hideKeyBoard() {
        try {
            // Check if no view has focus:
            View view = getCurrentFocus();
            if (view != null) {
                InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            }
        } catch (Throwable ex) {
            ex.printStackTrace();
        }
    }

    private void startMainActivity() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                final LoginData loginData = PrefsHelper.loadLastLoginData(LoginActivity.this);
                final AlertDialog dialog = DialogHelper.showDialog(LoginActivity.this, String.format(Locale.getDefault(), getString(R.string.hello), loginData.mFirstName));
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        LocationService.start(LoginActivity.this);
                        dialog.dismiss();
                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        Answers.getInstance().logLogin(new LoginEvent().putMethod("saved").putSuccess(true).putCustomAttribute("login", loginData.mLogin));
                        startActivity(intent);
                        finish();
                    }
                }, 1);
            }
        });
    }
}
