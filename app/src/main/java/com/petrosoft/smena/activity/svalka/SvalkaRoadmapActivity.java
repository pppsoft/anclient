package com.petrosoft.smena.activity.svalka;

import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.google.gson.Gson;
import com.petrosoft.smena.GreenApp;
import com.petrosoft.smena.R;
import com.petrosoft.smena.adapters.RoadMapAdapter;
import com.petrosoft.smena.server.RequestHelper;
import com.petrosoft.smena.server.response.data.svalka.RoadMap;
import com.petrosoft.smena.server.response.data.svalka.Svalka;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SvalkaRoadmapActivity extends AppCompatActivity {
    @BindView(R.id.roadmapview)
    RecyclerView mRoadMapView;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;


    private Long mSvalkaId;
    private List<RoadMap> mRoadmaps;


    private void loadSvalkaRoadmap(final long id) {
        // mRefreshLayout.setRefreshing(true);
        RequestHelper.roadMapSvalka(id, new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                // mRefreshLayout.setRefreshing(false);
                if (response.isSuccessful()) {

                    Gson gson = new Gson();
                    try {
                        RoadMap[] rm;
                        rm = gson.fromJson(response.body().string(), RoadMap[].class);
                        mRoadmaps = Arrays.asList(rm);
                        if (mRoadmaps.size() != 0) {
                            RoadMapAdapter adapter = new RoadMapAdapter(mRoadmaps, id);
                            mRoadMapView.setAdapter(adapter);
                        } else {
                            GreenApp.ownSnackbar(mRoadMapView, "Дорожная карта не создана").setAction("Назад", new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    finish();
                                }
                            }).show();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if (response.code() == 404) {

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                // mRefreshLayout.setRefreshing(false);
                GreenApp.ownSnackbar(mRoadMapView, "Не удалось загрузить дорожную карту.").setAction("Назад", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finish();
                    }
                }).addCallback(new Snackbar.Callback() {
                    @Override
                    public void onDismissed(Snackbar snackbar, int event) {
//                        finish();
                        super.onDismissed(snackbar, event);
                    }
                }).show();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (GreenApp.checkNetwork()) {

//            RequestHelper.loadSvalka(mSvalkaId, new Callback<ResponseBody>() {
//                @Override
//                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//                    if (response.isSuccessful()) {
//
//                        Gson gson = new Gson();
//                        try {
//                            Svalka sd = gson.fromJson(response.body().string(), Svalka.class);
//                            getSupportActionBar().setSubtitle(sd.getName());
//                        } catch (IOException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                }
//
//                @Override
//                public void onFailure(Call<ResponseBody> call, Throwable t) {
//                    Log.e("error load svalka", t.getMessage());
//
//                }
//            });

            loadSvalkaRoadmap(mSvalkaId);

        } else {
            GreenApp.showNoNetworkSnackbar(mRoadMapView, new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_svalka_roadmap);
        ButterKnife.bind(this);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //final long
        mSvalkaId = getIntent().getLongExtra("svalka_id", -1);
        getSupportActionBar().setTitle("Дорожная карта");

        getSupportActionBar().setSubtitle(getIntent().getStringExtra("name"));
        mRoadMapView.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(getBaseContext());
        mRoadMapView.setLayoutManager(llm);

        loadSvalkaRoadmap(mSvalkaId);
//        mRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
//            @Override
//            public void onRefresh() {
//                loadSvalkaRoadmap(mSvalkaId);
//            }
//        });


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
