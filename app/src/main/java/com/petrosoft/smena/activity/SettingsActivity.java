package com.petrosoft.smena.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;

import com.petrosoft.smena.R;
import com.petrosoft.smena.utils.PrefsHelper;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SettingsActivity extends AppCompatActivity {

    @BindView(R.id.server_url)
    EditText mServerUrl;

    @OnClick(R.id.btnSave)
    public void saveUrl(){
        PrefsHelper.saveServerUrl(mServerUrl.getText().toString(),getApplicationContext());
        finish();
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        ButterKnife.bind(this);
        mServerUrl.setSingleLine(true);
        mServerUrl.setText(PrefsHelper.loadServerUrl(getApplicationContext()));
    }
}
