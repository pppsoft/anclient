package com.petrosoft.smena.activity.svalka;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.TextView;

import com.petrosoft.smena.GreenApp;
import com.petrosoft.smena.R;
import com.petrosoft.smena.adapters.ImageAdapter;
import com.petrosoft.smena.adapters.ImageRemover;
import com.petrosoft.smena.dao.DaoSession;
import com.petrosoft.smena.dao.PendingImage;
import com.petrosoft.smena.dao.PendingImageDao;
import com.petrosoft.smena.db.DbHelper;
import com.petrosoft.smena.server.response.data.svalka.CommonSvalkaResult;
import com.petrosoft.smena.server.response.data.svalka.SimpleElementRequest;
import com.petrosoft.smena.server.response.data.svalka.SimpleElementResponse;
import com.petrosoft.smena.service.SentPendingDataService;
import com.petrosoft.smena.utils.PrefsHelper;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.os.Environment.DIRECTORY_PICTURES;

public class BorderSetupActivity extends AppCompatActivity implements ImageRemover {
    private static final int TYPE_PHOTO = 44;
    private static final int REQUEST_IMAGE_CAPTURE = 6466;
    private long mSvalkaId;

    @BindView(R.id.checkbox_completed)
    CheckBox mSwitchCompleted;
    @BindView(R.id.fab_save)
    FloatingActionButton mSaveButton;

    @BindView(R.id.future_photos)
    RecyclerView mFuturePhotosList;
    private ImageAdapter mImageAdapter;
    private List<Uri> mPhotosList;
    private Uri mSavedPhotoFile;

    @BindView(R.id.add_photo_button)
    TextView mAddPhotoButton;
    private File directory;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            if (mSavedPhotoFile != null) {
                mImageAdapter.addImage(mSavedPhotoFile);
                mImageAdapter.notifyDataSetChanged();
                mPhotosList.add(mSavedPhotoFile);

            }
        }
        super.onActivityResult(requestCode, resultCode, data);
        //   QuickImagePick.handleActivityResult(getApplicationContext(), requestCode, resultCode, data, mCallback);

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mSvalkaId = -1;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_border_setup);
        ButterKnife.bind(this);
        createDirectory();
        mPhotosList = new ArrayList<>();

        mSvalkaId = getIntent().getLongExtra("svalka_id", -1);
//        getSupportActionBar().setTitle("Установка ограждения");
        if (GreenApp.checkNetwork()) {
            loadData();
        } else {

            GreenApp.showNoNetworkSnackbar(mFuturePhotosList, null);
        }
        mSaveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveBorderSetup();
            }
        });

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getBaseContext(), LinearLayoutManager.HORIZONTAL, false);
        mFuturePhotosList.setLayoutManager(linearLayoutManager);
        mImageAdapter = new ImageAdapter(this);
        mFuturePhotosList.setAdapter(mImageAdapter);
        mAddPhotoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dispatchTakePictureIntent();
            }
        });
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        mSavedPhotoFile = generateFileUri(TYPE_PHOTO);
        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, mSavedPhotoFile);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    private void saveBorderSetup() {
        SimpleElementRequest simpleElementRequest = new SimpleElementRequest();
        simpleElementRequest.setSvalka_id(mSvalkaId);
        simpleElementRequest.setCheck_id(GreenApp.getInstance().getSvalkaCheckId());
        if (mSwitchCompleted.isChecked()) {
            simpleElementRequest.setFact_date(new Date());
        } else {
            simpleElementRequest.setFact_date(null);
        }
        //  simpleElementRequest.setIs_completed(mSwitchCompleted.isChecked());
        GreenApp.getInstance().getSvalkaApi().saveBorderSetup(simpleElementRequest).enqueue(new Callback<CommonSvalkaResult>() {
            @Override
            public void onResponse(Call<CommonSvalkaResult> call, Response<CommonSvalkaResult> response) {
                if (response.isSuccessful()) {
                    CommonSvalkaResult commonSvalkaResult = response.body();
                    DaoSession daoSession = DbHelper.getDaoMaster(BorderSetupActivity.this).newSession();
                    PendingImageDao pendingImageDao = daoSession.getPendingImageDao();
                    for (Uri uri : mPhotosList) {
                        PendingImage pendingImage = new PendingImage();
                        pendingImage.setEntity_id(commonSvalkaResult.getId());
                        pendingImage.setImage_code(commonSvalkaResult.getImageCode());
                        pendingImage.setImage_path(uri.getPath());
                        pendingImage.setToken(PrefsHelper.loadLastLoginData(BorderSetupActivity.this).mToken);
                        pendingImage.setIs_completed(false);
                        pendingImage.setPoint_id(commonSvalkaResult.getPoint_id());
                        pendingImageDao.insert(pendingImage);

                    }
                    Intent intent = new Intent(BorderSetupActivity.this, SentPendingDataService.class);
                    startService(intent);
                    finish();
                }
            }

            @Override
            public void onFailure(Call<CommonSvalkaResult> call, Throwable t) {

            }
        });

    }

    private void loadData() {


        GreenApp.getInstance().getSvalkaApi().loadElementInfo(mSvalkaId, "3.1").enqueue(new Callback<SimpleElementResponse>() {
            @Override
            public void onResponse(Call<SimpleElementResponse> call, Response<SimpleElementResponse> response) {
                if (response.isSuccessful()) {
                    SimpleElementResponse simpleElementResponse = response.body();
                    mSwitchCompleted.setChecked(simpleElementResponse.getFact_date() != null);

                }
            }

            @Override
            public void onFailure(Call<SimpleElementResponse> call, Throwable t) {
                Log.v("v", t.getMessage());
            }
        });


    }

    @Override
    public void notifyFileRemoved(final int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Удаление фотографии");
        builder.setMessage("Подтвердите удаление фото");
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                try {
                    mPhotosList.remove(position);
                    mImageAdapter.deleteImage(position);
                    mImageAdapter.notifyDataSetChanged();
                } catch (Exception ignored) {

                }

            }
        });
        builder.setNegativeButton(R.string.cancel, null);
        builder.create().show();
    }

    private Uri generateFileUri(int type) {
        File file = null;
        switch (type) {
            case TYPE_PHOTO:
                file = new File(directory.getAbsolutePath() + "/" + "photo_"
                        + System.currentTimeMillis() + ".jpg");
                break;

        }
        Log.d("File generate", "fileName = " + file);
        return Uri.fromFile(file);
    }

    private void createDirectory() {
        directory = GreenApp.getInstance().getExternalFilesDir(DIRECTORY_PICTURES);
        if (!directory.exists()) {
            boolean x = directory.mkdirs();
            Log.v("Folder create result", String.valueOf(x));
        }


    }
}
