package com.petrosoft.smena.activity.svalka;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.petrosoft.smena.GreenApp;
import com.petrosoft.smena.R;
import com.petrosoft.smena.adapters.ImageRemover;
import com.petrosoft.smena.adapters.SimpleCheckedItemAdapter;
import com.petrosoft.smena.dao.DaoSession;
import com.petrosoft.smena.dao.Issue;
import com.petrosoft.smena.dao.IssueDao;
import com.petrosoft.smena.db.DbHelper;
import com.petrosoft.smena.server.response.data.SimpleItem;
import com.petrosoft.smena.server.response.data.svalka.CheckIssueCompletion;
import com.petrosoft.smena.server.response.data.svalka.CommonSvalkaResult;
import com.petrosoft.smena.server.response.data.svalka.IssueCompletion;
import com.petrosoft.smena.utils.WaitGroup;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class IssuesFixationActivity extends AppCompatActivity implements ImageRemover {

    @BindView(R.id.issues_list)
    RecyclerView mIssuesList;
    @BindView(R.id.save_button)
    Button mSaveButton;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    private long mSvalkaId;
    private List<Issue> mIssues;
    private SimpleCheckedItemAdapter mSimpleCheckedItemAdapter;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_issues_fixation);
        ButterKnife.bind(this);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mSvalkaId = getIntent().getLongExtra("svalka_id", -1L);
        mSimpleCheckedItemAdapter = new SimpleCheckedItemAdapter();
        LinearLayoutManager llm = new LinearLayoutManager(this);
        mIssuesList.setAdapter(mSimpleCheckedItemAdapter);
        mIssuesList.setLayoutManager(llm);
        loadData();
        mSaveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        DaoSession daoSession = DbHelper.getDaoMaster(IssuesFixationActivity.this).newSession();
                        IssueDao issueDao = daoSession.getIssueDao();
                        CheckIssueCompletion checkIssueCompletion = new CheckIssueCompletion();
                        checkIssueCompletion.setCheck_id(GreenApp.getInstance().getSvalkaCheckId());

                        for (SimpleItem item : mSimpleCheckedItemAdapter.getItems()) {
                            Issue issue = issueDao.queryBuilder().where(IssueDao.Properties.ServerId.eq(item.getId())).unique();
                            if (issue != null) {
                                issue.setIsCompleted(item.getIsCompleted());
                                issueDao.update(issue);
                            }

                            checkIssueCompletion.setIs_completed(item.getIsCompleted());

                            try {
                                GreenApp.getInstance().getApi().completeIssue(item.getId(), checkIssueCompletion).execute();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                        finish();
                    }
                });
                thread.start();

            }
        });
    }

    public void loadData() {

        DaoSession daoSession = DbHelper.getDaoMaster(this).newSession();
        IssueDao issueDao = daoSession.getIssueDao();
        mIssues = issueDao.queryBuilder().where(IssueDao.Properties.CObjectId.eq(mSvalkaId)).list();
        List<SimpleItem> items = new ArrayList<>();
        for (Issue issue : mIssues) {
            SimpleItem item = new SimpleItem();
            item.setId(issue.getServerId());
            item.setIsCompleted(issue.getIsCompleted());
            item.setName(issue.getComment());
            items.add(item);
        }
        mSimpleCheckedItemAdapter.setItems(items);
        mSimpleCheckedItemAdapter.notifyDataSetChanged();


    }

    @Override
    public void notifyFileRemoved(final int position) {
        // CObject cObject= new CObject();

    }
}
