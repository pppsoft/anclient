package com.petrosoft.smena.activity.svalka;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.petrosoft.smena.GreenApp;
import com.petrosoft.smena.R;
import com.petrosoft.smena.dao.DaoSession;
import com.petrosoft.smena.dao.PendingImage;
import com.petrosoft.smena.dao.PendingImageDao;
import com.petrosoft.smena.dao.Route;
import com.petrosoft.smena.dao.RouteDao;
import com.petrosoft.smena.db.DbHelper;
import com.petrosoft.smena.server.response.data.svalka.CommonSvalkaResult;
import com.petrosoft.smena.server.response.data.svalka.RouteCheckResult;
import com.petrosoft.smena.service.SentPendingDataService;
import com.petrosoft.smena.utils.PrefsHelper;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.os.Environment.DIRECTORY_PICTURES;

public class RoutePointActivity extends AppCompatActivity {
    private static final int TYPE_PHOTO = 44;
    public static final String ID = "id";
    public static final String NAME = "name";
    private static final int REQUEST_IMAGE_CAPTURE = 6889;
    private long mRouteId;
    private Route mRoute;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.name_tv)
    TextView mNameTv;

    @BindView(R.id.lat_tv)
    TextView mLatTv;
    @BindView(R.id.lon_tv)
    TextView mLonTv;
    private File directory;
    private Uri mSavedPhotoFile;
    private CommonSvalkaResult mCommonResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_route_point);
        ButterKnife.bind(this);
        createDirectory();
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("Проверка точки");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mRouteId = getIntent().getLongExtra(ID, -1L);
        loadData();

    }

    private Uri generateFileUri(int type) {
        File file = null;
        switch (type) {
            case TYPE_PHOTO:
                file = new File(directory.getAbsolutePath() + "/" + "photo_"
                        + System.currentTimeMillis() + ".jpg");
                break;

        }
        Log.d("File generate", "fileName = " + file);
        return Uri.fromFile(file);
    }

    private void createDirectory() {
        directory = GreenApp.getInstance().getExternalFilesDir(DIRECTORY_PICTURES);
        if (!directory.exists()) {
            boolean x = directory.mkdirs();
            Log.v("Folder create result", String.valueOf(x));
        }


    }

    private void dlgAddPhoto() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.confirm_not_found_photo);
        builder.setMessage(R.string.message_svalka_not_found);
        builder.setPositiveButton(R.string.add_photo, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dispatchTakePictureIntent();
            }
        });
        builder.setNegativeButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        builder.create().show();
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        mSavedPhotoFile = generateFileUri(TYPE_PHOTO);
        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, mSavedPhotoFile);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void loadData() {
        DaoSession daoSession = DbHelper.getDaoMaster(this).newSession();
        RouteDao routeDao = daoSession.getRouteDao();
        mRoute = routeDao.load(mRouteId);
        if (mRoute != null) {
            getSupportActionBar().setSubtitle(mRoute.getName());

            mNameTv.setText(mRoute.getName());
            mLatTv.setText(String.valueOf(mRoute.getLatitude()));
            mLonTv.setText(String.valueOf(mRoute.getLongtitude()));
        }
    }

    @OnClick(R.id.no_issues_button)
    public void notFound() {
        final RouteCheckResult routeCheckResult = new RouteCheckResult();
        routeCheckResult.setArrivalDate(new Date());
        routeCheckResult.setPointId(mRouteId);
        routeCheckResult.setArrivalComment(getString(R.string.snf));
        routeCheckResult.setCheckId(GreenApp.getInstance().getSvalkaCheckId());
        //FIXME - create check
        DaoSession daoSession = DbHelper.getDaoMaster(this).newSession();
        final RouteDao routeDao = daoSession.getRouteDao();

        GreenApp.getInstance().getSvalkaApi().notFoundResults(routeCheckResult).enqueue(new Callback<CommonSvalkaResult>() {
            @Override
            public void onResponse(Call<CommonSvalkaResult> call, Response<CommonSvalkaResult> response) {
                if (response.isSuccessful()) {
                    mRoute.setIs_completed(true);
                    routeDao.update(mRoute);
                    mCommonResult = response.body();
                    Toast.makeText(RoutePointActivity.this, "Данные переданы на сервер", Toast.LENGTH_LONG).show();
                    dlgAddPhoto();

                } else {
                    try {
                        Toast.makeText(RoutePointActivity.this, "Ошибка отправки отчета на сервер : " + response.errorBody().string(), Toast.LENGTH_LONG).show();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<CommonSvalkaResult> call, Throwable t) {
                Toast.makeText(RoutePointActivity.this, t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            if (mSavedPhotoFile != null) {
                DaoSession daoSession = DbHelper.getDaoMaster(this).newSession();
                PendingImageDao pendingImageDao = daoSession.getPendingImageDao();
                //  mRoute = routeDao.load(mRouteId);
                PendingImage pendingImage = new PendingImage();
                pendingImage.setIs_completed(false);
                pendingImage.setToken(PrefsHelper.loadLastLoginData(RoutePointActivity.this).mToken);
                pendingImage.setImage_path(mSavedPhotoFile.getPath());
                pendingImage.setEntity_id(mCommonResult.getId());
                pendingImage.setImage_code(mCommonResult.getImageCode());
                pendingImageDao.insert(pendingImage);
               // pendingImage.setPoint_id(commonSvalkaResult.getPoint_id());

                Intent si = new Intent(RoutePointActivity.this, SentPendingDataService.class);
                // si.setAction(SendReportService.ACTION_MULTI_SEND);
                startService(si);
                dlgAddPhoto();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @OnClick(R.id.new_svalka_button)
    public void onSvalkaFound() {
        Intent intent = new Intent(this, AddEditSvalkaActivity.class);
        //intent.putExtra(AddEditSvalkaActivity.ID,-1);
        intent.putExtra(AddEditSvalkaActivity.ROUTE_ID, mRouteId);

        startActivity(intent);
    }
}
