package com.petrosoft.smena.activity.svalka;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.petrosoft.smena.GreenApp;
import com.petrosoft.smena.R;
import com.petrosoft.smena.activity.ArcGisMapActivity;
import com.petrosoft.smena.adapters.ImageAdapter;
import com.petrosoft.smena.adapters.ImageRemover;
import com.petrosoft.smena.adapters.SimpleCheckedItemAdapter;
import com.petrosoft.smena.adapters.WasteTypesAdapter;
import com.petrosoft.smena.dao.DaoSession;
import com.petrosoft.smena.dao.PendingImage;
import com.petrosoft.smena.dao.PendingImageDao;
import com.petrosoft.smena.dao.Route;
import com.petrosoft.smena.dao.RouteDao;
import com.petrosoft.smena.db.DbHelper;
import com.petrosoft.smena.fragment.map.LocationData;
import com.petrosoft.smena.server.response.data.SimpleItem;
import com.petrosoft.smena.server.response.data.login.LoginData;
import com.petrosoft.smena.server.response.data.svalka.AddSvalkaBody;
import com.petrosoft.smena.server.response.data.svalka.CommonSvalkaResult;
import com.petrosoft.smena.server.response.data.svalka.Svalka;
import com.petrosoft.smena.server.response.data.svalka.WasteClass;
import com.petrosoft.smena.server.response.data.svalka.WasteType;
import com.petrosoft.smena.service.SentPendingDataService;
import com.petrosoft.smena.utils.LocationHelper;
import com.petrosoft.smena.utils.PrefsHelper;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.os.Environment.DIRECTORY_PICTURES;


public class AddEditSvalkaActivity extends AppCompatActivity implements ImageRemover {
    private static final int REQUEST_IMAGE_CAPTURE = 19;
    private static final int TYPE_PHOTO = 44;
    public static final String ID = "svalka_id";
    public static final String ROUTE_ID = "route_id";
    public static final String ROUTE = "route";
    private static final int PERMISSION_READ_CAMERA = 6889;
    private String mAddress;

    @BindView(R.id.fab_save)
    FloatingActionButton mSaveButton;

    @BindView(R.id.button_show_on_map)
    Button mShowOnMap;

    @BindView(R.id.itemDescription)
    TextView mName;

    @BindView(R.id.capacity)
    EditText mCapacity;

    @BindView(R.id.length)
    EditText mWidth;

    @BindView(R.id.width)
    EditText mLength;

    @BindView(R.id.waste_classes)
    RecyclerView mWasteClassesRecycler;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;


    @BindView(R.id.waste_types_recycler)
    RecyclerView mWasteTypesRecycler;

    @BindView(R.id.photos_recycler)
    RecyclerView mPhotosRecycler;

    @BindView(R.id.tv_add_photo)
    TextView mAddPhotoButton;

    ArrayList<String> mWasteClassesList;
    ArrayList<WasteClass> mWastes;
    private Long mSvalkaId;
    private Long mRouteId;

    private Svalka mSsd;
    private WasteTypesAdapter mWasteTypesAdapter;
    private SimpleCheckedItemAdapter mWasteClassesAdapter;
    private List<Uri> mPhotosList;
    private List<SimpleItem> mAllWasteClasses;
    private Route mRoute;
    private File directory;
    private Uri mSavedPhotoFile;
    private List<Uri> mPhotos;
    private ImageAdapter mImageAdapter;
    private Svalka mSvalka;


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    protected void showInputDialog() {


        LayoutInflater layoutInflater = LayoutInflater.from(AddEditSvalkaActivity.this);
        View promptView = layoutInflater.inflate(R.layout.input_dialog, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(AddEditSvalkaActivity.this);
        alertDialogBuilder.setView(promptView);

        final EditText editText = (EditText) promptView.findViewById(R.id.address_text);
        // setup a dialog window
        alertDialogBuilder.setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        setAddress(editText.getText().toString());
                        if (mSvalka == null) {

                        }
                    }
                })
                .setNegativeButton("Отмена",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

        // create an alert dialog
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

    public void setAddress(final String address) {

        if (address == null) {
            GreenApp.ownSnackbar(mName, "Не удается определить адрес").setAction("Ввести вручную", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showInputDialog();
                }
            }).show();
        } else {
            if (mSvalka == null) {
                mName.setText(mAddress);
            }
            mAddress = address;
        }

    }

    private void getWasteClasses() {


        GreenApp.getInstance().getSvalkaApi().getWasteClasses().enqueue(new Callback<List<WasteClass>>() {
            @Override
            public void onResponse(Call<List<WasteClass>> call, Response<List<WasteClass>> response) {
                if (response.isSuccessful()) {

                    List<SimpleItem> simpleItemList = new ArrayList<SimpleItem>();
                    for (WasteClass w : response.body()) {
                        simpleItemList.add(w);
                    }

                    mWasteClassesAdapter = new SimpleCheckedItemAdapter();
                    mWasteClassesRecycler.setAdapter(mWasteClassesAdapter);
                    mWasteClassesAdapter.setItems(simpleItemList);
                    mWasteClassesAdapter.notifyDataSetChanged();
                    if (mSvalkaId != null) {
                        loadPreviousSvalkaData();
                    } else {

                    }


                }
            }


            @Override
            public void onFailure(Call<List<WasteClass>> call, Throwable t) {
                //   mRefreshLayout.setRefreshing(false);

                GreenApp.ownSnackbar(mName, "Ошибка полученния данных").show();
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_IMAGE_CAPTURE:
                if (grantResults.length > 0) {

                } else {
                    // Toast.makeText()
                }
                break;
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_svalka);

        ButterKnife.bind(this);
        setSupportActionBar(mToolbar);
        mImageAdapter = new ImageAdapter(this);
        LinearLayoutManager imlm = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        mPhotosRecycler.setLayoutManager(imlm);
        mPhotosRecycler.setAdapter(mImageAdapter);
        mPhotos = new ArrayList<>();

        createDirectory();
        mWasteClassesList = new ArrayList<>();
        mWastes = new ArrayList<>();
        mWasteTypesAdapter = new WasteTypesAdapter();
        mWasteTypesAdapter.setHasStableIds(true);

        //  mWasteTypesAdapter.setHasStableIds(true);
        //  mWasteTypesRecycler.setAdapter(mWasteTypesAdapter);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        mWasteTypesRecycler.setLayoutManager(linearLayoutManager);


        LinearLayoutManager linearLayoutManager2 = new LinearLayoutManager(this);
        mWasteClassesRecycler.setLayoutManager(linearLayoutManager2);

        mSvalkaId = getIntent().getLongExtra(AddEditSvalkaActivity.ID, -1);
        mRouteId = getIntent().getLongExtra(ROUTE_ID, -1L);
        final DaoSession daoSession = DbHelper.getDaoMaster(this).newSession();
        final RouteDao routeDao = daoSession.getRouteDao();
        mRoute = routeDao.load(mRouteId);
        if (mRoute != null) {
            LocationData.latitude = mRoute.getLatitude();
            LocationData.longitude = mRoute.getLongtitude();
        }
        mAddPhotoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dispatchTakePictureIntent();
                return;
//                if (ContextCompat.checkSelfPermission(AddEditSvalkaActivity.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
//                        && ContextCompat.checkSelfPermission(AddEditSvalkaActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
//
//                }
//                ActivityCompat.requestPermissions(AddEditSvalkaActivity.this, new String[]{Manifest.permission.CAMERA}, PERMISSION_READ_CAMERA);

            }
        });
        if (mRoute != null) {
            mName.setText(mRoute.getName());
        }


        if (mSvalkaId == -1) {
            mSvalkaId = null;
            getSupportActionBar().setTitle(getString(R.string.add_svalka));
        } else {
            getSupportActionBar().setTitle(getString(R.string.edit_svalka));
        }
        getWasteClasses();
        getWasteTypes();
        mSaveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                long svalkaCheckId = GreenApp.getInstance().getSvalkaCheckId();
                if (svalkaCheckId != -1) {
                    AddSvalkaBody addSvalkaBody = new AddSvalkaBody();
                    addSvalkaBody.setId(mSvalkaId);
                    if (mSvalka == null) {
                        addSvalkaBody.setName(mName.getText().toString());
                    } else {
                        addSvalkaBody.setName(mSvalka.getName());
                    }
                    addSvalkaBody.setCustomAddress(mAddress);
                    addSvalkaBody.setWidth(Integer.parseInt(mWidth.getText().toString()));
                    addSvalkaBody.setLength(Integer.parseInt(mLength.getText().toString()));
                    try {
                        addSvalkaBody.setCapacity(Float.parseFloat(mCapacity.getText().toString()));
                    } catch (Exception e) {
                        addSvalkaBody.setCapacity(null);
                    }


                    addSvalkaBody.setLatitude(LocationData.latitude);
                    addSvalkaBody.setLongitude(LocationData.longitude);
                    addSvalkaBody.setWasteIDS(mWasteClassesAdapter.getChekedItemIds());
                    addSvalkaBody.setCheckId(svalkaCheckId);
                    addSvalkaBody.setWasteTypeComplexList(mWasteTypesAdapter.getComplexWasteTypes());
                    addSvalkaBody.setRegistered_at(new Date());
                    if (mRouteId != -1) {
                        addSvalkaBody.setPointId(mRouteId);
                    } else {
                        addSvalkaBody.setPointId(null);
                    }
                    GreenApp.getInstance().getSvalkaApi().addSvalka(addSvalkaBody).enqueue(new Callback<CommonSvalkaResult>() {
                        @Override
                        public void onResponse(@NonNull Call<CommonSvalkaResult> call, Response<CommonSvalkaResult> response) {

                            if (response.isSuccessful()) {
                                if (mSvalkaId == null) {
                                    Toast.makeText(AddEditSvalkaActivity.this, "Свалка создана", Toast.LENGTH_LONG).show();
                                } else {
                                    Toast.makeText(AddEditSvalkaActivity.this, "Параметры свалки обновлены", Toast.LENGTH_LONG).show();
                                }
                                //TODO
                                CommonSvalkaResult sr = response.body();
                                //add images
                                PendingImageDao pendingImageDao = daoSession.getPendingImageDao();
                                for (Uri u : mPhotos) {
                                    PendingImage pi = new PendingImage();
                                    pi.setPoint_id(sr.getPoint_id());
                                    pi.setEntity_id(sr.getId());
                                    pi.setImage_code(sr.getImageCode());
                                    pi.setImage_path(u.getPath());
                                    pi.setIs_completed(false);
                                    LoginData loginData = PrefsHelper.loadLastLoginData(AddEditSvalkaActivity.this);
                                    pi.setToken(loginData.mToken);
                                    pendingImageDao.insert(pi);
                                }
                                if (mRoute != null) {
                                    mRoute.setIs_completed(true);
                                    routeDao.update(mRoute);
                                }
                                Intent piIntent = new Intent(AddEditSvalkaActivity.this, SentPendingDataService.class);
                                startService(piIntent);
                                finish();

                            } else {
                                Toast.makeText(AddEditSvalkaActivity.this, "Ошибка обновления параметров : "+response.message(), Toast.LENGTH_LONG).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<CommonSvalkaResult> call, Throwable t) {
                            Toast.makeText(AddEditSvalkaActivity.this, "Ошибка обновления параметров", Toast.LENGTH_LONG).show();
                        }
                    });
                }

            }
        });
        mShowOnMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    GreenApp.ownSnackbar(findViewById(R.id.main_layout), "Геолокация запрещена для приложения").show();
                }
                Location location = LocationHelper.getLastKnownLocation(getApplicationContext());

                if (location == null) {
                    GreenApp.ownSnackbar(findViewById(R.id.main_layout), "GPS сигнал не найден").show();

                } else {

                    Intent intent = new Intent(getApplicationContext(), ArcGisMapActivity.class);
                    intent.setAction(ArcGisMapActivity.SHOW_SVALKA);
                    if (mSvalkaId == null) {
                        if (mRoute != null) {
                            intent.putExtra(ArcGisMapActivity.LATITUDE, mRoute.getLatitude());
                            intent.putExtra(ArcGisMapActivity.LONGITUDE, mRoute.getLongtitude());
                        } else {
                            intent.putExtra(ArcGisMapActivity.LATITUDE, location.getLatitude());
                            intent.putExtra(ArcGisMapActivity.LONGITUDE, location.getLongitude());
                        }

                    } else {
                        intent.putExtra(ArcGisMapActivity.LATITUDE, mSsd.latitude);
                        intent.putExtra(ArcGisMapActivity.LONGITUDE, mSsd.longitude);
                    }
                    startActivityForResult(intent, 9889);

                }


            }
        });

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
    }


    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        mSavedPhotoFile = generateFileUri(TYPE_PHOTO);
        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, mSavedPhotoFile);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    private void getWasteTypes() {
        GreenApp.getInstance().getSvalkaApi().getWasteTypes().enqueue(new Callback<List<WasteType>>() {
            @Override
            public void onResponse(Call<List<WasteType>> call, Response<List<WasteType>> response) {
                if (response.isSuccessful()) {
                    List<WasteType> items = response.body();
                    // mWasteTypesAdapter = new WasteTypesAdapter();
                    mWasteTypesAdapter.setItems(items);
                    mWasteTypesAdapter.notifyDataSetChanged();
                    mWasteTypesRecycler.setAdapter(mWasteTypesAdapter);


                }
            }

            @Override
            public void onFailure(Call<List<WasteType>> call, Throwable t) {

            }
        });

    }

    private void loadPreviousSvalkaData() {
        GreenApp.getInstance().getSvalkaApi().loadSvalka(mSvalkaId).enqueue(new Callback<Svalka>() {
            @Override
            public void onResponse(Call<Svalka> call, Response<Svalka> response) {

                if (response.isSuccessful()) {
                    mSvalka = response.body();
                    Log.v("Svalka loaded", mSvalka.getName());
                    //fill views
                    mName.setText(mSvalka.getName());
                    mAddress = mSvalka.getCustom_address();
                    mWidth.setText(String.valueOf(mSvalka.width));
                    mLength.setText(String.valueOf(mSvalka.length));
                    mCapacity.setText(mSvalka.capacity);
                    mSsd = mSvalka;
                    LocationData.latitude = mSvalka.latitude;
                    LocationData.longitude = mSvalka.longitude;
                    mWasteClassesAdapter.setCheckedItems(mSvalka.wasteClasses);
                    mWasteClassesAdapter.notifyDataSetChanged();


                    mWasteTypesAdapter.setCheckedItems(mSvalka.wasteTypes);

                    mWasteTypesAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<Svalka> call, Throwable t) {
                GreenApp.ownSnackbar(mName, "Ошибка получения данных").show();
            }
        });
    }

    public void notifyFileRemoved(final int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Удаление фотографии");
        builder.setMessage("Подтвердите удаление фото");
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                try {
                    mPhotos.remove(position);
                    mImageAdapter.deleteImage(position);
                    mImageAdapter.notifyDataSetChanged();
                } catch (Exception ignored) {

                }

            }
        });
        builder.setNegativeButton(R.string.cancel, null);
        builder.create().show();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 9889 && resultCode == RESULT_OK) {
            LocationData.latitude = (float) data.getDoubleExtra("latitude", 0);
            LocationData.longitude = (float) data.getDoubleExtra("longitude", 0);
            mAddress = data.getStringExtra("address");

            setAddress(mAddress);
        }
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            if (mSavedPhotoFile != null) {
                mImageAdapter.addImage(mSavedPhotoFile);
                mImageAdapter.notifyDataSetChanged();
                mPhotos.add(mSavedPhotoFile);

            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    private Uri generateFileUri(int type) {
        File file = null;
        switch (type) {
            case TYPE_PHOTO:
                file = new File(directory.getAbsolutePath() + "/" + "photo_"
                        + System.currentTimeMillis() + ".jpg");
                break;

        }
        Log.d("File generate", "fileName = " + file);
        return Uri.fromFile(file);
    }

    private void createDirectory() {
        directory = GreenApp.getInstance().getExternalFilesDir(DIRECTORY_PICTURES);
        if (!directory.exists()) {
            boolean x = directory.mkdirs();
            Log.v("Folder create result", String.valueOf(x));
        }


    }
}
