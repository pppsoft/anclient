package com.petrosoft.smena.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v7.widget.AppCompatImageView;
import android.view.View;
import android.widget.TextView;

import com.petrosoft.smena.GreenApp;
import com.petrosoft.smena.R;
import com.petrosoft.smena.dao.CObject;
import com.petrosoft.smena.dao.CheckTable;
import com.petrosoft.smena.dao.Claim;
import com.petrosoft.smena.dao.Issue;
import com.petrosoft.smena.dao.ObjectType;
import com.petrosoft.smena.dao.Task;
import com.petrosoft.smena.db.DbHelper;
import com.petrosoft.smena.fragment.add_error.AddErrorNewFragment;
import com.petrosoft.smena.fragment.checks.ChecksFragment;
import com.petrosoft.smena.fragment.claim.ClaimFragment;
import com.petrosoft.smena.fragment.claims.BaseCobjectCurrentFragment;
import com.petrosoft.smena.fragment.claims.ClaimsFragment;
import com.petrosoft.smena.fragment.cobject.CObjectFragment;
import com.petrosoft.smena.fragment.current_object.CurrentObjectFragment;
import com.petrosoft.smena.fragment.issue.IssueFragment;
import com.petrosoft.smena.fragment.issues.IssuesFragment;
import com.petrosoft.smena.fragment.plan.CheckFragment;
import com.petrosoft.smena.fragment.plan.PlanFragment;
import com.petrosoft.smena.fragment.state.issues.StateFragment;
import com.petrosoft.smena.fragment.task.TaskFragment;
import com.petrosoft.smena.fragment.tasks.TasksFragment;
import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnMenuTabClickListener;

import java.util.ArrayList;
import java.util.List;


public class ObjectTypeActivity extends BaseFragmentActivity {

    public static final String EXTRA_OBJECT_TYPE_SERVER_ID = "object_type_server_id";

    AppCompatImageView mBack;
    private BottomBar mBottomBar;
    private long mObjectTypeServerId;
    TextView mHeader;

    boolean mClosable = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_object_type);

        mObjectTypeServerId = getIntent().getLongExtra(EXTRA_OBJECT_TYPE_SERVER_ID, 0);

        mBack = (AppCompatImageView) findViewById(R.id.back);
        mBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed(true);
            }
        });

        mBottomBar = (BottomBar) findViewById(R.id.bottom_bar);
        mBottomBar.useFixedMode();
        mBottomBar.setItems(R.menu.bottombar_menu);
        mBottomBar.setOnMenuTabClickListener(new OnMenuTabClickListener() {
            @Override
            public void onMenuTabSelected(@IdRes int menuItemId) {
                openFragmentByMenu(menuItemId);
            }

            @Override
            public void onMenuTabReSelected(@IdRes int menuItemId) {
            }
        });


        mHeader = (TextView) findViewById(R.id.header);

        CObject checkObject = DbHelper.getCheckCObject(this);
        if (checkObject != null) {
            openCurrentObjectFragment();
        }
    }

    private void openFragmentByMenu(@IdRes int menuItemId) {
        switch (menuItemId) {
            case R.id.menu_plan:
                openPlanFragment();
                break;
            case R.id.menu_current:
                openCurrentObjectFragment();
                break;
            case R.id.menu_check:
                openChecksFragmentFragment();
                break;
        }
    }

    public void setHeader(String title) {
        mHeader.setText(title);
    }

    public void openCheckFragment(CheckTable checkTable) {
        mCurFragment = new CheckFragment();
        Bundle bundle = new Bundle();
        bundle.putLong(CheckFragment.EXTRA_CHECK_ID, checkTable.getId());
        mCurFragment.setArguments(bundle);
        addFragment(mCurFragment);
    }

    public void openCObjectFragment(CObject cObject, boolean add) {
        mCurFragment = new CObjectFragment();
        Bundle bundle = new Bundle();
        bundle.putLong(CObjectFragment.EXTRA_COBJECT_ID, cObject.getId());
        bundle.putBoolean(CObjectFragment.EXTRA_ADD, add);
        mCurFragment.setArguments(bundle);
        addFragment(mCurFragment);
    }

    public void openCurrentObjectFragment() {
        mCurFragment = new CurrentObjectFragment();
        addFragmentRoot(mCurFragment);
        mBottomBar.selectTabAtPosition(1, false);
    }

    public void openPlanFragment() {
        mCurFragment = new PlanFragment();
        Bundle bundle = new Bundle();
        bundle.putLong(EXTRA_OBJECT_TYPE_SERVER_ID, mObjectTypeServerId);
        mCurFragment.setArguments(bundle);
        addFragmentRoot(mCurFragment);
    }

    public void openClaimsFragment(CObject cObject) {
        mCurFragment = new ClaimsFragment();
        Bundle bundle = new Bundle();
        bundle.putLong(BaseCobjectCurrentFragment.EXTRA_OBJECT_TYPE_ID, cObject.getId());
        mCurFragment.setArguments(bundle);
        addFragment(mCurFragment);
    }

    public void openTasksFragment(CObject cObject) {
        mCurFragment = new TasksFragment();
        Bundle bundle = new Bundle();
        bundle.putLong(BaseCobjectCurrentFragment.EXTRA_OBJECT_TYPE_ID, cObject.getId());
        mCurFragment.setArguments(bundle);
        addFragment(mCurFragment);
    }

    public void openAddErrorFragment(boolean isError, String type) {
//        mCurFragment = new AddErrorFragment();
//        Bundle bundle = new Bundle();
//        bundle.putBoolean(AddErrorFragment.EXTRA_IS_ERROR, isError);
//        bundle.putString(AddErrorFragment.EXTRA_TYPE, type);

        final CObject currentObject = DbHelper.getCheckCObject(this);
        mCurFragment = new AddErrorNewFragment();
        Bundle bundle = new Bundle();
        bundle.putBoolean(AddErrorNewFragment.EXTRA_IS_ERROR, isError);
        bundle.putString(AddErrorNewFragment.EXTRA_TYPE, type);
        bundle.putLong(AddErrorNewFragment.OBJECT, currentObject.getId());

        mCurFragment.setArguments(bundle);
        addFragment(mCurFragment);
    }

    public void openIssuesFragment(CObject cObject) {
        mCurFragment = new IssuesFragment();
        Bundle bundle = new Bundle();
        bundle.putLong(BaseCobjectCurrentFragment.EXTRA_OBJECT_TYPE_ID, cObject.getId());
        mCurFragment.setArguments(bundle);
        addFragment(mCurFragment);
    }

    public void openStateFragment(CObject cObject, String type) {
        mCurFragment = new StateFragment();
        Bundle bundle = new Bundle();
        bundle.putLong(BaseCobjectCurrentFragment.EXTRA_OBJECT_TYPE_ID, cObject.getId());
        bundle.putString(StateFragment.EXTRA_TYPE, type);
        mCurFragment.setArguments(bundle);
        addFragment(mCurFragment);
    }

    public void openClaimFragment(Claim claim) {
        mCurFragment = new ClaimFragment();
        Bundle bundle = new Bundle();
        bundle.putLong(ClaimFragment.EXTRA_ID, claim.getId());
        mCurFragment.setArguments(bundle);
        addFragment(mCurFragment);
    }

    public void openIssueFragment(Issue issue) {
        mCurFragment = new IssueFragment();
        Bundle bundle = new Bundle();
        bundle.putLong(IssueFragment.EXTRA_ID, issue.getId());
        mCurFragment.setArguments(bundle);
        addFragment(mCurFragment);
    }

    public void openTaskFragment(Task task) {
        mCurFragment = new TaskFragment();
        Bundle bundle = new Bundle();
        bundle.putLong(TaskFragment.EXTRA_ID, task.getId());
        mCurFragment.setArguments(bundle);
        addFragment(mCurFragment);
    }

    public void openObjectTypeMapFragment(ObjectType objectType) {


//        if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//
//            //    ActivityCompat#requestPermissions
//            // here to request the missing permissions, and then overriding
//            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
//            //                                          int[] grantResults)
//            // to handle the case where the user grants the permission. See the documentation
//            // for ActivityCompat#requestPermissions for more details.
//            GreenApp.ownSnackbar(mBottomBar, "Геолокация запрещена для приложения").show();
//        }
//        Location location = LocationHelper.getLastKnownLocation(getApplicationContext());

        if (!GreenApp.checkNetwork()) {
            GreenApp.ownSnackbar(mBottomBar, "Отсутвует соединение с сетью").show();
            return;
        }


        List<CObject> cObjects = objectType.getCObjects();
        List<Integer> al = new ArrayList<>();
        List<Integer> ol = new ArrayList<>();
        for (CObject cObject : cObjects) {
            if (cObject.getOid() != null) {
                al.add(cObject.getOid());
            }
            ol.add((int) cObject.getId().longValue());
        }

        if (al.isEmpty()) {
            GreenApp.ownSnackbar(mBottomBar, "Отстутствует информация об OID выбранных объектов").show();
            return;
        }
        Intent intent = new Intent(getBaseContext(), ArcGisMapActivity.class);
        intent.setAction(ArcGisMapActivity.ACTION_SELECT_OBJECTS);
        intent.putExtra("layer", objectType.getGisLayerUrl());
        intent.putIntegerArrayListExtra("ids", (ArrayList<Integer>) ol);
        intent.putIntegerArrayListExtra("oids", (ArrayList<Integer>) al);
        startActivity(intent);


    }

    public void openChecksFragmentFragment() {
        mCurFragment = new ChecksFragment();
        addFragmentRoot(mCurFragment);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data != null) {
            boolean is_added = data.getBooleanExtra("added", false);
            if (is_added) {
                PlanFragment lf = (PlanFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_container);
                if (lf != null) {

                    lf.updateFromDb();
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void setClosable(boolean closable) {
        mClosable = closable;
        updateUI();
    }

    @Override
    public void onBackPressed(boolean fromMenu) {
        if (mClosable) {
            super.onBackPressed(fromMenu);
        }
    }

    public void updateUI() {
        if (mClosable) {
            mBack.setVisibility(View.VISIBLE);
            //mBack.setColorFilter(ContextCompat.getColor(this, R.color.transparent));
        } else {
            mBack.setVisibility(View.INVISIBLE);
            //  mBack.setColorFilter(ContextCompat.getColor(this, R.color.gray));
        }
    }
}
