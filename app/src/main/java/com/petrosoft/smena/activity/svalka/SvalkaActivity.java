package com.petrosoft.smena.activity.svalka;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.petrosoft.smena.GreenApp;
import com.petrosoft.smena.R;
import com.petrosoft.smena.activity.ArcGisMapActivity;
import com.petrosoft.smena.adapters.BEHAVIOUR;
import com.petrosoft.smena.adapters.SimpleItemAdapter;
import com.petrosoft.smena.dao.CObject;
import com.petrosoft.smena.dao.DaoSession;
import com.petrosoft.smena.dao.ObjectType;
import com.petrosoft.smena.dao.ObjectTypeDao;
import com.petrosoft.smena.dao.Route;
import com.petrosoft.smena.dao.RouteDao;
import com.petrosoft.smena.db.DbHelper;
import com.petrosoft.smena.server.response.data.SimpleItem;
import com.petrosoft.smena.server.response.data.check.CheckData;
import com.petrosoft.smena.server.response.data.svalka.Svalka;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SvalkaActivity extends AppCompatActivity {


    public static final String ROUTE = "route";
    private static final int REQ_SELECT_POINT = 1345;
    public static final String ENTITY_ID = "entity_id";
    public static final String IS_SVALKA = "is_svalka";
    public static final String ENTITY_NAME = "entity_name";
    @BindView(R.id.svalkas_list)
    RecyclerView mSvalkasView;

    @BindView(R.id.routes_list)
    RecyclerView mRoutesView;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    private SimpleItemAdapter mSimpleSvalkaAdapter;
    private SimpleItemAdapter mSimpleRouteAdapter;

    @BindView(R.id.show_on_map_tv)
    TextView mShowOnMapButton;

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    protected void onResume() {
        loadData();
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        tryCloseSvalkaCheck();
        super.onBackPressed();
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.object_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //close check
                tryCloseSvalkaCheck();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void tryCloseSvalkaCheck() {
        long svalkaCheckId = GreenApp.getInstance().getSvalkaCheckId();
        if (svalkaCheckId != -1) {
            Toast.makeText(SvalkaActivity.this, "Закрываем ранее созданную проверку по свалке", Toast.LENGTH_SHORT).show();
            GreenApp.getInstance().getApi().closeCheck(svalkaCheckId).enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    Toast.makeText(SvalkaActivity.this, "Проверка успешно закрыта", Toast.LENGTH_LONG).show();
                    GreenApp.getInstance().setSvalkaCheckId(-1L);
                    finish();
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    Toast.makeText(SvalkaActivity.this, "Не удалось закрвть проверку: " + t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        } else {
            Toast.makeText(SvalkaActivity.this, "Проверка  закрыта ранее или не была создана", Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    private void tryOpenSvalkaCheck() {
        long svalkaCheckId = GreenApp.getInstance().getSvalkaCheckId();
        if (svalkaCheckId == -1) {
            SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
            final String dateString = dt.format(new Date());
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("start", dateString);
                jsonObject.put("object_type_id", 9999);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), jsonObject.toString());
            GreenApp.getInstance().getApi().addCheck(body).enqueue(new Callback<CheckData>() {
                @Override
                public void onResponse(Call<CheckData> call, Response<CheckData> response) {
                    if (response.isSuccessful()) {
                        GreenApp.getInstance().setSvalkaCheckId(response.body().id);
                        Toast.makeText(SvalkaActivity.this, "Создана проверка по свалке", Toast.LENGTH_LONG).show();

                    }
                }

                @Override
                public void onFailure(Call<CheckData> call, Throwable t) {

                }
            });
        } else {
            //Toast.makeText(SvalkaActivity.this, "Проверка по свалке уже была ранее создана", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_svalka);
        ButterKnife.bind(this);
        mSimpleSvalkaAdapter = new SimpleItemAdapter();
        mSimpleRouteAdapter = new SimpleItemAdapter();
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.title_activity_svalka);
        getSupportActionBar().setSubtitle(R.string.list_objects);

        LinearLayoutManager llm = new LinearLayoutManager(this);
        mSvalkasView.setLayoutManager(llm);
        mSvalkasView.setAdapter(mSimpleSvalkaAdapter);

        LinearLayoutManager rlm = new LinearLayoutManager(this);
        mRoutesView.setLayoutManager(rlm);
        mRoutesView.setAdapter(mSimpleRouteAdapter);
        //tryOpenSvalkaCheck();
        loadData();
        mShowOnMapButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SvalkaActivity.this, ArcGisMapActivity.class);
                intent.setAction(ArcGisMapActivity.ACTION_SHOW_MULTIPOINT);
                startActivityForResult(intent, REQ_SELECT_POINT);
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQ_SELECT_POINT) {
            if (resultCode == RESULT_OK) {
                long id = data.getLongExtra(ENTITY_ID, -1L);
                boolean is_svalka = data.getBooleanExtra(IS_SVALKA, true);
                String name = data.getStringExtra(ENTITY_NAME);
                if (is_svalka) {
                    Intent intent = new Intent(this, SvalkaRoadmapActivity.class);
                    intent.putExtra("svalka_id", id);
                    intent.putExtra("name", name);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(this, RoutePointActivity.class);
                    intent.putExtra(RoutePointActivity.ID, id);
                    intent.putExtra(RoutePointActivity.NAME, name);
                    startActivity(intent);
                }


            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void loadData() {
        DaoSession daoSession = DbHelper.getDaoMaster(this).newSession();
        ObjectTypeDao objectTypeDao = daoSession.getObjectTypeDao();
        ObjectType ot = objectTypeDao.queryBuilder().where(ObjectTypeDao.Properties.ServerId.eq(9999)).unique();
        List<SimpleItem> svalkas = new ArrayList<>();
        if (ot != null) {

            for (CObject ob : ot.getCObjects()) {
                SimpleItem s = new SimpleItem();
                s.setName(ob.getName());
                s.setId(ob.getId());
                svalkas.add(s);
            }

            mSimpleSvalkaAdapter.setItems(svalkas);
            mSimpleSvalkaAdapter.setBehaviour(BEHAVIOUR.SVALKA);
            mSimpleSvalkaAdapter.notifyDataSetChanged();
            if (svalkas.size() > 0) {
                tryOpenSvalkaCheck();
            }
        }
        RouteDao routeDao = daoSession.getRouteDao();
        List<Route> routes = routeDao.loadAll();
        List<SimpleItem> allroutes = new ArrayList<>();
        for (Route r : routes) {
            if (r.getIs_visible()) {
                SimpleItem item = new SimpleItem();
                item.setId(r.getId());
                item.setName(r.getName());
                item.setIsCompleted(r.getIs_completed());
                allroutes.add(item);
            }
        }
        mSimpleRouteAdapter.setBehaviour(BEHAVIOUR.ROUTE);
        mSimpleRouteAdapter.setItems(allroutes);
        mSimpleRouteAdapter.notifyDataSetChanged();
        //not created because svalkas empty
        if (allroutes.size() > 0 && svalkas.size() == 0) {
            tryOpenSvalkaCheck();
        }

    }
}
