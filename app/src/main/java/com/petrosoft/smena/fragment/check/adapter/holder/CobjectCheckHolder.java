package com.petrosoft.smena.fragment.check.adapter.holder;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.petrosoft.smena.R;
import com.petrosoft.smena.dao.CObject;
import com.petrosoft.smena.dao.Claim;
import com.petrosoft.smena.dao.ClaimDao;
import com.petrosoft.smena.dao.DaoSession;
import com.petrosoft.smena.dao.Issue;
import com.petrosoft.smena.dao.IssueDao;
import com.petrosoft.smena.dao.NewIssue;
import com.petrosoft.smena.dao.NewIssueDao;
import com.petrosoft.smena.dao.Task;
import com.petrosoft.smena.dao.TaskDao;
import com.petrosoft.smena.db.DbHelper;
import com.petrosoft.smena.fragment.plan.element.CObjectElement;
import com.petrosoft.smena.view.recycler.holder.BaseViewHolder;

import java.util.List;

import de.greenrobot.dao.query.QueryBuilder;

/**
 * Created by a.baskakov on 22/08/16.
 */
public class CobjectCheckHolder extends BaseViewHolder<CObjectElement> {
    TextView mTitle;
    TextView mStatus;
    TextView mTasks;
    TextView mIssues;
    TextView mClaims;
    TextView mTotal;

    public CobjectCheckHolder(View itemView) {
        super(itemView);
        mTitle = (TextView) itemView.findViewById(R.id.title);
        mStatus = (TextView) itemView.findViewById(R.id.status);
        mTasks = (TextView) itemView.findViewById(R.id.tasks);
        mIssues = (TextView) itemView.findViewById(R.id.issues);
        mClaims = (TextView) itemView.findViewById(R.id.claims);
        mTotal = (TextView) itemView.findViewById(R.id.total);
    }

    @Override
    public void setElement(CObjectElement element) {
        super.setElement(element);

        Context context = mTitle.getContext();

        CObject cObject = element.getData();
        mTitle.setText(cObject.getName());

        DaoSession daoSession = DbHelper.getDaoMaster(context).newSession();
        NewIssueDao newIssueDao = daoSession.getNewIssueDao();
        ClaimDao claimDao = daoSession.getClaimDao();
        TaskDao taskDao = daoSession.getTaskDao();
        IssueDao issueDao = daoSession.getIssueDao();

        QueryBuilder<Claim> qbClaim = claimDao.queryBuilder();
        qbClaim.where(ClaimDao.Properties.CObjectId.eq(cObject.getId()),
                ClaimDao.Properties.LastCheckId.isNotNull());
        List<Claim> checkedClaimslist = qbClaim.list();


        QueryBuilder<Task> qbTasks = taskDao.queryBuilder();
        List<Task> checkedTasksList = qbTasks.where(
                TaskDao.Properties.CObjectId.eq(cObject.getId()),
                TaskDao.Properties.LastCheckId.isNotNull()).list();

        QueryBuilder<Issue> qbIssues = issueDao.queryBuilder();
        List<Issue> checkeIssuesList = qbIssues.where(
                IssueDao.Properties.CObjectId.eq(cObject.getId()),
                IssueDao.Properties.LastCheckId.isNotNull()).list();

        QueryBuilder<NewIssue> qbNewIssue = newIssueDao.queryBuilder();
        List<NewIssue> newIssueListErrors = qbNewIssue.where(
                NewIssueDao.Properties.Object_id.eq(cObject.getServerId()),
                NewIssueDao.Properties.Is_recommendation.eq(false),
                NewIssueDao.Properties.Type.eq(NewIssue.TYPE_NEW)
        ).list();

        int newIssuesErrors = newIssueListErrors.size();


        QueryBuilder<NewIssue> qbNewIssueFromTask = newIssueDao.queryBuilder();
        List<NewIssue> newIssueListErrorsFromTask = qbNewIssueFromTask.where(
                NewIssueDao.Properties.Object_id.eq(cObject.getServerId()),
                NewIssueDao.Properties.Is_recommendation.eq(false),
              NewIssueDao.Properties.Type.eq(NewIssue.TYPE_FROM_TASK)
        ).list();

        int newIssuesErrorsFromTask = newIssueListErrorsFromTask.size();


        mStatus.setText(String.format(context.getString(R.string.cobject_check_status_text), newIssuesErrors));

        mTasks.setText(String.format(context.getString(R.string.cobject_check_tasks_text), checkedTasksList.size(), cObject.getTasks().size(), newIssuesErrorsFromTask));
        mIssues.setText(String.format(context.getString(R.string.cobject_check_issues_text), checkeIssuesList.size(), cObject.getIssues().size()));
        mClaims.setText(String.format(context.getString(R.string.cobject_check_claims_text), checkedClaimslist.size(), cObject.getClaims().size()));
        mTotal.setText(String.format(context.getString(R.string.cobject_check_total_text), newIssuesErrors + newIssuesErrorsFromTask));
    }
}
