package com.petrosoft.smena.fragment.tasks.adapter.holder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.petrosoft.smena.R;
import com.petrosoft.smena.dao.Task;
import com.petrosoft.smena.fragment.tasks.adapter.element.TaskElement;
import com.petrosoft.smena.view.recycler.holder.BaseViewHolder;

/**
 * Created by a.baskakov on 22/08/16.
 */
public class TaskHolder extends BaseViewHolder<TaskElement> {
    TextView mTitle;
    TextView mDescription;
    View mArrow;
    ImageView mCheck;

    public TaskHolder(View itemView) {
        super(itemView);
        mTitle = (TextView) itemView.findViewById(R.id.title);
        mDescription = (TextView) itemView.findViewById(R.id.description);
        mArrow = itemView.findViewById(R.id.arrow);
        mCheck = (ImageView) itemView.findViewById(R.id.check);
    }

    @Override
    public void setElement(TaskElement element) {
        super.setElement(element);
        Task task = element.getData();
        mTitle.setText(task.getWorkTypeName());
        mDescription.setVisibility(View.GONE);
        mArrow.setVisibility(View.VISIBLE);
        mCheck.setVisibility(View.GONE);
    }
}
