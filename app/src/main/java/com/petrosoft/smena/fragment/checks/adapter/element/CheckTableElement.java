package com.petrosoft.smena.fragment.checks.adapter.element;

import com.petrosoft.smena.dao.CObject;
import com.petrosoft.smena.dao.CheckTable;
import com.petrosoft.smena.view.recycler.element.BaseElement;

/**
 * Created by a.baskakov on 22/08/16.
 */
public class CheckTableElement extends BaseElement<CheckTable> {
    public CheckTableElement(CheckTable data) {
        super(data);
    }
}
