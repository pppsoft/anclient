package com.petrosoft.smena.fragment.issues.adapter.holder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.petrosoft.smena.R;
import com.petrosoft.smena.dao.Issue;
import com.petrosoft.smena.fragment.issues.adapter.element.IssueElement;
import com.petrosoft.smena.view.recycler.holder.BaseViewHolder;

/**
 * Created by a.baskakov on 22/08/16.
 */
public class IssueHolder extends BaseViewHolder<IssueElement> {
    TextView mTitle;
    TextView mDescription;
    View mArrow;
    ImageView mCheck;

    public IssueHolder(View itemView) {
        super(itemView);
        mTitle = (TextView) itemView.findViewById(R.id.title);
        mDescription = (TextView) itemView.findViewById(R.id.description);
        mArrow = itemView.findViewById(R.id.arrow);
        mCheck = (ImageView) itemView.findViewById(R.id.check);
    }

    @Override
    public void setElement(IssueElement element) {
        super.setElement(element);
        Issue issue = element.getData();
        mTitle.setText(issue.getCustomAddress());
        mDescription.setVisibility(View.GONE);
        mArrow.setVisibility(View.VISIBLE);
        mCheck.setVisibility(View.GONE);
    }
}
