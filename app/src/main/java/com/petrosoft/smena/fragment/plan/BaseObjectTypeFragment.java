package com.petrosoft.smena.fragment.plan;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.petrosoft.smena.activity.ObjectTypeActivity;
import com.petrosoft.smena.fragment.BaseFragment;

/**
 * Created by a.baskakov on 25/08/16.
 */
public abstract class BaseObjectTypeFragment extends BaseFragment {

    long mObjectTypeServerId;

    protected void init() {
        Bundle bundle = getArguments();
        if (bundle != null) {
            mObjectTypeServerId = bundle.getLong(ObjectTypeActivity.EXTRA_OBJECT_TYPE_SERVER_ID);
        }
    }

    public void setHeader(String header) {
        Activity activity = getActivity();
        if (activity != null && activity instanceof ObjectTypeActivity) {
            ObjectTypeActivity objectTypeActivity = (ObjectTypeActivity) activity;
            objectTypeActivity.setHeader(header);
        }
    }

    public void setClosable(boolean closable) {
        Activity activity = getActivity();
        if (activity != null && activity instanceof ObjectTypeActivity) {
            ObjectTypeActivity objectTypeActivity = (ObjectTypeActivity) activity;
            objectTypeActivity.setClosable(closable);
        }
    }
}
