package com.petrosoft.smena.fragment.add_error;


import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.petrosoft.smena.GreenApp;
import com.petrosoft.smena.R;
import com.petrosoft.smena.activity.ArcGisMapActivity;
import com.petrosoft.smena.activity.ObjectTypeActivity;
import com.petrosoft.smena.activity.SelectRoadExtensionActivity;
import com.petrosoft.smena.activity.SelectElementActivity;
import com.petrosoft.smena.activity.svalka.AddSvalkaIssuerActivity;
import com.petrosoft.smena.adapters.ImageAdapter;
import com.petrosoft.smena.adapters.ImageRemover;
import com.petrosoft.smena.dao.CObject;
import com.petrosoft.smena.dao.DaoMaster;
import com.petrosoft.smena.dao.DaoSession;
import com.petrosoft.smena.dao.IssueType;
import com.petrosoft.smena.dao.ObjectType;
import com.petrosoft.smena.dao.ObjectTypeDao;
import com.petrosoft.smena.dao.ObjectTypeToIssueType;
import com.petrosoft.smena.dao.PendingImage;
import com.petrosoft.smena.dao.PendingImageDao;
import com.petrosoft.smena.db.DbHelper;
import com.petrosoft.smena.fragment.BaseFragment;
import com.petrosoft.smena.fragment.plan.BaseObjectTypeFragment;
import com.petrosoft.smena.server.response.data.add_issue.AddIssueData;
import com.petrosoft.smena.server.response.data.add_issue.Element;
import com.petrosoft.smena.server.response.data.add_issue.RoadExtension;
import com.petrosoft.smena.service.SentPendingDataService;
import com.petrosoft.smena.utils.DialogHelper;
import com.petrosoft.smena.utils.PrefsHelper;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static android.os.Environment.DIRECTORY_PICTURES;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddErrorNewFragment extends BaseObjectTypeFragment implements ImageRemover {


    private static final int TYPE_PHOTO = 44;
    private static final int REQUEST_IMAGE_CAPTURE = 6466;

    public static final String EXTRA_IS_ERROR = "is_error";
    public static final String EXTRA_TYPE = "type";
    public static final int CODE_MAP = 9889;
    private static final int CODE_ELEMENT = 8899;
    private static final int CODE_ROAD = 9876;

    @BindView(R.id.cloud_spinner)
    Spinner mCloudSpinner;

    @BindView(R.id.temp_spinner)
    Spinner mTempSpinner;


    @BindView(R.id.address)
    EditText mAddress;

    @BindView(R.id.issue_type)
    TextView mIssueTypeButton;

    @BindView(R.id.element_id)
    TextView mElementButton;

    @BindView(R.id.length)
    EditText mLength;

    @BindView(R.id.width)
    EditText mWidth;

    @BindView(R.id.include_to_predpisanie)
    CheckBox mIncludeToPredpisanie;

    @BindView(R.id.photos_list)
    RecyclerView mPhotosRecycler;

    @BindView(R.id.width_layout)
    TextInputLayout mWidthLayout;

    @BindView(R.id.length_layout)
    TextInputLayout mLengthLayout;

    @BindView(R.id.map_button)
    Button mMapButton;

    @BindView(R.id.road_extension_caption)
    TextView mRoadExtensionCaption;

    @BindView(R.id.road_extension)
    TextView mRoadExtensionText;

    @BindView(R.id.add_photo_button)
    TextView mAddPhotoButton;
    @BindView(R.id.save_button)
    Button mSaveButton;

    @BindView(R.id.comment)
    EditText mComment;

    private Uri mSavedPhotoFile;


    private ImageAdapter mImageAdapter;
    private List<Uri> mPhotosList;
    private Long mId;
    private CObject mCurrentObject;
    private Float mLatitude;
    private Float mLongitude;
    private Element mElement;
    private Long mIssueTypeId;
    private Long mElementId;
    boolean mIsError;
    private RoadExtension mRoadExtension;
    private Long mRoadExtensionId;
    private File directory;
    private Date mEliminateDate;


    public AddErrorNewFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment


        View inflate = inflater.inflate(R.layout.fragment_add_error_new, container, false);
        ButterKnife.bind(this, inflate);
        mTempSpinner.setSelection(40);
        createDirectory();
        mPhotosList = new ArrayList<>();
        mId = getArguments().getLong(OBJECT);
        mIsError = getArguments().getBoolean(EXTRA_IS_ERROR);
        DaoSession daoSession = DbHelper.getDaoMaster(getContext()).newSession();
        mCurrentObject = daoSession.getCObjectDao().load(mId);
        mAddress.setText(mCurrentObject.getName());

        // if (currentObject.getObjectType().getOtElements().size() > 0) {
        if (mCurrentObject.getObjectType().getId() == 963) {
            // mElementContainer.setVisibility(View.VISIBLE);
            // mWidthLayout.setVisibility(View.VISIBLE);
            //mLengthLayout.setVisibility(View.VISIBLE);

        } else {
            mRoadExtensionCaption.setVisibility(View.GONE);
            mRoadExtensionText.setVisibility(View.GONE);
            mWidthLayout.setVisibility(View.GONE);
            mLengthLayout.setVisibility(View.GONE);
        }
        mRoadExtensionText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), SelectRoadExtensionActivity.class);
                intent.putExtra("id", mCurrentObject.getServerId());
                startActivityForResult(intent, CODE_ROAD);
            }
        });
        mElementButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), SelectElementActivity.class);
                intent.putExtra("object_id", mCurrentObject.getServerId());
                startActivityForResult(intent, CODE_ELEMENT);
            }
        });
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this.getContext(), LinearLayoutManager.HORIZONTAL, false);
        mPhotosRecycler.setLayoutManager(linearLayoutManager);
        mImageAdapter = new ImageAdapter(this);
        mPhotosRecycler.setAdapter(mImageAdapter);
        mWidth.setText(String.format(Locale.getDefault(), "%.2f", 1.0f));
        mLength.setText(String.format(Locale.getDefault(), "%.2f", 1.0f));
        mAddPhotoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dispatchTakePictureIntent();
            }
        });
        mIncludeToPredpisanie.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    Calendar myCalendar = Calendar.getInstance(Locale.getDefault());
                    new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                            Calendar cal = Calendar.getInstance();
                            cal.set(Calendar.YEAR, year);
                            cal.set(Calendar.MONTH, month);
                            cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                            cal.set(Calendar.HOUR_OF_DAY, 0);
                            cal.set(Calendar.MINUTE, 0);
                            cal.set(Calendar.SECOND, 0);
                            cal.set(Calendar.MILLISECOND, 0);
                            mEliminateDate = cal.getTime();

                        }
                    }, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH) + 1).show();
                } else {
                    mEliminateDate = null;
                }
            }
        });

        mIssueTypeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(
                        getActivity(),
                        R.layout.dialog_choice_element);

                DaoSession daoSession = DbHelper.getDaoMaster(getActivity()).newSession();
                ObjectTypeDao objectTypeDao = daoSession.getObjectTypeDao();
                long objectTypeServerId = DbHelper.getCheckCObject(getActivity()).getObjectTypeId();
                ObjectType objectType = objectTypeDao.queryBuilder().where(ObjectTypeDao.Properties.Id.eq(objectTypeServerId)).unique();
                final List<IssueType> listIssueTypes = new ArrayList<>();
                for (ObjectTypeToIssueType objectTypeToIssueType : objectType.getObjectTypeToIssueType()) {
                    IssueType issueType = objectTypeToIssueType.getIssueType();
                    arrayAdapter.add(issueType.getName());
                    listIssueTypes.add(issueType);
                }

                DialogHelper.showSelectDialog(getActivity(), mIsError ? getActivity().getString(R.string.error_type) : getActivity().getString(R.string.recommendation_type), arrayAdapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        IssueType issueType = listIssueTypes.get(i);
                        mIssueTypeButton.setText(issueType.getName());
                        mIssueTypeId = issueType.getServerId();

                    }
                });
            }
        });
        mMapButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(getContext(), ArcGisMapActivity.class);
                i.setAction(ArcGisMapActivity.ACTION_FORISSUE);
                if (mCurrentObject.getOid() != null) {
                    i.putExtra("layer", mCurrentObject.getObjectType().getGisLayerUrl());
                    i.putExtra("oid", mCurrentObject.getOid());
                }
                startActivityForResult(i, CODE_MAP);

            }
        });
        mSaveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mLatitude == null || mLongitude == null) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                    builder.setTitle(R.string.issue);
                    builder.setMessage(R.string.coordinates_failed);
                    builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            mMapButton.callOnClick();
                        }
                    });
                    builder.setNegativeButton(R.string.cancel, null);
                    builder.create().show();
                    return;
                }

                if (mIssueTypeId == null) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                    builder.setTitle(R.string.issue);
                    builder.setMessage(R.string.issue_type_not_selected);
                    builder.setNegativeButton(R.string.cancel, null);
                    builder.create().show();
                    return;
                }

                long checkId = DbHelper.getCurrentCheck((getActivity())).getServerId();
                Date eventDate = new Date();
                SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
                final String dateString = dt.format(eventDate);


                SimpleDateFormat eliminateDate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
                String eliminateStr = null;
                if (mEliminateDate != null) {
                    eliminateStr = eliminateDate.format(mEliminateDate);
                }
                String weatherStr = mCloudSpinner.getSelectedItem().toString() + ", " + mTempSpinner.getSelectedItem().toString();
                GreenApp.getInstance().getApi().saveIssue(checkId, mAddress.getText().toString(), dateString, mLatitude, mLongitude, mCurrentObject.getServerId(), mIssueTypeId,
                        Float.parseFloat(mLength.getText().toString().replace(",", ".")),
                        Float.parseFloat(mWidth.getText().toString().replace(",", ".")), mElementId, mRoadExtensionId, eliminateStr, !mIsError, mComment.getText().toString(), weatherStr).enqueue(new Callback<AddIssueData>() {
                    @Override
                    public void onResponse(Call<AddIssueData> call, Response<AddIssueData> response) {
                        if (response.isSuccessful()) {
                            AddIssueData addIssueData = response.body();
                            DaoSession daoSession = DbHelper.getDaoMaster(getContext()).newSession();
                            PendingImageDao pendingImageDao = daoSession.getPendingImageDao();

                            for (Uri uri : mPhotosList) {
                                PendingImage pendingImage = new PendingImage();
                                pendingImage.setImage_code("Issue");
                                pendingImage.setToken(PrefsHelper.loadLastLoginData(getContext()).mToken);
                                pendingImage.setEntity_id((int) addIssueData.id);
                                pendingImage.setIs_completed(false);
                                pendingImage.setImage_path(uri.getPath());
                                pendingImageDao.insert(pendingImage);
                            }
                            Intent intent = new Intent(getContext(), SentPendingDataService.class);
                            getActivity().startService(intent);
                            getActivity().onBackPressed();
                        }
                    }

                    @Override
                    public void onFailure(Call<AddIssueData> call, Throwable t) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                        builder.setTitle(R.string.issue);
                        builder.setMessage("Не удалось добавить нарушение. Повторить попытку?");
                        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                getActivity().onBackPressed();
                            }
                        });
                        builder.create().show();
                    }
                });
            }
        });
        setHeader(mIsError ? getString(R.string.new_error) : getString(R.string.new_recommendation));
        if (mIsError) {
            mComment.setVisibility(View.GONE);
        }
        // mRecommendationBlock.setVisibility(mIsError ? View.GONE : View.VISIBLE);
        setClosable(true);
        // return mRootView;
        mWidth.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    if (Float.parseFloat(s.toString()) > mRoadExtension.getWidth()) {
                        mWidth.setError(String.format(Locale.getDefault(), "Превышает допустимое %.2f", mRoadExtension.getWidth()));
                    }
                } catch (Exception ex) {

                }
            }
        });
        mLength.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    if (Float.parseFloat(s.toString()) > mRoadExtension.getLength()) {
                        mLength.setError(String.format(Locale.getDefault(), "Превышает допустимое %.2f", mRoadExtension.getLength()));
                    }
                } catch (Exception ex) {

                }
            }
        });
        return inflate;
    }

    @Override
    public void notifyFileRemoved(final int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Удаление фотографии");
        builder.setMessage("Подтвердите удаление фото");
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                try {
                    mPhotosList.remove(position);
                    mImageAdapter.deleteImage(position);
                    mImageAdapter.notifyDataSetChanged();
                } catch (Exception ignored) {

                }

            }
        });
        builder.setNegativeButton(R.string.cancel, null);
        builder.create().show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (requestCode) {

            case CODE_MAP:
                if (resultCode == RESULT_OK) {
                    mLatitude = ((float) data.getDoubleExtra("latitude", 0));
                    mLongitude = ((float) data.getDoubleExtra("longitude", 0));
                    mAddress.setText(data.getStringExtra("address"));
                }
                break;
            case CODE_ELEMENT:
                if (resultCode == RESULT_OK) {
                    mElement = (Element) data.getSerializableExtra("element");
                    mElementId = mElement.id;
                    mElementButton.setText(mElement.toString());
                }
                break;
            case CODE_ROAD:
                if (resultCode == RESULT_OK) {
                    mRoadExtension = (RoadExtension) data.getSerializableExtra(SelectRoadExtensionActivity.EXTENSION);
                    mRoadExtensionId = mRoadExtension.getId();
                    mWidth.setText(String.format(Locale.getDefault(), "%.2f", mRoadExtension.getWidth()));
                    mLength.setText(String.format(Locale.getDefault(), "%.2f", mRoadExtension.getLength()));
                    mRoadExtensionText.setText(mRoadExtension.getDisplayName());
                }
                break;
            case REQUEST_IMAGE_CAPTURE:
                if (resultCode == RESULT_OK) {
                    if (mSavedPhotoFile != null) {
                        Bitmap b2 = BitmapFactory.decodeFile(mSavedPhotoFile.getPath());
                        Bitmap b = b2.copy(Bitmap.Config.ARGB_8888, true);
                        Canvas c = new Canvas(b);
                        Paint p = new Paint();

                        p.setColor(getResources().getColor(R.color.red));
                        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm", Locale.getDefault());
                        String text = sdf.format(new Date());
                        //p.setTextScaleX(2);
                        p.setTextSize(b2.getHeight() / 25);
                        c.drawText(text, b2.getWidth() / 2, b2.getHeight() - 50, p);
                        FileOutputStream fileOutputStream = null;
                        try {
                            fileOutputStream = new FileOutputStream(mSavedPhotoFile.getPath());
                            b.compress(Bitmap.CompressFormat.JPEG, 60, fileOutputStream);
                        } catch (Exception e) {
                            e.printStackTrace();

                        } finally {
                            if (fileOutputStream != null) {
                                try {
                                    fileOutputStream.close();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                        //WRITE TEXT OSD
                        mImageAdapter.addImage(mSavedPhotoFile);
                        mImageAdapter.notifyDataSetChanged();
                        mPhotosList.add(mSavedPhotoFile);
                    }
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean backPressed() {
        return false;
    }

    private Uri generateFileUri(int type) {
        File file = null;
        switch (type) {
            case TYPE_PHOTO:

                file = new File(directory.getAbsolutePath() + "/" + "photo_"
                        + System.currentTimeMillis() + ".jpg");
                break;

        }
        Log.d("File generate", "fileName = " + file);
        return Uri.fromFile(file);
    }

    private void createDirectory() {

        directory = GreenApp.getInstance().getExternalFilesDir(DIRECTORY_PICTURES);
        if (!directory.exists()) {
            boolean x = directory.mkdirs();
            Log.v("Folder create result", String.valueOf(x));
        }
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        mSavedPhotoFile = generateFileUri(TYPE_PHOTO);
        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, mSavedPhotoFile);

        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }
}
