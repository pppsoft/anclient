package com.petrosoft.smena.fragment.plan.element;

import com.petrosoft.smena.dao.CObject;
import com.petrosoft.smena.view.recycler.element.BaseElement;

/**
 * Created by a.baskakov on 22/08/16.
 */
public class CObjectElement extends BaseElement<CObject> {
    public CObjectElement(CObject data) {
        super(data);
    }
}
