package com.petrosoft.smena.fragment.cobject;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.LevelStartEvent;
import com.crashlytics.android.answers.StartCheckoutEvent;
import com.petrosoft.smena.R;
import com.petrosoft.smena.activity.ObjectTypeActivity;
import com.petrosoft.smena.dao.CObject;
import com.petrosoft.smena.dao.CObjectDao;
import com.petrosoft.smena.dao.CheckTable;
import com.petrosoft.smena.dao.CheckTableDao;
import com.petrosoft.smena.dao.CheckToObject;
import com.petrosoft.smena.dao.CheckToObjectDao;
import com.petrosoft.smena.dao.DaoSession;
import com.petrosoft.smena.db.DbHelper;
import com.petrosoft.smena.fragment.cobject.adapter.CObjectFragmentAdapter;
import com.petrosoft.smena.fragment.cobject.element.NameNumberElement;
import com.petrosoft.smena.fragment.cobject.element.NameValueElement;
import com.petrosoft.smena.fragment.main.recycler.element.StringElement;
import com.petrosoft.smena.fragment.plan.BaseObjectTypeFragment;
import com.petrosoft.smena.server.IRequestListener;
import com.petrosoft.smena.server.RequestHelper;
import com.petrosoft.smena.server.response.data.check.CheckData;
import com.petrosoft.smena.utils.DialogHelper;
import com.petrosoft.smena.utils.recycler_view.ItemClickSupport;
import com.petrosoft.smena.view.recycler.element.BaseElement;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by a.baskakov on 22/08/16.
 */
public class CObjectFragment extends BaseObjectTypeFragment {

    public static final String EXTRA_COBJECT_ID = "cobject_id";
    public static final String EXTRA_ADD = "add";

    long mSObjectId;
    boolean mAdd;
    View mRootView;
    RecyclerView mRecyclerView;
    CObjectFragmentAdapter mAdapter;
    RecyclerView.LayoutManager mLayoutManager;
    Button mButtonStartCheck;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Bundle bundle = getArguments();
        if (bundle != null) {
            mSObjectId = bundle.getLong(EXTRA_COBJECT_ID);
            mAdd = bundle.getBoolean(EXTRA_ADD);
        }
        if (mRootView == null) {
            mRootView = inflater.inflate(R.layout.cobject_fragment, container, false);
            mRecyclerView = (RecyclerView) mRootView.findViewById(R.id.recycler_view);
            mRecyclerView.setHasFixedSize(true);


            ItemClickSupport.addTo(mRecyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {

                @Override
                public void onItemClick(RecyclerView parent, View view, int position, long id) {
                    if (position != -1) {

                    }
                }
            });

            mButtonStartCheck = (Button) mRootView.findViewById(R.id.button_start_check);

            mButtonStartCheck.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    CheckTable check = DbHelper.getCurrentCheck(getActivity());
                    final CObject cObject = getCObject();

                    if (check != null) {
                        DialogHelper.showDialog(getActivity(),
                                getString(R.string.dialog_check_title),
                                getString(R.string.dialog_check_description),
                                getString(R.string.dialog_check_add_in_current),
                                getString(R.string.dialog_check_new),
                                getString(R.string.dialog_check_cancel),
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        DbHelper.finishCurrentCObject(getActivity());
                                        addCheck(cObject, null, null);
                                    }
                                },
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        DbHelper.finishCurrentCheck(getActivity());
                                        addNewCheck(cObject);
                                    }
                                },
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {

                                    }
                                }
                        );
                    } else {
                        addNewCheck(cObject);
                    }
                }
            });

            updateFromDb();

        }
        setHeader(getString(R.string.object));
        setClosable(true);
        return mRootView;
    }

    private void addNewCheck(final CObject cObject) {
        final Date date = new Date();

        final AlertDialog alertDialog = DialogHelper.showProgressDialog(getActivity(), getString(R.string.loading_progress));
        alertDialog.show();

        RequestHelper.addCheckAsync(getActivity(), date, cObject.getObjectTypeId(),
                new IRequestListener<CheckData>() {
                    @Override
                    public void success(CheckData element) {
                        alertDialog.hide();

                        if (element != null) {
                            addCheck(cObject, date, element);
                        } else {
                            addCheck(cObject, date, null);
                        }
                    }

                    @Override
                    public void error() {
                        alertDialog.hide();
                        addCheck(cObject, date, null);
                    }
                });
    }

    private void addCheck(CObject cObject, Date date, CheckData checkData) {

        CheckTable check = DbHelper.getCurrentCheck(getActivity());
        DaoSession daoSession = DbHelper.getDaoMaster(getActivity()).newSession();
        CheckTableDao checkDao = daoSession.getCheckTableDao();
        CheckToObjectDao checkToObjectDao = daoSession.getCheckToObjectDao();

        if (check == null) {
            check = new CheckTable();
            check.setFinished(false);
            check.setSent(false);
            check.setLoadedFromServer(false);
            check.setObject_type_id_request(cObject.getObjectTypeId());
            check.setDate_request(date.getTime());
            if (checkData != null) {
                DbHelper.setDataFromCheckDataToCheckTable(checkData, check);
            }
            Answers.getInstance().logLevelStart(new LevelStartEvent().putLevelName("Проверка : " + cObject.getObjectType().getName()));
            checkDao.insert(check);
        }
        //search cobject in check
        List<CheckToObject> cCheckToObjectList = check.getCheckToCObject();
        boolean cObjectFound = false;
        for (CheckToObject checkToObject : cCheckToObjectList) {
            if (cObject.getId() == (checkToObject.getCObject().getId())) {
                cObjectFound = true;
                break;
            }
        }
        if (!cObjectFound) {
            CheckToObject checkToObject = new CheckToObject();
            checkToObject.setCheckId(check.getId());
            checkToObject.setCObjectId(cObject.getId());
            checkToObjectDao.insert(checkToObject);
        }

        CObjectDao cObjectDao = daoSession.getCObjectDao();
        cObject.setCheck(true);
        cObjectDao.update(cObject);

        ObjectTypeActivity objectTypeActivity = (ObjectTypeActivity) getActivity();
        objectTypeActivity.openCurrentObjectFragment();
    }

    private void updateFromDb() {
        if (!isAdded()) {
            return;
        }
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);


        CObject cObject = getCObject();

        if (cObject.getCheck() || !mAdd) {
            mButtonStartCheck.setVisibility(View.GONE);
        }

        List<BaseElement> list = new ArrayList<>();
        list.add(new StringElement(getString(R.string.info)));
        list.add(new NameValueElement(new NameValueData(getString(R.string.cobject_name), cObject.getName())));
        //list.add(new NameValueElement(new NameValueData(getString(R.string.cobject_region), null)));
        // list.add(new NameValueElement(new NameValueData(getString(R.string.cobject_organization), cObject.getOrganisations())));
        // list.add(new NameValueElement(new NameValueData(getString(R.string.cobject_info), null)));
        list.add(new StringElement(getString(R.string.planned_checks)));

        list.add(new NameNumberElement(new NameNumberData(getString(R.string.current_status), 0, false, false)));
        list.add(new NameNumberElement(new NameNumberData(getString(R.string.tasks), cObject.getTasks().size(), false, true)));
        list.add(new NameNumberElement(new NameNumberData(getString(R.string.issues), cObject.getIssues().size(), false, true)));
        list.add(new NameNumberElement(new NameNumberData(getString(R.string.claims), cObject.getClaims().size(), false, true)));

        Map<Class<? extends BaseElement>, Integer> typeMap = new HashMap<>();
        typeMap.put(StringElement.class, CObjectFragmentAdapter.TYPE_STRING);
        typeMap.put(NameValueElement.class, CObjectFragmentAdapter.TYPE_NAME_VALUE);
        typeMap.put(NameNumberElement.class, CObjectFragmentAdapter.TYPE_NAME_NUMBER);

        mAdapter = new CObjectFragmentAdapter(list, typeMap, getActivity());
        mRecyclerView.setAdapter(mAdapter);
    }

    private CObject getCObject() {
        DaoSession daoSession = DbHelper.getDaoMaster(getActivity()).newSession();
        CObjectDao cObjectDao = daoSession.getCObjectDao();
        CObject cObject = cObjectDao.queryBuilder().where(CObjectDao.Properties.Id.eq(mSObjectId)).unique();
        return cObject;
    }

    @Override
    public boolean backPressed() {
        return false;
    }
}
