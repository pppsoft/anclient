package com.petrosoft.smena.fragment.tasks;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.petrosoft.smena.R;
import com.petrosoft.smena.activity.ObjectTypeActivity;
import com.petrosoft.smena.dao.CheckTableDao;
import com.petrosoft.smena.dao.DaoSession;
import com.petrosoft.smena.dao.Task;
import com.petrosoft.smena.db.DbHelper;
import com.petrosoft.smena.fragment.claims.BaseCobjectCurrentFragment;
import com.petrosoft.smena.fragment.main.recycler.element.StringElement;
import com.petrosoft.smena.fragment.tasks.adapter.TasksFragmentAdapter;
import com.petrosoft.smena.fragment.tasks.adapter.element.TaskElement;
import com.petrosoft.smena.utils.recycler_view.ItemClickSupport;
import com.petrosoft.smena.view.recycler.element.BaseElement;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by a.baskakov on 22/08/16.
 */
public class TasksFragment extends BaseCobjectCurrentFragment {


    View mRootView;
    View mContent;
    TextView mNoContent;
    RecyclerView mRecyclerView;
    TasksFragmentAdapter mAdapter;
    RecyclerView.LayoutManager mLayoutManager;
    Button mButtonFinishCheck;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        init();
        if (mRootView == null) {
            mRootView = inflater.inflate(R.layout.current_object_fragment, container, false);
            mRecyclerView = (RecyclerView) mRootView.findViewById(R.id.recycler_view);
            mRecyclerView.setHasFixedSize(true);
            mContent = mRootView.findViewById(R.id.content);
            mNoContent = (TextView) mRootView.findViewById(R.id.no_content);
            mButtonFinishCheck = (Button) mRootView.findViewById(R.id.button_finish_check);

            mNoContent.setVisibility(View.GONE);
            mButtonFinishCheck.setVisibility(View.GONE);


            ItemClickSupport.addTo(mRecyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {

                @Override
                public void onItemClick(RecyclerView parent, View view, int position, long id) {
                    if (position != -1) {
                        BaseElement baseElement = mAdapter.getElement(position);
                        if (baseElement instanceof TaskElement) {
                            TaskElement taskElement = (TaskElement) baseElement;
                            ((ObjectTypeActivity) getActivity()).openTaskFragment(taskElement.getData());
                        }
                    }
                }
            });


            mButtonFinishCheck.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    DbHelper.finishCurrentCheck(getActivity());
                    ((ObjectTypeActivity) getActivity()).openChecksFragmentFragment();
                }
            });

            updateFromDb();

        }
        setHeader(getString(R.string.tasks));

        setClosable(true);
        return mRootView;
    }

    private void updateFromDb() {
        if (!isAdded()) {
            return;
        }

        DaoSession daoSession = DbHelper.getDaoMaster(getActivity()).newSession();
        CheckTableDao checkTableDao = daoSession.getCheckTableDao();

        List<Task> tasks = mCObject.getTasks();

        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        List<BaseElement> list = new ArrayList<>();
        list.add(new StringElement(mCObject.getName()));
        for (Task task : tasks) {
            list.add(new TaskElement(task));
        }

        Map<Class<? extends BaseElement>, Integer> typeMap = new HashMap<>();
        typeMap.put(StringElement.class, TasksFragmentAdapter.TYPE_STRING);
        typeMap.put(TaskElement.class, TasksFragmentAdapter.TYPE_OBJECT);

        mAdapter = new TasksFragmentAdapter(list, typeMap, getActivity());
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public boolean backPressed() {
        return false;
    }
}
