package com.petrosoft.smena.fragment.images.adapter.holder;

import android.view.View;

import com.petrosoft.smena.fragment.images.adapter.element.AddPhotoElement;
import com.petrosoft.smena.view.recycler.holder.BaseViewHolder;


/**
 * Created by a.baskakov on 22/08/16.
 */
public class AddPhotoHolder extends BaseViewHolder<AddPhotoElement> {

    public AddPhotoHolder(View itemView) {
        super(itemView);
    }

    @Override
    public void setElement(AddPhotoElement element) {
        super.setElement(element);

    }
}
