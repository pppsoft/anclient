package com.petrosoft.smena.fragment.state.issues;

import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.petrosoft.smena.R;
import com.petrosoft.smena.activity.ObjectTypeActivity;
import com.petrosoft.smena.dao.DaoSession;
import com.petrosoft.smena.dao.NewIssue;
import com.petrosoft.smena.db.DbHelper;
import com.petrosoft.smena.fragment.claims.BaseCobjectCurrentFragment;
import com.petrosoft.smena.fragment.main.recycler.element.StringElement;
import com.petrosoft.smena.fragment.state.issues.adapter.StateFragmentAdapter;
import com.petrosoft.smena.utils.LocationHelper;
import com.petrosoft.smena.view.recycler.element.BaseElement;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by a.baskakov on 22/08/16.
 */
public class StateFragment extends BaseCobjectCurrentFragment {
    public static final String EXTRA_TYPE = "type";

    String mType;
    View mRootView;
    View mContent;
    RecyclerView mRecyclerView;
    StateFragmentAdapter mAdapter;
    RecyclerView.LayoutManager mLayoutManager;

    Button mAddError;
    Button mAddRecommendation;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        init();

        Bundle bundle = getArguments();

        if (bundle != null) {
            mType = bundle.getString(EXTRA_TYPE);
        }
        if (mRootView == null) {
            mRootView = inflater.inflate(R.layout.state_fragment, container, false);
            mAddError = (Button) mRootView.findViewById(R.id.add_error);
            mAddRecommendation = (Button) mRootView.findViewById(R.id.add_recommendation);
            mRecyclerView = (RecyclerView) mRootView.findViewById(R.id.recycler_view);
            mRecyclerView.setHasFixedSize(true);
            mContent = mRootView.findViewById(R.id.content);

            mAddError.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((ObjectTypeActivity) getActivity()).openAddErrorFragment(true, mType);
                }
            });

            mAddRecommendation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((ObjectTypeActivity) getActivity()).openAddErrorFragment(false, mType);
                }
            });

            updateFromDb();

        }
        setHeader(getString(R.string.current_status));

        setClosable(true);
        return mRootView;
    }

    private void updateFromDb() {
        if (!isAdded()) {
            return;
        }

        DaoSession daoSession = DbHelper.getDaoMaster(getActivity()).newSession();

        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        List<BaseElement> list = new ArrayList<>();
        list.add(new StringElement(mCObject.getName()));

        Map<Class<? extends BaseElement>, Integer> typeMap = new HashMap<>();
        typeMap.put(StringElement.class, StateFragmentAdapter.TYPE_STRING);

        mAdapter = new StateFragmentAdapter(list, typeMap, getActivity());
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public boolean backPressed() {
        return false;
    }
}
