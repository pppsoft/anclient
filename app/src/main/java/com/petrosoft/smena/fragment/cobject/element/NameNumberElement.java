package com.petrosoft.smena.fragment.cobject.element;

import com.petrosoft.smena.fragment.cobject.NameNumberData;
import com.petrosoft.smena.view.recycler.element.BaseElement;

/**
 * Created by a.baskakov on 22/08/16.
 */
public class NameNumberElement extends BaseElement<NameNumberData> {
    public NameNumberElement(NameNumberData data) {
        super(data);
    }
}
