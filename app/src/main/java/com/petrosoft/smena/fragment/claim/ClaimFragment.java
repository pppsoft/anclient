package com.petrosoft.smena.fragment.claim;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.petrosoft.smena.R;
import com.petrosoft.smena.dao.CObject;
import com.petrosoft.smena.dao.CObjectDao;
import com.petrosoft.smena.dao.Claim;
import com.petrosoft.smena.dao.ClaimDao;
import com.petrosoft.smena.dao.ClaimImage;
import com.petrosoft.smena.dao.ClaimImageDao;
import com.petrosoft.smena.dao.DaoSession;
import com.petrosoft.smena.db.DbHelper;
import com.petrosoft.smena.fragment.images.BaseImagesFragment;
import com.petrosoft.smena.server.IRequestListener;
import com.petrosoft.smena.server.RequestHelper;
import com.petrosoft.smena.utils.DialogHelper;

/**
 * Created by a.baskakov on 22/08/16.
 */
public class ClaimFragment extends BaseImagesFragment {
    public static final String EXTRA_ID = "id";

    View mRootView;
    Button mButtonNotConfirmed;
    Button mButtonConfirmed;
    TextView mCObjectTitle;

    TextView mName;
    TextView mAddress;
    TextView mDescription;
    TextView mStatus;

    long mId;
    Claim mClaim;
    CObject mCObject;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mRootView == null) {
            Bundle bundle = getArguments();
            DaoSession daoSession = DbHelper.getDaoMaster(getActivity()).newSession();
            CObjectDao cObjectDao = daoSession.getCObjectDao();
            final ClaimDao claimDao = daoSession.getClaimDao();
            if (bundle != null) {
                mId = bundle.getLong(EXTRA_ID);
                mClaim = claimDao.queryBuilder().where(ClaimDao.Properties.Id.eq(mId)).unique();
                mCObject = cObjectDao.queryBuilder().where(CObjectDao.Properties.Id.eq(mClaim.getCObjectId())).unique();

            }

            mRootView = inflater.inflate(R.layout.claim_fragment, container, false);
            initImagesRecyclerView(mRootView);
            mCObjectTitle = (TextView) mRootView.findViewById(R.id.c_object_title);
            mName = (TextView) mRootView.findViewById(R.id.itemDescription);
            mAddress = (TextView) mRootView.findViewById(R.id.address);
            mDescription = (TextView) mRootView.findViewById(R.id.description);
            mStatus = (TextView) mRootView.findViewById(R.id.status);
            mButtonConfirmed = (Button) mRootView.findViewById(R.id.confirmed);
            mButtonNotConfirmed = (Button) mRootView.findViewById(R.id.not_confirmed);

            mCObjectTitle.setText(mCObject.getName());

            mName.setText(mClaim.getName());
            mAddress.setText(mClaim.getCustomAddress());
            mDescription.setText(mClaim.getComment());
            mStatus.setText(mClaim.getExtStatusName());

            mButtonNotConfirmed.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final AlertDialog alertDialog = DialogHelper.showProgressDialog(getActivity(), getActivity().getString(R.string.loading));
                    alertDialog.show();
                    mClaim.setLastCheckId(DbHelper.getCurrentCheck(getActivity()).getId());
                    mClaim.setSent(false);
                    mClaim.setIsCompleted(false);
                    claimDao.update(mClaim);
                    RequestHelper.tryToCompleteClaimAsync(getActivity(), mClaim, new IRequestListener<Boolean>() {
                        @Override
                        public void success(Boolean element) {
                            alertDialog.hide();
                            getActivity().onBackPressed();
                        }

                        @Override
                        public void error() {
                            alertDialog.hide();
                            getActivity().onBackPressed();
                        }
                    });
                }
            });

            mButtonConfirmed.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    completeClaim(true);
                }
            });

            mButtonNotConfirmed.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    completeClaim(false);
                }
            });

        }
        setHeader(getString(R.string.claim));
        setClosable(true);
        return mRootView;
    }

    public void completeClaim(boolean completed){
        DaoSession daoSession = DbHelper.getDaoMaster(getActivity()).newSession();
        final ClaimDao claimDao = daoSession.getClaimDao();
        final ClaimImageDao claimImageDao = daoSession.getClaimImageDao();

        final AlertDialog alertDialog = DialogHelper.showProgressDialog(getActivity(), getActivity().getString(R.string.loading));
        alertDialog.show();
        mClaim.setLastCheckId(DbHelper.getCurrentCheck(getActivity()).getId());
        mClaim.setSent(false);
        mClaim.setIsCompleted(completed);
        claimDao.update(mClaim);

        for (String path : mList) {
            ClaimImage claimImage = new ClaimImage();
            claimImage.setClaimId(mClaim.getId());
            claimImage.setPath(path);
            claimImage.setSent(false);
            claimImageDao.insert(claimImage);
        }

        RequestHelper.tryToCompleteClaimAsync(getActivity(), mClaim, new IRequestListener<Boolean>() {
            @Override
            public void success(Boolean element) {
                alertDialog.hide();
                getActivity().onBackPressed();
            }

            @Override
            public void error() {
                alertDialog.hide();
                getActivity().onBackPressed();
            }
        });
    }

}
