package com.petrosoft.smena.fragment;

import android.app.Activity;
import android.support.v4.app.Fragment;

import com.petrosoft.smena.activity.BaseFragmentActivity;
import com.petrosoft.smena.activity.MainActivity;

/**
 * Created by a.baskakov on 22/08/16.
 */
public abstract class BaseFragment extends Fragment{
    public static final String OBJECT = "object";

    /**
     * Возвращает true, если backPressed обработан внутри фрагмента
     *
     * @return
     */
    public abstract boolean backPressed();

    public void finishWork(){
        Activity activity = getActivity();
        if(activity instanceof BaseFragmentActivity){
            ((BaseFragmentActivity)activity).finishWork();
        }
    }
}
