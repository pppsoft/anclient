package com.petrosoft.smena.fragment.images.adapter.holder;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.petrosoft.smena.GreenApp;
import com.petrosoft.smena.R;
import com.petrosoft.smena.fragment.images.adapter.element.PhotoElement;
import com.petrosoft.smena.utils.image.IImageHelperListener;
import com.petrosoft.smena.utils.image.ImageHelper;
import com.petrosoft.smena.view.recycler.holder.BaseViewHolder;

import java.io.File;
import java.io.InputStream;

/**
 * Created by a.baskakov on 22/08/16.
 */
public class PhotoHolder extends BaseViewHolder<PhotoElement> {
    ImageView mImageView;
    ImageView mDelete;
    IDeleteListener mListener;
    View mProgressBar;

    public PhotoHolder(View itemView, IDeleteListener listener) {
        super(itemView);
        mListener = listener;
        mImageView = (ImageView) itemView.findViewById(R.id.photo);
        mDelete = (ImageView) itemView.findViewById(R.id.delete);
        mProgressBar = itemView.findViewById(R.id.progress_bar);

    }

    @Override
    public void setElement(final PhotoElement element) {
        super.setElement(element);
        mImageView.setVisibility(View.GONE);
        mProgressBar.setVisibility(View.VISIBLE);

        mDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.delete(element);
            }
        });
//        File f = new File(Uri.parse(element.getData()).getPath());
//        String data_dir = mImageView.getContext().getExternalFilesDir("Pictures").getAbsolutePath() + "/" + f.getName();
//          File nf = new File(data_dir);
//        //File nf = new File(Uri.parse(element.getData()).getPath());
        mProgressBar.setVisibility(View.GONE);
        mImageView.setVisibility(View.VISIBLE);
//      //  InputStream is=GreenApp.getInstance().getApplicationContext().getContentResolver().openInputStream(element.getData());
        String nf = ImageHelper.getImagePathFromUri(element.getData(),mImageView.getContext());
        Glide.with(GreenApp.getInstance().getApplicationContext())
                .load(nf)
                .centerCrop()
                .into(mImageView);


    }

    public static String getRealPathFromUri(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }
}
