package com.petrosoft.smena.fragment.plan;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;

import com.petrosoft.smena.GreenApp;
import com.petrosoft.smena.R;
import com.petrosoft.smena.activity.ObjectTypeActivity;
import com.petrosoft.smena.activity.SelectObjectActivity;
import com.petrosoft.smena.dao.CObject;
import com.petrosoft.smena.dao.CObjectDao;
import com.petrosoft.smena.dao.DaoSession;
import com.petrosoft.smena.dao.ObjectType;
import com.petrosoft.smena.dao.ObjectTypeDao;
import com.petrosoft.smena.db.DbHelper;
import com.petrosoft.smena.fragment.main.recycler.element.StringElement;
import com.petrosoft.smena.fragment.plan.adapter.CheckFragmentAdapter;
import com.petrosoft.smena.fragment.plan.adapter.PlanFragmentAdapter;
import com.petrosoft.smena.fragment.plan.element.CObjectElement;
import com.petrosoft.smena.server.RequestHelper;
import com.petrosoft.smena.server.response.data.login.LoginData;
import com.petrosoft.smena.server.response.data.plan.CObjectData;
import com.petrosoft.smena.utils.DialogHelper;
import com.petrosoft.smena.utils.PrefsHelper;
import com.petrosoft.smena.utils.recycler_view.ItemClickSupport;
import com.petrosoft.smena.view.recycler.element.BaseElement;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by a.baskakov on 22/08/16.
 */
public class PlanFragment extends BaseObjectTypeFragment {

    View mRootView;
    RecyclerView mRecyclerView;
    PlanFragmentAdapter mAdapter;
    RecyclerView.LayoutManager mLayoutManager;
    List<CObject> mList;
    Button mButtonShowOnMap;
    Button mButtonAddObject;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        init();
        if (mRootView == null) {
            mRootView = inflater.inflate(R.layout.plan_fragment, container, false);
            mRecyclerView = (RecyclerView) mRootView.findViewById(R.id.recycler_view);
            mRecyclerView.setHasFixedSize(true);


            ItemClickSupport.addTo(mRecyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {

                @Override
                public void onItemClick(RecyclerView parent, View view, int position, long id) {
                    if (position != -1) {
                        BaseElement baseElement = mAdapter.getElement(position);
                        if (baseElement instanceof CObjectElement) {
                            CObjectElement cObjectElement = (CObjectElement) baseElement;

                            Activity activity = getActivity();
                            ObjectTypeActivity objectTypeActivity = (ObjectTypeActivity) activity;
                            objectTypeActivity.openCObjectFragment(cObjectElement.getData(), true);
                        }
                    }
                }
            });

            mButtonShowOnMap = (Button) mRootView.findViewById(R.id.button_show_on_map);
            mButtonAddObject = (Button) mRootView.findViewById(R.id.add_object);

            mButtonShowOnMap.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    DaoSession daoSession = DbHelper.getDaoMaster(getActivity()).newSession();
                    ObjectTypeDao objectTypeDao = daoSession.getObjectTypeDao();
                    ObjectType objectType = objectTypeDao.queryBuilder().where(ObjectTypeDao.Properties.Id.eq(mObjectTypeServerId)).unique();
                    ((ObjectTypeActivity) getActivity()).openObjectTypeMapFragment(objectType);
                }
            });

            mButtonAddObject.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!GreenApp.checkNetwork()) {
                        GreenApp.showNoNetworkSnackbar(view, null);
                        return;
                    }
                    Intent intent = new Intent(getActivity(), SelectObjectActivity.class);
                    intent.putExtra("object_type_id", mObjectTypeServerId);
                    startActivityForResult(intent,1);

                   // ((ObjectTypeActivity) getActivity()).openPlanFragment();
                    if (true) {
                        return;
                    }
                }
            });
            updateFromDb();

        }

        LoginData loginData = PrefsHelper.loadLastLoginData(getActivity());
        SimpleDateFormat dt = new SimpleDateFormat("dd.MM.yyyy");
        setHeader(String.format(getString(R.string.plan_title), dt.format(loginData.mDate)));

        setClosable(DbHelper.getCurrentCheck(getActivity()) == null);
        return mRootView;
    }

    public void updateFromDb() {
        if (!isAdded()) {
            return;
        }

        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        DaoSession daoSession = DbHelper.getDaoMaster(getActivity()).newSession();
        ObjectTypeDao objectTypeDao = daoSession.getObjectTypeDao();

        ObjectType objectType = objectTypeDao.queryBuilder().where(ObjectTypeDao.Properties.Id.eq(mObjectTypeServerId)).unique();


        List<BaseElement> list = new ArrayList<>();
        list.add(new StringElement(objectType.getName()));

        mList = objectType.getCObjects();
        for (CObject cObj : mList) {
            list.add(new CObjectElement(cObj));
        }

        Map<Class<? extends BaseElement>, Integer> typeMap = new HashMap<>();
        typeMap.put(StringElement.class, CheckFragmentAdapter.TYPE_STRING);
        typeMap.put(CObjectElement.class, CheckFragmentAdapter.C_OBJECT_TYPE);

        mAdapter = new PlanFragmentAdapter(list, typeMap, getActivity());
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public boolean backPressed() {
        return false;
    }
}
