package com.petrosoft.smena.fragment.claims.adapter.element;

import com.petrosoft.smena.dao.Claim;
import com.petrosoft.smena.view.recycler.element.BaseElement;

/**
 * Created by a.baskakov on 22/08/16.
 */
public class ClaimElement extends BaseElement<Claim> {
    public ClaimElement(Claim data) {
        super(data);
    }
}
