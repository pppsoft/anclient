package com.petrosoft.smena.fragment.plan.holder;

import android.view.View;
import android.widget.TextView;

import com.petrosoft.smena.R;
import com.petrosoft.smena.dao.CObject;
import com.petrosoft.smena.fragment.plan.element.CObjectElement;
import com.petrosoft.smena.view.recycler.holder.BaseViewHolder;

/**
 * Created by a.baskakov on 22/08/16.
 */
public class CObjectHolder extends BaseViewHolder<CObjectElement> {
    TextView mTitle;
    TextView mDescription;
    View mArrow;
    View mCheck;

    public CObjectHolder(View itemView) {
        super(itemView);
        mTitle = (TextView) itemView.findViewById(R.id.title);
        mDescription = (TextView) itemView.findViewById(R.id.description);
        mArrow = itemView.findViewById(R.id.arrow);
        mCheck = itemView.findViewById(R.id.check);
    }

    @Override
    public void setElement(CObjectElement element) {
        super.setElement(element);
        CObject cObject = element.getData();
        mTitle.setText(cObject.getName());
        mDescription.setVisibility(View.VISIBLE);
        mDescription.setText(String.valueOf(cObject.getServerId()));
        mArrow.setVisibility(View.VISIBLE);
        mCheck.setVisibility(cObject.getCheck() ? View.VISIBLE : View.GONE);
    }
}
