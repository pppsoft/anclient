package com.petrosoft.smena.fragment.cobject;

import java.io.Serializable;

/**
 * Created by itrimf on 25/08/16.
 */

public class NameNumberData implements Serializable {
    public String name;
    public int number;
    public boolean isClickable;
    public boolean showNumber;

    public NameNumberData(String name, int number, boolean isClickable, boolean showNumber) {
        this.name = name;
        this.number = number;
        this.isClickable = isClickable;
        this.showNumber = showNumber;
    }
}
