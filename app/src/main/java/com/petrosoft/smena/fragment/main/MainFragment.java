package com.petrosoft.smena.fragment.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.petrosoft.smena.R;
import com.petrosoft.smena.activity.MainActivity;
import com.petrosoft.smena.activity.svalka.SvalkaActivity;
import com.petrosoft.smena.dao.DaoSession;
import com.petrosoft.smena.dao.ObjectType;
import com.petrosoft.smena.dao.ObjectTypeDao;
import com.petrosoft.smena.db.DbHelper;
import com.petrosoft.smena.fragment.BaseFragment;
import com.petrosoft.smena.fragment.main.recycler.adapter.MainFragmentAdapter;
import com.petrosoft.smena.fragment.main.recycler.element.ObjectTypeElement;
import com.petrosoft.smena.fragment.main.recycler.element.StringElement;
import com.petrosoft.smena.utils.recycler_view.ItemClickSupport;
import com.petrosoft.smena.view.recycler.element.BaseElement;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.petrosoft.smena.GreenApp.checkNetwork;
import static com.petrosoft.smena.GreenApp.showNoNetworkSnackbar;

/**
 * Created by a.baskakov on 22/08/16.
 */
public class MainFragment extends BaseFragment {

    View mRootView;
    RecyclerView mRecyclerView;
    MainFragmentAdapter mAdapter;
    RecyclerView.LayoutManager mLayoutManager;
    List<ObjectType> mList;
    Button mButtonFinishWork;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mRootView == null) {
            mRootView = inflater.inflate(R.layout.main_fragment, container, false);
            mRecyclerView = (RecyclerView) mRootView.findViewById(R.id.recycler_view);
            mRecyclerView.setHasFixedSize(true);


            ItemClickSupport.addTo(mRecyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {

                @Override
                public void onItemClick(RecyclerView parent, View view, int position, long id) {
                    if (position != -1) {
                        BaseElement baseElement = mAdapter.getElement(position);
                        if (baseElement instanceof ObjectTypeElement) {
                            ObjectTypeElement objectTypeElement = (ObjectTypeElement) baseElement;

                            if (objectTypeElement.getData().getServerId() == 9999) {
                                Intent intent = new Intent(getActivity(), SvalkaActivity.class);
                                startActivity(intent);
                                return;
                            }
                            ((MainActivity) getActivity()).openObjectTypeActivity(objectTypeElement.getData());
                        }
                    }
                }
            });

            mButtonFinishWork = (Button) mRootView.findViewById(R.id.button_finish_work);

            mButtonFinishWork.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!checkNetwork()) {
                        showNoNetworkSnackbar(view, null);
                        return;
                    }
                    finishWork();
                }
            });

            updateFromDb();

        }
        return mRootView;
    }

    private void updateFromDb() {
        if (!isAdded()) {
            return;
        }

        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        DaoSession daoSession = DbHelper.getDaoMaster(getActivity()).newSession();
        ObjectTypeDao objectTypeDao = daoSession.getObjectTypeDao();
        List<BaseElement> list = new ArrayList<>();
        list.add(new StringElement(getString(R.string.object_types_description)));

        mList = objectTypeDao.loadAll();
        Collections.sort(mList, new Comparator<ObjectType>() {
            @Override
            public int compare(ObjectType t1, ObjectType t2) {
                String str1 = t1.getName();
                String str2 = t2.getName();
                if (str1 == null) {
                    str1 = "";
                }
                return str1.compareTo(str2);
            }
        });
        for (ObjectType objectType : mList) {
            list.add(new ObjectTypeElement(objectType));
        }

        Map<Class<? extends BaseElement>, Integer> typeMap = new HashMap<>();
        typeMap.put(StringElement.class, MainFragmentAdapter.TYPE_STRING);
        typeMap.put(ObjectTypeElement.class, MainFragmentAdapter.TYPE_OBJECT_TYPE);

        mAdapter = new MainFragmentAdapter(list, typeMap, getActivity());

        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public boolean backPressed() {
        return false;
    }
}
