package com.petrosoft.smena.fragment.images.adapter.element;

import com.petrosoft.smena.view.recycler.element.BaseElement;

/**
 * Created by a.baskakov on 22/08/16.
 */
public class PhotoElement extends BaseElement<String> {
    public PhotoElement(String data) {
        super(data);
    }
}
