package com.petrosoft.smena.fragment.issues.adapter.element;

import com.petrosoft.smena.dao.Issue;
import com.petrosoft.smena.view.recycler.element.BaseElement;

/**
 * Created by a.baskakov on 22/08/16.
 */
public class IssueElement extends BaseElement<Issue> {
    public IssueElement(Issue data) {
        super(data);
    }
}
