package com.petrosoft.smena.fragment.plan;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.petrosoft.smena.R;
import com.petrosoft.smena.activity.ObjectTypeActivity;
import com.petrosoft.smena.dao.CObject;
import com.petrosoft.smena.dao.CheckTable;
import com.petrosoft.smena.dao.CheckTableDao;
import com.petrosoft.smena.dao.CheckToObject;
import com.petrosoft.smena.dao.CheckToObjectDao;
import com.petrosoft.smena.dao.DaoSession;
import com.petrosoft.smena.dao.ObjectType;
import com.petrosoft.smena.dao.ObjectTypeDao;
import com.petrosoft.smena.db.DbHelper;
import com.petrosoft.smena.fragment.checks.adapter.ChecksFragmentAdapter;
import com.petrosoft.smena.fragment.main.recycler.element.StringElement;
import com.petrosoft.smena.fragment.plan.adapter.CheckFragmentAdapter;
import com.petrosoft.smena.fragment.plan.element.CObjectElement;
import com.petrosoft.smena.server.response.data.login.LoginData;
import com.petrosoft.smena.utils.PrefsHelper;
import com.petrosoft.smena.utils.recycler_view.ItemClickSupport;
import com.petrosoft.smena.view.recycler.element.BaseElement;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by a.baskakov on 22/08/16.
 */
public class CheckFragment extends BaseObjectTypeFragment {

    public static final String EXTRA_CHECK_ID = "check_id";
    long mCheckId;

    View mRootView;
    View mContent;
    TextView mNoContent;
    RecyclerView mRecyclerView;
    CheckFragmentAdapter mAdapter;
    RecyclerView.LayoutManager mLayoutManager;
    Button mButtonFinishCheck;

    CheckTable mCheck;

    List<BaseElement> mList;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Bundle bundle = getArguments();
        if (bundle != null) {
            mCheckId = bundle.getLong(EXTRA_CHECK_ID);
            DaoSession daoSession = DbHelper.getDaoMaster(getActivity()).newSession();
            CheckTableDao checkTableDao = daoSession.getCheckTableDao();
            mCheck = checkTableDao.queryBuilder().where(CheckTableDao.Properties.Id.eq(mCheckId)).unique();

        }
        if (mRootView == null) {
            mRootView = inflater.inflate(R.layout.current_object_fragment, container, false);
            mRecyclerView = (RecyclerView) mRootView.findViewById(R.id.recycler_view);
            mRecyclerView.setHasFixedSize(true);
            mContent = mRootView.findViewById(R.id.content);
            mNoContent = (TextView) mRootView.findViewById(R.id.no_content);
            mButtonFinishCheck = (Button) mRootView.findViewById(R.id.button_finish_check);

            mNoContent.setText(getText(R.string.no_active_checks));
            mButtonFinishCheck.setText(getText(R.string.finish_current_check));


            ItemClickSupport.addTo(mRecyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {

                @Override
                public void onItemClick(RecyclerView parent, View view, int position, long id) {
                    if (position != -1) {
                        BaseElement baseElement = mAdapter.getElement(position);
                        if (baseElement instanceof CObjectElement) {
                            CObjectElement cObjectElement = (CObjectElement) baseElement;

                            Activity activity = getActivity();
                            ObjectTypeActivity objectTypeActivity = (ObjectTypeActivity) activity;
                            objectTypeActivity.openCObjectFragment(cObjectElement.getData(), false);
                        }
                    }
                }
            });

            mButtonFinishCheck.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    DbHelper.finishCurrentCheck(getActivity());
                    ((ObjectTypeActivity) getActivity()).openChecksFragmentFragment();
                }
            });

            updateFromDb();

        }

        Date date = new Date();
        date.setTime(mCheck.getDate_request());
        SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm");
        setHeader(String.format(getString(R.string.check_from_check), df.format(date)));
        setClosable(true);
        return mRootView;
    }

    private void updateFromDb() {
        if (!isAdded()) {
            return;
        }

        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        List<CheckToObject> list = mCheck.getCheckToCObject();
        mNoContent.setVisibility(View.GONE);

        CheckTable currentCheck = DbHelper.getCurrentCheck(getActivity());
        if (currentCheck!=null&&currentCheck.getId()==mCheck.getId()) {
            mButtonFinishCheck.setVisibility(View.VISIBLE);
        } else {
            mButtonFinishCheck.setVisibility(View.GONE);
        }


        mList = new ArrayList<>();

        for (CheckToObject checkToObject : list) {
            mList.add(new CObjectElement(checkToObject.getCObject()));
        }

        Map<Class<? extends BaseElement>, Integer> typeMap = new HashMap<>();
        typeMap.put(StringElement.class, CheckFragmentAdapter.TYPE_STRING);
        typeMap.put(CObjectElement.class, CheckFragmentAdapter.C_OBJECT_TYPE);

        mAdapter = new CheckFragmentAdapter(mList, typeMap, getActivity());
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public boolean backPressed() {
        return false;
    }
}
