package com.petrosoft.smena.fragment.main.recycler.holder;

import android.view.View;
import android.widget.TextView;

import com.petrosoft.smena.R;
import com.petrosoft.smena.dao.CObject;
import com.petrosoft.smena.dao.ObjectType;
import com.petrosoft.smena.fragment.main.recycler.element.ObjectTypeElement;
import com.petrosoft.smena.view.recycler.holder.BaseViewHolder;

import java.util.List;

/**
 * Created by a.baskakov on 22/08/16.
 */
public class ObjectTypeHolder extends BaseViewHolder<ObjectTypeElement> {
    TextView mTitle;
    TextView mDescription;
    View mArrow;
    View mCheck;


    public ObjectTypeHolder(View itemView) {
        super(itemView);
        mTitle = (TextView) itemView.findViewById(R.id.title);
        mDescription = (TextView) itemView.findViewById(R.id.description);
        mArrow = itemView.findViewById(R.id.arrow);
        mCheck = itemView.findViewById(R.id.check);
    }

    @Override
    public void setElement(ObjectTypeElement element) {
        super.setElement(element);
        mArrow.setVisibility(View.GONE);
        ObjectType objectType = element.getData();
        mTitle.setText(objectType.getName());
        List<CObject> cObjects = objectType.getCObjects();
        mCheck.setVisibility(View.GONE);
        if (cObjects.size() > 0) {
            mDescription.setVisibility(View.VISIBLE);
            String description = mTitle.getContext().getString(R.string.object_type_description);

            int currentStatus = 0;
            int claims = 0;
            int issues = 0;
            int tasks = 0;
            for (CObject cObject : cObjects) {
                currentStatus += cObject.getCurrent() ? 1 : 0;
                claims += cObject.getClaims().size();
                issues += cObject.getIssues().size();
                tasks += cObject.getTasks().size();
            }

            mDescription.setText(String.format(description, cObjects.size(), currentStatus, issues, tasks, claims));
        } else {
            mDescription.setVisibility(View.GONE);
        }
    }
}
