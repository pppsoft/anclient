package com.petrosoft.smena.fragment.images.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.petrosoft.smena.R;
import com.petrosoft.smena.fragment.images.adapter.element.AddPhotoElement;
import com.petrosoft.smena.fragment.images.adapter.element.PhotoElement;
import com.petrosoft.smena.fragment.images.adapter.holder.AddPhotoHolder;
import com.petrosoft.smena.fragment.images.adapter.holder.IDeleteListener;
import com.petrosoft.smena.fragment.images.adapter.holder.PhotoHolder;
import com.petrosoft.smena.view.recycler.adapter.BaseAdapter;
import com.petrosoft.smena.view.recycler.element.BaseElement;
import com.petrosoft.smena.view.recycler.holder.BaseViewHolder;

import java.util.List;
import java.util.Map;

/**
 * Created by a.baskakov on 22/08/16.
 */
public class ImagesAdapter extends BaseAdapter {
    public static final int TYPE_PHOTO = 0;
    public static final int TYPE_ADD_PHOTO = 1;
    IDeleteListener mListener;

    public ImagesAdapter(List<BaseElement> mList, Map<Class<? extends BaseElement>, Integer> typeMap, Context mContext, IDeleteListener listener) {
        super(mList, typeMap, mContext);
        mListener = listener;
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case TYPE_PHOTO: {
                View contentView = LayoutInflater.from(parent.getContext()).inflate(R.layout.photo_element, parent, false);
                return new PhotoHolder(contentView, mListener);
            }
            case TYPE_ADD_PHOTO: {
                View contentView = LayoutInflater.from(parent.getContext()).inflate(R.layout.add_photo_element, parent, false);
                return new AddPhotoHolder(contentView);
            }
        }
        return null;
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        BaseElement element = getElement(position);
        int viewType = getItemViewType(position);
        switch (viewType) {
            case TYPE_PHOTO: {
                PhotoHolder h = (PhotoHolder) holder;
                PhotoElement e = (PhotoElement) element;
                h.setElement(e);
                break;
            }
            case TYPE_ADD_PHOTO: {
                AddPhotoHolder h = (AddPhotoHolder) holder;
                AddPhotoElement e = (AddPhotoElement) element;
                h.setElement(e);
                break;
            }
        }
    }
}
