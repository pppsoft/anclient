package com.petrosoft.smena.fragment.claims.adapter.holder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.petrosoft.smena.R;
import com.petrosoft.smena.fragment.claims.adapter.element.ClaimElement;
import com.petrosoft.smena.dao.Claim;
import com.petrosoft.smena.view.recycler.holder.BaseViewHolder;

/**
 * Created by a.baskakov on 22/08/16.
 */
public class ClaimHolder extends BaseViewHolder<ClaimElement> {
    TextView mTitle;
    TextView mDescription;
    View mArrow;
    ImageView mCheck;

    public ClaimHolder(View itemView) {
        super(itemView);
        mTitle = (TextView) itemView.findViewById(R.id.title);
        mDescription = (TextView) itemView.findViewById(R.id.description);
        mArrow = itemView.findViewById(R.id.arrow);
        mCheck = (ImageView) itemView.findViewById(R.id.check);
    }

    @Override
    public void setElement(ClaimElement element) {
        super.setElement(element);
        Claim claim = element.getData();
        mTitle.setText(claim.getName());
        mDescription.setVisibility(View.GONE);
        mArrow.setVisibility(View.VISIBLE);
        mCheck.setVisibility(View.GONE);
    }
}
