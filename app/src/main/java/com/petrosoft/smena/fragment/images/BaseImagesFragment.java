package com.petrosoft.smena.fragment.images;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ArrayAdapter;


import com.petrosoft.smena.R;
import com.petrosoft.smena.fragment.claims.BaseCobjectCurrentFragment;
import com.petrosoft.smena.fragment.images.adapter.ImagesAdapter;
import com.petrosoft.smena.fragment.images.adapter.element.AddPhotoElement;
import com.petrosoft.smena.fragment.images.adapter.element.PhotoElement;
import com.petrosoft.smena.fragment.images.adapter.holder.IDeleteListener;
import com.petrosoft.smena.utils.DialogHelper;
import com.petrosoft.smena.utils.imgtool.PickerBuilder;
import com.petrosoft.smena.utils.recycler_view.ItemClickSupport;
import com.petrosoft.smena.view.recycler.element.BaseElement;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by a.baskakov on 31/08/16.
 */
public abstract class BaseImagesFragment extends BaseCobjectCurrentFragment {
    protected RecyclerView mRecyclerView;
    protected ImagesAdapter mAdapter;
    protected RecyclerView.LayoutManager mLayoutManager;
    protected List<String> mList = new ArrayList<>();

//    final QuickImagePick.Callback mCallback = new QuickImagePick.Callback() {
//
//        @Override
//        public void onImagePicked(@NonNull final PickSource pPickSource, final int pRequestType, @NonNull final Uri pImageUri) {
//            mList.add(pImageUri.toString());
//            imageAdded(pImageUri.toString());
//            updateRecyclerView();
//        }
//
//        @Override
//        public void onMultipleImagesPicked(int pRequestType, @NonNull List<Uri> pImageUris) {
//
//        }
//
//        @Override
//        public void onError(@NonNull final PickSource pPickSource, final int pRequestType, @NonNull final String pErrorString) {
//            DialogHelper.showDialog(getActivity(), getActivity().getString(R.string.error), getActivity().getString(R.string.error_get_photo), new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialogInterface, int i) {
//                }
//            });
//        }
//
//        @Override
//        public void onCancel(@NonNull final PickSource pPickSource, final int pRequestType) {
//        }
//
//    };

    protected void imageAdded(String uri) {

    }

    @Override
    public void onActivityResult(final int pRequestCode, final int pResultCode, final Intent pData) {

//        if (!QuickImagePick.handleActivityResult(getActivity().getApplicationContext(), pRequestCode, pResultCode, pData, this.mCallback)) {
//
//        }
        super.onActivityResult(pRequestCode, pResultCode, pData);

    }

    public void initImagesRecyclerView(View rootView) {
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(true);
        ItemClickSupport.addTo(mRecyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {

            @Override
            public void onItemClick(RecyclerView parent, View view, int position, long id) {
                if (position != -1) {
                    BaseElement baseElement = mAdapter.getElement(position);
                    if (baseElement instanceof AddPhotoElement) {
                        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(
                                getActivity(),
                                R.layout.dialog_choice_element);

                        arrayAdapter.add(getActivity().getString(R.string.camera));
                        arrayAdapter.add(getActivity().getString(R.string.gallery));

                        DialogHelper.showSelectDialog(getActivity(), getActivity().getString(R.string.select_source), arrayAdapter, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                switch (i) {
                                    case 0:
                                        //QuickImagePick.pickFromCamera(BaseImagesFragment.this);
                                        new PickerBuilder(getActivity(), PickerBuilder.SELECT_FROM_CAMERA)
                                                .setOnImageReceivedListener(new PickerBuilder.onImageReceivedListener() {
                                                    @Override
                                                    public void onImageReceived(Uri imageUri) {

                                                        mList.add(imageUri.toString());
                                                        imageAdded(imageUri.toString());
                                                        updateRecyclerView();


                                                    }
                                                }).setOnPermissionRefusedListener(new PickerBuilder.onPermissionRefusedListener() {
                                            @Override
                                            public void onPermissionRefused() {

                                            }
                                        }).withTimeStamp(true).setCustomizedUcrop(null)
                                                .setCropScreenColor(Color.BLACK)
                                                .start();
                                        break;
                                    case 1:
                                        new PickerBuilder(getActivity(), PickerBuilder.SELECT_FROM_GALLERY)
                                                .setOnImageReceivedListener(new PickerBuilder.onImageReceivedListener() {
                                                    @Override
                                                    public void onImageReceived(Uri imageUri) {

                                                        mList.add(imageUri.toString());
                                                        imageAdded(imageUri.toString());
                                                        updateRecyclerView();


                                                    }
                                                }).setOnPermissionRefusedListener(new PickerBuilder.onPermissionRefusedListener() {
                                            @Override
                                            public void onPermissionRefused() {

                                            }
                                        }).withTimeStamp(true).setCustomizedUcrop(null)
                                                .setCropScreenColor(Color.BLACK)
                                                .start();
                                        break;
                                }
                            }
                        });
                    }
                }
            }
        });
        updateRecyclerView();
    }

    private void updateRecyclerView() {
        if (!isAdded()) {
            return;
        }

        mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        mRecyclerView.setLayoutManager(mLayoutManager);

        List<BaseElement> list = new ArrayList<>();
        for (String path : mList) {
            list.add(new PhotoElement(path));
        }
        if (list.size() < 5) {
            list.add(new AddPhotoElement(""));
        }
        Map<Class<? extends BaseElement>, Integer> typeMap = new HashMap<>();
        typeMap.put(AddPhotoElement.class, ImagesAdapter.TYPE_ADD_PHOTO);
        typeMap.put(PhotoElement.class, ImagesAdapter.TYPE_PHOTO);

        mAdapter = new ImagesAdapter(list, typeMap, getActivity(), new IDeleteListener() {
            @Override
            public void delete(PhotoElement photoElement) {
                mList.remove(photoElement.getData());
                updateRecyclerView();
            }
        });
        mRecyclerView.setAdapter(mAdapter);
    }


    @Override
    public boolean backPressed() {
        return false;
    }
}
