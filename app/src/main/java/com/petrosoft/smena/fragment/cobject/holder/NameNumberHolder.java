package com.petrosoft.smena.fragment.cobject.holder;

import android.view.View;
import android.widget.TextView;

import com.petrosoft.smena.R;
import com.petrosoft.smena.fragment.cobject.NameNumberData;
import com.petrosoft.smena.fragment.cobject.element.NameNumberElement;
import com.petrosoft.smena.view.recycler.holder.BaseViewHolder;

/**
 * Created by a.baskakov on 22/08/16.
 */
public class NameNumberHolder extends BaseViewHolder<NameNumberElement> {
    TextView mName;
    TextView mValue;
    View mArrow;


    public NameNumberHolder(View itemView) {
        super(itemView);
        mName = (TextView) itemView.findViewById(R.id.itemDescription);
        mValue = (TextView) itemView.findViewById(R.id.value);
        mArrow = itemView.findViewById(R.id.arrow);
    }

    @Override
    public void setElement(NameNumberElement element) {
        super.setElement(element);
        NameNumberData nameNumberData = element.getData();
        mName.setText(nameNumberData.name);
        mValue.setText(String.valueOf(nameNumberData.number));
        if (nameNumberData.showNumber) {
            mValue.setVisibility(View.VISIBLE);
        } else {
            mValue.setVisibility(View.GONE);
        }

        if (nameNumberData.isClickable) {
            mArrow.setVisibility(View.VISIBLE);
        } else {
            mArrow.setVisibility(View.GONE);
        }
    }
}
