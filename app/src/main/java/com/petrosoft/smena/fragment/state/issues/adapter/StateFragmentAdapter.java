package com.petrosoft.smena.fragment.state.issues.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.petrosoft.smena.R;
import com.petrosoft.smena.fragment.main.recycler.element.StringElement;
import com.petrosoft.smena.fragment.main.recycler.holder.StringHolder;
import com.petrosoft.smena.view.recycler.adapter.BaseAdapter;
import com.petrosoft.smena.view.recycler.element.BaseElement;
import com.petrosoft.smena.view.recycler.holder.BaseViewHolder;

import java.util.List;
import java.util.Map;

/**
 * Created by a.baskakov on 22/08/16.
 */
public class StateFragmentAdapter extends BaseAdapter {
    public static final int TYPE_STRING = 0;
    public static final int TYPE_OBJECT = 1;

    public StateFragmentAdapter(List<BaseElement> mList, Map<Class<? extends BaseElement>, Integer> typeMap, Context mContext) {
        super(mList, typeMap, mContext);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case TYPE_STRING: {
                View contentView = LayoutInflater.from(parent.getContext()).inflate(R.layout.string_element, parent, false);
                return new StringHolder(contentView);
            }
        }
        return null;
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        BaseElement element = getElement(position);
        int viewType = getItemViewType(position);
        switch (viewType) {
            case TYPE_STRING: {
                StringHolder h = (StringHolder) holder;
                StringElement e = (StringElement) element;
                h.setElement(e);
                break;
            }
        }
    }
}
