package com.petrosoft.smena.fragment.images.adapter.holder;

import com.petrosoft.smena.fragment.images.adapter.element.PhotoElement;

/**
 * Created by itrimf on 29/08/16.
 */
public interface IDeleteListener {
    void delete(PhotoElement photoElement);
}
