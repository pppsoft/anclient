package com.petrosoft.smena.fragment.issue;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.petrosoft.smena.GreenApp;
import com.petrosoft.smena.R;
import com.petrosoft.smena.activity.SelectRoadExtensionActivity;
import com.petrosoft.smena.activity.svalka.BorderSetupActivity;
import com.petrosoft.smena.adapters.ImageAdapter;
import com.petrosoft.smena.adapters.ImageRemover;
import com.petrosoft.smena.dao.CObject;
import com.petrosoft.smena.dao.CObjectDao;
import com.petrosoft.smena.dao.Claim;
import com.petrosoft.smena.dao.ClaimDao;
import com.petrosoft.smena.dao.ClaimImage;
import com.petrosoft.smena.dao.ClaimImageDao;
import com.petrosoft.smena.dao.DaoSession;
import com.petrosoft.smena.dao.Issue;
import com.petrosoft.smena.dao.IssueDao;
import com.petrosoft.smena.dao.IssueImage;
import com.petrosoft.smena.dao.IssueImageDao;
import com.petrosoft.smena.dao.PendingImage;
import com.petrosoft.smena.dao.PendingImageDao;
import com.petrosoft.smena.db.DbHelper;
import com.petrosoft.smena.fragment.images.BaseImagesFragment;
import com.petrosoft.smena.fragment.plan.BaseObjectTypeFragment;
import com.petrosoft.smena.server.IRequestListener;
import com.petrosoft.smena.server.RequestHelper;
import com.petrosoft.smena.server.response.data.add_issue.Element;
import com.petrosoft.smena.server.response.data.add_issue.RoadExtension;
import com.petrosoft.smena.server.response.data.svalka.CheckIssueCompletion;
import com.petrosoft.smena.server.response.data.svalka.CommonSvalkaResult;
import com.petrosoft.smena.service.SentPendingDataService;
import com.petrosoft.smena.utils.DialogHelper;
import com.petrosoft.smena.utils.PrefsHelper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static android.os.Environment.DIRECTORY_PICTURES;

/**
 * Created by a.baskakov on 22/08/16.
 */
public class IssueFragment extends BaseObjectTypeFragment implements ImageRemover {
    public static final String EXTRA_ID = "id";
    private static final int TYPE_PHOTO = 44;
    private static final int REQUEST_IMAGE_CAPTURE = 6466;

    View mRootView;
    Button mButtonComplete;
    Button mButtonNotComplete;
    TextView mCObjectTitle;
    TextView mType;

    long mId;
    Issue mIssue;
    CObject mCObject;
    private TextView mAddress;
    private TextView mElement;

    private Uri mSavedPhotoFile;


    private ImageAdapter mImageAdapter;
    private List<Uri> mPhotosList;
    private File directory;
    @BindView(R.id.recycler_view)
    RecyclerView mPhotosRecycler;

    @BindView(R.id.add_photo_button)
    TextView mAddPhotoButton;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        if (mRootView == null) {
            Bundle bundle = getArguments();
            DaoSession daoSession = DbHelper.getDaoMaster(getActivity()).newSession();
            CObjectDao cObjectDao = daoSession.getCObjectDao();
            final IssueDao issueDao = daoSession.getIssueDao();
            if (bundle != null) {
                mId = bundle.getLong(EXTRA_ID);
                mIssue = issueDao.queryBuilder().where(IssueDao.Properties.Id.eq(mId)).unique();
                mCObject = cObjectDao.queryBuilder().where(CObjectDao.Properties.Id.eq(mIssue.getCObjectId())).unique();

            }
            createDirectory();
            mPhotosList = new ArrayList<>();
            mRootView = inflater.inflate(R.layout.issue_fragment_2, container, false);
            ButterKnife.bind(this, mRootView);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this.getContext(), LinearLayoutManager.HORIZONTAL, false);
            mPhotosRecycler.setLayoutManager(linearLayoutManager);
            mImageAdapter = new ImageAdapter(this);
            mPhotosRecycler.setAdapter(mImageAdapter);

            //initImagesRecyclerView(mRootView);
            mCObjectTitle = (TextView) mRootView.findViewById(R.id.c_object_title);

            mType = (TextView) mRootView.findViewById(R.id.type);
            mButtonComplete = (Button) mRootView.findViewById(R.id.complete);
            mButtonNotComplete = (Button) mRootView.findViewById(R.id.not_complete);
            mAddress = (TextView) mRootView.findViewById(R.id.address);
            mElement = (TextView) mRootView.findViewById(R.id.element);
//            mCObjectTitle.setText(mCObject.getName());

            mAddress.setText(mIssue.getCustomAddress());

            mElement.setText("");
            mType.setText(mIssue.getIssueTypes());

            mButtonComplete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    completeIssue(true);
                }
            });

            mButtonNotComplete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    completeIssue(false);
                }
            });
            mAddPhotoButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dispatchTakePictureIntent();
                }
            });


        }
        setHeader(getString(R.string.issue));
        setClosable(true);
        return mRootView;
    }

    private void createDirectory() {

        directory = GreenApp.getInstance().getExternalFilesDir(DIRECTORY_PICTURES);
        if (!directory.exists()) {
            boolean x = directory.mkdirs();
            Log.v("Folder create result", String.valueOf(x));
        }
    }

    private Uri generateFileUri(int type) {
        File file = null;
        switch (type) {
            case TYPE_PHOTO:

                file = new File(directory.getAbsolutePath() + "/" + "photo_"
                        + System.currentTimeMillis() + ".jpg");
                break;

        }
        Log.d("File generate", "fileName = " + file);
        return Uri.fromFile(file);
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        mSavedPhotoFile = generateFileUri(TYPE_PHOTO);
        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, mSavedPhotoFile);

        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (requestCode) {


            case REQUEST_IMAGE_CAPTURE:
                if (resultCode == RESULT_OK) {
                    if (mSavedPhotoFile != null) {
                        Bitmap b2 = BitmapFactory.decodeFile(mSavedPhotoFile.getPath());
                        Bitmap b = b2.copy(Bitmap.Config.ARGB_8888, true);
                        Canvas c = new Canvas(b);
                        Paint p = new Paint();

                        p.setColor(getResources().getColor(R.color.red));
                        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm", Locale.getDefault());
                        String text = sdf.format(new Date());
                        //p.setTextScaleX(2);
                        p.setTextSize(b2.getHeight() / 25);
                        c.drawText(text, b2.getWidth() / 2, b2.getHeight() - 50, p);
                        FileOutputStream fileOutputStream = null;
                        try {
                            fileOutputStream = new FileOutputStream(mSavedPhotoFile.getPath());
                            b.compress(Bitmap.CompressFormat.JPEG, 60, fileOutputStream);
                        } catch (Exception e) {
                            e.printStackTrace();

                        } finally {
                            if (fileOutputStream != null) {
                                try {
                                    fileOutputStream.close();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                        //WRITE TEXT OSD
                        mImageAdapter.addImage(mSavedPhotoFile);
                        mImageAdapter.notifyDataSetChanged();
                        mPhotosList.add(mSavedPhotoFile);
                    }
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void completeIssue(final boolean completed) {
        DaoSession daoSession = DbHelper.getDaoMaster(getActivity()).newSession();
        final IssueDao issueDao = daoSession.getIssueDao();
        final PendingImageDao pendingImageDao = daoSession.getPendingImageDao();

        final AlertDialog alertDialog = DialogHelper.showProgressDialog(getActivity(), getActivity().getString(R.string.loading));
        alertDialog.show();
        mIssue.setLastCheckId(DbHelper.getCurrentCheck(getActivity()).getId());
        mIssue.setSent(false);
        mIssue.setIsCompleted(completed);
        CheckIssueCompletion checkIssueCompletion = new CheckIssueCompletion();
        checkIssueCompletion.setCheck_id(DbHelper.getCurrentCheck(getActivity()).getServerId());
        checkIssueCompletion.setIs_completed(completed);
        issueDao.update(mIssue);

        GreenApp.getInstance().getApi().completeIssue(mIssue.getServerId(), checkIssueCompletion).enqueue(new Callback<CommonSvalkaResult>() {
            @Override
            public void onResponse(Call<CommonSvalkaResult> call, Response<CommonSvalkaResult> response) {
                alertDialog.hide();
                if (response.isSuccessful()) {
                    CommonSvalkaResult csr = response.body();
                    for (Uri uri : mPhotosList) {
                        PendingImage pendingImage = new PendingImage();
                        pendingImage.setEntity_id(csr.getId());
                        pendingImage.setImage_code(csr.getImageCode());
                        pendingImage.setImage_path(uri.getPath());
                        pendingImage.setToken(PrefsHelper.loadLastLoginData(getContext()).mToken);
                        pendingImage.setIs_completed(false);
                        pendingImage.setPoint_id(null);
                        pendingImageDao.insert(pendingImage);
                    }

                    Intent intent = new Intent(getContext(), SentPendingDataService.class);
                    getActivity().startService(intent);
                    getActivity().onBackPressed();

                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                    builder.setTitle("Ошибка взаимодействия с сервером");
                    builder.setMessage("Отправить повторно?");
                    builder.setPositiveButton(getString(R.string.retry), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            completeIssue(completed);
                        }
                    });
                    builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            getActivity().onBackPressed();
                        }
                    });
                    builder.create().show();
                }
            }

            @Override
            public void onFailure(Call<CommonSvalkaResult> call, Throwable t) {
                alertDialog.hide();
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle("Ошибка сети");
                builder.setMessage("Произошла ошибка при взаимодействии с сервером. Повторите попытку позднее");
                builder.setPositiveButton(getString(R.string.retry), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        completeIssue(completed);
                    }
                });
                builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getActivity().onBackPressed();
                    }
                });
                builder.create().show();

            }
        });
    }

    @Override
    public void notifyFileRemoved(final int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Удаление фотографии");
        builder.setMessage("Подтвердите удаление фото");
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                try {
                    mPhotosList.remove(position);
                    mImageAdapter.deleteImage(position);
                    mImageAdapter.notifyDataSetChanged();
                } catch (Exception ignored) {

                }

            }
        });
        builder.setNegativeButton(R.string.cancel, null);
        builder.create().show();
    }

    @Override
    public boolean backPressed() {
        return false;
    }
}
