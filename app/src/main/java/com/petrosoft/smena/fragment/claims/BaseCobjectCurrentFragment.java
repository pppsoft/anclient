package com.petrosoft.smena.fragment.claims;

import android.os.Bundle;

import com.petrosoft.smena.dao.CObject;
import com.petrosoft.smena.dao.CObjectDao;
import com.petrosoft.smena.dao.DaoSession;
import com.petrosoft.smena.db.DbHelper;
import com.petrosoft.smena.fragment.plan.BaseObjectTypeFragment;

/**
 * Created by itrimf on 28/08/16.
 */
public abstract class BaseCobjectCurrentFragment extends BaseObjectTypeFragment {
    public static final String EXTRA_OBJECT_TYPE_ID = "object_type_id";

    long mObjectTypeId;
    protected CObject mCObject;

    protected void init() {
        Bundle bundle = getArguments();
        if (bundle != null) {
            mObjectTypeId = bundle.getLong(EXTRA_OBJECT_TYPE_ID);
            DaoSession daoSession = DbHelper.getDaoMaster(getActivity()).newSession();
            CObjectDao cObjectDao = daoSession.getCObjectDao();
            mCObject = cObjectDao.queryBuilder().where(CObjectDao.Properties.Id.eq(mObjectTypeId)).unique();
        }
    }
}
