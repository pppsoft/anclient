package com.petrosoft.smena.fragment.plan.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.petrosoft.smena.R;
import com.petrosoft.smena.fragment.check.adapter.holder.CobjectCheckHolder;
import com.petrosoft.smena.fragment.main.recycler.element.StringElement;
import com.petrosoft.smena.fragment.main.recycler.holder.StringHolder;
import com.petrosoft.smena.fragment.plan.element.CObjectElement;
import com.petrosoft.smena.fragment.plan.holder.CObjectHolder;
import com.petrosoft.smena.view.recycler.adapter.BaseAdapter;
import com.petrosoft.smena.view.recycler.element.BaseElement;
import com.petrosoft.smena.view.recycler.holder.BaseViewHolder;

import java.util.List;
import java.util.Map;

/**
 * Created by a.baskakov on 22/08/16.
 */
public class CheckFragmentAdapter extends BaseAdapter {
    public static final int TYPE_STRING = 0;
    public static final int C_OBJECT_TYPE = 1;

    public CheckFragmentAdapter(List<BaseElement> mList, Map<Class<? extends BaseElement>, Integer> typeMap, Context mContext) {
        super(mList, typeMap, mContext);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case C_OBJECT_TYPE: {
                View contentView = LayoutInflater.from(parent.getContext()).inflate(R.layout.cobject_check_element, parent, false);
                return new CobjectCheckHolder(contentView);
            }
            case TYPE_STRING: {
                View contentView = LayoutInflater.from(parent.getContext()).inflate(R.layout.string_element, parent, false);
                return new StringHolder(contentView);
            }
        }
        return null;
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        BaseElement element = getElement(position);
        int viewType = getItemViewType(position);
        switch (viewType) {
            case C_OBJECT_TYPE: {
                CobjectCheckHolder h = (CobjectCheckHolder) holder;
                CObjectElement e = (CObjectElement) element;
                h.setElement(e);
                break;
            }
            case TYPE_STRING: {
                StringHolder h = (StringHolder) holder;
                StringElement e = (StringElement) element;
                h.setElement(e);
                break;
            }
        }
    }
}
