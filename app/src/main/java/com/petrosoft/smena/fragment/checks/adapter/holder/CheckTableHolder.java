package com.petrosoft.smena.fragment.checks.adapter.holder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.petrosoft.smena.R;
import com.petrosoft.smena.dao.CObject;
import com.petrosoft.smena.dao.CheckTable;
import com.petrosoft.smena.fragment.checks.adapter.element.CheckTableElement;
import com.petrosoft.smena.view.recycler.holder.BaseViewHolder;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by a.baskakov on 22/08/16.
 */
public class CheckTableHolder extends BaseViewHolder<CheckTableElement> {
    TextView mTitle;
    TextView mDescription;
    View mArrow;
    ImageView mCheck;

    public CheckTableHolder(View itemView) {
        super(itemView);
        mTitle = (TextView) itemView.findViewById(R.id.title);
        mDescription = (TextView) itemView.findViewById(R.id.description);
        mArrow = itemView.findViewById(R.id.arrow);
        mCheck = (ImageView)itemView.findViewById(R.id.check);
    }

    @Override
    public void setElement(CheckTableElement element) {
        super.setElement(element);
        CheckTable checkTable = element.getData();
        mTitle.setText(mTitle.getContext().getString(R.string.check));
        mDescription.setVisibility(View.VISIBLE);

        Date date = new Date();
        date.setTime(checkTable.getDate_request());
        SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm");
        mDescription.setText(String.format(mTitle.getContext().getString(R.string.check_from),df.format(date)));
        mArrow.setVisibility(View.VISIBLE);
        mCheck.setVisibility(View.VISIBLE);
        if(checkTable.getFinished()){
            mCheck.setImageResource(R.drawable.check);
        }else{
            mCheck.setImageResource(R.drawable.current);
        }
    }
}
