package com.petrosoft.smena.fragment.checks;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.petrosoft.smena.R;
import com.petrosoft.smena.activity.ObjectTypeActivity;
import com.petrosoft.smena.dao.CheckTable;
import com.petrosoft.smena.dao.CheckTableDao;
import com.petrosoft.smena.dao.DaoSession;
import com.petrosoft.smena.fragment.checks.adapter.ChecksFragmentAdapter;
import com.petrosoft.smena.db.DbHelper;
import com.petrosoft.smena.fragment.checks.adapter.element.CheckTableElement;
import com.petrosoft.smena.fragment.main.recycler.element.StringElement;
import com.petrosoft.smena.fragment.plan.BaseObjectTypeFragment;
import com.petrosoft.smena.utils.recycler_view.ItemClickSupport;
import com.petrosoft.smena.view.recycler.element.BaseElement;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by a.baskakov on 22/08/16.
 */
public class ChecksFragment extends BaseObjectTypeFragment {

    View mRootView;
    View mContent;
    TextView mNoContent;
    RecyclerView mRecyclerView;
    ChecksFragmentAdapter mAdapter;
    RecyclerView.LayoutManager mLayoutManager;
    Button mButtonFinishCheck;

    List<CheckTable> mList;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mRootView == null) {
            mRootView = inflater.inflate(R.layout.current_object_fragment, container, false);
            mRecyclerView = (RecyclerView) mRootView.findViewById(R.id.recycler_view);
            mRecyclerView.setHasFixedSize(true);
            mContent = mRootView.findViewById(R.id.content);
            mNoContent = (TextView) mRootView.findViewById(R.id.no_content);
            mButtonFinishCheck = (Button) mRootView.findViewById(R.id.button_finish_check);

            mNoContent.setText(getText(R.string.no_active_checks));
            mButtonFinishCheck.setText(getText(R.string.finish_current_check));


            ItemClickSupport.addTo(mRecyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {

                @Override
                public void onItemClick(RecyclerView parent, View view, int position, long id) {
                    if (position != -1) {
                        BaseElement baseElement = mAdapter.getElement(position);
                        if(baseElement instanceof CheckTableElement){
                            CheckTableElement checkTableElement = (CheckTableElement)baseElement;
                            Activity activity = getActivity();
                            ObjectTypeActivity objectTypeActivity = (ObjectTypeActivity) activity;
                            objectTypeActivity.openCheckFragment(checkTableElement.getData());
                        }
                    }
                }
            });


            mButtonFinishCheck.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    DbHelper.finishCurrentCheck(getActivity());
                    ((ObjectTypeActivity) getActivity()).openChecksFragmentFragment();
                }
            });

            updateFromDb();

        }
        if (DbHelper.getCurrentCheck(getActivity())==null) {
            mButtonFinishCheck.setVisibility(View.GONE);
        } else {
            mButtonFinishCheck.setVisibility(View.VISIBLE);
        }

        setHeader(getString(R.string.checks));

        setClosable(DbHelper.getCurrentCheck(getActivity())==null);
        return mRootView;
    }

    private void updateFromDb() {
        if (!isAdded()) {
            return;
        }

        DaoSession daoSession = DbHelper.getDaoMaster(getActivity()).newSession();
        CheckTableDao checkTableDao = daoSession.getCheckTableDao();

        mList = checkTableDao.loadAll();
        if (mList.size() == 0) {
            mNoContent.setVisibility(View.VISIBLE);
        } else {
            mNoContent.setVisibility(View.GONE);
        }
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        List<BaseElement> list = new ArrayList<>();
        list.add(new StringElement(getString(R.string.checks_description)));
        for(CheckTable check:mList){
            list.add(new CheckTableElement(check));
        }

        Map<Class<? extends BaseElement>, Integer> typeMap = new HashMap<>();
        typeMap.put(StringElement.class, ChecksFragmentAdapter.TYPE_STRING);
        typeMap.put(CheckTableElement.class, ChecksFragmentAdapter.TYPE_CHECKS);

        mAdapter = new ChecksFragmentAdapter(list, typeMap, getActivity());
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public boolean backPressed() {
        return false;
    }
}
