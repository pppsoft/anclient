package com.petrosoft.smena.fragment.main.recycler.holder;

import android.view.View;
import android.widget.TextView;

import com.petrosoft.smena.R;
import com.petrosoft.smena.fragment.main.recycler.element.ObjectTypeElement;
import com.petrosoft.smena.fragment.main.recycler.element.StringElement;
import com.petrosoft.smena.view.recycler.holder.BaseViewHolder;

/**
 * Created by a.baskakov on 22/08/16.
 */
public class StringHolder extends BaseViewHolder<StringElement> {
    TextView mTextView;


    public StringHolder(View itemView) {
        super(itemView);
        mTextView = (TextView) itemView.findViewById(R.id.title);
    }

    @Override
    public void setElement(StringElement element) {
        super.setElement(element);
        mTextView.setText(element.getData());
    }
}
