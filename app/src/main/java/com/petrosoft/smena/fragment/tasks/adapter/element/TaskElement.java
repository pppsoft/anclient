package com.petrosoft.smena.fragment.tasks.adapter.element;

import com.petrosoft.smena.dao.Task;
import com.petrosoft.smena.view.recycler.element.BaseElement;

/**
 * Created by a.baskakov on 22/08/16.
 */
public class TaskElement extends BaseElement<Task> {
    public TaskElement(Task data) {
        super(data);
    }
}
