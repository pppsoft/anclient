package com.petrosoft.smena.fragment.cobject;

import java.io.Serializable;

/**
 * Created by itrimf on 25/08/16.
 */

public class NameValueData implements Serializable {
    public String name;
    public String value;

    public NameValueData(String name, String value) {
        this.name = name;
        this.value = value;
    }
}
