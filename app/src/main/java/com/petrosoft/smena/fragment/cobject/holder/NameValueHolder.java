package com.petrosoft.smena.fragment.cobject.holder;

import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.petrosoft.smena.R;
import com.petrosoft.smena.fragment.cobject.NameValueData;
import com.petrosoft.smena.fragment.cobject.element.NameValueElement;
import com.petrosoft.smena.view.recycler.holder.BaseViewHolder;

/**
 * Created by a.baskakov on 22/08/16.
 */
public class NameValueHolder extends BaseViewHolder<NameValueElement> {
    TextView mName;
    TextView mValue;


    public NameValueHolder(View itemView) {
        super(itemView);
        mName = (TextView) itemView.findViewById(R.id.itemDescription);
        mValue = (TextView) itemView.findViewById(R.id.value);
    }

    @Override
    public void setElement(NameValueElement element) {
        super.setElement(element);
        NameValueData nameValueData = element.getData();
        mName.setText(nameValueData.name);

        String value = nameValueData.value;
        if(TextUtils.isEmpty(value)){
            value = mName.getContext().getString(R.string.no_info);
        }

        mValue.setText(value);

    }
}
