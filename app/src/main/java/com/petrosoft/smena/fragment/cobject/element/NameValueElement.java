package com.petrosoft.smena.fragment.cobject.element;

import com.petrosoft.smena.fragment.cobject.NameValueData;
import com.petrosoft.smena.view.recycler.element.BaseElement;

/**
 * Created by a.baskakov on 22/08/16.
 */
public class NameValueElement extends BaseElement<NameValueData> {
    public NameValueElement(NameValueData data) {
        super(data);
    }
}
