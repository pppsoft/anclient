package com.petrosoft.smena.fragment.current_object;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.petrosoft.smena.R;
import com.petrosoft.smena.activity.ObjectTypeActivity;
import com.petrosoft.smena.dao.CObject;
import com.petrosoft.smena.dao.NewIssue;
import com.petrosoft.smena.db.DbHelper;
import com.petrosoft.smena.fragment.cobject.NameNumberData;
import com.petrosoft.smena.fragment.cobject.element.NameNumberElement;
import com.petrosoft.smena.fragment.cobject.element.NameValueElement;
import com.petrosoft.smena.fragment.current_object.adapter.CurrentObjectFragmentAdapter;
import com.petrosoft.smena.fragment.main.recycler.element.StringElement;
import com.petrosoft.smena.fragment.plan.BaseObjectTypeFragment;
import com.petrosoft.smena.utils.recycler_view.ItemClickSupport;
import com.petrosoft.smena.view.recycler.element.BaseElement;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by a.baskakov on 22/08/16.
 */
public class CurrentObjectFragment extends BaseObjectTypeFragment {

    long mSObjectId;
    View mRootView;
    View mContent;
    View mNoContent;
    RecyclerView mRecyclerView;
    CurrentObjectFragmentAdapter mAdapter;
    RecyclerView.LayoutManager mLayoutManager;
    Button mButtonFinishCheck;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mRootView == null) {
            mRootView = inflater.inflate(R.layout.current_object_fragment, container, false);
            mRecyclerView = (RecyclerView) mRootView.findViewById(R.id.recycler_view);
            mRecyclerView.setHasFixedSize(true);
            mContent = mRootView.findViewById(R.id.content);
            mNoContent = mRootView.findViewById(R.id.no_content);

            ItemClickSupport.addTo(mRecyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {

                @Override
                public void onItemClick(RecyclerView parent, View view, int position, long id) {
                    if (position != -1) {
                        BaseElement baseElement = mAdapter.getElement(position);
                        if (baseElement instanceof NameNumberElement) {
                            NameNumberElement nameNumberElement = (NameNumberElement) baseElement;
                            if (nameNumberElement.getData().isClickable) {
                                ObjectTypeActivity objectTypeActivity = (ObjectTypeActivity) getActivity();
                                CObject cObject = DbHelper.getCheckCObject(getActivity());
                                switch (position) {
                                    case 1:
                                        objectTypeActivity.openStateFragment(cObject, NewIssue.TYPE_NEW);
                                        break;
                                    case 2:
                                        objectTypeActivity.openTasksFragment(cObject);
                                        break;
                                    case 3:
                                        objectTypeActivity.openIssuesFragment(cObject);
                                        break;
                                    case 4:
                                        objectTypeActivity.openClaimsFragment(cObject);
                                }
                            }
                        }
                    }
                }
            });

            mButtonFinishCheck = (Button) mRootView.findViewById(R.id.button_finish_check);

            mButtonFinishCheck.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    DbHelper.finishCurrentCObject(getActivity());
                    ((ObjectTypeActivity) getActivity()).openCurrentObjectFragment();
                }
            });

            updateFromDb();

        }
        setHeader(getString(R.string.current_object));
        setClosable(DbHelper.getCurrentCheck(getActivity()) == null);

        return mRootView;
    }

    private void updateFromDb() {
        if (!isAdded()) {
            return;
        }
        CObject cObject = DbHelper.getCheckCObject(getActivity());

        if (cObject != null) {
            mContent.setVisibility(View.VISIBLE);
            mNoContent.setVisibility(View.GONE);

            mLayoutManager = new LinearLayoutManager(getActivity());
            mRecyclerView.setLayoutManager(mLayoutManager);

            List<BaseElement> list = new ArrayList<>();
            list.add(new StringElement(cObject.getName()));

            list.add(new NameNumberElement(new NameNumberData(getString(R.string.current_status), 0, cObject.getCurrent(), false)));
            list.add(new NameNumberElement(new NameNumberData(getString(R.string.tasks), cObject.getTasks().size(), cObject.getTasks().size() > 0, true)));
            list.add(new NameNumberElement(new NameNumberData(getString(R.string.issues), cObject.getIssues().size(), cObject.getIssues().size() > 0, true)));
            list.add(new NameNumberElement(new NameNumberData(getString(R.string.claims), cObject.getClaims().size(), cObject.getClaims().size() > 0, true)));

            Map<Class<? extends BaseElement>, Integer> typeMap = new HashMap<>();
            typeMap.put(StringElement.class, CurrentObjectFragmentAdapter.TYPE_STRING);
            typeMap.put(NameValueElement.class, CurrentObjectFragmentAdapter.TYPE_NAME_VALUE);
            typeMap.put(NameNumberElement.class, CurrentObjectFragmentAdapter.TYPE_NAME_NUMBER);

            mAdapter = new CurrentObjectFragmentAdapter(list, typeMap, getActivity());
            mRecyclerView.setAdapter(mAdapter);
        } else {
            mContent.setVisibility(View.GONE);
            mNoContent.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public boolean backPressed() {
        return false;
    }
}
