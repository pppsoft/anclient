package com.petrosoft.smena.fragment.task;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.petrosoft.smena.R;
import com.petrosoft.smena.activity.ObjectTypeActivity;
import com.petrosoft.smena.dao.CObject;
import com.petrosoft.smena.dao.CObjectDao;
import com.petrosoft.smena.dao.ClaimDao;
import com.petrosoft.smena.dao.DaoSession;
import com.petrosoft.smena.dao.NewIssue;
import com.petrosoft.smena.dao.Task;
import com.petrosoft.smena.dao.TaskDao;
import com.petrosoft.smena.db.DbHelper;
import com.petrosoft.smena.fragment.images.BaseImagesFragment;
import com.petrosoft.smena.server.IRequestListener;
import com.petrosoft.smena.server.RequestHelper;
import com.petrosoft.smena.utils.DialogHelper;

/**
 * Created by a.baskakov on 22/08/16.
 */
public class TaskFragment extends BaseImagesFragment {
    public static final String EXTRA_ID = "id";

    View mRootView;
    Button mButtonErrors;
    Button mButtonComptete;
    Button mButtonNotComplete;
    TextView mCObjectTitle;

    TextView mDate;
    TextView mType;
    TextView mCount;
    TextView mDescription;

    long mId;
    Task mTask;
    CObject mCObject;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mRootView == null) {
            Bundle bundle = getArguments();
            DaoSession daoSession = DbHelper.getDaoMaster(getActivity()).newSession();
            CObjectDao cObjectDao = daoSession.getCObjectDao();
            final TaskDao taskDao = daoSession.getTaskDao();
            if (bundle != null) {
                mId = bundle.getLong(EXTRA_ID);
                mTask = taskDao.queryBuilder().where(ClaimDao.Properties.Id.eq(mId)).unique();
                mCObject = cObjectDao.queryBuilder().where(CObjectDao.Properties.Id.eq(mTask.getCObjectId())).unique();
            }

            mRootView = inflater.inflate(R.layout.task_fragment, container, false);
            mCObjectTitle = (TextView) mRootView.findViewById(R.id.c_object_title);
            mDate = (TextView) mRootView.findViewById(R.id.date);
            mType = (TextView) mRootView.findViewById(R.id.type);
            mCount = (TextView) mRootView.findViewById(R.id.count);
            mDescription = (TextView) mRootView.findViewById(R.id.description);
            mButtonErrors = (Button) mRootView.findViewById(R.id.errors);
            mButtonComptete = (Button) mRootView.findViewById(R.id.complete);
            mButtonNotComplete = (Button) mRootView.findViewById(R.id.not_complete);

            mCObjectTitle.setText(mCObject.getName());

            mDate.setText(mTask.getCompleteDate());
            mType.setText(mTask.getWorkTypeName());
            mCount.setText(String.valueOf(mTask.getQuantity()));
            mDescription.setText(mTask.getContractStage());

            mButtonErrors.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((ObjectTypeActivity) getActivity()).openStateFragment(mCObject, NewIssue.TYPE_FROM_TASK);
                }
            });

            mButtonNotComplete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final AlertDialog alertDialog = DialogHelper.showProgressDialog(getActivity(), getActivity().getString(R.string.loading));
                    alertDialog.show();
                    mTask.setLastCheckId(DbHelper.getCurrentCheck(getActivity()).getId());
                    mTask.setSent(false);
                    mTask.setIsCompleted(false);
                    taskDao.update(mTask);
                    RequestHelper.tryToCompleteTaskAsync(getActivity(), mTask, new IRequestListener<Boolean>() {
                        @Override
                        public void success(Boolean element) {
                            alertDialog.hide();
                            getActivity().onBackPressed();
                        }

                        @Override
                        public void error() {
                            alertDialog.hide();
                            getActivity().onBackPressed();
                        }
                    });
                }
            });

            mButtonComptete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final AlertDialog alertDialog = DialogHelper.showProgressDialog(getActivity(), getActivity().getString(R.string.loading));
                    alertDialog.show();
                    mTask.setLastCheckId(DbHelper.getCurrentCheck(getActivity()).getId());
                    mTask.setSent(false);
                    mTask.setIsCompleted(true);
                    taskDao.update(mTask);
                    RequestHelper.tryToCompleteTaskAsync(getActivity(), mTask, new IRequestListener<Boolean>() {
                        @Override
                        public void success(Boolean element) {
                            alertDialog.hide();
                            getActivity().onBackPressed();
                        }

                        @Override
                        public void error() {
                            alertDialog.hide();
                            getActivity().onBackPressed();
                        }
                    });
                }
            });

        }
        setHeader(getString(R.string.task));
        setClosable(true);
        return mRootView;
    }

}
