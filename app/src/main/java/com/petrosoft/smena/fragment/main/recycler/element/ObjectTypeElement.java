package com.petrosoft.smena.fragment.main.recycler.element;

import com.petrosoft.smena.dao.ObjectType;
import com.petrosoft.smena.view.recycler.element.BaseElement;

/**
 * Created by a.baskakov on 22/08/16.
 */
public class ObjectTypeElement extends BaseElement<ObjectType> {
    public ObjectTypeElement(ObjectType data) {
        super(data);
    }
}
