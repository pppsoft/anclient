package com.petrosoft.smena.service;

import android.app.IntentService;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;

import com.petrosoft.smena.GreenApp;
import com.petrosoft.smena.dao.CObjectToElements;
import com.petrosoft.smena.dao.CObjectToElementsDao;
import com.petrosoft.smena.dao.DaoSession;
import com.petrosoft.smena.dao.GreenElement;
import com.petrosoft.smena.dao.GreenElementDao;
import com.petrosoft.smena.db.DbHelper;
import com.petrosoft.smena.server.response.data.add_issue.Element;
import com.petrosoft.smena.server.response.data.login.LoginData;
import com.petrosoft.smena.utils.PrefsHelper;

import java.io.IOException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Response;


public class ObjectSyncService extends IntentService {

    public static final String ACTION_SYNC_OBJECT_ELEMENTS = "com.petrosoft.smena.service.action.FOO";

    public static final String OBJECT_ID = "object_id";


    public ObjectSyncService() {
        super("ObjectSyncService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_SYNC_OBJECT_ELEMENTS.equals(action)) {
                final Long param1 = intent.getLongExtra(OBJECT_ID, -1);
                //  final String param2 = intent.getStringExtra(EXTRA_PARAM2);
                getObjectElements(param1);
                stopSelf();
            }
        }
    }

    private void getObjectElements(Long objectId) {
        LoginData loginData = PrefsHelper.loadLastLoginData(GreenApp.getInstance().getApplicationContext());
        try {
            Call<ArrayList<Element>> arrayListCall = GreenApp.getInstance().getApi().getObjectElements(objectId, loginData.mToken);

            ArrayList<Element> elements = arrayListCall.execute().body();
            final DaoSession daoSession = DbHelper.getDaoMaster(getBaseContext()).newSession();
            GreenElementDao greenElementDao = daoSession.getGreenElementDao();
            SQLiteDatabase db = DbHelper.getDaoMaster(getApplicationContext()).getDatabase();
            db.beginTransaction();


            for (Element element : elements) {
                GreenElement greenElement = greenElementDao.queryBuilder().where(GreenElementDao.Properties.Id.eq(element.id)).unique();
                if (greenElement == null) {
                    greenElement = new GreenElement();
                    greenElement.setId(element.id);
                    greenElement.setName(element.name);
                    greenElement.setInventory_number(element.inventory_number);
                    greenElement.setOid(element.oid);
                    greenElement.setElement_type_id(element.element_type_id);
                    greenElementDao.insert(greenElement);
                } else {

//always false
                }

                //find link or create
                CObjectToElementsDao cObjectToElementsDao = daoSession.getCObjectToElementsDao();
                CObjectToElements objectToElements = cObjectToElementsDao.queryBuilder()
                        .where(CObjectToElementsDao.Properties.CObjectId.eq(objectId))
                        .where(CObjectToElementsDao.Properties.ElementId.eq(greenElement.getId())).unique();
                if (objectToElements == null) {
                    CObjectToElements co = new CObjectToElements();
                    co.setCObjectId(objectId);
                    co.setElementId(greenElement.getId());
                    cObjectToElementsDao.insert(co);
                }
            }
            db.setTransactionSuccessful();
            db.endTransaction();

        } catch (IOException e) {
            e.printStackTrace();
        }


    }


}
