package com.petrosoft.smena.service;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.petrosoft.smena.GreenApp;
import com.petrosoft.smena.R;
import com.petrosoft.smena.activity.ArcGisMapActivity;
import com.petrosoft.smena.server.RequestHelper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 */
public class MapDownloadService extends IntentService {

    private NotificationCompat.Builder builder;
    private NotificationManager notificationManager;

    public MapDownloadService() {
        super("MapDownloadService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            download();
        }
    }

    private void saveFile(ResponseBody body) throws IOException {
        String oflineMapPath = GreenApp.getInstance().getExternalFilesDir("maps").getAbsolutePath() + "/iac.mbtile";
        File df = new File(oflineMapPath);


        InputStream inputStream;
        OutputStream outputStream;
        byte[] fileReader = new byte[65536];

        long fileSize = body.contentLength();
        long fileSizeDownloaded = 0;

        inputStream = body.byteStream();
        outputStream = new FileOutputStream(df);

        long startTime = System.currentTimeMillis();
        int timeCount = 1;
        notificationManager.notify(1, builder.build());
        while (true) {
            int read = inputStream.read(fileReader);

            if (read == -1) {
                break;
            }

            outputStream.write(fileReader, 0, read);

            fileSizeDownloaded += read;
            long currentTime = System.currentTimeMillis() - startTime;
            if (currentTime > 5000 * timeCount) {

                builder.setProgress(((int) fileSize), ((int) fileSizeDownloaded), false);
                notificationManager.notify(1, builder.build());

                timeCount++;
            }

            Log.d("DOWNLOADER", "file download: " + fileSizeDownloaded + " of " + fileSize);
            outputStream.flush();
        }
        outputStream.flush();
        outputStream.close();
        inputStream.close();
        boolean isOk = df.renameTo(new File(df.getAbsolutePath() + "s"));


    }

    private void download() {

        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        builder = new NotificationCompat.Builder(this);
        builder.setContentTitle("Загрузка данных")
                .setAutoCancel(false)
                .setContentText("Идет загрузка тайлового слоя")
                .setSmallIcon(R.drawable.ic_sync_black_24dp);
        builder.setProgress(0, 0, false);
        //notificationManager.notify(1, builder.build());
        Call<ResponseBody> rb = GreenApp.getInstance().getApi().downloadMbTilesMap17();
        try {
            builder.build();
            saveFile(rb.execute().body());
            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            builder.setSound(defaultSoundUri);
            builder.setContentText("Загрузка завершена").setProgress(0, 0, false);
            builder.setVibrate(new long[]{1000, 1000});
            notificationManager.notify(1, builder.build());
        } catch (IOException e) {
            Uri errSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            builder.setSound(errSound);
            builder.setVibrate(new long[]{1000, 1000});
            builder.setContentText("Загрузка не удалась").setProgress(0, 0, false);
            notificationManager.notify(1, builder.build());
        }


    }
}