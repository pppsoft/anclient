package com.petrosoft.smena.service;

import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.IBinder;
import android.util.Log;

import com.petrosoft.smena.GreenApp;
import com.petrosoft.smena.activity.LoginActivity;
import com.petrosoft.smena.dao.CObject;
import com.petrosoft.smena.dao.CObjectDao;
import com.petrosoft.smena.dao.CObjectToElements;
import com.petrosoft.smena.dao.CObjectToElementsDao;
import com.petrosoft.smena.dao.Claim;
import com.petrosoft.smena.dao.ClaimDao;
import com.petrosoft.smena.dao.DaoSession;

import com.petrosoft.smena.dao.GreenElement;
import com.petrosoft.smena.dao.GreenElementDao;
import com.petrosoft.smena.dao.GreenElementType;
import com.petrosoft.smena.dao.GreenElementTypeDao;
import com.petrosoft.smena.dao.Issue;
import com.petrosoft.smena.dao.IssueDao;
import com.petrosoft.smena.dao.IssueType;
import com.petrosoft.smena.dao.IssueTypeDao;
import com.petrosoft.smena.dao.ObjectType;
import com.petrosoft.smena.dao.ObjectTypeDao;
import com.petrosoft.smena.dao.ObjectTypeToIssueType;
import com.petrosoft.smena.dao.ObjectTypeToIssueTypeDao;
import com.petrosoft.smena.dao.Route;
import com.petrosoft.smena.dao.RouteDao;
import com.petrosoft.smena.dao.Task;
import com.petrosoft.smena.dao.TaskDao;
import com.petrosoft.smena.dao.WorkType;
import com.petrosoft.smena.dao.WorkTypeDao;
import com.petrosoft.smena.db.DbHelper;
import com.petrosoft.smena.server.RequestHelper;
import com.petrosoft.smena.server.response.data.add_issue.Element;
import com.petrosoft.smena.server.response.data.add_issue.ElementGroup;
import com.petrosoft.smena.server.response.data.issue_types.IssueTypeData;
import com.petrosoft.smena.server.response.data.object_types.ObjectTypesData;
import com.petrosoft.smena.server.response.data.plan.CObjectData;
import com.petrosoft.smena.server.response.data.plan.ClaimData;
import com.petrosoft.smena.server.response.data.plan.IssueData;
import com.petrosoft.smena.server.response.data.plan.PlanData;
import com.petrosoft.smena.server.response.data.plan.TaskData;
import com.petrosoft.smena.server.response.data.svalka.RouteData;
import com.petrosoft.smena.server.response.data.work_types.WorkTypeData;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginSyncService extends Service {
    ExecutorService mExecutorService;
    final String LOG_TAG = "syncdata_service";

    public LoginSyncService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(LOG_TAG, "sync data service started");
        mExecutorService = Executors.newFixedThreadPool(2);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(LOG_TAG, "Service destroyed");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        String token = intent.getStringExtra("token");
        String loginDate = intent.getStringExtra("date");
        PendingIntent pi = intent.getParcelableExtra(LoginActivity.PARAM_PINTENT);
        SyncRun syncRun = new SyncRun(token, loginDate, startId, pi);
        mExecutorService.execute(syncRun);
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public IBinder onBind(Intent intent) {

        return null;
        //throw new UnsupportedOperationException("Not yet implemented");
    }

    class SyncRun implements Runnable {
        String mToken;
        String mLoginDate;
        int startId;
        PendingIntent mPendingIntent;

        public SyncRun(String token, String loginDate, int startId, PendingIntent pendingIntent) {
            mLoginDate = loginDate;
            mToken = token;
            this.startId = startId;
            mPendingIntent = pendingIntent;
        }


        @Override
        public void run() {
            try {
                Long l1 = System.currentTimeMillis();
                clearDb();
                Long l2 = System.currentTimeMillis();
                loadElementTypes();
                loadObjectTypes();
                Long l3 = System.currentTimeMillis();
                loadIssueTypes();
                Long l4 = System.currentTimeMillis();
                loadWorkTypes();
                Long l5 = System.currentTimeMillis();
                loadMyPlan();
                Long l6 = System.currentTimeMillis();
                Log.d("Load time", String.format("%1$s %2$s %3$s %4$s %5$s", l2 - l1, l3 - l2, l4 - l3, l5 - l4, l6 - l5));
                Log.d(LOG_TAG, "SYNC done");
            } catch (Exception e) {
                Log.e(LOG_TAG, "SYNC FAILED");
                e.printStackTrace();
                Intent intent = new Intent().putExtra("result", false);
                try {
                    mPendingIntent.send(LoginSyncService.this, LoginActivity.STATUS_FINISH, intent);
                } catch (PendingIntent.CanceledException exc) {
                    exc.printStackTrace();
                }
            }
            Intent intent = new Intent().putExtra("result", true);
            try {
                mPendingIntent.send(LoginSyncService.this, LoginActivity.STATUS_FINISH, intent);
            } catch (PendingIntent.CanceledException e) {
                e.printStackTrace();
            }

            Log.d(LOG_TAG, "SYNC shutdown");
            stopSelfResult(startId);

        }

        private void loadMyPlan() throws IOException {
            Call<PlanData[]> call = GreenApp.getInstance().getApi().plan(mToken, mLoginDate);
            Response<PlanData[]> response = call.execute();
            if (response.isSuccessful()) {

                DaoSession daoSession = DbHelper.getDaoMaster(getApplicationContext()).newSession();
                SQLiteDatabase db = DbHelper.getDaoMaster(getApplicationContext()).getDatabase();
                db.beginTransaction();
                CObjectDao cObjectDao = daoSession.getCObjectDao();
                TaskDao taskDao = daoSession.getTaskDao();
                IssueDao issueDao = daoSession.getIssueDao();
                ClaimDao claimDao = daoSession.getClaimDao();
                RouteDao routeDao = daoSession.getRouteDao();
                CObjectToElementsDao cObjectToElementsDao = daoSession.getCObjectToElementsDao();
                GreenElementDao greenElementDao = daoSession.getGreenElementDao();

                for (PlanData planData : response.body()) {
                    List<CObjectData> cObjectDatas = planData.mCObjects;
                    for (CObjectData cObjectData : cObjectDatas) {
                        CObject cObject = cObjectDao.queryBuilder().where(CObjectDao.Properties.ServerId.eq(cObjectData.mId)).unique();
                        if (cObject == null) {
                            cObject = new CObject();
                            cObject.setServerId(cObjectData.mId);
                            cObject.setId((long) cObjectData.mId);
                            cObject.setName(cObjectData.mName);
                            cObject.setOrganisations(cObjectData.mOrganisaitons);
                            cObject.setOid(cObjectData.mOid);
                            // cObject.setObjectTypeServerId(cObjectData.mObjectTypeId);
                            cObject.setObjectTypeId(cObjectData.mObjectTypeId);
                            cObject.setCurrent(cObjectData.mCurrent);
                            cObject.setCheck(false);
                            cObjectDao.insert(cObject);
                            if (cObjectData.mElements != null) {
                                for (Element element : cObjectData.mElements) {
                                    GreenElement greenElement = new GreenElement();
                                    greenElement.setId(element.id);
                                    greenElement.setOid(element.oid);
                                    greenElement.setElement_type_id(element.element_type_id);
                                    greenElement.setInventory_number(element.inventory_number);
                                    greenElement.setName(element.name);

                                    long eid = greenElementDao.insert(greenElement);
                                    CObjectToElements cto = new CObjectToElements();
                                    cto.setElementId(greenElement.getId());
                                    cto.setCObjectId(cObject.getServerId());
                                    cObjectToElementsDao.insert(cto);
                                    Log.v("plan elements", "Inserted element " + String.valueOf(eid));
                                }
                            }
                        }
                        if (cObjectData.mTasks != null) {
                            for (TaskData taskData : cObjectData.mTasks) {

                                Task task = taskDao.queryBuilder().where(TaskDao.Properties.ServerId.eq(taskData.id)).unique();
                                if (task == null) {
                                    task = new Task();
                                    task.setCObjectId(cObject.getId());
                                    task.setServerId(taskData.id);
                                    task.setWorkTypeId(taskData.work_type_id);
                                    task.setWorkTypeName(taskData.work_type_name);
                                    task.setQuantity(1.0f);
                                    task.setCompleteDate(taskData.complete_date);
                                    task.setContractStage(taskData.contract_stage);
                                    taskDao.insert(task);
                                }
                            }
                        }
                        if (cObjectData.mIssues != null) {
                            for (IssueData issueData : cObjectData.mIssues) {
                                Issue issue = issueDao.queryBuilder().where(IssueDao.Properties.ServerId.eq(issueData.id)).unique();
                                if (issue == null) {
                                    issue = new Issue();
                                    issue.setCObjectId(cObject.getId());
                                    issue.setServerId(issueData.id);
                                    issue.setLatitude(issueData.latitude);
                                    issue.setLongtitude(issueData.longtitude);
                                    issue.setEventDate(issueData.event_date);
                                    issue.setCustomAddress(issueData.custom_address);
                                    issue.setIssueTypes(issueData.issue_types);
                                    issue.setComment(issueData.comment);
                                    issue.setIsCompleted(issueData.is_completed);
                                    issueDao.insert(issue);
                                }
                            }
                        }

                        if (cObjectData.mClaims != null) {
                            for (ClaimData claimData : cObjectData.mClaims) {
                                Claim claim = claimDao.queryBuilder().where(ClaimDao.Properties.ServerId.eq(claimData.id)).unique();
                                if (claim == null) {
                                    claim = new Claim();
                                    claim.setCObjectId(cObject.getId());
                                    claim.setServerId(claimData.id);
                                    claim.setName(claimData.name);
                                    claim.setComment(claimData.comment);
                                    claim.setCustomAddress(claimData.custom_address);
                                    claim.setGreenObjectId((long) claimData.green_object_id);
                                    claim.setIsCompleted(claimData.is_completed);
                                    claim.setClaimStatusId((long) claimData.claim_status_id);
                                    claim.setLatitude(claimData.latitude);
                                    claim.setLongtitude(claimData.longtitude);
                                    claim.setIsArchive(claimData.is_archive);
                                    claim.setRegisteredAt(claimData.registered_at);
                                    claim.setExtId((long) claimData.ext_id);
                                    claim.setIsExternal(claimData.is_external);
                                    claim.setLastExternalUpdate(claimData.last_external_update);
                                    claim.setLastExternalStatusUpdate(claimData.last_external_status_update);
                                    claim.setCityObjectName(claimData.city_object_name);
                                    claim.setCategoryName(claimData.category_name);
                                    claim.setDistrictId((long) claimData.district_id);
                                    claim.setExpectedAnswerDt(claimData.expected_answer_dt);
                                    claim.setExecutor(claimData.executor);
                                    claim.setExtStatusName(claimData.ext_status_name);
                                    claimDao.insert(claim);
                                }
                            }
                        }
                    }
                    if (planData.mRouteDatas != null) {
                        for (RouteData routeDataData : planData.mRouteDatas) {
                            Route route = routeDao.queryBuilder().where(RouteDao.Properties.Id.eq(routeDataData.getId())).unique();
                            if (route == null) {
                                route = new Route();
                                Long object_id = routeDataData.getObject_id();
                                route.setCobject_id(object_id);
                                route.setName(routeDataData.getName());
                                route.setId(routeDataData.getId());
                                route.setIs_completed(false);
                                route.setLatitude(routeDataData.getLatitude());
                                route.setLongtitude(routeDataData.getLongitude());
                                route.setIs_visible(routeDataData.getIsVisible());
                                routeDao.insert(route);

                            }
                        }
                    }
                }
                db.setTransactionSuccessful();
                db.endTransaction();


            }

        }

        private void loadIssueTypes() throws IOException {
            Call<IssueTypeData[]> call = GreenApp.getInstance().getApi().issueTypes(mToken);
            Response<IssueTypeData[]> response = call.execute();
            if (response.isSuccessful()) {
                SQLiteDatabase db = DbHelper.getDaoMaster(getApplicationContext()).getDatabase();
                db.beginTransaction();
                DaoSession daoSession = DbHelper.getDaoMaster(getApplicationContext()).newSession();
                IssueTypeDao issueTypeDao = daoSession.getIssueTypeDao();
                ObjectTypeToIssueTypeDao objectTypeToIssueTypeDao = daoSession.getObjectTypeToIssueTypeDao();
                for (IssueTypeData issueTypeData : response.body()) {
                    IssueType issueType = new IssueType();
                    issueType.setServerId(issueTypeData.id);
                    issueType.setName(issueTypeData.name);
                    issueTypeDao.insert(issueType);
                    for (ObjectTypesData objectTypeData : issueTypeData.object_types) {
                        ObjectTypeToIssueType objectTypeToIssueType = new ObjectTypeToIssueType();
                        objectTypeToIssueType.setIssueTypeId(issueType.getId());
                        objectTypeToIssueType.setObjectTypeServerId(objectTypeData.mId);
                        objectTypeToIssueTypeDao.insert(objectTypeToIssueType);
                    }
                }
                db.setTransactionSuccessful();
                db.endTransaction();

            }

        }

        private void loadWorkTypes() throws IOException {
            Call<WorkTypeData[]> call = GreenApp.getInstance().getApi().workTypes(mToken);
            Response<WorkTypeData[]> response = call.execute();

            if (response.isSuccessful()) {
                SQLiteDatabase db = DbHelper.getDaoMaster(getApplicationContext()).getDatabase();
                db.beginTransaction();

                DaoSession daoSession = DbHelper.getDaoMaster(getApplicationContext()).newSession();
                WorkTypeDao workTypeDao = daoSession.getWorkTypeDao();
                for (WorkTypeData workTypeData : response.body()) {
                    WorkType workType = new WorkType();
                    workType.setServerId(workTypeData.id);
                    workType.setName(workTypeData.name);
                    workType.setMeasure(workTypeData.measure);
                    workTypeDao.insert(workType);
                }
                db.setTransactionSuccessful();
                db.endTransaction();
            }
        }

        private void loadObjectTypes() throws IOException {
            Call<ObjectTypesData[]> call = GreenApp.getInstance().getApi().objectTypes(mToken);
            Response<ObjectTypesData[]> response = call.execute();
            if (response.isSuccessful()) {
                SQLiteDatabase db = DbHelper.getDaoMaster(getApplicationContext()).getDatabase();
                db.beginTransaction();
                DaoSession daoSession = DbHelper.getDaoMaster(GreenApp.getInstance().getApplicationContext()).newSession();

                ObjectTypeDao objectTypeDao = daoSession.getObjectTypeDao();
                for (ObjectTypesData objectTypesData : response.body()) {
                    ObjectType objectType = new ObjectType();
                    objectType.setName(objectTypesData.mName);
                    objectType.setId((long) objectTypesData.mId);
                    objectType.setServerId(objectTypesData.mId);
                    objectType.setGisLayerUrl(objectTypesData.mGisLayerUrl);
                    objectTypeDao.insert(objectType);
                }
                db.setTransactionSuccessful();
                db.endTransaction();
            }
        }


        private void clearDb() {

            DaoSession daoSession = DbHelper.getDaoMaster(getApplicationContext()).newSession();
            SQLiteDatabase db = DbHelper.getDaoMaster(getApplicationContext()).getDatabase();
            db.beginTransaction();

            daoSession.getObjectTypeDao().deleteAll();

            daoSession.getCObjectDao().deleteAll();
            daoSession.getClaimDao().deleteAll();
            daoSession.getIssueDao().deleteAll();
            daoSession.getTaskDao().deleteAll();
            daoSession.getCheckTableDao().deleteAll();
            daoSession.getCheckToObjectDao().deleteAll();
            daoSession.getWorkTypeDao().deleteAll();
            daoSession.getNewIssueDao().deleteAll();
            daoSession.getIssueTypeDao().deleteAll();
            daoSession.getObjectTypeToIssueTypeDao().deleteAll();
            daoSession.getNewIssueImageDao().deleteAll();
            daoSession.getClaimImageDao().deleteAll();
            daoSession.getIssueImageDao().deleteAll();
            daoSession.getTrackDao().deleteAll();
            daoSession.getGreenElementDao().deleteAll();
            daoSession.getGreenElementTypeDao().deleteAll();
            daoSession.getCObjectToElementsDao().deleteAll();
            daoSession.getRouteDao().deleteAll();

            db.setTransactionSuccessful();
            db.endTransaction();
        }

    }

    private void loadElementTypes() {
        RequestHelper.getElementTypes(new Callback<List<ElementGroup>>() {
            @Override
            public void onResponse(Call<List<ElementGroup>> call, Response<List<ElementGroup>> response) {
                if (response.isSuccessful()) {
                    SQLiteDatabase db = DbHelper.getDaoMaster(getApplicationContext()).getDatabase();
                    db.beginTransaction();
                    DaoSession daoSession = DbHelper.getDaoMaster(GreenApp.getInstance().getApplicationContext()).newSession();

                    GreenElementTypeDao greenElementTypeDao = daoSession.getGreenElementTypeDao();
                    for (ElementGroup eg : response.body()) {
                        GreenElementType greenElementType = new GreenElementType();
                        greenElementType.setId(eg.id);
                        greenElementType.setName(eg.name);
                        greenElementType.setGis_layer_url(eg.gis_layer_url);
                        greenElementTypeDao.insert(greenElementType);
                    }
                    db.setTransactionSuccessful();
                    db.endTransaction();

                }
            }

            @Override
            public void onFailure(Call<List<ElementGroup>> call, Throwable t) {

            }
        });
    }
}
