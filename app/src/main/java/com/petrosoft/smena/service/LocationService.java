package com.petrosoft.smena.service;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;

import com.petrosoft.smena.R;
import com.petrosoft.smena.activity.LoginActivity;
import com.petrosoft.smena.dao.DaoSession;
import com.petrosoft.smena.dao.Track;
import com.petrosoft.smena.dao.TrackDao;
import com.petrosoft.smena.db.DbHelper;
import com.petrosoft.smena.fragment.map.LocationData;
import com.petrosoft.smena.server.IRequestListener;
import com.petrosoft.smena.server.RequestHelper;
import com.petrosoft.smena.server.response.data.geocoding.GeoData;
import com.petrosoft.smena.server.response.data.login.LoginData;
import com.petrosoft.smena.utils.LocationHelper;
import com.petrosoft.smena.utils.PrefsHelper;

import java.text.SimpleDateFormat;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LocationService extends Service {
    private static final int NOTIFICATION_ID = 1001;

    LocationListener mFineLocationListener;

    long mLastLocationChangeTime = 0;
    private static final int MIN_LOCATION_CHANGE_PERIOD = 5 * 1000;

    public static final String EXTRA_START = "com.petrosoft.smena.service.start";

    // Intent myIntent;
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        stopLocationUpdates();
    }

    private void stopLocationUpdates() {
        if (mFineLocationListener != null) {
            LocationHelper.stopLocationUpdates(this, mFineLocationListener);
            mFineLocationListener = null;
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if (intent != null) {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                boolean start = bundle.getBoolean(EXTRA_START, false);
                final LoginData loginData = PrefsHelper.loadLastLoginData(LocationService.this);
                if (start && loginData != null) {
                    Location location = LocationHelper.getLastKnownLocation(this);
                    addTrackData(location);

                    NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
                    Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);

                    Intent activityIntent = new Intent(this, LoginActivity.class);

                    PendingIntent notificationPendingIntent = PendingIntent.getActivity(this, NOTIFICATION_ID, activityIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                    builder.setLargeIcon(bitmap)
                            .setContentTitle(getString(R.string.app_name))
//                            .setContentText(getString(R.string.app_name))
                            .setSmallIcon(R.drawable.marker)
                            .setAutoCancel(false)
                            .setContentIntent(notificationPendingIntent)
                            .setPriority(Notification.PRIORITY_MAX);

                    Notification notification = builder.build();

                    mFineLocationListener = new LocationListener() {
                        @Override
                        public void onLocationChanged(Location location) {
                            if (location != null) {
                                addTrackData(location);
                                LocationData.latitude = (float) location.getLatitude();
                                LocationData.longitude = (float) location.getLongitude();
//                                RequestHelper.reverseGeoCoding(location.getLatitude(), location.getLongitude(), new Callback<GeoData>() {
//                                    @Override
//                                    public void onResponse(Call<GeoData> call, Response<GeoData> response) {
//                                        if (response.isSuccessful()) {
//                                            String address = response.body().address;
//                                            if (address != null) {
//                                                LocationData.address = address;
//                                            }
//                                        }
//                                    }
//
//                                    @Override
//                                    public void onFailure(Call<GeoData> call, Throwable t) {
//
//                                    }
//                                });
                            }
                        }

                        @Override
                        public void onStatusChanged(String s, int i, Bundle bundle) {

                        }

                        @Override
                        public void onProviderEnabled(String s) {

                        }

                        @Override
                        public void onProviderDisabled(String s) {

                        }
                    };

                    LocationHelper.requestLocationUpdates(this, mFineLocationListener);

                    startForeground(NOTIFICATION_ID, notification);
                } else {
                    stopForeground(true);
                    stopLocationUpdates();
                    stopSelf();
                }
            }
        }
        return START_STICKY;
    }

    private void addTrackData(Location location) {
        final LoginData loginData = PrefsHelper.loadLastLoginData(LocationService.this);
        if (loginData == null || location == null) {
            return;
        }
        Date date = new Date();
        Long session_number;
        try {
            session_number = loginData.mDate.getTime();
        } catch (Exception e) {
            session_number = date.getTime();
        }


        long curTime = date.getTime();
        if (curTime > mLastLocationChangeTime + MIN_LOCATION_CHANGE_PERIOD) {
            mLastLocationChangeTime = curTime;
            DaoSession daoSession = DbHelper.getDaoMaster(LocationService.this).newSession();
            TrackDao trackDao = daoSession.getTrackDao();
            try {


                Track track = new Track();

                track.setSent(false);
                track.setLatitude(location.getLatitude());
                track.setLongtitude(location.getLongitude());
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                track.setDatetime(simpleDateFormat.format(date).replace("\"", ""));
                track.setSession(session_number);
                track.setAccuracy(location.getAccuracy());
                trackDao.insert(track);
            } catch (Exception e) {
                e.printStackTrace();
                return;
            }


            RequestHelper.addTracksAsync(LocationService.this, new IRequestListener<Boolean>() {
                @Override
                public void success(Boolean element) {

                }

                @Override
                public void error() {

                }
            });
        }
    }

    public static void start(Context context) {
        Intent serviceIntent = new Intent(
                context,
                LocationService.class
        );
        serviceIntent.putExtra(
                EXTRA_START,
                true
        );
        context.startService(serviceIntent);
    }

    public static void stop(Context context) {
        Intent serviceIntent = new Intent(
                context,
                LocationService.class
        );
        serviceIntent.putExtra(
                EXTRA_START,
                false
        );
        context.startService(serviceIntent);
    }

}
