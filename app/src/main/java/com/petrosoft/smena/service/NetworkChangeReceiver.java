package com.petrosoft.smena.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.petrosoft.smena.GreenApp;


public class NetworkChangeReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        //final LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(context);
        Log.v("Changed", "Network");
//        Intent backIntent = new Intent(LoginActivity.INET_CHECK);
//        broadcastManager.sendBroadcast(backIntent);

        if (GreenApp.checkNetwork()) {
            //on network available
            Log.v("NETWORK OK","Sending photos");
            Intent si = new Intent(context, SentPendingDataService.class);
            // si.setAction(SendReportService.ACTION_MULTI_SEND);
            context.startService(si);


        }
    }
}
