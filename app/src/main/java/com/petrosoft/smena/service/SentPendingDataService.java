package com.petrosoft.smena.service;

import android.app.IntentService;
import android.content.Intent;

import com.petrosoft.smena.GreenApp;
import com.petrosoft.smena.dao.DaoSession;
import com.petrosoft.smena.dao.PendingImage;
import com.petrosoft.smena.dao.PendingImageDao;
import com.petrosoft.smena.db.DbHelper;

import java.io.File;
import java.io.IOException;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Response;


public class SentPendingDataService extends IntentService {
    final String ACTION_PENDING_ISSUE_CLOSED = "pi_close";

    public SentPendingDataService() {
        super("SentPendingDataService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        String action = intent.getAction();
        if (action == null) {
            final DaoSession daoSession = DbHelper.getDaoMaster(this).newSession();

            PendingImageDao pendingImageDao = daoSession.getPendingImageDao();
            List<PendingImage> pidao = pendingImageDao.queryBuilder().where(PendingImageDao.Properties.Is_completed.eq(0)).list();
            for (PendingImage pendingImage : pidao) {
                File file = new File(pendingImage.getImage_path());
                RequestBody requestFile =
                        RequestBody.create(MediaType.parse("multipart/form-data"), file);

                MultipartBody.Part body =
                        MultipartBody.Part.createFormData("file", file.getName(), requestFile);
                try {
                    Response<ResponseBody> response = GreenApp.getInstance().getPendingImageApi().attachFile(pendingImage.getEntity_id(), pendingImage.getImage_code(), body,pendingImage.getPoint_id()).execute();
                    if (response.isSuccessful()) {
                        pendingImage.setIs_completed(true);
                        pendingImageDao.update(pendingImage);

                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else {
            switch (action) {
                case ACTION_PENDING_ISSUE_CLOSED:
                    break;
            }
        }
        stopSelf();

    }


}
