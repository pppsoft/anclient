package com.petrosoft.smena.adapters;

import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.petrosoft.smena.GreenApp;
import com.petrosoft.smena.R;
import com.petrosoft.smena.activity.svalka.AddEditSvalkaActivity;
import com.petrosoft.smena.activity.svalka.AddSvalkaIssuerActivity;
import com.petrosoft.smena.activity.svalka.BorderSetupActivity;
import com.petrosoft.smena.activity.svalka.InformationBoardActivity;
import com.petrosoft.smena.activity.svalka.IssuesFixationActivity;
import com.petrosoft.smena.activity.svalka.LiquidationResultActivity;
import com.petrosoft.smena.activity.svalka.SvalkaPatrolActivity;
import com.petrosoft.smena.activity.svalka.VideoFixActivity;
import com.petrosoft.smena.server.response.data.svalka.RoadMap;

import java.util.List;

public class RoadMapAdapter extends RecyclerView.Adapter<RoadMapAdapter.RoadMapViewHolder> {

    List<RoadMap> roadMaps;
    long mSvalkaId;

    public RoadMapAdapter(List<RoadMap> roadmaps, long svalkaId) {
        this.roadMaps = roadmaps;
        this.mSvalkaId = svalkaId;
    }

    @Override
    public RoadMapViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.roadmap_card_single, parent, false);
        RoadMapViewHolder rvh = new RoadMapViewHolder(v);
        return rvh;

    }


    @Override
    public void onBindViewHolder(RoadMapViewHolder holder, final int position) {
        holder.headText.setText(roadMaps.get(position).name);
        holder.changeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RoadMap roadMap = roadMaps.get(position);
                Log.v("Element clicked:", roadMap.code);

                switch (roadMap.code) {

                    case "2":
                        Intent intent = new Intent(view.getContext(), AddEditSvalkaActivity.class);
                        intent.putExtra("svalka_id", mSvalkaId);
                        view.getContext().startActivity(intent);
                        break;
                    case "3.1":
                        Intent intentbs = new Intent(view.getContext(), BorderSetupActivity.class);
                        intentbs.putExtra("svalka_id", mSvalkaId);
                        view.getContext().startActivity(intentbs);
                        break;
                    case "3.3":
                        Intent intenpat = new Intent(view.getContext(), SvalkaPatrolActivity.class);
                        intenpat.putExtra("svalka_id", mSvalkaId);
                        view.getContext().startActivity(intenpat);
                        break;
                    case "3.4":
                        Intent is = new Intent(view.getContext(), InformationBoardActivity.class);
                        is.putExtra("svalka_id", mSvalkaId);
                        view.getContext().startActivity(is);

                        break;
                    case "3.5":
                        Intent vs = new Intent(view.getContext(), VideoFixActivity.class);
                        vs.putExtra("svalka_id", mSvalkaId);
                        view.getContext().startActivity(vs);

                        break;
                    case "4":
                        Intent intent_issue = new Intent(view.getContext(), AddSvalkaIssuerActivity.class);
                        intent_issue.putExtra("svalka_id", mSvalkaId);
                        view.getContext().startActivity(intent_issue);
                        break;

                    case "12":
                        Intent lr = new Intent(view.getContext(), LiquidationResultActivity.class);
                        lr.putExtra("svalka_id", mSvalkaId);
                        view.getContext().startActivity(lr);
                        //Intent
                        break;
                    case "13":
                        Intent llll = new Intent(view.getContext(), IssuesFixationActivity.class);
                        llll.putExtra("svalka_id", mSvalkaId);
                        view.getContext().startActivity(llll);
                        //Intent
                        break;
                    default:
                        GreenApp.ownSnackbar(view, "Не реализовано : " + roadMap.code).show();
                        break;

                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return roadMaps.size();
    }

    public static class RoadMapViewHolder extends RecyclerView.ViewHolder {
        CardView cw;
        TextView headText;
        Button changeButton;

        public RoadMapViewHolder(final View itemView) {
            super(itemView);
            cw = (CardView) itemView.findViewById(R.id.elem_view);
            headText = (TextView) itemView.findViewById(R.id.head_title);
            changeButton = (Button) itemView.findViewById(R.id.change_element);


        }
    }


}
