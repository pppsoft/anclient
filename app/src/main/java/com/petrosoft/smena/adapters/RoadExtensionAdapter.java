package com.petrosoft.smena.adapters;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.petrosoft.smena.R;
import com.petrosoft.smena.activity.SelectRoadExtensionActivity;
import com.petrosoft.smena.server.response.data.add_issue.RoadExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by kovalev on 22.12.16.
 */

public class RoadExtensionAdapter extends RecyclerView.Adapter<RoadExtensionAdapter.Holder> {
    private final SelectRoadExtensionActivity mActivity;
    private List<RoadExtension> mRoadExtensions;


    public RoadExtensionAdapter(SelectRoadExtensionActivity activity) {
        mActivity = activity;
        mRoadExtensions = new ArrayList<>();
    }


    public void setItems(List<RoadExtension> svalkas) {
        mRoadExtensions = svalkas;
    }


    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_road_extension, parent, false);
        Holder rvh = new Holder(v);

        return rvh;
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        final RoadExtension s = mRoadExtensions.get(position);


        holder.dst.setText(s.getDst());
        holder.src.setText(s.getSrc());
        holder.width.setText(String.format(Locale.getDefault(),"%.2f",s.getWidth()));
        holder.length.setText(String.format(Locale.getDefault(),"%.2f",s.getLength()));
        holder.location.setText(s.getLocation());
        holder.square.setText(String.format(Locale.getDefault(),"%.2f",s.getSquare()));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.putExtra(SelectRoadExtensionActivity.EXTENSION, s);
                mActivity.setResult(Activity.RESULT_OK, intent);
                mActivity.finish();


            }
        });


    }


    @Override
    public int getItemCount() {
        return mRoadExtensions.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        @BindView(R.id.width)
        TextView width;

        @BindView(R.id.length)
        TextView length;

        @BindView(R.id.location)
        TextView location;

        @BindView(R.id.square)
        TextView square;

        @BindView(R.id.src)
        TextView src;
        @BindView(R.id.dst)
        TextView dst;

        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
