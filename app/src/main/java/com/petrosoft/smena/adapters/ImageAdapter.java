package com.petrosoft.smena.adapters;

import android.net.Uri;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.petrosoft.smena.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by kovalev on 23.03.17.
 */

public class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.ImageViewHolder>  {
    ImageRemover mMainActivity;

    public ImageAdapter(ImageRemover activity) {
        mMainActivity = activity;
        mImages = new ArrayList<>();
    }

    public void addImage(Uri imagePath) {
        mImages.add(imagePath);
    }

    private List<Uri> mImages;

    public void deleteImage(int position) {
        mImages.remove(position);

    }


    @Override
    public ImageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_image, parent, false);
        ImageViewHolder rvh = new ImageViewHolder(v);
        return rvh;
    }

    @Override
    public void onBindViewHolder(final ImageViewHolder holder, final int position) {
        final Uri repObj = mImages.get(position);
        Glide.with(holder.image_view.getContext()).load(repObj).centerCrop().into(holder.image_view);
        holder.mDeleteImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             mMainActivity.notifyFileRemoved(position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return mImages.size();
    }

    public class ImageViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.lay)
        CardView mCardView;
        @BindView(R.id.delete_photo_btn)
        TextView mDeleteImageButton;
        @BindView(R.id.image_view)
        ImageView image_view;

        public ImageViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
