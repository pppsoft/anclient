package com.petrosoft.smena.adapters;

import android.content.Intent;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.petrosoft.smena.R;
import com.petrosoft.smena.activity.svalka.RoutePointActivity;
import com.petrosoft.smena.activity.svalka.SvalkaRoadmapActivity;
import com.petrosoft.smena.server.response.data.SimpleItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by kovalev on 22.12.16.
 */

public class SimpleItemAdapter extends RecyclerView.Adapter<SimpleItemAdapter.SvalkaViewHolder> {
    private List<SimpleItem> mSvalkas;
    private BEHAVIOUR mBEHAVIOUR;

    public SimpleItemAdapter() {
        mSvalkas = new ArrayList<>();
    }

    public void setBehaviour(BEHAVIOUR behaviour) {
        mBEHAVIOUR = behaviour;

    }

    public void setItems(List<SimpleItem> svalkas) {
        mSvalkas = svalkas;
    }


    @Override
    public SvalkaViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_svalka, parent, false);
        SvalkaViewHolder rvh = new SvalkaViewHolder(v);

        return rvh;
    }

    @Override
    public void onBindViewHolder(SvalkaViewHolder holder, int position) {
        final SimpleItem s = mSvalkas.get(position);
        holder.svalka_name.setText(s.getName());
        if ( s.getIsCompleted() ) {
            holder.svalka_name.setPaintFlags(holder.svalka_name.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (mBEHAVIOUR) {
                    case SVALKA: {
                        Intent intent = new Intent(v.getContext(), SvalkaRoadmapActivity.class);
                        intent.putExtra("svalka_id", s.getId());
                        intent.putExtra("name", s.getName());
                        v.getContext().startActivity(intent);
                    }
                    break;
                    case ROUTE: {
                        Intent intent = new Intent(v.getContext(), RoutePointActivity.class);
                        intent.putExtra(RoutePointActivity.ID, s.getId());
                        intent.putExtra(RoutePointActivity.NAME, s.getName());

                        v.getContext().startActivity(intent);
                    }
                    break;
                }


            }
        });


    }


    @Override
    public int getItemCount() {
        return mSvalkas.size();
    }

    public class SvalkaViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.svalka_name)
        TextView svalka_name;


        public SvalkaViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
