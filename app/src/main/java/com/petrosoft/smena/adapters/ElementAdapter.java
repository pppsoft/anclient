package com.petrosoft.smena.adapters;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.petrosoft.smena.R;
import com.petrosoft.smena.activity.SelectElementActivity;
import com.petrosoft.smena.server.response.data.add_issue.Element;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by kovalev on 07.02.17.
 */

public class ElementAdapter extends RecyclerView.Adapter<ElementAdapter.ViewHolder> {
    private ArrayList<Element> mElements;
    private ArrayList<Element> mAllElements;
    private SelectElementActivity mActivity;

    public ElementAdapter(ArrayList<Element> elementDatas, SelectElementActivity ac) {
        mElements = elementDatas;
        mAllElements = mElements;

        mActivity = ac;

    }

    public void search(int gn) {
        if (gn >= 0) {
            ArrayList<Element> se = new ArrayList<>();
            for (Element e : mAllElements) {
                if (e.element_type_id == gn) {
                    se.add(e);
                }

            }
            mElements = se;
            //notifyDataSetChanged();
        } else {
            mElements = mAllElements;
            // notifyDataSetChanged();
        }
    }

    public void search(String query) {
        if (query.isEmpty()) {
            mElements = mAllElements;

        } else {

            ArrayList<Element> se = new ArrayList<>();
            for (Element e : mAllElements) {

                if (e.name.toLowerCase().contains(query.toLowerCase()) ||
                        (e.inventory_number != null && e.inventory_number.toLowerCase().contains(query.toLowerCase())) ||
                        String.valueOf(e.id).contains(query)) {
                    se.add(e);
                }

            }
            mElements = se;
            //  notifyDataSetChanged();
        }


    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v2 = LayoutInflater.from(parent.getContext()).inflate(R.layout.element_item, parent, false);
        ElementAdapter.ViewHolder rvh = new ElementAdapter.ViewHolder(v2);

        return rvh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Element element = mElements.get(position);
        if (element.inventory_number != null) {
            holder.txt_inv_number.setText(element.inventory_number);
        }
        holder.element_id.setText(String.valueOf(element.id));
        holder.elem_name.setText(element.name);
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.putExtra("element", element);
                mActivity.setResult(Activity.RESULT_OK, intent);
                mActivity.finish();
            }
        });

    }

    @Override
    public int getItemCount() {
        return mElements.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.element_captionnn)
        TextView element_id;

        @BindView(R.id.txt_inv_number)
        TextView txt_inv_number;


        @BindView(R.id.text_name)
        TextView elem_name;

        @BindView(R.id.cv)
        CardView cardView;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
