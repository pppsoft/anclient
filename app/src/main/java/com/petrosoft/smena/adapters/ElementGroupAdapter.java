package com.petrosoft.smena.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.petrosoft.smena.R;
import com.petrosoft.smena.server.response.data.add_issue.ElementGroup;

/**
 * Created by kovalev on 08.02.17.
 */

public class ElementGroupAdapter extends ArrayAdapter<ElementGroup> {
    private Context mContext;
    private ElementGroup[] values;

    public ElementGroupAdapter(Context context, int resource, ElementGroup[] objects) {
        super(context, resource, objects);
        mContext = context;
        values = objects;
    }

    @Override
    public int getCount() {
        return values.length;
    }

    @Nullable
    @Override
    public ElementGroup getItem(int position) {
        return values[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        TextView label = new TextView(mContext);
        label.setTextSize(20);
        String tx = String.valueOf(values[position].name);
        if (tx.length() > 40) {
            tx = String.valueOf(values[position].name).substring(0, 40);
        }
        label.setText(tx);
        return label;
        //return super.getView(position, convertView, parent);
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        TextView label = new TextView(mContext);
        String tx = String.valueOf(values[position].name);
        if (tx.length() > 40) {
            tx = String.valueOf(values[position].name).substring(0, 40);
        }
        label.setText(tx);
        label.setTextSize(20);
        label.setTextColor(mContext.getResources().getColor(R.color.black));
        return label;
    }
}
