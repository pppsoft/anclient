package com.petrosoft.smena.adapters;

import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.petrosoft.smena.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kovalev on 16.12.16.
 */

public class SimpleImageAdapter extends RecyclerView.Adapter<ImageFileAdapter.ImageFileViewHolder> {
    private List<Uri> imageFiles;
    private List<Boolean> pbShow;

    public SimpleImageAdapter() {
        imageFiles=new ArrayList<>();
        pbShow=new ArrayList<>();
    }

    public void add(Uri u) {
        imageFiles.add(u);
        pbShow.add(true);
        notifyItemInserted(imageFiles.size() - 1);

    }
    public void hidePb(int position){
        pbShow.set(position,false);
    }

    @Override
    public ImageFileAdapter.ImageFileViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.image_view_single, parent, false);
        return new ImageFileAdapter.ImageFileViewHolder(v);
    }



    @Override
    public void onBindViewHolder(ImageFileAdapter.ImageFileViewHolder holder, int position) {
        Uri url = imageFiles.get(position);
        Glide
                .with(holder.imageView.getContext())
                .load(url)
                .placeholder(R.drawable.circular_progress_bar)
                .into(holder.imageView);
        if (pbShow.get(position)) {
            holder.uploadBar.setVisibility(View.VISIBLE);
            holder.uploadBar.setProgress(20);
            holder.uploadBar.setMax(100);


        } else {
            holder.uploadBar.setProgress(100);
            holder.uploadBar.setMax(100);
        }


    }

    @Override
    public int getItemCount() {
        return imageFiles.size();
    }
}
