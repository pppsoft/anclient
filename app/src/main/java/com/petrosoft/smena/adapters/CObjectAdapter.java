package com.petrosoft.smena.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.petrosoft.smena.R;
import com.petrosoft.smena.dao.CObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kovalev on 22.12.16.
 */

public class CObjectAdapter extends RecyclerView.Adapter<CObjectAdapter.CObjectViewHolder> {
    private List<CObject> cObjects;

    public CObjectAdapter(List<CObject> cObjects) {
        cObjects=new ArrayList<>();
        this.cObjects = cObjects;
    }

    @Override
    public CObjectAdapter.CObjectViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    public void onBindViewHolder(CObjectAdapter.CObjectViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return cObjects.size();
    }

    public class CObjectViewHolder extends RecyclerView.ViewHolder {
        TextView idView, itemDescriptionView,itemCountView;
        public CObjectViewHolder(View itemView) {
            super(itemView);
            idView =(TextView) itemView.findViewById(R.id.itemid);
            itemDescriptionView =(TextView) itemView.findViewById(R.id.itemDescription);
            itemCountView=(TextView) itemView.findViewById(R.id.itemCount);
        }
    }
}
