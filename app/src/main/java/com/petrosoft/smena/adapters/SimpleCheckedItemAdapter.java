package com.petrosoft.smena.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.petrosoft.smena.R;
import com.petrosoft.smena.server.response.data.SimpleItem;
import com.petrosoft.smena.server.response.data.svalka.WasteClass;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by kovalev on 22.12.16.
 */

public class SimpleCheckedItemAdapter extends RecyclerView.Adapter<SimpleCheckedItemAdapter.SvalkaViewHolder> {
    private List<SimpleItem> mItems;


    public SimpleCheckedItemAdapter() {

        mItems = new ArrayList<>();
    }


    public void setItems(List<SimpleItem> svalkas) {
        mItems = svalkas;
    }

    public List<Long> getChekedItemIds() {
        List<Long> result = new ArrayList<>();
        for (SimpleItem item : mItems) {
            if (item.getIsCompleted()) {
                result.add(item.getId());
            }
        }
        return result;
    }

    @Override
    public SvalkaViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_waste_class, parent, false);
        SvalkaViewHolder rvh = new SvalkaViewHolder(v);

        return rvh;
    }

    @Override
    public void onBindViewHolder(SvalkaViewHolder holder, int position) {
        final SimpleItem s = mItems.get(position);

        holder.name.setText(s.getName());
        holder.name.setChecked(s.getIsCompleted());
        holder.name.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                s.setIsCompleted(isChecked);
            }
        });


    }


    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public void setCheckedItems(List<WasteClass> wasteClasses) {
        for (SimpleItem s:mItems){
            for (SimpleItem i:wasteClasses){
                if (s.getId()==i.getId()){
                    s.setIsCompleted(true);
                }
            }
        }
    }

    public List<SimpleItem> getCheckedItems() {
        List<SimpleItem> ci= new ArrayList<>();
        for (SimpleItem s:mItems){
            if (s.getIsCompleted()){
                ci.add(s);
            }
        }
        return ci;

    }
    public List<SimpleItem> getItems() {

        return mItems;

    }

    public class SvalkaViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.waste_class_box)
        CheckBox name;
//        @BindView(R.id.svalka_width)
//        TextView svalka_width;
//        @BindView(R.id.svalka_length)
//        TextView svalka_length;
//
//        @BindView(R.id.svalka_capacity)
//        TextView svalka_capacity;
//        @BindView(R.id.waste_classes)
//        TextView svalka_waste_classes;

        public SvalkaViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
