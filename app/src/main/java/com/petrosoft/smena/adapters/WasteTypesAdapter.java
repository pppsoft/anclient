package com.petrosoft.smena.adapters;

import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;

import com.petrosoft.smena.GreenApp;
import com.petrosoft.smena.R;
import com.petrosoft.smena.server.response.data.SimpleItem;
import com.petrosoft.smena.server.response.data.svalka.MeasureType;
import com.petrosoft.smena.server.response.data.svalka.WasteClass;
import com.petrosoft.smena.server.response.data.svalka.WasteType;
import com.petrosoft.smena.server.response.data.svalka.WasteTypeComplex;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by kovalev on 22.12.16.
 */

public class WasteTypesAdapter extends RecyclerView.Adapter<WasteTypesAdapter.WasteViewHolder> {
    private List<WasteType> mSvalkas;
    private List<MeasureType> mMeasures;
    private List<Long> countList;
    private List<WasteTypeComplex> mWasteTypeComplexList;


    public WasteTypesAdapter() {
        mMeasures = new ArrayList<>();
        countList = new ArrayList<>();
        mWasteTypeComplexList = new ArrayList<>();

        GreenApp.getInstance().getSvalkaApi().getMeasures().enqueue(new Callback<List<MeasureType>>() {
            @Override
            public void onResponse(Call<List<MeasureType>> call, Response<List<MeasureType>> response) {
                if (response.isSuccessful()) {
                    mMeasures = response.body();
                }
            }

            @Override
            public void onFailure(Call<List<MeasureType>> call, Throwable t) {
                Log.v("Could not get measures", t.getMessage());
            }

        });
        mSvalkas = new ArrayList<>();

    }

    @Override
    public long getItemId(int position) {
        return mWasteTypeComplexList.get(position).getWasteTypeId();
        //return super.getItemId(position);
    }

    public List<WasteTypeComplex> getComplexWasteTypes() {
        // mWasteTypeComplexList = new ArrayList<>();
        List<WasteTypeComplex> mWasteTypeComplexListNew = new ArrayList<>();
        for (int i = 0; i < mWasteTypeComplexList.size(); i++) {
            WasteTypeComplex wtc = mWasteTypeComplexList.get(i);

            if (wtc.getIsChecked()) {
                wtc.setCount(countList.get(i));
                mWasteTypeComplexListNew.add(wtc);

            }
        }
        return mWasteTypeComplexListNew;
    }


    public void setItems(List<WasteType> svalkas) {
        mSvalkas = svalkas;
        for (WasteType s : mSvalkas) {
            WasteTypeComplex wtc = new WasteTypeComplex();
            wtc.setWasteTypeId(s.getId());
            wtc.setCount(0);

            mWasteTypeComplexList.add(wtc);
            countList.add(wtc.getCount());
        }
    }


    @Override
    public WasteViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_waste_type, parent, false);
        WasteViewHolder rvh = new WasteViewHolder(v);

        return rvh;
    }

    @Override
    public void onBindViewHolder(final WasteViewHolder holder, final int position) {
        final WasteType s = mSvalkas.get(position);


        final TextWatcher watcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    String s1 = s.toString();
                    long l = Long.parseLong(s1);
                    countList.set(position, l);

                } catch (Exception e) {
                }
            }
        };
        holder.count.addTextChangedListener(watcher);
        holder.count.setText(String.valueOf(mWasteTypeComplexList.get(position).getCount()));
        ArrayAdapter<MeasureType> measureTypeArrayAdapter = new ArrayAdapter<MeasureType>(holder.itemView.getContext(), android.R.layout.simple_spinner_item, mMeasures);

        holder.measure_spinner.setAdapter(measureTypeArrayAdapter);

        holder.measure_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int mpos, long id) {
                long typeId = mMeasures.get(mpos).getId();
                mWasteTypeComplexList.get(position).setMeasureTypeId(typeId);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        holder.waste_type_name.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mWasteTypeComplexList.get(position).setIs_checked(isChecked);
                if (isChecked) {
                    holder.measure_spinner.setVisibility(View.VISIBLE);
                    holder.count.setVisibility(View.VISIBLE);
                } else {
                    holder.measure_spinner.setVisibility(View.GONE);
                    holder.count.setVisibility(View.GONE);
                }
            }


        });

        holder.waste_type_name.setText(s.getName());
        holder.waste_type_name.setChecked(mWasteTypeComplexList.get(position).getIsChecked());
        int selectedmeasureItemId = 0;
        try {
            long measureTypeId = mWasteTypeComplexList.get(position).getMeasureTypeId();

            for (int i = 0; i < mMeasures.size(); i++) {
                if (measureTypeId == mMeasures.get(i).getId()) {
                    selectedmeasureItemId = i;
                    break;
                }
            }
        } catch (Exception exception) {


        }
        holder.measure_spinner.setSelection(selectedmeasureItemId);
    }

    public void setCheckedItems(List<WasteTypeComplex> wasteClasses) {
        for (WasteTypeComplex s : wasteClasses) {
            for (WasteTypeComplex i : mWasteTypeComplexList) {
                if (i.getWasteTypeId() == s.getWasteTypeId()) {
                    i.setCount(s.getCount());
                    i.setMeasureTypeId(s.getMeasureTypeId());
                    i.setIs_checked(true);
                }
            }
        }
    }

    @Override
    public int getItemCount() {
        return mSvalkas.size();
    }

    class WasteViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.waste_type_box)
        CheckBox waste_type_name;

        @BindView(R.id.count)
        EditText count;

        @BindView(R.id.measure_spinner)
        Spinner measure_spinner;


        public WasteViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
