package com.petrosoft.smena.adapters;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.petrosoft.smena.R;
import com.petrosoft.smena.server.response.data.svalka.ImageFile;
import com.petrosoft.smena.utils.PrefsHelper;

import java.util.List;

public class ImageFileAdapter extends RecyclerView.Adapter<ImageFileAdapter.ImageFileViewHolder> {

    private List<ImageFile> imageFiles;

    public ImageFileAdapter(List<ImageFile> imageFiles) {
        this.imageFiles = imageFiles;

    }

    public void add(ImageFile f) {
        imageFiles.add(f);
        notifyItemInserted(getItemCount());
    }

    @Override
    public ImageFileViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.image_view_single, parent, false);
        return new ImageFileViewHolder(v);

    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public void onBindViewHolder(ImageFileViewHolder holder, final int position) {

        String url = PrefsHelper.loadServerUrl(holder.itemView.getContext()) + "uploads/" + imageFiles.get(position).mini_path;
        Glide
                .with(holder.imageView.getContext())
                .load(url)
                .placeholder(R.drawable.circular_progress_bar)
                .into(holder.imageView);
        holder.uploadBar.setVisibility(View.GONE);

    }

    @Override
    public int getItemCount() {
        return imageFiles.size();
    }

    public static class ImageFileViewHolder extends RecyclerView.ViewHolder {
        CardView cw;
        ImageView imageView;
        ProgressBar uploadBar;

        public ImageFileViewHolder(final View itemView) {
            super(itemView);
            cw = (CardView) itemView.findViewById(R.id.elem_view);
            imageView = (ImageView) itemView.findViewById(R.id.image);
            uploadBar = (ProgressBar) itemView.findViewById(R.id.uploading_bar);

        }
    }


}
