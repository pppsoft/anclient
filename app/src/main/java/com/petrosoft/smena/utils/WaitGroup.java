package com.petrosoft.smena.utils;

/**
 * Created by kovalev on 18.05.17.
 */

public class WaitGroup {
    private int jobs = 0;

    public synchronized void add(int i) {
        jobs += i;
    }

    public synchronized void done() {
        if (--jobs == 0) {
            notifyAll();
        }
    }

    public synchronized void await() throws InterruptedException {
        while (jobs > 0) {
            wait();
        }
    }
}
