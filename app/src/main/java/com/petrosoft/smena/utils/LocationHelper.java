package com.petrosoft.smena.utils;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;

import com.petrosoft.smena.R;
import com.petrosoft.smena.server.IRequestListener;
import com.petrosoft.smena.server.RequestHelper;
import com.petrosoft.smena.server.response.data.geocoding.GeoData;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by a.baskakov on 11/04/16.
 */
public class LocationHelper {

    public static final int ACCURACY = 5;

    public static Location getLastKnownLocation(Context context) {
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return null;
        }

        Location location = null;
        LocationManager mLocationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

        List<String> providers = mLocationManager.getAllProviders();

        for (String provider : providers) {

            Location newLocation = mLocationManager.getLastKnownLocation(provider);
            if (newLocation != null) {
                if (location != null) {
                    float newAccuracy = newLocation.getAccuracy();
                    float oldAccuracy = location.getAccuracy();
                    if (newAccuracy < oldAccuracy) {
                        location = newLocation;
                    }
                } else {
                    location = newLocation;
                }
            }

        }
        return location;
    }


    public static void requestLocationUpdates(Context context, LocationListener locationListener) {
        try {
            LocationManager locationManager = (LocationManager)
                    context.getSystemService(Context.LOCATION_SERVICE);


            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            locationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER, 5000, ACCURACY, locationListener);
        } catch (Throwable ex) {
            ex.printStackTrace();
        }
    }

    public static void stopLocationUpdates(Context context, LocationListener locationListener) {
        try {
            LocationManager locationManager = (LocationManager)
                    context.getSystemService(Context.LOCATION_SERVICE);
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            locationManager.removeUpdates(locationListener);
        } catch (Throwable ex) {
            ex.printStackTrace();
        }
    }

    public static Location getDefaultLocation() {
        Location location = new Location("");//provider name is unecessary
        location.setLatitude(59.9343);//your coords of course
        location.setLongitude(30.3351);

        return location;
    }

    public static void reverseGeoCoding(double latitude, double longtitude, final Context context, final IRequestListener<String> listener) {
        RequestHelper.reverseGeoCoding(latitude, longtitude, new Callback<GeoData>() {
            @Override
            public void onResponse(Call<GeoData> call, Response<GeoData> response) {
                if (response.isSuccessful()) {
                    String address = response.body().address;
                    if (TextUtils.isEmpty(address)) {
                        listener.success(context.getString(R.string.unknown_address));
                    } else {
                        listener.success(address);
                    }
                } else {
                    listener.success(context.getString(R.string.unknown_address));
                }
            }

            @Override
            public void onFailure(Call<GeoData> call, Throwable t) {
                listener.success(context.getString(R.string.error_address));
            }
        });
    }
}
