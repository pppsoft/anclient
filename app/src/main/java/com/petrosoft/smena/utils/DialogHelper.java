package com.petrosoft.smena.utils;

import android.app.Activity;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.petrosoft.smena.R;
import com.petrosoft.smena.server.IRequestListener;

/**
 * Created by a.baskakov on 24/08/16.
 */
public class DialogHelper {
    public static AlertDialog showDialog(Activity activity, String title, String text, DialogInterface.OnClickListener ok) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                activity);

        // set title
        alertDialogBuilder.setTitle(title);

        // set dialog message
        alertDialogBuilder
                .setMessage(text)
                .setCancelable(false)
                .setPositiveButton(activity.getString(R.string.ok), ok);

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
        return alertDialog;
    }

    public static AlertDialog showSelectDialog(Activity activity, String title, ListAdapter adapter, DialogInterface.OnClickListener listClickListener) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                activity);

        // set title
        alertDialogBuilder.setTitle(title);
        alertDialogBuilder.setAdapter(adapter, listClickListener);

        // set dialog message
        alertDialogBuilder
                .setCancelable(true);

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
        return alertDialog;
    }

    public static AlertDialog showDialog(Activity activity,
                                         String title,
                                         String text,
                                         String okText,
                                         String noText,
                                         String neutralText,
                                         DialogInterface.OnClickListener ok,
                                         DialogInterface.OnClickListener no,
                                         DialogInterface.OnClickListener neutral) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                activity);

        // set title
        alertDialogBuilder.setTitle(title);

        // set dialog message
        alertDialogBuilder
                .setMessage(text)
                .setCancelable(false)
                .setPositiveButton(okText, ok)
                .setNegativeButton(noText, no)
                .setNeutralButton(neutralText, neutral);

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
        return alertDialog;
    }

    public static AlertDialog showDialog(Activity activity, String text) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                activity);

        // set dialog message
        alertDialogBuilder
                .setMessage(text)
                .setCancelable(false);

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
        return alertDialog;
    }

    public static AlertDialog showProgressDialog(Activity activity, String text) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                activity);


        LayoutInflater inflater = LayoutInflater.from(activity);
        View body = inflater.inflate(R.layout.dialog_progress, null);

        TextView textView = (TextView) body.findViewById(R.id.text);
        textView.setText(text);

        alertDialogBuilder
                .setView(body)
                .setCancelable(false);

        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
       // alertDialog.show();
        return alertDialog;
    }

    public static void ObjectAdded(final IRequestListener<Boolean> listener) {
        listener.success(true);
    }
}
