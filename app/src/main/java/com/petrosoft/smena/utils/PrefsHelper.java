package com.petrosoft.smena.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;


import com.petrosoft.smena.GreenApp;
import com.petrosoft.smena.R;
import com.petrosoft.smena.server.response.data.login.LoginData;

/**
 * Created by alexey on 05/05/15.
 */
public class PrefsHelper {
    private static final String EXTRA_PREFIX = "prefs";
    private static final String PREF_SETTINGS_FILE_NAME = EXTRA_PREFIX + "prefs.xml";
    private static final String PREFS_LAST_LOGIN_DATA = EXTRA_PREFIX + ".last_login_data";

    public synchronized static void saveServerUrl(String serverurl, Context context) {
        if (!serverurl.endsWith("/")) {
            serverurl += "/";

        }
        SharedPreferences settings = context.getSharedPreferences(PREF_SETTINGS_FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor e = settings.edit();
        e.putString("server_url", serverurl);
        e.apply();
        GreenApp.getInstance().recreateNetwork(serverurl);

    }

    public synchronized static String loadServerUrl(Context context) {
        String su = context.getSharedPreferences(PREF_SETTINGS_FILE_NAME, Context.MODE_PRIVATE)
                .getString("server_url", context.getString(R.string.server_url_location));
        return su;


    }

    public synchronized static void saveLastLoginData(LoginData networkData, Context context) {
        SharedPreferences settings = context.getSharedPreferences(PREF_SETTINGS_FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(PREFS_LAST_LOGIN_DATA, SerializeHelper.objectToString(networkData));
        editor.apply();
    }

    public synchronized static LoginData loadLastLoginData(Context context) {
        String serializableString = null;
        SharedPreferences settings = context.getSharedPreferences(PREF_SETTINGS_FILE_NAME, Context.MODE_PRIVATE);
        if (settings != null) {
            serializableString = settings.getString(PREFS_LAST_LOGIN_DATA, null);
        }
        try {
            if (!TextUtils.isEmpty(serializableString)) {
                Object serializableObject = SerializeHelper.stringToObject(serializableString);
                if (serializableObject != null && serializableObject instanceof LoginData) {
                    return (LoginData) serializableObject;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return new LoginData();
    }
}
