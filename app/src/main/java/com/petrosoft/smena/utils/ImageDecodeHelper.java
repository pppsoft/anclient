package com.petrosoft.smena.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

/**
 * Created by alexey on 15/02/16.
 */
public class ImageDecodeHelper {
    private static final int MAX_IMAGE_SIZE = 700;
    private static final int IMAGE_SIZE = 250;
    private static final int MIN_IMAGE_SIZE = 175;

    public static Bitmap loadBitmapFromUri(String imgUri, Context context) {
        Bitmap bitmap = null;
        // First decode with inJustDecodeBounds=true to check dimensions
        try {
            final BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(imgUri, options);
            // Calculate inSampleSize
            options.inSampleSize = calculateInSampleSize(options, getImageSize(context));
            options.inJustDecodeBounds = false;
            bitmap = BitmapFactory.decodeFile(imgUri, options);
        } catch (Throwable ex) {
            ex.printStackTrace();
        }
        return bitmap;

    }

    public static int calculateInSampleSize(BitmapFactory.Options options, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        int inSampleSize = 1;

        if (height > reqHeight) {

            final int halfHeight = height / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static int getImageSize(Context context) {
        float density = context.getResources().getDisplayMetrics().density;
        int imageSize = (int) (density * IMAGE_SIZE);
        if (imageSize > MAX_IMAGE_SIZE) {
            imageSize = MAX_IMAGE_SIZE;
        }
        if (imageSize < MIN_IMAGE_SIZE) {
            imageSize = MIN_IMAGE_SIZE;
        }
        return imageSize;
    }
}
