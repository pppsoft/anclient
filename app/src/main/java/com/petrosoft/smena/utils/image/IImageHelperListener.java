package com.petrosoft.smena.utils.image;

import android.graphics.Bitmap;

/**
 * Created by a.baskakov on 02/09/16.
 */
public interface IImageHelperListener {
    void loaded(Bitmap bitmap);
    void failed();
}
