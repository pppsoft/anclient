package com.petrosoft.smena.utils.image;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.LruCache;

//import com.aviadmini.quickimagepick.QuickImagePick;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by a.baskakov on 02/09/16.
 */
public class ImageHelper {

    private static final int cacheSize = 10 * 1024 * 1024; // 10MiB

    public static LruCache<String, Bitmap> cache = new LruCache<>(cacheSize);

    private static final int MAX_WIDTH = 800;
    private static final int MAX_HEIGHT = 800;

    public static String getImagePathFromUri(String uri, Context context) {
      return   Uri.parse(uri).getPath();
//        Uri fileUri = Uri.parse(uri);
//        //TODO
//        File cameraPicsDirectory = QuickImagePick.getCameraPicsDirectory(context);
//        File file = new File(cameraPicsDirectory, fileUri.getLastPathSegment());
//        if (!file.exists()) {
//            String filePath = getRealPathFromUri(context, fileUri);
//           file = new File(filePath);
//
//        }
//
//        if (!file.exists()) {
//            String a="";
//            try {
//                InputStream inputStream = context.getContentResolver().openInputStream(fileUri);
//
//                OutputStream out = new FileOutputStream(file);
//                byte[] buf = new byte[1024];
//                int len;
//                while ((len = inputStream.read(buf)) > 0) {
//                    out.write(buf, 0, len);
//                }
//                out.close();
//                inputStream.close();
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//
//            //return file.getAbsolutePath();
//        }
//        return file.getAbsolutePath();
    }

    private static String getRealPathFromUri(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } catch (Throwable ex) {
            ex.printStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return contentUri.getPath();
    }

    public static void loadImageAsync(final String uri, final Context context, final boolean saveToFile, final IImageHelperListener listener) {
        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                String path = getImagePathFromUri(uri, context);
                final Bitmap bitmap = loadImage(path, saveToFile);

                if (bitmap != null) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            listener.loaded(bitmap);
                        }
                    });
                } else {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            listener.failed();
                        }
                    });
                }
            }
        };
        Thread thread = new Thread(runnable);
        thread.start();
    }

    public static Bitmap loadImage(String path, boolean saveToFile) {
        String key = path + saveToFile;
        Bitmap bitmap = cache.get(key);
        if (bitmap != null) {
            return bitmap;
        }

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleFactor = Math.min(photoW / MAX_WIDTH, photoH / MAX_HEIGHT);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;


        bitmap = BitmapFactory.decodeFile(path, bmOptions);

        boolean rotate = false;
        try {
            ExifInterface ei = null;
            ei = new ExifInterface(path);

            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_UNDEFINED);

            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotate = true;
                    bitmap = rotateImage(bitmap, 90);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotate = true;
                    bitmap = rotateImage(bitmap, 180);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotate = true;
                    bitmap = rotateImage(bitmap, 270);
                    break;
                case ExifInterface.ORIENTATION_NORMAL:
                    break;
            }

        } catch (Throwable ex) {
            ex.printStackTrace();
        }


        if (rotate && saveToFile) {
            FileOutputStream out = null;
            try {
                out = new FileOutputStream(path);
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    if (out != null) {
                        out.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        cache.put(key, bitmap);

        return bitmap;
    }


    public static Bitmap rotateImage(Bitmap bitmap, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix,
                true);
    }
}
