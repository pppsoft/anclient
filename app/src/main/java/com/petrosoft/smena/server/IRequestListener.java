package com.petrosoft.smena.server;

/**
 * Created by a.baskakov on 29/08/16.
 */
public interface IRequestListener<T> {
    void success(T element);
    void error();
}
