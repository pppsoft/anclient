package com.petrosoft.smena.server;

import com.petrosoft.smena.server.response.data.add_issue.AddIssueData;
import com.petrosoft.smena.server.response.data.check.CheckData;
import com.petrosoft.smena.server.response.data.geocoding.GeoData;
import com.petrosoft.smena.server.response.data.issue_types.IssueTypeData;
import com.petrosoft.smena.server.response.data.login.LoginData;
import com.petrosoft.smena.server.response.data.object_types.ObjectTypesData;
import com.petrosoft.smena.server.response.data.plan.PlanData;
import com.petrosoft.smena.server.response.data.work_types.WorkTypeData;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

/**
 * Created by a.baskakov on 16/08/16.
 */
public interface GeoCodeApi {

    @GET("Services/ReverseGeocoding/wgs84/building")
    Call<GeoData> reverseGeoCoding(@Query("y") double latitude, @Query("x") double longitude);

}
