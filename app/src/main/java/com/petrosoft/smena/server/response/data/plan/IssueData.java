package com.petrosoft.smena.server.response.data.plan;

import java.io.Serializable;

/**
 * Created by a.baskakov on 16/08/16.
 */
public class IssueData implements Serializable{
    public int id;

    public float latitude;

    public float longtitude;

    public String event_date;

    public String custom_address;
    public String issue_types;
    public String comment;

    public Boolean is_completed;
}
