package com.petrosoft.smena.server.response.data.svalka;

import com.petrosoft.smena.server.response.data.SimpleItem;

public class MeasureType extends SimpleItem implements java.io.Serializable {
    @Override
    public String toString() {
        return getName();
    }
}
