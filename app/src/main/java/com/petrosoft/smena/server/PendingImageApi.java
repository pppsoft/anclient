package com.petrosoft.smena.server;


import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface PendingImageApi {
    @Multipart
    @POST("attach_entity_file.json")
    Call<ResponseBody> attachFile(@Part("id") int id, @Part("imageCode") String imageCode, @Part MultipartBody.Part file,@Part("pointId") Integer pointId);
}
