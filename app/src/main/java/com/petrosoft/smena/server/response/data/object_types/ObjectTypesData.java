package com.petrosoft.smena.server.response.data.object_types;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by a.baskakov on 16/08/16.
 */
public class ObjectTypesData implements Serializable{
    @SerializedName("id")
    public int mId;

    @SerializedName("name")
    public String mName;

    @SerializedName("gis_layer_url")
    public String mGisLayerUrl;





}
