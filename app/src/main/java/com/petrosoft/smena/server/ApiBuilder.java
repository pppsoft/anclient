package com.petrosoft.smena.server;


import com.petrosoft.smena.GreenApp;
import com.petrosoft.smena.utils.PrefsHelper;
import com.petrosoft.smena.utils.PrimitiveConverterFactory;


import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by a.baskakov on 16/08/16.
 */
public class ApiBuilder {

    private static final String GEO_CODE_BASE_URL = "http://gis.toris.kis.gov.spb.ru/";

    static Api buildApiService() {
//        OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
//                .connectTimeout(10, TimeUnit.MINUTES)
//                .readTimeout(10, TimeUnit.MINUTES)
//                .writeTimeout(10, TimeUnit.MINUTES)
//                .build();
//        Retrofit retrofit = new Retrofit.Builder()
//
//                .baseUrl(PrefsHelper.loadServerUrl(GreenApp.getContext()) + "api/")
//                .addConverterFactory(GsonConverterFactory.create())
//                .client(okHttpClient)
//                .build();
//        return retrofit.create(Api.class);
        return GreenApp.getInstance().getApi();
    }

    static SvalkaApi buildSvalkaApiService() {
//        OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
//                .connectTimeout(10, TimeUnit.MINUTES)
//                .readTimeout(10, TimeUnit.MINUTES)
//                .writeTimeout(10, TimeUnit.MINUTES)
//                .build();
//
//        Retrofit retrofit = new Retrofit.Builder()
//                .baseUrl(PrefsHelper.loadServerUrl(GreenApp.getContext()) + "api_svalka/")
//                .client(okHttpClient)
//                .build();
//
//        return retrofit.create(SvalkaApi.class);
        return GreenApp.getInstance().getSvalkaApi();
    }

    static GeoCodeApi buildGeocodeApiService() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiBuilder.GEO_CODE_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit.create(GeoCodeApi.class);
    }
}
