package com.petrosoft.smena.server;

import com.petrosoft.smena.server.response.data.svalka.*;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface SvalkaApi {
    @GET("index.json")
    Call<ResponseBody> svalkaIndex();

    @POST("add.json")
    Call<CommonSvalkaResult> addSvalka(@Body AddSvalkaBody addSvalkaBody);


//    @GET("load_border_setup.json")
//    Call<ResponseBody> loadBorderSetup(@Query("svalka_id") long id);

    @GET("load_roadmap_element.json")
    Call<SimpleElementResponse> loadElementInfo(@Query("dumpId") long svalka_id, @Query("code") String code);


    @POST("border_setup.json")
    Call<CommonSvalkaResult> saveBorderSetup(@Body SimpleElementRequest simpleElementRequest);


    @GET("road_maps.json")
    Call<ResponseBody> getRoadMaps(@Query("svalka_id") long id);


    @GET("validate_address.json")
    Call<Void> validateAddress(@Query("name") String name);

    @GET("load.json")
    Call<Svalka> loadSvalka(@Query("svalka_id") long id);

//
//    @HEAD("road_maps.json")
//    Call<Void> hasRoadmaps(@Query("svalka_id") long id);


    @POST("add_issue.json")
    Call<CommonSvalkaResult> addIssuer(@Body SvalkaIssuer svalkaIssuer);


    @GET("waste_classes.json")
    Call<List<WasteClass>> getWasteClasses();

    @GET("waste_types.json")
    Call<List<WasteType>> getWasteTypes();

    @GET("work_types.json")
    Call<ResponseBody> getWorkTypes();

    @FormUrlEncoded
    @POST("add_liquidation_work.json")
    Call<ResponseBody> addLiquidationWork(@Query("svalka_id") long id,
                                          @Field("work_type_id") long work_type_id,
                                          @Field("quantity") String quantity,
                                          @Field("waste_type_id") long waste_type_id,
                                          @Field("comission_date") String comission_date,
                                          @Field("planned_start_date") String planned_start_date,
                                          @Field("planned_end_date") String planned_end_date
    );




    @POST("add_check_point.json")
    Call<CommonSvalkaResult> notFoundResults(@Body RouteCheckResult rcr);

    @GET("waste_measure_type.json")
    Call<List<MeasureType>> getMeasures();

    @POST("patrol.json")
    Call<CommonSvalkaResult> savePatrol(@Body SimpleElementRequest simpleElementRequest);

    @POST("info_board_setup.json")
    Call<CommonSvalkaResult> saveInfoBoard(@Body SimpleElementRequest simpleElementRequest);

    @POST("video_fix_setup.json")
    Call<CommonSvalkaResult> saveVideoFix(@Body SimpleElementRequest simpleElementRequest);

    @POST("liquidation_result.json")
    Call<CommonSvalkaResult> liquidationComplete(@Body SimpleElementRequest simpleElementRequest);

    @POST("add_issue_completion.json")
    Call<CommonSvalkaResult> addIssueCompletion(@Body IssueCompletion issueCompletion);

    @POST("complete_issue.json")
    Call<CommonSvalkaResult> completeIssue(@Query("id") long issueId, @Body CheckIssueCompletion checkIssueCompletion);
}
