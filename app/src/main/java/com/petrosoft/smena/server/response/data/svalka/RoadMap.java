package com.petrosoft.smena.server.response.data.svalka;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by kovalev on 07.12.16.
 */

public class RoadMap implements Serializable {
    public String code;
    public String name;

    @SerializedName("sortIndex")
    public String sort_index;

}
