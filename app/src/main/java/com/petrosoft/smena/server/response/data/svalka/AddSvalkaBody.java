package com.petrosoft.smena.server.response.data.svalka;

import com.google.gson.annotations.SerializedName;
import com.petrosoft.smena.dao.Route;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by kovalev on 10.05.17.
 */

public class AddSvalkaBody implements Serializable {


    @SerializedName("pointId")
    private Long pointId;


    @SerializedName("checkId")
    private long checkId;
    private String name;
    @SerializedName("customAddress")
    private String customAddress;

    private int length;
    private int width;
    private Float capacity;
    private float latitude;
    private float longitude;
    private Date registered_at;
    @SerializedName("wasteClassIds")
    private List<Long> wasteIDS;
    @SerializedName("wasteTypes")
    private List<WasteTypeComplex> mWasteTypeComplexList;
    private Long id;


    public long getCheckId() {
        return checkId;
    }

    public void setCheckId(long checkId) {
        this.checkId = checkId;
    }

    public Long getPointId() {
        return pointId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCustomAddress() {
        return customAddress;
    }

    public void setCustomAddress(String customAddress) {
        this.customAddress = customAddress;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public Float getCapacity() {
        return capacity;
    }

    public void setCapacity(Float capacity) {
        this.capacity = capacity;
    }

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    public List<Long> getWasteIDS() {
        return wasteIDS;
    }

    public void setWasteIDS(List<Long> wasteIDS) {
        this.wasteIDS = wasteIDS;
    }

    public List<WasteTypeComplex> getWasteTypeComplexList() {
        return mWasteTypeComplexList;
    }

    public void setWasteTypeComplexList(List<WasteTypeComplex> wasteTypeComplexList) {
        mWasteTypeComplexList = wasteTypeComplexList;
    }


    public void setPointId(Long pointId) {
        this.pointId = pointId;
    }


    public Date getRegistered_at() {
        return registered_at;
    }

    public void setRegistered_at(Date registered_at) {
        this.registered_at = registered_at;
    }

    public void setId(Long sid) {
        id = sid;

    }

    public Long getId() {
        return id;
    }
}
