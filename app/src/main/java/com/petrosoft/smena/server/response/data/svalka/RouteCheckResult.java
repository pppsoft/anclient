package com.petrosoft.smena.server.response.data.svalka;

import java.util.Date;

public class RouteCheckResult implements java.io.Serializable {
    private static final long serialVersionUID = 1510956485361626159L;
    private long pointId;
    private String arrivalComment;
    private long checkId;
    private Date arrivalDate;

    public long getPointId() {
        return this.pointId;
    }

    public void setPointId(long pointId) {
        this.pointId = pointId;
    }

    public String getArrivalComment() {
        return this.arrivalComment;
    }

    public void setArrivalComment(String arrivalComment) {
        this.arrivalComment = arrivalComment;
    }

    public long getCheckId() {
        return this.checkId;
    }

    public void setCheckId(long checkId) {
        this.checkId = checkId;
    }

    public Date getArrivalDate() {
        return this.arrivalDate;
    }

    public void setArrivalDate(Date arrivalDate) {
        this.arrivalDate = arrivalDate;
    }
}
