package com.petrosoft.smena.server;

import com.petrosoft.smena.server.response.data.add_issue.AddIssueData;
import com.petrosoft.smena.server.response.data.add_issue.Element;
import com.petrosoft.smena.server.response.data.add_issue.ElementGroup;
import com.petrosoft.smena.server.response.data.add_issue.RoadExtension;
import com.petrosoft.smena.server.response.data.check.CheckData;
import com.petrosoft.smena.server.response.data.issue_types.IssueTypeData;
import com.petrosoft.smena.server.response.data.login.LoginData;
import com.petrosoft.smena.server.response.data.object_types.ObjectTypesData;
import com.petrosoft.smena.server.response.data.plan.CObjectData;
import com.petrosoft.smena.server.response.data.plan.PlanData;
import com.petrosoft.smena.server.response.data.svalka.CheckIssueCompletion;
import com.petrosoft.smena.server.response.data.svalka.CommonSvalkaResult;
import com.petrosoft.smena.server.response.data.work_types.WorkTypeData;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;
import retrofit2.http.Streaming;

/**
 * Created by a.baskakov on 16/08/16.
 */
public interface Api {
    @FormUrlEncoded
    @POST("login.json")
    Call<LoginData> login(@Field("login") String login, @Field("password") String password);

    @GET("object_types.json")
    Call<ObjectTypesData[]> objectTypes(@Header("X-Token") String token);

    @GET("my_plan.json")
    Call<PlanData[]> plan(@Header("X-Token") String token, @Query("date") String date);

    @POST("add_check.json")
    Call<CheckData> addCheck(@Body RequestBody body);

    @POST("close_check.json")
    Call<String> closeCheck(@Query("id") Long id);

    @GET("work_types.json")
    Call<WorkTypeData[]> workTypes(@Header("X-Token") String token);

    @GET("issue_types.json")
    Call<IssueTypeData[]> issueTypes(@Header("X-Token") String token);

    @FormUrlEncoded
    @POST("add_issue.json")
    Call<AddIssueData> addIssue(@FieldMap Map<String, Object> queryParams);

    @FormUrlEncoded
    @POST("complete_claim.json")
    Call<String> completeClaim(@Query("id") long claimId, @Field("is_completed") Integer isCompleted, @Field("check_id") Long checkId, @Header("X-Token") String token);


    @POST("complete_task.json")
    Call<String> completeTask(@Query("id") long taskId, @Body RequestBody body, @Header("X-Token") String token);

    @POST("complete_issue.json")
    Call<CommonSvalkaResult> completeIssue(@Query("id") long issueId, @Body CheckIssueCompletion checkIssueCompletion);


    @Multipart
    @POST("attach_claim_files.json")
    Call<ResponseBody> attachClaimFile(@Part("id") Long claimId,
                                       @Part MultipartBody.Part file,
                                       @Header("X-Token") String token);

    @Multipart
    @POST("attach_file.json")
    Call<ResponseBody> attachIssueFile(@Part("id") Long issueId,
                                       @Part MultipartBody.Part file,
                                       @Header("X-Token") String token);

    @Multipart
    @POST("add_tracks.json")
    Call<ResponseBody> addTracks(@Part("latitude[]") List<Double> latitude,
                                 @Part("longitude[]") List<Double> longtitude,
                                 @Part("datetime[]") List<String> datetime,
                                 @Part("session[]") List<Long> session,
                                 @Part("accuracy[]") List<Integer> accuracy,
                                 @Header("X-Token") String token);

    @GET("objects_by_type.json")
    Call<CObjectData[]> objectsByType(@Query("id") long id, @Header("X-Token") String token);

    @GET("road_maps.json")
    Call<RequestBody> roadMaps(@Query("id") long id);


    @Streaming
    @GET("http://sneg.rails.petrosoft.su/uploads/iac.mbtiles")
    Call<ResponseBody> downloadMbTilesMap17();


    @GET("elements.json")
    Call<ArrayList<Element>> getObjectElements(@Query("object_id") long objectId, @Header("X-Token") String token);

    @GET("element_types.json")
    Call<List<ElementGroup>> getElementTypes();


    @GET("road-extensions.json")
    Call<List<RoadExtension>> getRoadExtensionById(@Query("object_id") long id);

    @FormUrlEncoded
    @POST("add_issue.json")
    Call<AddIssueData> saveIssue(@Field("check_id") long checkId, @Field("custom_address") String customAddress, @Field("event_date") String eventDate,
                                 @Field("latitude") float latitude, @Field("longtitude") float longitude, @Field("object_id") long objectId,
                                 @Field("issue_types[]") Long issueTypes,
                                 @Field("length") Float length, @Field("width") Float width,
                                 @Field("element_id") Long elementId, @Field("road_extension_id") Long roadExtensionId, @Field("eliminate_date") String eliminatedate,
                                 @Field("is_recommendation") boolean isRecommendation, @Field("comment") String comment,@Field("weather_info") String weather) ;


}
