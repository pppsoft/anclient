package com.petrosoft.smena.server;

import android.util.Log;

import com.petrosoft.smena.GreenApp;
import com.petrosoft.smena.server.response.data.login.LoginData;
import com.petrosoft.smena.utils.PrefsHelper;

import java.io.IOException;
import java.util.HashSet;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by kovalev on 05.05.17.
 */

public class AuthInterceptor implements Interceptor {
    @Override
    public Response intercept(Chain chain) throws IOException {
        Request.Builder builder = chain.request().newBuilder();
        LoginData loginData = PrefsHelper.loadLastLoginData(GreenApp.getInstance());
        builder.header("User-Agent", "Android green client");
        if (loginData.mToken != null) {
           builder.header("X-Token", loginData.mToken);
        }
        //  //Log.v("OkHttp", "Adding Header: " + cookie); // This is done so I know which headers are being added; this interceptor is used after the normal logging of OkHttp


        return chain.proceed(builder.build());
    }
}
