package com.petrosoft.smena.server.response.data.plan;

import com.google.gson.annotations.SerializedName;
import com.petrosoft.smena.server.response.data.add_issue.Element;

import java.io.Serializable;
import java.util.List;

/**
 * Created by a.baskakov on 16/08/16.
 */
public class CObjectData implements Serializable {
    @SerializedName("id")
    public int mId;

    @SerializedName("name")
    public String mName;

    @SerializedName("organisations")
    public String mOrganisaitons;

    @SerializedName("object_type_id")
    public int mObjectTypeId;

    @SerializedName("current")
    public boolean mCurrent;

    @SerializedName("claims")
    public List<ClaimData> mClaims;

    @SerializedName("tasks")
    public List<TaskData> mTasks;


    @SerializedName("issues")
    public List<IssueData> mIssues;

    @SerializedName("elements")
    public List<Element> mElements;


    @SerializedName("oid")
    public Integer mOid;
}
