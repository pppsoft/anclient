package com.petrosoft.smena.server.response.data.svalka;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by kovalev on 10.05.17.
 */

public class WasteTypeComplex implements Serializable {

    @SerializedName("count")
    private long count;
    @SerializedName("typeId")
    private long mWasteTypeId;

    private transient boolean is_checked;

    @SerializedName("measureId")
    private long mMeasureTypeId;


    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    public long getWasteTypeId() {
        return mWasteTypeId;
    }

    public void setWasteTypeId(long wasteTypeId) {
        mWasteTypeId = wasteTypeId;
    }

    public long getMeasureTypeId() {
        return mMeasureTypeId;
    }

    public void setMeasureTypeId(long measureTypeId) {
        mMeasureTypeId = measureTypeId;
    }

    public boolean getIsChecked() {
        return is_checked;
    }

    public void setIs_checked(boolean is_checked) {
        this.is_checked = is_checked;
    }
}
