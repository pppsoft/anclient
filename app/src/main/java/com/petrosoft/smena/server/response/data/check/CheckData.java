package com.petrosoft.smena.server.response.data.check;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by a.baskakov on 16/08/16.
 */
public class CheckData implements Serializable {

    public long id;
    public String start;
    public String end;
    public boolean is_completed;
    public String created_at;
    public String updated_at;
    public long user_id;
    public long object_type_id;
}
