package com.petrosoft.smena.server.response.data;

/**
 * Created by kovalev on 12.05.17.
 */

public class SimpleItem {
    private long id;
    private String name;
    private boolean is_completed;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getIsCompleted() {
        return is_completed;
    }

    public void setIsCompleted(Boolean is_completed) {
        this.is_completed = is_completed;
    }
}
