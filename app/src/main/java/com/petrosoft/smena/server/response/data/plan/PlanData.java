package com.petrosoft.smena.server.response.data.plan;

import com.google.gson.annotations.SerializedName;
import com.petrosoft.smena.server.response.data.svalka.RouteData;

import java.io.Serializable;
import java.util.List;

/**
 * Created by a.baskakov on 16/08/16.
 */
public class PlanData implements Serializable{
    @SerializedName("id")
    public int mId;

    @SerializedName("name")
    public String mName;

    @SerializedName("object_type_id")
    public int mObjectTypeId;

    @SerializedName("c_objects")
    public List<CObjectData> mCObjects;

    @SerializedName("routes")
    public List<RouteData> mRouteDatas;
}
