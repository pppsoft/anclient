package com.petrosoft.smena.server.response.data.plan;

import java.io.Serializable;

/**
 * Created by a.baskakov on 16/08/16.
 */
public class TaskData implements Serializable{
    public int id;

    public long work_type_id;

    public String work_type_name;

    public float quantity;

    public String complete_date;

    public String contract_stage;
}
