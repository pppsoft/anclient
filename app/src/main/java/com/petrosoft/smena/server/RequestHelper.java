package com.petrosoft.smena.server;

import android.content.Context;
import android.location.Location;
import android.os.Handler;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.LevelEndEvent;
import com.petrosoft.smena.GreenApp;
import com.petrosoft.smena.dao.CheckTable;
import com.petrosoft.smena.dao.CheckTableDao;
import com.petrosoft.smena.dao.Claim;
import com.petrosoft.smena.dao.ClaimDao;
import com.petrosoft.smena.dao.ClaimImage;
import com.petrosoft.smena.dao.ClaimImageDao;
import com.petrosoft.smena.dao.DaoSession;
import com.petrosoft.smena.dao.Issue;
import com.petrosoft.smena.dao.IssueDao;
import com.petrosoft.smena.dao.IssueImage;
import com.petrosoft.smena.dao.IssueImageDao;
import com.petrosoft.smena.dao.NewIssue;
import com.petrosoft.smena.dao.NewIssueDao;
import com.petrosoft.smena.dao.NewIssueImage;
import com.petrosoft.smena.dao.NewIssueImageDao;
import com.petrosoft.smena.dao.Task;
import com.petrosoft.smena.dao.TaskDao;
import com.petrosoft.smena.dao.Track;
import com.petrosoft.smena.dao.TrackDao;
import com.petrosoft.smena.db.DbHelper;
import com.petrosoft.smena.server.response.data.add_issue.AddIssueData;
import com.petrosoft.smena.server.response.data.add_issue.Element;
import com.petrosoft.smena.server.response.data.add_issue.ElementGroup;
import com.petrosoft.smena.server.response.data.check.CheckData;
import com.petrosoft.smena.server.response.data.geocoding.GeoData;
import com.petrosoft.smena.server.response.data.issue_types.IssueTypeData;
import com.petrosoft.smena.server.response.data.login.LoginData;
import com.petrosoft.smena.server.response.data.object_types.ObjectTypesData;
import com.petrosoft.smena.server.response.data.plan.CObjectData;
import com.petrosoft.smena.server.response.data.plan.PlanData;
import com.petrosoft.smena.server.response.data.svalka.AddSvalkaBody;
import com.petrosoft.smena.server.response.data.svalka.CheckIssueCompletion;
import com.petrosoft.smena.server.response.data.svalka.CommonSvalkaResult;
import com.petrosoft.smena.server.response.data.svalka.SimpleElementRequest;
import com.petrosoft.smena.server.response.data.svalka.SimpleElementResponse;
import com.petrosoft.smena.server.response.data.work_types.WorkTypeData;
import com.petrosoft.smena.utils.LocationHelper;
import com.petrosoft.smena.utils.PrefsHelper;
import com.petrosoft.smena.utils.image.ImageHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by a.baskakov on 16/08/16.
 */

public class RequestHelper {

//    @Inject
//    Api svalkaApi;

    public static void login(String login, String password, Callback<LoginData> callback) {
        Call<LoginData> call = ApiBuilder.buildApiService().login(login, password);
        call.enqueue(callback);
    }

    public static void objectTypes(Context context, Callback<ObjectTypesData[]> callback) {
        LoginData loginData = PrefsHelper.loadLastLoginData(context);
        Call<ObjectTypesData[]> call = ApiBuilder.buildApiService().objectTypes(loginData.mToken);
        call.enqueue(callback);
    }

    public static void plan(Context context, Callback<PlanData[]> callback) {
        LoginData loginData = PrefsHelper.loadLastLoginData(context);
        Call<PlanData[]> call = ApiBuilder.buildApiService().plan(loginData.mToken, getDateString(loginData.mDate));
        call.enqueue(callback);
    }

    public static void workTypes(Context context, Callback<WorkTypeData[]> callback) {
        LoginData loginData = PrefsHelper.loadLastLoginData(context);
        Call<WorkTypeData[]> call = ApiBuilder.buildApiService().workTypes(loginData.mToken);
        call.enqueue(callback);
    }

    public static void issueTypes(Context context, Callback<IssueTypeData[]> callback) {
        LoginData loginData = PrefsHelper.loadLastLoginData(context);
        Call<IssueTypeData[]> call = ApiBuilder.buildApiService().issueTypes(loginData.mToken);
        call.enqueue(callback);
    }


    public static void listSvalka(Callback<ResponseBody> callback) {
        Call<ResponseBody> call = ApiBuilder.buildSvalkaApiService().svalkaIndex();
        call.enqueue(callback);
    }

    public static void validateSvalkaAddress(String address, Callback<Void> callback) {
        Call<Void> call = ApiBuilder.buildSvalkaApiService().validateAddress(address);
        call.enqueue(callback);
    }


    public static void roadMapSvalka(long id, Callback<ResponseBody> callback) {

        Call<ResponseBody> call = ApiBuilder.buildSvalkaApiService().getRoadMaps(id);
        call.enqueue(callback);
    }





    public static void addCheckAsync(final Context context, final Date date, final long objectTypeId, final IRequestListener<CheckData> listener) {
        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                final CheckData checkData = addCheck(context, date, objectTypeId);
                if (checkData != null) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            listener.success(checkData);
                        }
                    });
                } else {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            listener.error();
                        }
                    });
                }
            }
        };
        Thread thread = new Thread(runnable);
        thread.start();
    }

    public static CheckData addCheck(Context context, Date date, long objectTypeId) {
        SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        final String dateString = dt.format(date);
        LoginData loginData = PrefsHelper.loadLastLoginData(context);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("start", dateString);
            jsonObject.put("object_type_id", objectTypeId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), jsonObject.toString());
        Call<CheckData> call = ApiBuilder.buildApiService().addCheck(body);

        try {
            Response<CheckData> response = call.execute();
            if (response.isSuccessful()) {
                return response.body();
            }
        } catch (Throwable ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public static void tryToCloseCheckAsync(final Context context, final CheckTable check, final IRequestListener<Boolean> listener) {
        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                final Boolean closed = tryToCloseCheck(context, check);
                if (closed) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            listener.success(closed);
                        }
                    });
                } else {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            listener.error();
                        }
                    });
                }
            }
        };
        Thread thread = new Thread(runnable);
        thread.start();
    }

    public static boolean tryToCloseCheck(Context context, CheckTable check) {
        DaoSession daoSession = DbHelper.getDaoMaster(context).newSession();
        if (check.getSent()) {
            return true;
        }
        final CheckTableDao checkTableDao = daoSession.getCheckTableDao();
        if (!check.getLoadedFromServer()) {
            Date date = new Date();
            date.setTime(check.getDate_request());
            long objectTypeId = check.getObject_type_id_request();
            CheckData checkData = addCheck(context, date, objectTypeId);
            if (checkData != null) {
                DbHelper.setDataFromCheckDataToCheckTable(checkData, check);
                checkTableDao.update(check);
                return closeCheck(context, check);
            }
        } else {
            return closeCheck(context, check);
        }
        return false;
    }

    public static void clearDataBase(Context context) {
        DaoSession daoSession = DbHelper.getDaoMaster(context).newSession();
        daoSession.getObjectTypeDao().deleteAll();
        daoSession.getCObjectDao().deleteAll();
        daoSession.getClaimDao().deleteAll();
        daoSession.getIssueDao().deleteAll();
        daoSession.getTaskDao().deleteAll();
        daoSession.getCheckTableDao().deleteAll();
        daoSession.getCheckToObjectDao().deleteAll();
        daoSession.getWorkTypeDao().deleteAll();
        daoSession.getNewIssueDao().deleteAll();
        daoSession.getIssueTypeDao().deleteAll();
        daoSession.getObjectTypeToIssueTypeDao().deleteAll();
        daoSession.getNewIssueImageDao().deleteAll();
        daoSession.getClaimImageDao().deleteAll();
        daoSession.getIssueImageDao().deleteAll();
        daoSession.getTrackDao().deleteAll();
        daoSession.getRouteDao().deleteAll();
    }

    public static boolean closeCheck(Context context, CheckTable check) {
        DaoSession daoSession = DbHelper.getDaoMaster(context).newSession();
        final CheckTableDao checkTableDao = daoSession.getCheckTableDao();
        Call<String> call = ApiBuilder.buildApiService().closeCheck(check.getServerId());
        try {
            Response<String> response = call.execute();
            if (response.isSuccessful()) {
                check.setSent(true);
                Answers.getInstance().logLevelEnd(new LevelEndEvent().putLevelName("Проверка : " + String.valueOf(check.getServerId())));
                checkTableDao.update(check);
                return true;
            }
        } catch (Throwable ex) {
            ex.printStackTrace();
        }
        return false;
    }

    public static void tryToCompleteClaimAsync(final Context context, final Claim claim, final IRequestListener<Boolean> listener) {
        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                final Boolean closed = tryToCompleteClaim(context, claim);
                if (closed) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            listener.success(closed);
                        }
                    });
                } else {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            listener.error();
                        }
                    });
                }
            }
        };
        Thread thread = new Thread(runnable);
        thread.start();
    }

    public static boolean tryToCompleteClaim(Context context, Claim claim) {
        if (claim.getLastCheckId() == null || claim.getSent()) {
            return true;
        }
        DaoSession daoSession = DbHelper.getDaoMaster(context).newSession();
        CheckTableDao checkTableDao = daoSession.getCheckTableDao();
        CheckTable check = checkTableDao.queryBuilder().where(CheckTableDao.Properties.Id.eq(claim.getLastCheckId())).unique();
        if (!check.getLoadedFromServer()) {
            Date date = new Date();
            date.setTime(check.getDate_request());
            long objectTypeId = check.getObject_type_id_request();
            CheckData checkData = addCheck(context, date, objectTypeId);
            if (checkData != null) {
                DbHelper.setDataFromCheckDataToCheckTable(checkData, check);
                checkTableDao.update(check);
                return completeClaim(context, claim);
            }
        } else {
            return completeClaim(context, claim);
        }
        return false;
    }

    public static boolean completeClaim(Context context, Claim claim) {
        DaoSession daoSession = DbHelper.getDaoMaster(context).newSession();
        CheckTableDao checkTableDao = daoSession.getCheckTableDao();
        ClaimDao claimDao = daoSession.getClaimDao();
        CheckTable check = checkTableDao.queryBuilder().where(CheckTableDao.Properties.Id.eq(claim.getLastCheckId())).unique();

        LoginData loginData = PrefsHelper.loadLastLoginData(context);
        Integer is_c = 0;
        if (claim.getIsCompleted()) {
            is_c = 1;
        }
        Call<String> call = ApiBuilder.buildApiService().completeClaim(claim.getServerId(), is_c, check.getServerId(), loginData.mToken);
        try {
            Response<String> response = call.execute();
            if (response.isSuccessful()) {
                claim.setSent(true);
                claimDao.update(claim);
                return tryToSendClaimImages(context, claim);
            }
        } catch (Throwable ex) {
            ex.printStackTrace();
        }
        return false;
    }


    public static void tryToCompleteTaskAsync(final Context context, final Task task, final IRequestListener<Boolean> listener) {
        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                final Boolean closed = tryToCompleteTask(context, task);
                if (closed) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            listener.success(closed);
                        }
                    });
                } else {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            listener.error();
                        }
                    });
                }
            }
        };
        Thread thread = new Thread(runnable);
        thread.start();
    }

    public static boolean tryToCompleteTask(Context context, Task task) {
        if (task.getLastCheckId() == null || task.getSent()) {
            return true;
        }
        DaoSession daoSession = DbHelper.getDaoMaster(context).newSession();
        CheckTableDao checkTableDao = daoSession.getCheckTableDao();
        CheckTable check = checkTableDao.queryBuilder().where(CheckTableDao.Properties.Id.eq(task.getLastCheckId())).unique();
        if (!check.getLoadedFromServer()) {
            Date date = new Date();
            date.setTime(check.getDate_request());
            long objectTypeId = check.getObject_type_id_request();
            CheckData checkData = addCheck(context, date, objectTypeId);
            if (checkData != null) {
                DbHelper.setDataFromCheckDataToCheckTable(checkData, check);
                checkTableDao.update(check);
                return completeTask(context, task);
            }
        } else {
            return completeTask(context, task);
        }
        return false;
    }


    public static boolean completeTask(Context context, Task task) {
        DaoSession daoSession = DbHelper.getDaoMaster(context).newSession();
        CheckTableDao checkTableDao = daoSession.getCheckTableDao();
        TaskDao taskDao = daoSession.getTaskDao();
        CheckTable check = checkTableDao.queryBuilder().where(CheckTableDao.Properties.Id.eq(task.getLastCheckId())).unique();

        LoginData loginData = PrefsHelper.loadLastLoginData(context);
        JSONObject jsonObject = new JSONObject();
        try {
            Integer is_c = 0;
            if (task.getIsCompleted()) {
                is_c = 1;
            }

            jsonObject.put("is_completed", is_c);
            jsonObject.put("check_id", check.getServerId());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), jsonObject.toString());
        Call<String> call = ApiBuilder.buildApiService().completeTask(task.getServerId(), body, loginData.mToken);
        try {
            Response<String> response = call.execute();
            if (response.isSuccessful()) {
                task.setSent(true);
                taskDao.update(task);
                return true;
            }
        } catch (Throwable ex) {
            ex.printStackTrace();
        }
        return false;
    }

    public static void tryToCompleteIssueAsync(final Context context, final Issue issue, final IRequestListener<Boolean> listener) {
        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                final Boolean closed = tryToCompleteIssue(context, issue);
                if (closed) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            listener.success(closed);
                        }
                    });
                } else {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            listener.error();
                        }
                    });
                }
            }
        };
        Thread thread = new Thread(runnable);
        thread.start();
    }

    public static void getElements(long objectId, Callback<ArrayList<Element>> callback) {
        LoginData loginData = PrefsHelper.loadLastLoginData(GreenApp.getInstance().getApplicationContext());
        Call<ArrayList<Element>> call = GreenApp.getInstance().getApi().getObjectElements(objectId, loginData.mToken);
        call.enqueue(callback);
    }

    public static void getElementTypes(Callback<List<ElementGroup>> callback) {
        Call<List<ElementGroup>> call = GreenApp.getInstance().getApi().getElementTypes();
        call.enqueue(callback);


    }

    public static boolean tryToCompleteIssue(Context context, Issue issue) {
        if (issue.getLastCheckId() == null || issue.getSent()) {
            return true;
        }
        DaoSession daoSession = DbHelper.getDaoMaster(context).newSession();
        CheckTableDao checkTableDao = daoSession.getCheckTableDao();
        CheckTable check = checkTableDao.queryBuilder().where(CheckTableDao.Properties.Id.eq(issue.getLastCheckId())).unique();
        if (!check.getLoadedFromServer()) {
            Date date = new Date();
            date.setTime(check.getDate_request());
            long objectTypeId = check.getObject_type_id_request();
            CheckData checkData = addCheck(context, date, objectTypeId);
            if (checkData != null) {
                DbHelper.setDataFromCheckDataToCheckTable(checkData, check);
                checkTableDao.update(check);
                return completeIssue(context, issue);
            }
        } else {
            return completeIssue(context, issue);
        }
        return false;
    }

    public static boolean completeIssue(Context context, Issue issue) {
        DaoSession daoSession = DbHelper.getDaoMaster(context).newSession();
        CheckTableDao checkTableDao = daoSession.getCheckTableDao();
        IssueDao issueDao = daoSession.getIssueDao();
        CheckTable check = checkTableDao.queryBuilder().where(CheckTableDao.Properties.Id.eq(issue.getLastCheckId())).unique();


//        JSONObject jsonObject = new JSONObject();
//        Integer is_c = 0;
//        if (issue.getIsCompleted()) {
//            is_c = 1;
//        }
//        try {
//            jsonObject.put("is_completed", is_c);
//            jsonObject.put("check_id", check.getServerId());
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
        CheckIssueCompletion checkIssueCompletion = new CheckIssueCompletion();
        checkIssueCompletion.setIs_completed(issue.getIsCompleted());
        checkIssueCompletion.setCheck_id(check.getServerId());
//        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), jsonObject.toString());
        Call<CommonSvalkaResult> call = ApiBuilder.buildApiService().completeIssue(issue.getServerId(), checkIssueCompletion);
        try {
            Response<CommonSvalkaResult> response = call.execute();
            if (response.isSuccessful()) {
                issue.setSent(true);
                issueDao.update(issue);
                return tryToSendIssueImages(context, issue);
            }
        } catch (Throwable ex) {
            ex.printStackTrace();
        }
        return false;
    }


    public static void tryToAddIssueAsync(final Context context, final NewIssue newIssue, final IRequestListener<Boolean> listener) {
        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                final boolean result = tryToAddNewIssue(context, newIssue);
                if (result) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            listener.success(true);
                        }
                    });
                } else {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            listener.error();
                        }
                    });
                }
            }
        };
        Thread thread = new Thread(runnable);
        thread.start();
    }


    public static boolean tryToAddNewIssue(final Context context, final NewIssue newIssue) {
        long checkId = newIssue.getCheck_id();
        DaoSession daoSession = DbHelper.getDaoMaster(context).newSession();
        final CheckTableDao checkTableDao = daoSession.getCheckTableDao();
        final CheckTable check = checkTableDao.queryBuilder().where(CheckTableDao.Properties.Id.eq(checkId)).unique();
        if (!check.getLoadedFromServer()) {
            Date date = new Date();
            date.setTime(check.getDate_request());
            long objectTypeId = check.getObject_type_id_request();
            CheckData checkData = addCheck(context, date, objectTypeId);
            if (checkData != null) {
                DbHelper.setDataFromCheckDataToCheckTable(checkData, check);
                checkTableDao.update(check);
                return addNewIssue(context, newIssue);
            }
        } else {
            return addNewIssue(context, newIssue);
        }
        return false;
    }

    private static boolean addNewIssue(final Context context, NewIssue newIssue) {
        LoginData loginData = PrefsHelper.loadLastLoginData(context);
        long checkId = newIssue.getCheck_id();
        DaoSession daoSession = DbHelper.getDaoMaster(context).newSession();
        final CheckTableDao checkTableDao = daoSession.getCheckTableDao();
        final NewIssueDao newIssueDao = daoSession.getNewIssueDao();
        final CheckTable check = checkTableDao.queryBuilder().where(CheckTableDao.Properties.Id.eq(checkId)).unique();

        JSONArray jsonArray = new JSONArray();
        jsonArray.put(newIssue.getIssue_type());

        if (newIssue.getServerId() == null) {
            Map<String, Object> params = new HashMap<>();

            params.put("check_id", check.getServerId());
            params.put("custom_address", newIssue.getCustom_address());
            params.put("event_date", newIssue.getEvent_date());

            if (newIssue.getLongtitude() == null) {
                params.put("latitude", LocationHelper.getDefaultLocation().getLatitude());
                params.put("longtitude", LocationHelper.getDefaultLocation().getLongitude());
            } else {
                params.put("latitude", newIssue.getLatitude());
                params.put("longtitude", newIssue.getLongtitude());
            }


            params.put("object_id", newIssue.getObject_id());
            params.put("issue_types[]", newIssue.getIssue_type());

            if (newIssue.getWidth() != null) {
                params.put("width", newIssue.getWidth());
            }
            if (newIssue.getLength() != null) {
                params.put("length", newIssue.getLength());
            }
            if (newIssue.getElement_id() != null) {
                params.put("element_id", newIssue.getElement_id());
            }

            if (newIssue.getRoad_extension_id() != null) {
                params.put("road_extension_id", newIssue.getRoad_extension_id());
            }

            if (newIssue.getEliminate_date() != null && newIssue.getEliminate_date().length() == 10) {
                params.put("eliminate_date", newIssue.getEliminate_date());
            }

            if (newIssue.getElement_id() != null) {
                params.put("element_id", newIssue.getElement_id());
            }

            if (newIssue.getIs_recommendation()) {
                params.put("is_recommendation", newIssue.getIs_recommendation());
                params.put("eliminate_date", newIssue.getEliminate_date());
                params.put("comment", newIssue.getComment());
            }

            Call<AddIssueData> call = ApiBuilder.buildApiService().addIssue(params);

            try {
                Response<AddIssueData> response = call.execute();
                if (response.isSuccessful()) {
                    AddIssueData addIssueData = response.body();
                    newIssue.setServerId(addIssueData.id);
                    newIssueDao.update(newIssue);
                    return tryToSendNewIssueImages(context, newIssue);
                }
                return true;
            } catch (Throwable e) {
                e.printStackTrace();
                return false;
            }
        } else {
            return tryToSendNewIssueImages(context, newIssue);
        }
    }

    public static boolean tryToSendNewIssueImages(Context context, NewIssue newIssue) {
        List<NewIssueImage> issueImages = newIssue.getImages();
        for (NewIssueImage newIssueImage : issueImages) {
            if (!newIssueImage.getSent()) {
                if (!sendNewIssueImage(context, newIssueImage)) {
                    return false;
                }
            }
        }
        return true;
    }

    private static String getDateString(Date date) {
        SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd");
        return dt.format(date);
    }

    public static boolean sendNewIssueImage(Context context, NewIssueImage newIssueImage) {
        DaoSession daoSession = DbHelper.getDaoMaster(context).newSession();
        final NewIssueImageDao newIssueImageDao = daoSession.getNewIssueImageDao();
        final NewIssueDao newIssueDao = daoSession.getNewIssueDao();
        LoginData loginData = PrefsHelper.loadLastLoginData(context);

        String path = ImageHelper.getImagePathFromUri(newIssueImage.getPath(), context);//save rotated file
        ImageHelper.loadImage(path, true);
        File file = new File(path);

        if (!file.exists()) {
            return true;
        }
        // create RequestBody instance from file
        RequestBody requestFile =
                RequestBody.create(MediaType.parse("multipart/form-data"), file);

        MultipartBody.Part body =
                MultipartBody.Part.createFormData("file", file.getName(), requestFile);


        NewIssue newIssue = newIssueDao.load(newIssueImage.getNewIssueId());

        Call<ResponseBody> call = ApiBuilder.buildApiService().attachIssueFile(
                newIssue.getServerId(),
                body,
                loginData.mToken);

        try {
            Response<ResponseBody> response = call.execute();
            if (response.isSuccessful()) {
                newIssueImage.setSent(true);
                newIssueImageDao.update(newIssueImage);
            }
            return response.isSuccessful();
        } catch (Throwable ex) {
            ex.printStackTrace();
        }
        return false;
    }

    public static boolean tryToSendClaimImages(Context context, Claim claim) {
        List<ClaimImage> claimImages = claim.getImages();
        for (ClaimImage claimImage : claimImages) {
            if (!claimImage.getSent()) {
                if (!sendClaimImage(context, claimImage)) {
                    return false;
                }
            }
        }
        return true;
    }

    public static boolean sendClaimImage(Context context, ClaimImage claimImage) {
        DaoSession daoSession = DbHelper.getDaoMaster(context).newSession();
        final ClaimImageDao claimImageDao = daoSession.getClaimImageDao();
        final ClaimDao claimDao = daoSession.getClaimDao();
        LoginData loginData = PrefsHelper.loadLastLoginData(context);


        String path = ImageHelper.getImagePathFromUri(claimImage.getPath(), context);//save rotated file
        ImageHelper.loadImage(path, true);
        File file = new File(path);

        if (!file.exists()) {
            return true;
        }

        // create RequestBody instance from file
        RequestBody requestFile =
                RequestBody.create(MediaType.parse("multipart/form-data"), file);

        MultipartBody.Part body =
                MultipartBody.Part.createFormData("file[]", file.getName(), requestFile);


        Claim claim = claimDao.load(claimImage.getClaimId());


        try {
            Call<ResponseBody> call = ApiBuilder.buildApiService().attachClaimFile(
                    claim.getServerId(),
                    body,
                    loginData.mToken);

            Response<ResponseBody> response = call.execute();
            if (response.isSuccessful()) {
                claimImage.setSent(true);
                claimImageDao.update(claimImage);
            }
            return response.isSuccessful();
        } catch (Throwable ex) {
            ex.printStackTrace();
        }
        return false;
    }

    public static boolean tryToSendIssueImages(Context context, Issue issue) {
        List<IssueImage> issueImages = issue.getImages();
        for (IssueImage issueImage : issueImages) {
            if (!issueImage.getSent()) {
                if (!sendIssueImage(context, issueImage)) {
                    return false;
                }
            }
        }
        return true;
    }


    public static boolean sendIssueImage(Context context, IssueImage issueImage) {
        DaoSession daoSession = DbHelper.getDaoMaster(context).newSession();
        final IssueImageDao issueImageDao = daoSession.getIssueImageDao();
        final IssueDao issueDao = daoSession.getIssueDao();
        LoginData loginData = PrefsHelper.loadLastLoginData(context);

        String path = ImageHelper.getImagePathFromUri(issueImage.getPath(), context);//save rotated file
        ImageHelper.loadImage(path, true);
        File file = new File(path);

        if (!file.exists()) {
            return true;
        }

        // create RequestBody instance from file
        RequestBody requestFile =
                RequestBody.create(MediaType.parse("multipart/form-data"), file);

        MultipartBody.Part body =
                MultipartBody.Part.createFormData("file", file.getName(), requestFile);


        Issue issue = issueDao.load(issueImage.getIssueId());

        try {
            Call<ResponseBody> call = ApiBuilder.buildApiService().attachIssueFile(
                    issue.getServerId(),
                    body,
                    loginData.mToken);

            Response<ResponseBody> response = call.execute();
            if (response.isSuccessful()) {
                issueImage.setSent(true);
                issueImageDao.update(issueImage);
            }
            return response.isSuccessful();
        } catch (Throwable ex) {
            ex.printStackTrace();
        }
        return false;
    }

    public static void tryToFinishWorkAsync(final Context context, final IRequestListener<Boolean> listener) {

        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                final Boolean finished = tryToFinishWork(context);
                if (finished) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            listener.success(finished);
                        }
                    });
                } else {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            listener.error();
                        }
                    });
                }
            }
        };

        Thread thread = new Thread(runnable);
        thread.start();

    }

    public static boolean tryToFinishWork(Context context) {
        DaoSession daoSession = DbHelper.getDaoMaster(context).newSession();
        CheckTableDao checkTableDao = daoSession.getCheckTableDao();
        NewIssueDao newIssueDao = daoSession.getNewIssueDao();
        ClaimDao claimDao = daoSession.getClaimDao();
        TaskDao taskDao = daoSession.getTaskDao();
        IssueDao issueDao = daoSession.getIssueDao();

        //add all checks to server if not and close them
        for (CheckTable check : checkTableDao.loadAll()) {
            if (!check.getLoadedFromServer()) {
                Date date = new Date();
                date.setTime(check.getDate_request());
                long objectTypeId = check.getObject_type_id_request();
                CheckData checkData = addCheck(context, date, objectTypeId);
                if (checkData != null) {
                    DbHelper.setDataFromCheckDataToCheckTable(checkData, check);
                    checkTableDao.update(check);
                    if (!tryToCloseCheck(context, check)) {
                        return false;
                    }
                } else {
                    return false;
                }
            }
        }

        for (NewIssue newIssue : newIssueDao.loadAll()) {
            if (newIssue.getLatitude() == null || newIssue.getLongtitude() == null) {
                Location l = LocationHelper.getDefaultLocation();
                newIssue.setLatitude(((float) l.getLatitude()));
                newIssue.setLongtitude(((float) l.getLongitude()));

            }
            if (newIssue.getCustom_address() == null) {
                newIssue.setCustom_address("неизвестный адрес");
            }
            if (!tryToAddNewIssue(context, newIssue)) {
                return false;
            }
        }

        for (Claim claim : claimDao.loadAll()) {
            if (!tryToCompleteClaim(context, claim)) {
                return false;
            }
        }

        for (Task task : taskDao.loadAll()) {
            if (!tryToCompleteTask(context, task)) {
                return false;
            }
        }

        for (Issue issue : issueDao.loadAll()) {
            if (!tryToCompleteIssue(context, issue)) {
                return false;
            }
        }

        return addTracks(context);
    }

    public static void reverseGeoCoding(double latitude, double longtitude, Callback<GeoData> callback) {
        Call<GeoData> call = ApiBuilder.buildGeocodeApiService().reverseGeoCoding(latitude, longtitude);
        call.enqueue(callback);
    }

    public static synchronized void addTracksAsync(final Context context, final IRequestListener<Boolean> listener) {

        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                final Boolean finished = addTracks(context);
                if (finished) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            listener.success(finished);
                        }
                    });
                } else {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            listener.error();
                        }
                    });
                }
            }
        };

        Thread thread = new Thread(runnable);
        thread.start();
    }


    public static boolean addTracks(Context context) {
        LoginData loginData = PrefsHelper.loadLastLoginData(context);

        List<Double> latitude = new ArrayList<>();
        List<Double> longtitude = new ArrayList<>();
        List<String> datetime = new ArrayList<>();
        List<Long> session = new ArrayList<>();
        List<Integer> accuracy = new ArrayList<>();

        DaoSession daoSession = DbHelper.getDaoMaster(context).newSession();
        TrackDao trackDao = daoSession.getTrackDao();

        List<Track> tracks = trackDao.queryBuilder().where(TrackDao.Properties.Sent.eq(false)).list();

        if (tracks.size() == 0) {
            return true;
        }

        for (Track track : tracks) {
            latitude.add(track.getLatitude());
            longtitude.add(track.getLongtitude());
            String dt = track.getDatetime();
            datetime.add(dt);
            session.add(track.getSession());
            accuracy.add(track.getAccuracy().intValue());
        }

        Call<ResponseBody> call = ApiBuilder.buildApiService().addTracks(
                latitude,
                longtitude,
                datetime,
                session,
                accuracy,
                loginData.mToken);

        try {
            Response<ResponseBody> response = call.execute();
            if (response.isSuccessful()) {
                for (Track track : tracks) {
                    track.setSent(true);
                    trackDao.update(track);
                }
                return true;
            }
        } catch (Throwable ex) {
            ex.printStackTrace();
        }
        return true;
    }

    public static void objectsByTypeTypes(Context context, long id, Callback<CObjectData[]> callback) {
        LoginData loginData = PrefsHelper.loadLastLoginData(context);
        Call<CObjectData[]> call = ApiBuilder.buildApiService().objectsByType(id, loginData.mToken);
        call.enqueue(callback);
    }
}
