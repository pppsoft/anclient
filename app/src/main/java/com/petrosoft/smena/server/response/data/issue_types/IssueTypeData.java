package com.petrosoft.smena.server.response.data.issue_types;

import com.google.gson.annotations.SerializedName;
import com.petrosoft.smena.server.response.data.object_types.ObjectTypesData;
import com.petrosoft.smena.server.response.data.plan.CObjectData;

import java.io.Serializable;
import java.util.List;

/**
 * Created by a.baskakov on 16/08/16.
 */
public class IssueTypeData implements Serializable{
    public long id;
    public String name;
    public List<ObjectTypesData> object_types;
}
