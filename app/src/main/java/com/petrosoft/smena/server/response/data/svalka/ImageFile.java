package com.petrosoft.smena.server.response.data.svalka;

import java.io.Serializable;

/**
 * Created by kovalev on 15.12.16.
 */

public class ImageFile implements Serializable {
    public long id;
    public String name, path, mini_path, pre_path, sm_path;
    public boolean is_image;

}
