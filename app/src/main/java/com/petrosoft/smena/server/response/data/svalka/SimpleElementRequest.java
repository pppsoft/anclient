package com.petrosoft.smena.server.response.data.svalka;

import java.util.Date;

public class SimpleElementRequest implements java.io.Serializable {
    private long check_id;
    private long svalka_id;
    private Date fact_date;
    private boolean is_completed;

    public long getSvalka_id() {
        return this.svalka_id;
    }

    public void setSvalka_id(long svalka_id) {
        this.svalka_id = svalka_id;
    }

    public Date getFact_date() {
        return this.fact_date;
    }

    public void setFact_date(Date fact_date) {
        this.fact_date = fact_date;
    }

    public boolean is_completed() {
        return is_completed;
    }

    public void setIs_completed(boolean is_completed) {
        this.is_completed = is_completed;
    }

    public void setCheck_id(long check_id) {
        this.check_id = check_id;
    }
}
