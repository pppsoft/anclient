package com.petrosoft.smena.server.response.data.add_issue;

import java.io.Serializable;

/**
 * Created by a.baskakov on 16/08/16.
 */
public class AddIssueData implements Serializable {
    public long id;
    public float latitude;
    public float longtitude;
    public String event_date;
    public String created_at;
    public String updated_at;
    public long green_object_id;
    public long user_id;
    public String eliminate_date;
    public int square;
    public long width;
    public long height;
    public long length;
    public long quantity;
    public boolean is_completed;
    public long check_id;
    public String comment;
    public boolean is_recomendation;
    public boolean is_archive;
    public String custom_address;
    public long green_element_id;
    public long contract_work_id;
}
