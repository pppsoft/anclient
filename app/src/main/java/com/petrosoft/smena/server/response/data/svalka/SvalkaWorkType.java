package com.petrosoft.smena.server.response.data.svalka;

import java.io.Serializable;

/**
 * Created by kovalev on 13.12.16.
 */

public class SvalkaWorkType implements Serializable {
    public long id;
    public String name;
    public String measure;
}
