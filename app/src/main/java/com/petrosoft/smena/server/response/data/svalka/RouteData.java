package com.petrosoft.smena.server.response.data.svalka;

import java.io.Serializable;

/**
 * Created by kovalev on 05.05.17.
 */

public class RouteData implements Serializable {
    private int id;
    private String name;
    private float latitude;
    private float longitude;
    private Long object_id;
    private Long object_type_id;

    public boolean getIsVisible() {
        boolean is_visible = true;
        if (object_type_id != null) {
            if (object_type_id == 9999L) {
                is_visible = false;
            }
        }
        return is_visible;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public float getLatitude() {
        return latitude;
    }

    public float getLongitude() {
        return longitude;
    }

    public Long getObject_id() {
        return object_id;
    }

    public Long getObject_type_id() {
        return object_type_id;
    }
}
