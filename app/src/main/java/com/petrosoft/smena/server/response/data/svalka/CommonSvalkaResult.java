package com.petrosoft.smena.server.response.data.svalka;

import com.google.gson.annotations.SerializedName;

public class CommonSvalkaResult implements java.io.Serializable {
    private String name;
    private String imageCode;
    @SerializedName("pointId")
    private Integer point_id;
    private int id;

    public String getName() {
        return this.name;
    }

    public String getImageCode() {
        return this.imageCode;
    }

    public int getId() {
        return this.id;
    }

    public void setPoint_id(Integer point_id) {
        this.point_id = point_id;
    }

    public Integer getPoint_id() {
        return point_id;
    }
}
