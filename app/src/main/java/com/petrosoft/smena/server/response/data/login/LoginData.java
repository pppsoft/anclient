package com.petrosoft.smena.server.response.data.login;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by a.baskakov on 16/08/16.
 */
public class LoginData implements Serializable {

    @SerializedName("id")
    public String mId;

    @SerializedName("first_name")
    public String mFirstName;

    @SerializedName("last_name")
    public String mLastName;

    @SerializedName("login")
    public String mLogin;

    @SerializedName("created_at")
    public String mCreatedAt;

    @SerializedName("updated_at")
    public String mUpdatedAt;

    @SerializedName("is_external")
    public boolean mIsExternal;

    @SerializedName("token")
    public String mToken = null;

    @Expose
    public Date mDate;
}
