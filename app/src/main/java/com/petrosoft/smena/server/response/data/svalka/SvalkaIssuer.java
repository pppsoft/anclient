package com.petrosoft.smena.server.response.data.svalka;

import java.util.Date;

/**
 * Created by kovalev on 17.05.17.
 */

public class SvalkaIssuer {
    private long check_id;
    private long svalka_id;
    private String full_name_fiz;
    private String location_fiz;
    private String name_yur;
    private Date registered_at;
    private String comment;

    public long getSvalka_id() {
        return svalka_id;
    }

    public void setSvalka_id(long svalka_id) {
        this.svalka_id = svalka_id;
    }

    public String getFull_name_fiz() {
        return full_name_fiz;
    }

    public void setFull_name_fiz(String full_name_fiz) {
        this.full_name_fiz = full_name_fiz;
    }

    public String getLocation_fiz() {
        return location_fiz;
    }

    public void setLocation_fiz(String location_fiz) {
        this.location_fiz = location_fiz;
    }

    public String getName_yur() {
        return name_yur;
    }

    public void setName_yur(String name_yur) {
        this.name_yur = name_yur;
    }

    public Date getRegistered_at() {
        return registered_at;
    }

    public void setRegistered_at(Date registered_at) {
        this.registered_at = registered_at;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
