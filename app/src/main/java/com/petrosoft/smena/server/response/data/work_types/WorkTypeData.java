package com.petrosoft.smena.server.response.data.work_types;

import java.io.Serializable;

/**
 * Created by a.baskakov on 16/08/16.
 */
public class WorkTypeData implements Serializable {

    public long id;
    public String name;
    public String measure;
}
