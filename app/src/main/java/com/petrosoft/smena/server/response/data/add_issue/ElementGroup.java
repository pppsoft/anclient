package com.petrosoft.smena.server.response.data.add_issue;

import java.io.Serializable;

/**
 * Created by kovalev on 08.02.17.
 */

public class ElementGroup implements Serializable {
    public int id;
    public String gis_layer_url;
    public String name;
}
