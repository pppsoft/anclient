package com.petrosoft.smena.server.response.data.add_issue;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by kovalev on 07.02.17.
 */

public class Element implements Serializable {
    public Long id;
    public Long element_type_id;
    public String name;
    public int oid;

    public String inventory_number;

    public String toString() {
        String result = "";
        result += name + " (" + String.valueOf(id);
        try {
            if (inventory_number != null || !inventory_number.isEmpty()) {
                result += "|" + inventory_number;
            }
        } catch (Exception ex) {
        }
        result += ")";
        return result;

    }

    public Element(Long id) {
        this.id = id;
    }

    protected Element(Parcel in) {
        id = in.readLong();
        name = in.readString();
        oid = in.readInt();
        inventory_number = in.readString();
    }


}
