package com.petrosoft.smena.server.response.data.object_types;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by kovalev on 12.09.16.
 */
public class ElementsData implements Serializable {
    @SerializedName("id")
    public int mId;

    @SerializedName("name")
    public String mName;
}
