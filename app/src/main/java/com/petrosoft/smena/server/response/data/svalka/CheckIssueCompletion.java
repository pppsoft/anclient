package com.petrosoft.smena.server.response.data.svalka;

public class CheckIssueCompletion implements java.io.Serializable {

    private long check_id;
    private int is_completed;

    public long getCheck_id() {
        return this.check_id;
    }

    public void setCheck_id(long check_id) {
        this.check_id = check_id;
    }

    public boolean getIs_completed() {
        return this.is_completed==1;
    }

    public void setIs_completed(boolean is_completed) {
        if (is_completed) {
            this.is_completed = 1;
        } else {
            this.is_completed = 0;
        }
    }
}
