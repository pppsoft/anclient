package com.petrosoft.smena.server.response.data.svalka;

import java.util.Date;

public class IssueCompletion implements java.io.Serializable {
    private long dumpId;
    private float latitude;
    private String comment;
    private long checkId;
    private Date eventDate;
    private String customAddress;
    private float longitude;

    public long getDumpId() {
        return this.dumpId;
    }

    public void setDumpId(long dumpId) {
        this.dumpId = dumpId;
    }

    public double getLatitude() {
        return this.latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public String getComment() {
        return this.comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public long getCheckId() {
        return this.checkId;
    }

    public void setCheckId(long checkId) {
        this.checkId = checkId;
    }

    public Date getEventDate() {
        return this.eventDate;
    }

    public void setEventDate(Date eventDate) {
        this.eventDate = eventDate;
    }

    public String getCustomAddress() {
        return this.customAddress;
    }

    public void setCustomAddress(String customAddress) {
        this.customAddress = customAddress;
    }

    public double getLongitude() {
        return this.longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }
}
