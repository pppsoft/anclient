package com.petrosoft.smena.server.response.data.svalka;

import com.google.gson.annotations.SerializedName;

import com.petrosoft.smena.server.response.data.SimpleItem;

import java.io.Serializable;
import java.util.List;

/**
 * Created by kovalev on 25.11.16.
 */

public class Svalka  extends SimpleItem implements Serializable {
    @SerializedName("customAddress")
    private String custom_address;
    public float latitude;
    public float longitude;
    public int width;
    public int length;

    public String capacity;

    @SerializedName("wasteClasses")
    public List<WasteClass> wasteClasses;

    @SerializedName("wasteTypes")
    public List<WasteTypeComplex> wasteTypes;

    public String getCustom_address() {
        return custom_address;
    }

    public void setCustom_address(String custom_address) {
        this.custom_address = custom_address;
    }
}
