package com.petrosoft.smena.server.response.data.plan;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by a.baskakov on 16/08/16.
 */
public class ClaimData implements Serializable{
    public int id;
    public String name;
    public String comment;
    public String custom_address;
    public int green_object_id;
    public boolean is_completed;
    public int claim_status_id;
    public float latitude;
    public float longtitude;
    public boolean is_archive;
    public String registered_at;
    public int ext_id;
    public boolean is_external;
    public String last_external_update;
    public String last_external_status_update;
    public String city_object_name;
    public String category_name;
    public int district_id;
    public String expected_answer_dt;
    public String executor;
    public String ext_status_name;
}
