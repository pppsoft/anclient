package com.petrosoft.smena;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.TextView;

import com.facebook.stetho.Stetho;
import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.petrosoft.smena.server.Api;
import com.petrosoft.smena.server.AuthInterceptor;
import com.petrosoft.smena.server.PendingImageApi;
import com.petrosoft.smena.server.SvalkaApi;
import com.petrosoft.smena.service.SentPendingDataService;
import com.petrosoft.smena.utils.PrefsHelper;

import java.io.File;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;


/**
 * Created by kovalev on 12.09.16.
 */
public class GreenApp extends Application {

    private static Api api;
    private static SvalkaApi svalkaApi;
    private static PendingImageApi pendingImageApi;
    private static GreenApp ourInstance = new GreenApp();

    public static GreenApp getInstance() {
        return ourInstance;
    }

    public static void showNoNetworkSnackbar(View sv, View.OnClickListener l) {
        Snackbar sb = Snackbar.make(sv, R.string.no_network, Snackbar.LENGTH_LONG).setAction("Повторить вход", l);
        View v = sb.getView();
        v.setBackgroundColor(Color.BLACK);
        TextView tv = (TextView) v.findViewById(android.support.design.R.id.snackbar_text);
        tv.setTextColor(Color.WHITE);
        sb.show();
    }

    public static Snackbar ownSnackbar(View sv, String message) {
        Snackbar sb = Snackbar.make(sv, message, Snackbar.LENGTH_LONG);
        View v = sb.getView();
        v.setBackgroundColor(Color.BLACK);
        TextView tv = (TextView) v.findViewById(android.support.design.R.id.snackbar_text);
        tv.setTextColor(Color.WHITE);
        return sb;
    }

    public static boolean checkNetwork() {
        ConnectivityManager cm = (ConnectivityManager) GreenApp.getInstance().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        return !(ni == null || !ni.isAvailable() || !ni.isConnected());
    }

//    public static Context getContext() {
//        return this.getApplicationContext();
//    }

    public Api getApi() {
        return api;
    }

    public PendingImageApi getPendingImageApi() {
        return pendingImageApi;
    }

    public SvalkaApi getSvalkaApi() {
        return svalkaApi;
    }

    private static long svalkaCheckId;

    public long getSvalkaCheckId() {
        return svalkaCheckId;
    }

    public void onCreate() {
        super.onCreate();
        // Create an InitializerBuilder
        Stetho.initializeWithDefaults(this);
        // this.mContext = getApplicationContext();
        ourInstance = this;
        String serverUrl = PrefsHelper.loadServerUrl(ourInstance.getApplicationContext());
        recreateNetwork(serverUrl);
        svalkaCheckId = -1L;
        Intent piIntent = new Intent(getApplicationContext(), SentPendingDataService.class);
        startService(piIntent);

    }

    public void recreateNetwork(String serverUrl) {
        File cacheDir = new File(this.getCacheDir(), "caches");
        Cache cache = new Cache(cacheDir, 24 * 1024 * 1024);
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(new AuthInterceptor())
                .addNetworkInterceptor(new StethoInterceptor())
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .retryOnConnectionFailure(true)
                .writeTimeout(600, TimeUnit.SECONDS)
                .cache(cache)
                .build();
        //FIXME server full timezone
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ss")
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(serverUrl + "api/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();

        Retrofit retrofitclean = new Retrofit.Builder()
                .baseUrl(serverUrl + "api/")
                .addConverterFactory(ScalarsConverterFactory.create())
                .client(okHttpClient)
                .build();

        Retrofit retrofit_svalka = new Retrofit.Builder()
                .baseUrl(serverUrl + "api_svalka/")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(okHttpClient)
                .build();

        api = retrofit.create(Api.class);
        svalkaApi = retrofit_svalka.create(SvalkaApi.class);
        pendingImageApi = retrofitclean.create(PendingImageApi.class);
    }

    public void setSvalkaCheckId(long id) {
        svalkaCheckId = id;
    }
}
