package com.petrosoft.smena.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.petrosoft.smena.dao.CObject;
import com.petrosoft.smena.dao.CObjectDao;
import com.petrosoft.smena.dao.CheckTable;
import com.petrosoft.smena.dao.CheckTableDao;
import com.petrosoft.smena.dao.DaoMaster;
import com.petrosoft.smena.dao.DaoSession;
import com.petrosoft.smena.server.IRequestListener;
import com.petrosoft.smena.server.RequestHelper;
import com.petrosoft.smena.server.response.data.check.CheckData;

/**
 * Created by alexey on 03/07/15.
 */
public class DbHelper {

    private static volatile DaoMaster daoMaster;

    public static DaoMaster getDaoMaster(Context context) {
        DaoMaster localDaoMaster = daoMaster;
        if (localDaoMaster == null) {
            synchronized (DaoMaster.class) {
                localDaoMaster = daoMaster;
                if (localDaoMaster == null) {
                    DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(context.getApplicationContext(), "dbsmena", null);
                    SQLiteDatabase db = helper.getWritableDatabase();
                    daoMaster = localDaoMaster = new DaoMaster(db);
                }
            }
        }
        return localDaoMaster;
    }

    public static CObject getCheckCObject(Context context) {
        DaoSession daoSession = DbHelper.getDaoMaster(context).newSession();
        CObjectDao cObjectDao = daoSession.getCObjectDao();
        return cObjectDao.queryBuilder().where(CObjectDao.Properties.Check.eq(true)).unique();
    }

    public static CheckTable getCurrentCheck(Context context) {
        DaoSession daoSession = DbHelper.getDaoMaster(context).newSession();
        CheckTableDao checkTableDao = daoSession.getCheckTableDao();
        return checkTableDao.queryBuilder().where(CheckTableDao.Properties.Finished.eq(false)).unique();
    }

    public static void finishCurrentCheck(Context context) {
        CheckTable check = getCurrentCheck(context);
        if (check != null) {
            check.setFinished(true);
            DaoSession daoSession = DbHelper.getDaoMaster(context).newSession();
            CheckTableDao checkTableDao = daoSession.getCheckTableDao();
            checkTableDao.update(check);
            RequestHelper.tryToCloseCheckAsync(context, check, new IRequestListener<Boolean>() {
                @Override
                public void success(Boolean element) {

                }

                @Override
                public void error() {

                }
            });
        }
        finishCurrentCObject(context);
    }

    public static void finishCurrentCObject(Context context) {
        CObject cObject = getCheckCObject(context);
        if (cObject != null) {
            cObject.setCheck(false);
            DaoSession daoSession = DbHelper.getDaoMaster(context).newSession();
            CObjectDao cObjectDao = daoSession.getCObjectDao();
            cObjectDao.update(cObject);
        }
    }

    public static void setDataFromCheckDataToCheckTable(CheckData checkData, CheckTable check) {
        check.setLoadedFromServer(true);
        check.setServerId(checkData.id);
        check.setStart(checkData.start);
        check.setEnd(checkData.end);
        check.setIs_completed(checkData.is_completed);
      //  check.setCreated_at(checkData.created_at);
       // check.setUpdated_at(checkData.updated_at);
        check.setUser_id(checkData.user_id);
        check.setObject_type_id(checkData.object_type_id);
    }
}
