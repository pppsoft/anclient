package com.petrosoft.smena.dao;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import de.greenrobot.dao.AbstractDao;
import de.greenrobot.dao.Property;
import de.greenrobot.dao.internal.DaoConfig;

import com.petrosoft.smena.dao.CheckTable;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.
/** 
 * DAO for table CHECK_TABLE.
*/
public class CheckTableDao extends AbstractDao<CheckTable, Long> {

    public static final String TABLENAME = "CHECK_TABLE";

    /**
     * Properties of entity CheckTable.<br/>
     * Can be used for QueryBuilder and for referencing column names.
    */
    public static class Properties {
        public final static Property Id = new Property(0, Long.class, "id", true, "_id");
        public final static Property ServerId = new Property(1, long.class, "serverId", false, "SERVER_ID");
        public final static Property Start = new Property(2, String.class, "start", false, "START");
        public final static Property End = new Property(3, String.class, "end", false, "END");
        public final static Property Is_completed = new Property(4, Boolean.class, "is_completed", false, "IS_COMPLETED");
        public final static Property User_id = new Property(5, Long.class, "user_id", false, "USER_ID");
        public final static Property Object_type_id = new Property(6, Long.class, "object_type_id", false, "OBJECT_TYPE_ID");
        public final static Property Date_request = new Property(7, Long.class, "date_request", false, "DATE_REQUEST");
        public final static Property Object_type_id_request = new Property(8, Long.class, "object_type_id_request", false, "OBJECT_TYPE_ID_REQUEST");
        public final static Property Finished = new Property(9, Boolean.class, "finished", false, "FINISHED");
        public final static Property Sent = new Property(10, Boolean.class, "sent", false, "SENT");
        public final static Property LoadedFromServer = new Property(11, Boolean.class, "loadedFromServer", false, "LOADED_FROM_SERVER");
    };

    private DaoSession daoSession;


    public CheckTableDao(DaoConfig config) {
        super(config);
    }
    
    public CheckTableDao(DaoConfig config, DaoSession daoSession) {
        super(config, daoSession);
        this.daoSession = daoSession;
    }

    /** Creates the underlying database table. */
    public static void createTable(SQLiteDatabase db, boolean ifNotExists) {
        String constraint = ifNotExists? "IF NOT EXISTS ": "";
        db.execSQL("CREATE TABLE " + constraint + "'CHECK_TABLE' (" + //
                "'_id' INTEGER PRIMARY KEY AUTOINCREMENT ," + // 0: id
                "'SERVER_ID' INTEGER NOT NULL ," + // 1: serverId
                "'START' TEXT," + // 2: start
                "'END' TEXT," + // 3: end
                "'IS_COMPLETED' INTEGER," + // 4: is_completed
                "'USER_ID' INTEGER," + // 5: user_id
                "'OBJECT_TYPE_ID' INTEGER," + // 6: object_type_id
                "'DATE_REQUEST' INTEGER," + // 7: date_request
                "'OBJECT_TYPE_ID_REQUEST' INTEGER," + // 8: object_type_id_request
                "'FINISHED' INTEGER," + // 9: finished
                "'SENT' INTEGER," + // 10: sent
                "'LOADED_FROM_SERVER' INTEGER);"); // 11: loadedFromServer
    }

    /** Drops the underlying database table. */
    public static void dropTable(SQLiteDatabase db, boolean ifExists) {
        String sql = "DROP TABLE " + (ifExists ? "IF EXISTS " : "") + "'CHECK_TABLE'";
        db.execSQL(sql);
    }

    /** @inheritdoc */
    @Override
    protected void bindValues(SQLiteStatement stmt, CheckTable entity) {
        stmt.clearBindings();
 
        Long id = entity.getId();
        if (id != null) {
            stmt.bindLong(1, id);
        }
        stmt.bindLong(2, entity.getServerId());
 
        String start = entity.getStart();
        if (start != null) {
            stmt.bindString(3, start);
        }
 
        String end = entity.getEnd();
        if (end != null) {
            stmt.bindString(4, end);
        }
 
        Boolean is_completed = entity.getIs_completed();
        if (is_completed != null) {
            stmt.bindLong(5, is_completed ? 1l: 0l);
        }
 
        Long user_id = entity.getUser_id();
        if (user_id != null) {
            stmt.bindLong(6, user_id);
        }
 
        Long object_type_id = entity.getObject_type_id();
        if (object_type_id != null) {
            stmt.bindLong(7, object_type_id);
        }
 
        Long date_request = entity.getDate_request();
        if (date_request != null) {
            stmt.bindLong(8, date_request);
        }
 
        Long object_type_id_request = entity.getObject_type_id_request();
        if (object_type_id_request != null) {
            stmt.bindLong(9, object_type_id_request);
        }
 
        Boolean finished = entity.getFinished();
        if (finished != null) {
            stmt.bindLong(10, finished ? 1l: 0l);
        }
 
        Boolean sent = entity.getSent();
        if (sent != null) {
            stmt.bindLong(11, sent ? 1l: 0l);
        }
 
        Boolean loadedFromServer = entity.getLoadedFromServer();
        if (loadedFromServer != null) {
            stmt.bindLong(12, loadedFromServer ? 1l: 0l);
        }
    }

    @Override
    protected void attachEntity(CheckTable entity) {
        super.attachEntity(entity);
        entity.__setDaoSession(daoSession);
    }

    /** @inheritdoc */
    @Override
    public Long readKey(Cursor cursor, int offset) {
        return cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0);
    }    

    /** @inheritdoc */
    @Override
    public CheckTable readEntity(Cursor cursor, int offset) {
        CheckTable entity = new CheckTable( //
            cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0), // id
            cursor.getLong(offset + 1), // serverId
            cursor.isNull(offset + 2) ? null : cursor.getString(offset + 2), // start
            cursor.isNull(offset + 3) ? null : cursor.getString(offset + 3), // end
            cursor.isNull(offset + 4) ? null : cursor.getShort(offset + 4) != 0, // is_completed
            cursor.isNull(offset + 5) ? null : cursor.getLong(offset + 5), // user_id
            cursor.isNull(offset + 6) ? null : cursor.getLong(offset + 6), // object_type_id
            cursor.isNull(offset + 7) ? null : cursor.getLong(offset + 7), // date_request
            cursor.isNull(offset + 8) ? null : cursor.getLong(offset + 8), // object_type_id_request
            cursor.isNull(offset + 9) ? null : cursor.getShort(offset + 9) != 0, // finished
            cursor.isNull(offset + 10) ? null : cursor.getShort(offset + 10) != 0, // sent
            cursor.isNull(offset + 11) ? null : cursor.getShort(offset + 11) != 0 // loadedFromServer
        );
        return entity;
    }
     
    /** @inheritdoc */
    @Override
    public void readEntity(Cursor cursor, CheckTable entity, int offset) {
        entity.setId(cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0));
        entity.setServerId(cursor.getLong(offset + 1));
        entity.setStart(cursor.isNull(offset + 2) ? null : cursor.getString(offset + 2));
        entity.setEnd(cursor.isNull(offset + 3) ? null : cursor.getString(offset + 3));
        entity.setIs_completed(cursor.isNull(offset + 4) ? null : cursor.getShort(offset + 4) != 0);
        entity.setUser_id(cursor.isNull(offset + 5) ? null : cursor.getLong(offset + 5));
        entity.setObject_type_id(cursor.isNull(offset + 6) ? null : cursor.getLong(offset + 6));
        entity.setDate_request(cursor.isNull(offset + 7) ? null : cursor.getLong(offset + 7));
        entity.setObject_type_id_request(cursor.isNull(offset + 8) ? null : cursor.getLong(offset + 8));
        entity.setFinished(cursor.isNull(offset + 9) ? null : cursor.getShort(offset + 9) != 0);
        entity.setSent(cursor.isNull(offset + 10) ? null : cursor.getShort(offset + 10) != 0);
        entity.setLoadedFromServer(cursor.isNull(offset + 11) ? null : cursor.getShort(offset + 11) != 0);
     }
    
    /** @inheritdoc */
    @Override
    protected Long updateKeyAfterInsert(CheckTable entity, long rowId) {
        entity.setId(rowId);
        return rowId;
    }
    
    /** @inheritdoc */
    @Override
    public Long getKey(CheckTable entity) {
        if(entity != null) {
            return entity.getId();
        } else {
            return null;
        }
    }

    /** @inheritdoc */
    @Override    
    protected boolean isEntityUpdateable() {
        return true;
    }
    
}
