package com.petrosoft.smena.dao;

import java.util.List;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import de.greenrobot.dao.AbstractDao;
import de.greenrobot.dao.Property;
import de.greenrobot.dao.internal.DaoConfig;
import de.greenrobot.dao.query.Query;
import de.greenrobot.dao.query.QueryBuilder;

import com.petrosoft.smena.dao.Claim;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.
/** 
 * DAO for table CLAIM.
*/
public class ClaimDao extends AbstractDao<Claim, Long> {

    public static final String TABLENAME = "CLAIM";

    /**
     * Properties of entity Claim.<br/>
     * Can be used for QueryBuilder and for referencing column names.
    */
    public static class Properties {
        public final static Property Id = new Property(0, Long.class, "id", true, "_id");
        public final static Property ServerId = new Property(1, long.class, "serverId", false, "SERVER_ID");
        public final static Property LastCheckId = new Property(2, Long.class, "LastCheckId", false, "LAST_CHECK_ID");
        public final static Property Sent = new Property(3, Boolean.class, "Sent", false, "SENT");
        public final static Property CObjectId = new Property(4, long.class, "cObjectId", false, "C_OBJECT_ID");
        public final static Property Name = new Property(5, String.class, "name", false, "NAME");
        public final static Property Comment = new Property(6, String.class, "comment", false, "COMMENT");
        public final static Property CustomAddress = new Property(7, String.class, "customAddress", false, "CUSTOM_ADDRESS");
        public final static Property GreenObjectId = new Property(8, Long.class, "greenObjectId", false, "GREEN_OBJECT_ID");
        public final static Property IsCompleted = new Property(9, Boolean.class, "isCompleted", false, "IS_COMPLETED");
        public final static Property ClaimStatusId = new Property(10, Long.class, "claimStatusId", false, "CLAIM_STATUS_ID");
        public final static Property Latitude = new Property(11, Float.class, "latitude", false, "LATITUDE");
        public final static Property Longtitude = new Property(12, Float.class, "longtitude", false, "LONGTITUDE");
        public final static Property IsArchive = new Property(13, Boolean.class, "isArchive", false, "IS_ARCHIVE");
        public final static Property RegisteredAt = new Property(14, String.class, "registeredAt", false, "REGISTERED_AT");
        public final static Property ExtId = new Property(15, Long.class, "extId", false, "EXT_ID");
        public final static Property IsExternal = new Property(16, Boolean.class, "isExternal", false, "IS_EXTERNAL");
        public final static Property LastExternalUpdate = new Property(17, String.class, "lastExternalUpdate", false, "LAST_EXTERNAL_UPDATE");
        public final static Property LastExternalStatusUpdate = new Property(18, String.class, "lastExternalStatusUpdate", false, "LAST_EXTERNAL_STATUS_UPDATE");
        public final static Property CityObjectName = new Property(19, String.class, "cityObjectName", false, "CITY_OBJECT_NAME");
        public final static Property CategoryName = new Property(20, String.class, "categoryName", false, "CATEGORY_NAME");
        public final static Property DistrictId = new Property(21, Long.class, "districtId", false, "DISTRICT_ID");
        public final static Property ExpectedAnswerDt = new Property(22, String.class, "expectedAnswerDt", false, "EXPECTED_ANSWER_DT");
        public final static Property Executor = new Property(23, String.class, "executor", false, "EXECUTOR");
        public final static Property ExtStatusName = new Property(24, String.class, "extStatusName", false, "EXT_STATUS_NAME");
    };

    private DaoSession daoSession;

    private Query<Claim> cObject_ClaimsQuery;

    public ClaimDao(DaoConfig config) {
        super(config);
    }
    
    public ClaimDao(DaoConfig config, DaoSession daoSession) {
        super(config, daoSession);
        this.daoSession = daoSession;
    }

    /** Creates the underlying database table. */
    public static void createTable(SQLiteDatabase db, boolean ifNotExists) {
        String constraint = ifNotExists? "IF NOT EXISTS ": "";
        db.execSQL("CREATE TABLE " + constraint + "'CLAIM' (" + //
                "'_id' INTEGER PRIMARY KEY AUTOINCREMENT ," + // 0: id
                "'SERVER_ID' INTEGER NOT NULL ," + // 1: serverId
                "'LAST_CHECK_ID' INTEGER," + // 2: LastCheckId
                "'SENT' INTEGER," + // 3: Sent
                "'C_OBJECT_ID' INTEGER NOT NULL ," + // 4: cObjectId
                "'NAME' TEXT," + // 5: name
                "'COMMENT' TEXT," + // 6: comment
                "'CUSTOM_ADDRESS' TEXT," + // 7: customAddress
                "'GREEN_OBJECT_ID' INTEGER," + // 8: greenObjectId
                "'IS_COMPLETED' INTEGER," + // 9: isCompleted
                "'CLAIM_STATUS_ID' INTEGER," + // 10: claimStatusId
                "'LATITUDE' REAL," + // 11: latitude
                "'LONGTITUDE' REAL," + // 12: longtitude
                "'IS_ARCHIVE' INTEGER," + // 13: isArchive
                "'REGISTERED_AT' TEXT," + // 14: registeredAt
                "'EXT_ID' INTEGER," + // 15: extId
                "'IS_EXTERNAL' INTEGER," + // 16: isExternal
                "'LAST_EXTERNAL_UPDATE' TEXT," + // 17: lastExternalUpdate
                "'LAST_EXTERNAL_STATUS_UPDATE' TEXT," + // 18: lastExternalStatusUpdate
                "'CITY_OBJECT_NAME' TEXT," + // 19: cityObjectName
                "'CATEGORY_NAME' TEXT," + // 20: categoryName
                "'DISTRICT_ID' INTEGER," + // 21: districtId
                "'EXPECTED_ANSWER_DT' TEXT," + // 22: expectedAnswerDt
                "'EXECUTOR' TEXT," + // 23: executor
                "'EXT_STATUS_NAME' TEXT);"); // 24: extStatusName
    }

    /** Drops the underlying database table. */
    public static void dropTable(SQLiteDatabase db, boolean ifExists) {
        String sql = "DROP TABLE " + (ifExists ? "IF EXISTS " : "") + "'CLAIM'";
        db.execSQL(sql);
    }

    /** @inheritdoc */
    @Override
    protected void bindValues(SQLiteStatement stmt, Claim entity) {
        stmt.clearBindings();
 
        Long id = entity.getId();
        if (id != null) {
            stmt.bindLong(1, id);
        }
        stmt.bindLong(2, entity.getServerId());
 
        Long LastCheckId = entity.getLastCheckId();
        if (LastCheckId != null) {
            stmt.bindLong(3, LastCheckId);
        }
 
        Boolean Sent = entity.getSent();
        if (Sent != null) {
            stmt.bindLong(4, Sent ? 1l: 0l);
        }
        stmt.bindLong(5, entity.getCObjectId());
 
        String name = entity.getName();
        if (name != null) {
            stmt.bindString(6, name);
        }
 
        String comment = entity.getComment();
        if (comment != null) {
            stmt.bindString(7, comment);
        }
 
        String customAddress = entity.getCustomAddress();
        if (customAddress != null) {
            stmt.bindString(8, customAddress);
        }
 
        Long greenObjectId = entity.getGreenObjectId();
        if (greenObjectId != null) {
            stmt.bindLong(9, greenObjectId);
        }
 
        Boolean isCompleted = entity.getIsCompleted();
        if (isCompleted != null) {
            stmt.bindLong(10, isCompleted ? 1l: 0l);
        }
 
        Long claimStatusId = entity.getClaimStatusId();
        if (claimStatusId != null) {
            stmt.bindLong(11, claimStatusId);
        }
 
        Float latitude = entity.getLatitude();
        if (latitude != null) {
            stmt.bindDouble(12, latitude);
        }
 
        Float longtitude = entity.getLongtitude();
        if (longtitude != null) {
            stmt.bindDouble(13, longtitude);
        }
 
        Boolean isArchive = entity.getIsArchive();
        if (isArchive != null) {
            stmt.bindLong(14, isArchive ? 1l: 0l);
        }
 
        String registeredAt = entity.getRegisteredAt();
        if (registeredAt != null) {
            stmt.bindString(15, registeredAt);
        }
 
        Long extId = entity.getExtId();
        if (extId != null) {
            stmt.bindLong(16, extId);
        }
 
        Boolean isExternal = entity.getIsExternal();
        if (isExternal != null) {
            stmt.bindLong(17, isExternal ? 1l: 0l);
        }
 
        String lastExternalUpdate = entity.getLastExternalUpdate();
        if (lastExternalUpdate != null) {
            stmt.bindString(18, lastExternalUpdate);
        }
 
        String lastExternalStatusUpdate = entity.getLastExternalStatusUpdate();
        if (lastExternalStatusUpdate != null) {
            stmt.bindString(19, lastExternalStatusUpdate);
        }
 
        String cityObjectName = entity.getCityObjectName();
        if (cityObjectName != null) {
            stmt.bindString(20, cityObjectName);
        }
 
        String categoryName = entity.getCategoryName();
        if (categoryName != null) {
            stmt.bindString(21, categoryName);
        }
 
        Long districtId = entity.getDistrictId();
        if (districtId != null) {
            stmt.bindLong(22, districtId);
        }
 
        String expectedAnswerDt = entity.getExpectedAnswerDt();
        if (expectedAnswerDt != null) {
            stmt.bindString(23, expectedAnswerDt);
        }
 
        String executor = entity.getExecutor();
        if (executor != null) {
            stmt.bindString(24, executor);
        }
 
        String extStatusName = entity.getExtStatusName();
        if (extStatusName != null) {
            stmt.bindString(25, extStatusName);
        }
    }

    @Override
    protected void attachEntity(Claim entity) {
        super.attachEntity(entity);
        entity.__setDaoSession(daoSession);
    }

    /** @inheritdoc */
    @Override
    public Long readKey(Cursor cursor, int offset) {
        return cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0);
    }    

    /** @inheritdoc */
    @Override
    public Claim readEntity(Cursor cursor, int offset) {
        Claim entity = new Claim( //
            cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0), // id
            cursor.getLong(offset + 1), // serverId
            cursor.isNull(offset + 2) ? null : cursor.getLong(offset + 2), // LastCheckId
            cursor.isNull(offset + 3) ? null : cursor.getShort(offset + 3) != 0, // Sent
            cursor.getLong(offset + 4), // cObjectId
            cursor.isNull(offset + 5) ? null : cursor.getString(offset + 5), // name
            cursor.isNull(offset + 6) ? null : cursor.getString(offset + 6), // comment
            cursor.isNull(offset + 7) ? null : cursor.getString(offset + 7), // customAddress
            cursor.isNull(offset + 8) ? null : cursor.getLong(offset + 8), // greenObjectId
            cursor.isNull(offset + 9) ? null : cursor.getShort(offset + 9) != 0, // isCompleted
            cursor.isNull(offset + 10) ? null : cursor.getLong(offset + 10), // claimStatusId
            cursor.isNull(offset + 11) ? null : cursor.getFloat(offset + 11), // latitude
            cursor.isNull(offset + 12) ? null : cursor.getFloat(offset + 12), // longtitude
            cursor.isNull(offset + 13) ? null : cursor.getShort(offset + 13) != 0, // isArchive
            cursor.isNull(offset + 14) ? null : cursor.getString(offset + 14), // registeredAt
            cursor.isNull(offset + 15) ? null : cursor.getLong(offset + 15), // extId
            cursor.isNull(offset + 16) ? null : cursor.getShort(offset + 16) != 0, // isExternal
            cursor.isNull(offset + 17) ? null : cursor.getString(offset + 17), // lastExternalUpdate
            cursor.isNull(offset + 18) ? null : cursor.getString(offset + 18), // lastExternalStatusUpdate
            cursor.isNull(offset + 19) ? null : cursor.getString(offset + 19), // cityObjectName
            cursor.isNull(offset + 20) ? null : cursor.getString(offset + 20), // categoryName
            cursor.isNull(offset + 21) ? null : cursor.getLong(offset + 21), // districtId
            cursor.isNull(offset + 22) ? null : cursor.getString(offset + 22), // expectedAnswerDt
            cursor.isNull(offset + 23) ? null : cursor.getString(offset + 23), // executor
            cursor.isNull(offset + 24) ? null : cursor.getString(offset + 24) // extStatusName
        );
        return entity;
    }
     
    /** @inheritdoc */
    @Override
    public void readEntity(Cursor cursor, Claim entity, int offset) {
        entity.setId(cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0));
        entity.setServerId(cursor.getLong(offset + 1));
        entity.setLastCheckId(cursor.isNull(offset + 2) ? null : cursor.getLong(offset + 2));
        entity.setSent(cursor.isNull(offset + 3) ? null : cursor.getShort(offset + 3) != 0);
        entity.setCObjectId(cursor.getLong(offset + 4));
        entity.setName(cursor.isNull(offset + 5) ? null : cursor.getString(offset + 5));
        entity.setComment(cursor.isNull(offset + 6) ? null : cursor.getString(offset + 6));
        entity.setCustomAddress(cursor.isNull(offset + 7) ? null : cursor.getString(offset + 7));
        entity.setGreenObjectId(cursor.isNull(offset + 8) ? null : cursor.getLong(offset + 8));
        entity.setIsCompleted(cursor.isNull(offset + 9) ? null : cursor.getShort(offset + 9) != 0);
        entity.setClaimStatusId(cursor.isNull(offset + 10) ? null : cursor.getLong(offset + 10));
        entity.setLatitude(cursor.isNull(offset + 11) ? null : cursor.getFloat(offset + 11));
        entity.setLongtitude(cursor.isNull(offset + 12) ? null : cursor.getFloat(offset + 12));
        entity.setIsArchive(cursor.isNull(offset + 13) ? null : cursor.getShort(offset + 13) != 0);
        entity.setRegisteredAt(cursor.isNull(offset + 14) ? null : cursor.getString(offset + 14));
        entity.setExtId(cursor.isNull(offset + 15) ? null : cursor.getLong(offset + 15));
        entity.setIsExternal(cursor.isNull(offset + 16) ? null : cursor.getShort(offset + 16) != 0);
        entity.setLastExternalUpdate(cursor.isNull(offset + 17) ? null : cursor.getString(offset + 17));
        entity.setLastExternalStatusUpdate(cursor.isNull(offset + 18) ? null : cursor.getString(offset + 18));
        entity.setCityObjectName(cursor.isNull(offset + 19) ? null : cursor.getString(offset + 19));
        entity.setCategoryName(cursor.isNull(offset + 20) ? null : cursor.getString(offset + 20));
        entity.setDistrictId(cursor.isNull(offset + 21) ? null : cursor.getLong(offset + 21));
        entity.setExpectedAnswerDt(cursor.isNull(offset + 22) ? null : cursor.getString(offset + 22));
        entity.setExecutor(cursor.isNull(offset + 23) ? null : cursor.getString(offset + 23));
        entity.setExtStatusName(cursor.isNull(offset + 24) ? null : cursor.getString(offset + 24));
     }
    
    /** @inheritdoc */
    @Override
    protected Long updateKeyAfterInsert(Claim entity, long rowId) {
        entity.setId(rowId);
        return rowId;
    }
    
    /** @inheritdoc */
    @Override
    public Long getKey(Claim entity) {
        if(entity != null) {
            return entity.getId();
        } else {
            return null;
        }
    }

    /** @inheritdoc */
    @Override    
    protected boolean isEntityUpdateable() {
        return true;
    }
    
    /** Internal query to resolve the "claims" to-many relationship of CObject. */
    public List<Claim> _queryCObject_Claims(long cObjectId) {
        synchronized (this) {
            if (cObject_ClaimsQuery == null) {
                QueryBuilder<Claim> queryBuilder = queryBuilder();
                queryBuilder.where(Properties.CObjectId.eq(null));
                cObject_ClaimsQuery = queryBuilder.build();
            }
        }
        Query<Claim> query = cObject_ClaimsQuery.forCurrentThread();
        query.setParameter(0, cObjectId);
        return query.list();
    }

}
