package com.petrosoft.smena.dao;

// THIS CODE IS GENERATED BY greenDAO, EDIT ONLY INSIDE THE "KEEP"-SECTIONS

// KEEP INCLUDES - put your custom includes here
// KEEP INCLUDES END
/**
 * Entity mapped to table ISSUE_IMAGE.
 */
public class IssueImage {

    private Long id;
    private String path;
    private Boolean sent;
    private long issueId;

    // KEEP FIELDS - put your custom fields here
    // KEEP FIELDS END

    public IssueImage() {
    }

    public IssueImage(Long id) {
        this.id = id;
    }

    public IssueImage(Long id, String path, Boolean sent, long issueId) {
        this.id = id;
        this.path = path;
        this.sent = sent;
        this.issueId = issueId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Boolean getSent() {
        return sent;
    }

    public void setSent(Boolean sent) {
        this.sent = sent;
    }

    public long getIssueId() {
        return issueId;
    }

    public void setIssueId(long issueId) {
        this.issueId = issueId;
    }

    // KEEP METHODS - put your custom methods here
    // KEEP METHODS END

}
