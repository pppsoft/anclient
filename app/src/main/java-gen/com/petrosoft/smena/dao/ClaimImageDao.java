package com.petrosoft.smena.dao;

import java.util.List;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import de.greenrobot.dao.AbstractDao;
import de.greenrobot.dao.Property;
import de.greenrobot.dao.internal.DaoConfig;
import de.greenrobot.dao.query.Query;
import de.greenrobot.dao.query.QueryBuilder;

import com.petrosoft.smena.dao.ClaimImage;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.
/** 
 * DAO for table CLAIM_IMAGE.
*/
public class ClaimImageDao extends AbstractDao<ClaimImage, Long> {

    public static final String TABLENAME = "CLAIM_IMAGE";

    /**
     * Properties of entity ClaimImage.<br/>
     * Can be used for QueryBuilder and for referencing column names.
    */
    public static class Properties {
        public final static Property Id = new Property(0, Long.class, "id", true, "_id");
        public final static Property Path = new Property(1, String.class, "path", false, "PATH");
        public final static Property Sent = new Property(2, Boolean.class, "sent", false, "SENT");
        public final static Property ClaimId = new Property(3, long.class, "claimId", false, "CLAIM_ID");
    };

    private Query<ClaimImage> claim_ImagesQuery;

    public ClaimImageDao(DaoConfig config) {
        super(config);
    }
    
    public ClaimImageDao(DaoConfig config, DaoSession daoSession) {
        super(config, daoSession);
    }

    /** Creates the underlying database table. */
    public static void createTable(SQLiteDatabase db, boolean ifNotExists) {
        String constraint = ifNotExists? "IF NOT EXISTS ": "";
        db.execSQL("CREATE TABLE " + constraint + "'CLAIM_IMAGE' (" + //
                "'_id' INTEGER PRIMARY KEY AUTOINCREMENT ," + // 0: id
                "'PATH' TEXT," + // 1: path
                "'SENT' INTEGER," + // 2: sent
                "'CLAIM_ID' INTEGER NOT NULL );"); // 3: claimId
    }

    /** Drops the underlying database table. */
    public static void dropTable(SQLiteDatabase db, boolean ifExists) {
        String sql = "DROP TABLE " + (ifExists ? "IF EXISTS " : "") + "'CLAIM_IMAGE'";
        db.execSQL(sql);
    }

    /** @inheritdoc */
    @Override
    protected void bindValues(SQLiteStatement stmt, ClaimImage entity) {
        stmt.clearBindings();
 
        Long id = entity.getId();
        if (id != null) {
            stmt.bindLong(1, id);
        }
 
        String path = entity.getPath();
        if (path != null) {
            stmt.bindString(2, path);
        }
 
        Boolean sent = entity.getSent();
        if (sent != null) {
            stmt.bindLong(3, sent ? 1l: 0l);
        }
        stmt.bindLong(4, entity.getClaimId());
    }

    /** @inheritdoc */
    @Override
    public Long readKey(Cursor cursor, int offset) {
        return cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0);
    }    

    /** @inheritdoc */
    @Override
    public ClaimImage readEntity(Cursor cursor, int offset) {
        ClaimImage entity = new ClaimImage( //
            cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0), // id
            cursor.isNull(offset + 1) ? null : cursor.getString(offset + 1), // path
            cursor.isNull(offset + 2) ? null : cursor.getShort(offset + 2) != 0, // sent
            cursor.getLong(offset + 3) // claimId
        );
        return entity;
    }
     
    /** @inheritdoc */
    @Override
    public void readEntity(Cursor cursor, ClaimImage entity, int offset) {
        entity.setId(cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0));
        entity.setPath(cursor.isNull(offset + 1) ? null : cursor.getString(offset + 1));
        entity.setSent(cursor.isNull(offset + 2) ? null : cursor.getShort(offset + 2) != 0);
        entity.setClaimId(cursor.getLong(offset + 3));
     }
    
    /** @inheritdoc */
    @Override
    protected Long updateKeyAfterInsert(ClaimImage entity, long rowId) {
        entity.setId(rowId);
        return rowId;
    }
    
    /** @inheritdoc */
    @Override
    public Long getKey(ClaimImage entity) {
        if(entity != null) {
            return entity.getId();
        } else {
            return null;
        }
    }

    /** @inheritdoc */
    @Override    
    protected boolean isEntityUpdateable() {
        return true;
    }
    
    /** Internal query to resolve the "Images" to-many relationship of Claim. */
    public List<ClaimImage> _queryClaim_Images(long claimId) {
        synchronized (this) {
            if (claim_ImagesQuery == null) {
                QueryBuilder<ClaimImage> queryBuilder = queryBuilder();
                queryBuilder.where(Properties.ClaimId.eq(null));
                claim_ImagesQuery = queryBuilder.build();
            }
        }
        Query<ClaimImage> query = claim_ImagesQuery.forCurrentThread();
        query.setParameter(0, claimId);
        return query.list();
    }

}
