package com.petrosoft.smena.dao;

import android.database.sqlite.SQLiteDatabase;

import java.util.Map;

import de.greenrobot.dao.AbstractDao;
import de.greenrobot.dao.AbstractDaoSession;
import de.greenrobot.dao.identityscope.IdentityScopeType;
import de.greenrobot.dao.internal.DaoConfig;

import com.petrosoft.smena.dao.ObjectType;
import com.petrosoft.smena.dao.CObject;
import com.petrosoft.smena.dao.Task;
import com.petrosoft.smena.dao.Issue;
import com.petrosoft.smena.dao.IssueImage;
import com.petrosoft.smena.dao.Claim;
import com.petrosoft.smena.dao.ClaimImage;
import com.petrosoft.smena.dao.CheckTable;
import com.petrosoft.smena.dao.CheckToObject;
import com.petrosoft.smena.dao.WorkType;
import com.petrosoft.smena.dao.NewIssue;
import com.petrosoft.smena.dao.NewIssueImage;
import com.petrosoft.smena.dao.IssueType;
import com.petrosoft.smena.dao.ObjectTypeToIssueType;
import com.petrosoft.smena.dao.Track;
import com.petrosoft.smena.dao.GreenElement;
import com.petrosoft.smena.dao.CObjectToElements;
import com.petrosoft.smena.dao.GreenElementType;
import com.petrosoft.smena.dao.Route;
import com.petrosoft.smena.dao.PendingImage;

import com.petrosoft.smena.dao.ObjectTypeDao;
import com.petrosoft.smena.dao.CObjectDao;
import com.petrosoft.smena.dao.TaskDao;
import com.petrosoft.smena.dao.IssueDao;
import com.petrosoft.smena.dao.IssueImageDao;
import com.petrosoft.smena.dao.ClaimDao;
import com.petrosoft.smena.dao.ClaimImageDao;
import com.petrosoft.smena.dao.CheckTableDao;
import com.petrosoft.smena.dao.CheckToObjectDao;
import com.petrosoft.smena.dao.WorkTypeDao;
import com.petrosoft.smena.dao.NewIssueDao;
import com.petrosoft.smena.dao.NewIssueImageDao;
import com.petrosoft.smena.dao.IssueTypeDao;
import com.petrosoft.smena.dao.ObjectTypeToIssueTypeDao;
import com.petrosoft.smena.dao.TrackDao;
import com.petrosoft.smena.dao.GreenElementDao;
import com.petrosoft.smena.dao.CObjectToElementsDao;
import com.petrosoft.smena.dao.GreenElementTypeDao;
import com.petrosoft.smena.dao.RouteDao;
import com.petrosoft.smena.dao.PendingImageDao;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.

/**
 * {@inheritDoc}
 * 
 * @see de.greenrobot.dao.AbstractDaoSession
 */
public class DaoSession extends AbstractDaoSession {

    private final DaoConfig objectTypeDaoConfig;
    private final DaoConfig cObjectDaoConfig;
    private final DaoConfig taskDaoConfig;
    private final DaoConfig issueDaoConfig;
    private final DaoConfig issueImageDaoConfig;
    private final DaoConfig claimDaoConfig;
    private final DaoConfig claimImageDaoConfig;
    private final DaoConfig checkTableDaoConfig;
    private final DaoConfig checkToObjectDaoConfig;
    private final DaoConfig workTypeDaoConfig;
    private final DaoConfig newIssueDaoConfig;
    private final DaoConfig newIssueImageDaoConfig;
    private final DaoConfig issueTypeDaoConfig;
    private final DaoConfig objectTypeToIssueTypeDaoConfig;
    private final DaoConfig trackDaoConfig;
    private final DaoConfig greenElementDaoConfig;
    private final DaoConfig cObjectToElementsDaoConfig;
    private final DaoConfig greenElementTypeDaoConfig;
    private final DaoConfig routeDaoConfig;
    private final DaoConfig pendingImageDaoConfig;

    private final ObjectTypeDao objectTypeDao;
    private final CObjectDao cObjectDao;
    private final TaskDao taskDao;
    private final IssueDao issueDao;
    private final IssueImageDao issueImageDao;
    private final ClaimDao claimDao;
    private final ClaimImageDao claimImageDao;
    private final CheckTableDao checkTableDao;
    private final CheckToObjectDao checkToObjectDao;
    private final WorkTypeDao workTypeDao;
    private final NewIssueDao newIssueDao;
    private final NewIssueImageDao newIssueImageDao;
    private final IssueTypeDao issueTypeDao;
    private final ObjectTypeToIssueTypeDao objectTypeToIssueTypeDao;
    private final TrackDao trackDao;
    private final GreenElementDao greenElementDao;
    private final CObjectToElementsDao cObjectToElementsDao;
    private final GreenElementTypeDao greenElementTypeDao;
    private final RouteDao routeDao;
    private final PendingImageDao pendingImageDao;

    public DaoSession(SQLiteDatabase db, IdentityScopeType type, Map<Class<? extends AbstractDao<?, ?>>, DaoConfig>
            daoConfigMap) {
        super(db);

        objectTypeDaoConfig = daoConfigMap.get(ObjectTypeDao.class).clone();
        objectTypeDaoConfig.initIdentityScope(type);

        cObjectDaoConfig = daoConfigMap.get(CObjectDao.class).clone();
        cObjectDaoConfig.initIdentityScope(type);

        taskDaoConfig = daoConfigMap.get(TaskDao.class).clone();
        taskDaoConfig.initIdentityScope(type);

        issueDaoConfig = daoConfigMap.get(IssueDao.class).clone();
        issueDaoConfig.initIdentityScope(type);

        issueImageDaoConfig = daoConfigMap.get(IssueImageDao.class).clone();
        issueImageDaoConfig.initIdentityScope(type);

        claimDaoConfig = daoConfigMap.get(ClaimDao.class).clone();
        claimDaoConfig.initIdentityScope(type);

        claimImageDaoConfig = daoConfigMap.get(ClaimImageDao.class).clone();
        claimImageDaoConfig.initIdentityScope(type);

        checkTableDaoConfig = daoConfigMap.get(CheckTableDao.class).clone();
        checkTableDaoConfig.initIdentityScope(type);

        checkToObjectDaoConfig = daoConfigMap.get(CheckToObjectDao.class).clone();
        checkToObjectDaoConfig.initIdentityScope(type);

        workTypeDaoConfig = daoConfigMap.get(WorkTypeDao.class).clone();
        workTypeDaoConfig.initIdentityScope(type);

        newIssueDaoConfig = daoConfigMap.get(NewIssueDao.class).clone();
        newIssueDaoConfig.initIdentityScope(type);

        newIssueImageDaoConfig = daoConfigMap.get(NewIssueImageDao.class).clone();
        newIssueImageDaoConfig.initIdentityScope(type);

        issueTypeDaoConfig = daoConfigMap.get(IssueTypeDao.class).clone();
        issueTypeDaoConfig.initIdentityScope(type);

        objectTypeToIssueTypeDaoConfig = daoConfigMap.get(ObjectTypeToIssueTypeDao.class).clone();
        objectTypeToIssueTypeDaoConfig.initIdentityScope(type);

        trackDaoConfig = daoConfigMap.get(TrackDao.class).clone();
        trackDaoConfig.initIdentityScope(type);

        greenElementDaoConfig = daoConfigMap.get(GreenElementDao.class).clone();
        greenElementDaoConfig.initIdentityScope(type);

        cObjectToElementsDaoConfig = daoConfigMap.get(CObjectToElementsDao.class).clone();
        cObjectToElementsDaoConfig.initIdentityScope(type);

        greenElementTypeDaoConfig = daoConfigMap.get(GreenElementTypeDao.class).clone();
        greenElementTypeDaoConfig.initIdentityScope(type);

        routeDaoConfig = daoConfigMap.get(RouteDao.class).clone();
        routeDaoConfig.initIdentityScope(type);

        pendingImageDaoConfig = daoConfigMap.get(PendingImageDao.class).clone();
        pendingImageDaoConfig.initIdentityScope(type);

        objectTypeDao = new ObjectTypeDao(objectTypeDaoConfig, this);
        cObjectDao = new CObjectDao(cObjectDaoConfig, this);
        taskDao = new TaskDao(taskDaoConfig, this);
        issueDao = new IssueDao(issueDaoConfig, this);
        issueImageDao = new IssueImageDao(issueImageDaoConfig, this);
        claimDao = new ClaimDao(claimDaoConfig, this);
        claimImageDao = new ClaimImageDao(claimImageDaoConfig, this);
        checkTableDao = new CheckTableDao(checkTableDaoConfig, this);
        checkToObjectDao = new CheckToObjectDao(checkToObjectDaoConfig, this);
        workTypeDao = new WorkTypeDao(workTypeDaoConfig, this);
        newIssueDao = new NewIssueDao(newIssueDaoConfig, this);
        newIssueImageDao = new NewIssueImageDao(newIssueImageDaoConfig, this);
        issueTypeDao = new IssueTypeDao(issueTypeDaoConfig, this);
        objectTypeToIssueTypeDao = new ObjectTypeToIssueTypeDao(objectTypeToIssueTypeDaoConfig, this);
        trackDao = new TrackDao(trackDaoConfig, this);
        greenElementDao = new GreenElementDao(greenElementDaoConfig, this);
        cObjectToElementsDao = new CObjectToElementsDao(cObjectToElementsDaoConfig, this);
        greenElementTypeDao = new GreenElementTypeDao(greenElementTypeDaoConfig, this);
        routeDao = new RouteDao(routeDaoConfig, this);
        pendingImageDao = new PendingImageDao(pendingImageDaoConfig, this);

        registerDao(ObjectType.class, objectTypeDao);
        registerDao(CObject.class, cObjectDao);
        registerDao(Task.class, taskDao);
        registerDao(Issue.class, issueDao);
        registerDao(IssueImage.class, issueImageDao);
        registerDao(Claim.class, claimDao);
        registerDao(ClaimImage.class, claimImageDao);
        registerDao(CheckTable.class, checkTableDao);
        registerDao(CheckToObject.class, checkToObjectDao);
        registerDao(WorkType.class, workTypeDao);
        registerDao(NewIssue.class, newIssueDao);
        registerDao(NewIssueImage.class, newIssueImageDao);
        registerDao(IssueType.class, issueTypeDao);
        registerDao(ObjectTypeToIssueType.class, objectTypeToIssueTypeDao);
        registerDao(Track.class, trackDao);
        registerDao(GreenElement.class, greenElementDao);
        registerDao(CObjectToElements.class, cObjectToElementsDao);
        registerDao(GreenElementType.class, greenElementTypeDao);
        registerDao(Route.class, routeDao);
        registerDao(PendingImage.class, pendingImageDao);
    }
    
    public void clear() {
        objectTypeDaoConfig.getIdentityScope().clear();
        cObjectDaoConfig.getIdentityScope().clear();
        taskDaoConfig.getIdentityScope().clear();
        issueDaoConfig.getIdentityScope().clear();
        issueImageDaoConfig.getIdentityScope().clear();
        claimDaoConfig.getIdentityScope().clear();
        claimImageDaoConfig.getIdentityScope().clear();
        checkTableDaoConfig.getIdentityScope().clear();
        checkToObjectDaoConfig.getIdentityScope().clear();
        workTypeDaoConfig.getIdentityScope().clear();
        newIssueDaoConfig.getIdentityScope().clear();
        newIssueImageDaoConfig.getIdentityScope().clear();
        issueTypeDaoConfig.getIdentityScope().clear();
        objectTypeToIssueTypeDaoConfig.getIdentityScope().clear();
        trackDaoConfig.getIdentityScope().clear();
        greenElementDaoConfig.getIdentityScope().clear();
        cObjectToElementsDaoConfig.getIdentityScope().clear();
        greenElementTypeDaoConfig.getIdentityScope().clear();
        routeDaoConfig.getIdentityScope().clear();
        pendingImageDaoConfig.getIdentityScope().clear();
    }

    public ObjectTypeDao getObjectTypeDao() {
        return objectTypeDao;
    }

    public CObjectDao getCObjectDao() {
        return cObjectDao;
    }

    public TaskDao getTaskDao() {
        return taskDao;
    }

    public IssueDao getIssueDao() {
        return issueDao;
    }

    public IssueImageDao getIssueImageDao() {
        return issueImageDao;
    }

    public ClaimDao getClaimDao() {
        return claimDao;
    }

    public ClaimImageDao getClaimImageDao() {
        return claimImageDao;
    }

    public CheckTableDao getCheckTableDao() {
        return checkTableDao;
    }

    public CheckToObjectDao getCheckToObjectDao() {
        return checkToObjectDao;
    }

    public WorkTypeDao getWorkTypeDao() {
        return workTypeDao;
    }

    public NewIssueDao getNewIssueDao() {
        return newIssueDao;
    }

    public NewIssueImageDao getNewIssueImageDao() {
        return newIssueImageDao;
    }

    public IssueTypeDao getIssueTypeDao() {
        return issueTypeDao;
    }

    public ObjectTypeToIssueTypeDao getObjectTypeToIssueTypeDao() {
        return objectTypeToIssueTypeDao;
    }

    public TrackDao getTrackDao() {
        return trackDao;
    }

    public GreenElementDao getGreenElementDao() {
        return greenElementDao;
    }

    public CObjectToElementsDao getCObjectToElementsDao() {
        return cObjectToElementsDao;
    }

    public GreenElementTypeDao getGreenElementTypeDao() {
        return greenElementTypeDao;
    }

    public RouteDao getRouteDao() {
        return routeDao;
    }

    public PendingImageDao getPendingImageDao() {
        return pendingImageDao;
    }

}
