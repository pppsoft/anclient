package com.petrosoft.smena.activity;


import android.content.Context;
import android.content.SharedPreferences;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.ViewInteraction;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import com.petrosoft.smena.GreenApp;
import com.petrosoft.smena.R;
import com.petrosoft.smena.utils.PrefsHelper;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.hamcrest.core.IsInstanceOf;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.concurrent.TimeUnit;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.pressBack;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.longClick;
import static android.support.test.espresso.action.ViewActions.replaceText;
import static android.support.test.espresso.action.ViewActions.scrollTo;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withParent;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class LoginActivityTest {

    private static final String EXTRA_PREFIX = "prefs";
    private static final String PREF_SETTINGS_FILE_NAME = EXTRA_PREFIX + "prefs.xml";
    private static final String PREFS_LAST_LOGIN_DATA = EXTRA_PREFIX + ".last_login_data";
    @Rule
    public ActivityTestRule mActivityTestRule = new ActivityTestRule<>(LoginActivity.class);

    @Test
    public void loginActivityTest() {
        PrefsHelper.saveServerUrl("http://app3-test.pob.adc.spb.ru:8082/", GreenApp.getInstance().getApplicationContext());
        PrefsHelper.saveLastLoginData(null,GreenApp.getInstance().getApplicationContext());


        ViewInteraction editText2 = onView(
                allOf(withId(R.id.login),
                        withParent(allOf(withId(R.id.login_group),
                                withParent(withId(R.id.main_layout)))),
                        isDisplayed()));
        editText2.perform(replaceText("nevzorov"), closeSoftKeyboard());

        ViewInteraction editText5 = onView(
                allOf(withId(R.id.password),
                        withParent(allOf(withId(R.id.login_group),
                                withParent(withId(R.id.main_layout)))),
                        isDisplayed()));
        editText5.perform(replaceText("lwTDmusK"), closeSoftKeyboard());

        ViewInteraction button = onView(
                allOf(withId(R.id.button_login), withText("Начать смену"), isDisplayed()));
        button.perform(click());





        ViewInteraction button2 = onView(
                allOf(withId(R.id.button_login), isDisplayed()));
        button2.perform(click());

        ViewInteraction textView = onView(
                allOf(withId(R.id.title), withText("На этом экране отображены типы объектов. Для продолжения выберите тип проверяемого объекта."),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.recycler_view),
                                        0),
                                0),
                        isDisplayed()));
        textView.check(matches(withText("На этом экране отображены типы объектов. Для продолжения выберите тип проверяемого объекта.")));

        ViewInteraction button3 = onView(
                allOf(withId(R.id.button_finish_work),
                        childAtPosition(
                                allOf(withId(R.id.button_container),
                                        childAtPosition(
                                                IsInstanceOf.<View>instanceOf(android.widget.RelativeLayout.class),
                                                4)),
                                0),
                        isDisplayed()));
        button3.check(matches(isDisplayed()));

//        ViewInteraction button4 = onView(
//                allOf(withId(R.id.add_object), withText("Добавить объект"),
//                        withParent(withId(R.id.button_container)),
//                        isDisplayed()));
//        button4.perform(click());
//
//        ViewInteraction listView = onView(
//                allOf(withId(R.id.object_list),
//                        childAtPosition(
//                                allOf(withId(R.id.activity_select_object),
//                                        childAtPosition(
//                                                withId(android.R.id.content),
//                                                0)),
//                                1),
//                        isDisplayed()));
//        listView.check(matches(isDisplayed()));
//
//        pressBack();
//
//        pressBack();

    }

    private static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }
}
