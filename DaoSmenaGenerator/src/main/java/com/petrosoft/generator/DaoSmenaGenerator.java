/*
 * Copyright (C) 2011 Markus Junginger, greenrobot (http://greenrobot.de)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.petrosoft.generator;

import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Property;
import de.greenrobot.daogenerator.Schema;
import de.greenrobot.daogenerator.ToMany;
import de.greenrobot.daogenerator.ToOne;

/**
 * Generates entities and DAOs for the example project DaoExample.
 * <p/>
 * Run it as a Java application (not Android).
 *
 * @author Markus
 */
public class DaoSmenaGenerator {

    public static void main(String[] args) throws Exception {
        Schema schema = new Schema(1200, "com.petrosoft.smena.dao");

        schema.enableKeepSectionsByDefault();

        addAll(schema);
//        addPreferences(schema);
//        addCategories(schema);

        schema.setDefaultJavaPackageTest("com.petrosoft.smena.test");
        schema.setDefaultJavaPackageDao("com.petrosoft.smena.dao");
        new DaoGenerator().generateAll(schema, "app/src/main/java-gen");
    }

    private static void addAll(Schema schema) {
        Entity objectType = schema.addEntity("ObjectType");
        objectType.addIdProperty();
        Property serverIdObjectType = objectType.addLongProperty("serverId").notNull().getProperty();
        objectType.addStringProperty("name").notNull();
        objectType.addStringProperty("gisLayerUrl");

//        Entity element = schema.addEntity("Element");
//        element.addIdProperty().notNull();
////        Property serverIdElement = element.addLongProperty("serverId").notNull().getProperty();
//        element.addStringProperty("name").notNull();

//        Entity elementOt = schema.addEntity("ElementObjectTypes");
//        elementOt.addIdProperty().autoincrement();
//        Property elementIdProperty = elementOt.addLongProperty("element_id").notNull().getProperty();
//        Property objectTypeIdProperty = elementOt.addLongProperty("object_type_id").notNull().getProperty();
//
//        ToMany objectTypesToElements = objectType.addToMany(elementOt, objectTypeIdProperty);
//        objectTypesToElements.setName("otElements");
//
//        ToOne otEls = elementOt.addToOne(element, elementIdProperty);
//        otEls.setName("greenElement");

        Entity cObject = schema.addEntity("CObject");
        Property pkp = cObject.addIdProperty().autoincrement().getProperty();

        cObject.addLongProperty("serverId").notNull().unique().getProperty();
        cObject.addStringProperty("name");
        cObject.addIntProperty("oid");
        cObject.addStringProperty("organisations");
        Property objectTypeId = cObject.addLongProperty("objectTypeId").notNull().getProperty();

        cObject.addBooleanProperty("current");
        cObject.addBooleanProperty("check").notNull();

        ToMany objectTypesToCObjects = objectType.addToMany(cObject, objectTypeId);
        objectTypesToCObjects.setName("cObjects");
        ToOne cObjectObjectType = cObject.addToOne(objectType, objectTypeId);
        cObjectObjectType.setName("objectType");
        {
            Entity task = schema.addEntity("Task");
            task.addIdProperty().autoincrement();
            task.addLongProperty("serverId").notNull();
            Property cObjectIdTask = task.addLongProperty("cObjectId").notNull().getProperty();
            task.addLongProperty("LastCheckId");
            task.addBooleanProperty("Sent");
            task.addLongProperty("workTypeId");
            task.addStringProperty("workTypeName");
            task.addFloatProperty("quantity");
            task.addBooleanProperty("isCompleted");
            task.addStringProperty("completeDate");
            task.addStringProperty("contractStage");
            ToMany cObjectToTasks = cObject.addToMany(task, cObjectIdTask);
            cObjectToTasks.setName("tasks");
        }
        {
            Entity issue = schema.addEntity("Issue");
            issue.addIdProperty().autoincrement();
            issue.addLongProperty("serverId").notNull();
            issue.addLongProperty("LastCheckId");
            issue.addBooleanProperty("Sent");
            Property cObjectIdIssue = issue.addLongProperty("cObjectId").notNull().getProperty();
            issue.addFloatProperty("latitude");
            issue.addFloatProperty("longtitude");
            issue.addBooleanProperty("isCompleted");
            issue.addStringProperty("eventDate");
            issue.addStringProperty("customAddress");
            issue.addStringProperty("issueTypes");
            issue.addStringProperty("comment");
            ToMany cObjectToIssues = cObject.addToMany(issue, cObjectIdIssue);
            cObjectToIssues.setName("issues");

            Entity issueImage = schema.addEntity("IssueImage");
            issueImage.addIdProperty().autoincrement();
            issueImage.addStringProperty("path");
            issueImage.addBooleanProperty("sent");
            Property issueId = issueImage.addLongProperty("issueId").notNull().getProperty();

            ToMany toMany = issue.addToMany(issueImage, issueId);
            toMany.setName("Images");
        }
        {
            Entity claim = schema.addEntity("Claim");
            claim.addIdProperty().autoincrement();
            claim.addLongProperty("serverId").notNull();
            claim.addLongProperty("LastCheckId");
            claim.addBooleanProperty("Sent");
            Property cObjectIdClaim = claim.addLongProperty("cObjectId").notNull().getProperty();
            claim.addStringProperty("name");
            claim.addStringProperty("comment");
            claim.addStringProperty("customAddress");
            claim.addLongProperty("greenObjectId");
            claim.addBooleanProperty("isCompleted");
            claim.addLongProperty("claimStatusId");
            claim.addFloatProperty("latitude");
            claim.addFloatProperty("longtitude");
            claim.addBooleanProperty("isArchive");
            claim.addStringProperty("registeredAt");
            claim.addLongProperty("extId");
            claim.addBooleanProperty("isExternal");
            claim.addStringProperty("lastExternalUpdate");
            claim.addStringProperty("lastExternalStatusUpdate");
            claim.addStringProperty("cityObjectName");
            claim.addStringProperty("categoryName");
            claim.addLongProperty("districtId");
            claim.addStringProperty("expectedAnswerDt");
            claim.addStringProperty("executor");
            claim.addStringProperty("extStatusName");
            ToMany cObjectToClaims = cObject.addToMany(claim, cObjectIdClaim);
            cObjectToClaims.setName("claims");

            Entity claimImage = schema.addEntity("ClaimImage");
            claimImage.addIdProperty().autoincrement();
            claimImage.addStringProperty("path");
            claimImage.addBooleanProperty("sent");
            Property claimId = claimImage.addLongProperty("claimId").notNull().getProperty();

            ToMany toMany = claim.addToMany(claimImage, claimId);
            toMany.setName("Images");
        }

        {
            Entity check = schema.addEntity("CheckTable");
            check.addIdProperty().autoincrement();
            check.addLongProperty("serverId").notNull();
            check.addStringProperty("start");
            check.addStringProperty("end");
            check.addBooleanProperty("is_completed");

            check.addLongProperty("user_id");
            check.addLongProperty("object_type_id");
            check.addLongProperty("date_request");
            check.addLongProperty("object_type_id_request");
            check.addBooleanProperty("finished");
            check.addBooleanProperty("sent");
            check.addBooleanProperty("loadedFromServer");

            Entity checkToCObject = schema.addEntity("CheckToObject");
            checkToCObject.addIdProperty().autoincrement();
            Property checkIdCheckToCObject = checkToCObject.addLongProperty("checkId").notNull().getProperty();
            Property cObjectIdCheckToCObject = checkToCObject.addLongProperty("cObjectId").notNull().getProperty();

            ToMany checkToCheckToCObject = check.addToMany(checkToCObject, checkIdCheckToCObject);
            checkToCheckToCObject.setName("checkToCObject");

            ToOne checkToCObjectToCObject = checkToCObject.addToOne(cObject, cObjectIdCheckToCObject);
            checkToCObjectToCObject.setName("CObject");
        }

        {
            Entity workType = schema.addEntity("WorkType");
            workType.addIdProperty().autoincrement();
            workType.addLongProperty("serverId").notNull();
            workType.addStringProperty("name");
            workType.addStringProperty("measure");
        }

        {
            Entity newIssue = schema.addEntity("NewIssue");
            newIssue.addIdProperty().autoincrement();
            newIssue.addStringProperty("event_date");
            newIssue.addLongProperty("serverId");
            newIssue.addFloatProperty("latitude");
            newIssue.addFloatProperty("longtitude");
            newIssue.addLongProperty("check_id");
            newIssue.addLongProperty("object_id");
            newIssue.addLongProperty("issue_type");

            newIssue.addStringProperty("comment");
            newIssue.addStringProperty("custom_address");
            newIssue.addFloatProperty("width");
            newIssue.addFloatProperty("length");
            newIssue.addFloatProperty("quantity");
            newIssue.addLongProperty("element_id");
            newIssue.addIntProperty("contract_work_id");
            newIssue.addIntProperty("road_extension_id");
            newIssue.addStringProperty("type");
            newIssue.addBooleanProperty("sent");
            newIssue.addBooleanProperty("is_recommendation");
            newIssue.addStringProperty("eliminate_date");

            Entity newIssueImage = schema.addEntity("NewIssueImage");
            newIssueImage.addIdProperty().autoincrement();
            newIssueImage.addStringProperty("path");
            newIssueImage.addBooleanProperty("sent");
            Property newIssueId = newIssueImage.addLongProperty("newIssueId").notNull().getProperty();

            ToMany toMany = newIssue.addToMany(newIssueImage, newIssueId);
            toMany.setName("Images");
        }

        {
            Entity issueType = schema.addEntity("IssueType");
            issueType.addIdProperty().autoincrement();
            issueType.addLongProperty("serverId");
            issueType.addStringProperty("name");

            Entity objectTypeToIssueType = schema.addEntity("ObjectTypeToIssueType");
            objectTypeToIssueType.addIdProperty().autoincrement();
            Property objectTypeServerIdObjectTypeToIssueType = objectTypeToIssueType.addLongProperty("objectTypeServerId").notNull().getProperty();
            Property issueTypeIdObjectTypeToIssueType = objectTypeToIssueType.addLongProperty("issueTypeId").notNull().getProperty();


            ToMany toMany = objectType.addToMany(serverIdObjectType, objectTypeToIssueType, objectTypeServerIdObjectTypeToIssueType);
            toMany.setName("objectTypeToIssueType");

            ToOne toOne = objectTypeToIssueType.addToOne(issueType, issueTypeIdObjectTypeToIssueType);
            toOne.setName("IssueType");
        }


        {
            Entity track = schema.addEntity("Track");
            track.addIdProperty().autoincrement();
            track.addDoubleProperty("latitude");
            track.addDoubleProperty("longtitude");
            track.addStringProperty("datetime");
            track.addLongProperty("session");
            track.addFloatProperty("accuracy");
            track.addBooleanProperty("sent");
        }

        {
            Entity greenElement = schema.addEntity("GreenElement");
            Property gePk = greenElement.addIdProperty().getProperty();
            Property fkElementType = greenElement.addLongProperty("element_type_id").notNull().getProperty();
            greenElement.addStringProperty("name");
            greenElement.addIntProperty("oid");
            greenElement.addStringProperty("inventory_number");


            //link between object and elements
            Entity objectToElement = schema.addEntity("CObjectToElements");

            objectToElement.addIdProperty().autoincrement();
            Property cobProperty = objectToElement.addLongProperty("CObjectId").notNull().getProperty();
            Property elPropery = objectToElement.addLongProperty("ElementId").notNull().getProperty();


            cObject.addToMany(serverIdObjectType, objectToElement, cobProperty).setName("ElementsToElemenstCobjects");

            objectToElement.addToOne(greenElement, elPropery).setName("GreenElement");
            objectToElement.addToOne(cObject, cobProperty).setName("CObject");
            //  ToMany toMany = objectType.addToMany(serverIdObjectType, objectTypeToIssueType, objectTypeServerIdObjectTypeToIssueType);
            //toMany.setName("objectTypeToIssueType");


            Entity greenElementType = schema.addEntity("GreenElementType");
            greenElementType.addIdProperty().notNull().unique();
            greenElementType.addStringProperty("name");
            greenElementType.addStringProperty("code");
            greenElementType.addStringProperty("gis_layer_url");

            greenElementType.addToMany(greenElement, fkElementType).setName("GreenElements");

            greenElement.addToOne(greenElementType, fkElementType).setName("GreenElementType");


        }
        {
            Entity route = schema.addEntity("Route");
            route.addIdProperty().notNull().unique();
            route.addStringProperty("name");
            route.addFloatProperty("latitude");
            route.addFloatProperty("longtitude");
            route.addBooleanProperty("is_completed").notNull();
            route.addBooleanProperty("is_visible").notNull();
            Property fkobjectId = route.addLongProperty("cobject_id").getProperty();
            route.addToOne(cObject, fkobjectId);
        }
        {
            Entity pendindImage=schema.addEntity("PendingImage");
            pendindImage.addIdProperty().autoincrement();
            pendindImage.addIntProperty("entity_id").notNull();
            pendindImage.addStringProperty("image_path").notNull();
            pendindImage.addStringProperty("image_code").notNull();
            pendindImage.addBooleanProperty("is_completed").notNull();
            pendindImage.addIntProperty("point_id");
            pendindImage.addStringProperty("token").notNull();

        }
    }
}
